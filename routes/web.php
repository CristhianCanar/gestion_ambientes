<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Rutas Autenticacion
Auth::routes();

//Ruta Inicio Publico
Route::get('/', 'HomePublicController@index');

//Rutas Tablero
Route::get('/tablero', 'HomeAuthController@index')->name('home')->middleware(['auth', 'usuario_estado']);
Route::get('/tablero_ambientes_programados', 'HomeAuthController@ambientes_programados')->name('ambientes_programados')->middleware('auth');
Route::get('/tablero_usuarios_registrados', 'HomeAuthController@usuarios_registrados')->name('usuarios_registrados')->middleware('auth');

//Rutas Ambientes
Route::resource('/ambientes', 'AmbientesController')->middleware('auth');
Route::get('/gestionar_programa/{id_ambiente}', 'AmbientesController@gestionar_programa')->name('ambientes.gestionar_programa')->middleware('auth');
Route::post('/store_programa/{id_ambiente}', 'AmbientesController@store_programa')->name('ambientes.store_programa')->middleware('auth');
Route::get('/ambientes_programa/{id_programa}', 'AmbientesController@get_ambientes')->name('ambientes_programa.getambientes');
Route::any('/buscar_ambientes', 'AmbientesController@buscar_ambientes')->name('buscar.ambiente')->middleware('auth');
Route::get('/autocompletar_productos','AmbientesController@autocompletar_productos')->name('autocompletar.productos')->middleware('auth');

//Rutas Centros de Formación
Route::resource('/centros_formacion', 'CentrosFormacionController')->middleware('auth');
Route::any('/buscar_centros_formacion', 'CentrosFormacionController@buscar_centros_formacion')->name('buscar.centro_formacion')->middleware('auth');

//Ruta Anidada de Sedes
Route::resource('/centros_formacion.sedes', 'CentrosFormacionSedesController')->middleware('auth');
Route::post('/sedes_ambientes', 'AmbientesController@store_sede')->name('registro.sedes')->middleware('auth');

//Rutas Programas de Formación
Route::resource('/programas_formacion', 'ProgramasFormacionController')->middleware('auth');
Route::resource('/programas_formacion.fichas', 'ProgramasFormacionFichasController')->middleware('auth');
Route::resource('/programas_formacion.actividades', 'ProgramasFormacionActividadesController')->middleware('auth');
Route::any('/buscar_programas_formacion', 'ProgramasFormacionController@buscar_programas_formacion')->name('buscar.programa_formacion')->middleware('auth');

//Rutas Redes de Conocimiento
Route::resource('/redes_conocimiento', 'RedesConocimientoController')->middleware('auth');
Route::get('/redes_conocimiento/lineas_tematicas/{idred}', 'RedesConocimientoController@lineas_red')->name('lineas.red')->middleware('auth');
Route::any('/buscar_redes_conocimiento', 'RedesConocimientoController@buscar_redes_conocimiento')->name('buscar.red_conocimiento')->middleware('auth');

Route::get('/redes/lineas/{idredconocimiento}', 'RedesConocimientoController@get_lineas')->name('redes.getlinea');
Route::get('/redes/programas/{id_linea_tematica}', 'RedesConocimientoController@get_programas')->name('redes.getprograma');

//Ruta Anidada de Lineas Temáticas
Route::resource('/redes_conocimiento.lineas_tematicas', 'RedesConocimientoLineasTematicasController')->middleware('auth');

Route::get('/regionales/municipios/{id_regional}', 'RegionalesController@get_municipios')->name('regionales.getmunicipio');
Route::get('/regionales/centros/{id_municipio}', 'RegionalesController@get_centros')->name('regionales.getcentro');
Route::get('/regionales/sedes/{id_centro_formacion}', 'RegionalesController@get_sedes')->name('regionales.getsede');

//Rutas Usuarios
Route::resource('/usuarios', 'UsuariosController')->middleware('auth');
Route::patch('/usuarios_acceso/{id}{estado}', 'UsuariosController@usuarios_acceso')->name('usuarios.usuarios_acceso')->middleware('auth');
Route::get('/usuario_perfil/{id}', 'UsuariosController@edit_perfil')->name('usuario.perfil')->middleware('auth');
Route::put('/update_perfil/{id}', 'UsuariosController@update_perfil')->name('usuario.update_perfil')->middleware('auth');
Route::any('/buscar_usuarios', 'UsuariosController@buscar_usuarios')->name('buscar.usuario')->middleware('auth');

//Rutas Espacios Complementarios
Route::resource('/espacios_complementarios', 'EspaciosComplementariosController');
Route::get('/gestionar_espacios_complementarios/{id_ambiente}', 'EspaciosComplementariosController@gestionar_espacios_complementarios')->middleware('auth')->name('espacios_complementarios.gestionar_espacios_complementarios');
Route::get('/create_espacios_complementarios/{id_ambiente}', 'EspaciosComplementariosController@create_espacios_complementarios')->middleware('auth')->name('espacios_complementarios.create_espacios_complementarios');
Route::delete('/delete_espacios_complementarios/{id_espacio}/{id_ambiente}', 'EspaciosComplementariosController@delete_espacios_complementarios')->middleware('auth')->name('espacios_complementarios.delete_espacios_complementarios');

//Rutas Programas Formacion de Ambientes
Route::resource('/programas_formacion_ambiente', 'AmbientesProgramaFormacionController');
Route::get('/gestionar_programas_ambiente/{id_ambiente}', 'AmbientesProgramaFormacionController@gestionar_programas_ambiente')->middleware('auth')->name('programas_formacion_ambiente.gestionar_programas_ambiente');
Route::get('/create_programas_ambiente/{id_ambiente}', 'AmbientesProgramaFormacionController@create_programas_ambiente')->middleware('auth')->name('programas_formacion_ambiente.create_programas_ambiente');
Route::delete('/delete_programas_ambiente/{id_programa}/{id_ambiente}', 'AmbientesProgramaFormacionController@delete_programas_ambiente')->middleware('auth')->name('programas_formacion_ambiente.delete_programas_ambiente');

//Rutas Reportes
Route::get('/reportes', 'ReportesController@index')->name('reportes.index')->middleware('auth');
Route::post('/reporte_ambientes', 'ReportesController@store_ambientes')->name('reportes.store_ambientes')->middleware('auth');
Route::post('/reporte_centros_formacion', 'ReportesController@store_centros_formacion')->name('reportes.store_centros_formacion')->middleware('auth');
Route::post('/reporte_redes_conocimiento', 'ReportesController@store_redes_conocimiento')->name('reportes.store_redes_conocimiento')->middleware('auth');
Route::post('/reporte_programas_formacion', 'ReportesController@store_programas_formacion')->name('reportes.store_programas_formacion')->middleware('auth');

//Rutas Gestion Ambientes
Route::resource('/ambientes.elementos', 'AmbienteElementoController')->middleware('auth');
Route::get('/importar_elementos/{id_ambiente}', 'AmbienteElementoController@importar_elementos')->name('importar.elementos')->middleware('auth');
Route::post('/almacenar_elementos/{id_ambiente}', 'AmbienteElementoController@almacenar_elementos')->name('almacenar.elementos')->middleware('auth');
Route::post('/validacion_elemento/{id_ambiente}', 'AmbienteElementoController@validacion_elemento')->name('validacion.elemento')->middleware('auth');

//Rutas Perfiles Administración
Route::resource('/perfiles', 'PerfilesController')->middleware('auth');
Route::get('/gestionar_permisos', 'PerfilesController@gestionar_permisos')->name('perfiles.gestionar_permisos')->middleware('auth');
Route::post('/perfiles_permisos', 'PerfilesController@gestion_permisos')->name('perfiles.gestionpermisos')->middleware('auth');

//Rutas Elementos Administración
Route::resource('/elementos', 'ElementosController')->middleware('auth');
Route::post('/elementos_importacion', 'ElementosController@store_importacion')->name('elementos.store_importacion')->middleware('auth');
Route::get('/elementos_subcategorias/{id_categoria_elemento}', 'ElementosController@get_subcategorias_elementos')->name('elementos_subcategorias.getsubcategorias')->middleware('auth');
Route::get('/elementos_plantilla', 'ElementosController@exportar_plantilla')->name('elementos.exportar_plantilla')->middleware('auth');

//Rutas Programacion Ambientes
Route::resource('/programaciones_ambiente', 'ProgramacionesAmbienteController')->middleware('auth');
Route::get('/programaciones_ambiente_filtro', 'ProgramacionesAmbienteController@filtro_programacion')->name('programaciones_ambiente.filtro')->middleware('auth');
Route::post('/programaciones_ambiente_calendario', 'ProgramacionesAmbienteController@calendario_programacion')->name('programaciones_ambiente.calendario')->middleware('auth');
Route::get('/programaciones_ambiente_eventos', 'ProgramacionesAmbienteController@calendario_eventos')->name('programaciones_ambiente.eventos')->middleware('auth');
Route::get('/programaciones_ambiente_festivos', 'ProgramacionesAmbienteController@calendario_festivos')->name('programaciones_ambiente.festivos')->middleware('auth');
Route::get('/programaciones_ambiente_exportar/{id}/{filtro}', 'ProgramacionesAmbienteController@exportar_programacion')->name('programaciones_ambiente.exportar')->middleware('auth');

Route::get('/programas_ambiente/{id_ambiente}', 'ProgramacionesAmbienteController@programas_ambiente')->name('programas_ambiente.getprogramas')->middleware('auth');
Route::get('/fichas_programa/{id_programa}', 'ProgramacionesAmbienteController@fichas_programa')->name('fichas_programa.getfichas')->middleware('auth');
Route::get('/competencias_programa/{id_programa}', 'ProgramacionesAmbienteController@competencias_programa')->name('competencias_programa.getcompetencias')->middleware('auth');
Route::get('/resultados_programa/{id_competencia}/{id_ficha}', 'ProgramacionesAmbienteController@resultados_programa')->name('resultados_programa.getresultados')->middleware('auth');

Route::any('/buscar_programaciones_ambiente', 'ProgramacionesAmbienteController@buscar_programaciones_ambiente')->name('buscar.programacion_ambiente')->middleware('auth');

//Rutas Administracion
    //Ambientes: Tipos de Ambiente
    Route::resource('/tipos_ambiente', 'TiposAmbienteController')->middleware('auth');
    //Ambientes: Produccion de Centros
    Route::resource('/produccion_centros', 'ProduccionCentrosController')->middleware('auth');
    //Ambientes: Niveles de Formación
    Route::resource('/niveles_formacion', 'NivelesFormacionController')->middleware('auth');