<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramaFormacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programa_formacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('codigo_programa');
            $table->string('denominacion','100');
            $table->integer('version_programa');
            $table->string('nivel_programa','40');
            $table->string('duracion_maxima','30');
            $table->bigInteger('area_tematica_id');
            $table->bigInteger('ficha_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programa_formacion');
    }
}
