<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspecificacionRealTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especificacion_real', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('verificacion_cumplimiento','200');
            $table->string('observacion_cumplimiento','200');
            $table->bigInteger('ambiente_ideal_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('especificacion_real');
    }
}
