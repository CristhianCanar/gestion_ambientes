<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentroFormacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centro_formacion', function (Blueprint $table) {
            $table->integerIncrements('id_centro');
            $table->string('nombre_centro','80');
            $table->string('telefono_centro','20');
            $table->string('correo_centro','40');
            $table->string('direccion_centro','40');
            $table->integer('id_regional');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centro_formacion');
    }
}
