<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmbientesAprendizajeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ambientes_aprendizaje', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_ambiente_aprendizaje','60');
            $table->bigInteger('especificacion_real_id');
            $table->bigInteger('caracteristica_id');
            $table->bigInteger('tipo_ambiente_id');
            $table->bigInteger('regional_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ambientes_aprendizaje');
    }
}
