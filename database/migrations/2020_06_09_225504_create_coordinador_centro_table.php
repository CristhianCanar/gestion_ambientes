<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoordinadorCentroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coordinador_centro', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_coordinador','40');
            $table->string('ip_coordinador','6');
            $table->string('correo_coordinador','50');
            $table->bigInteger('centro_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coordinador_centro');
    }
}
