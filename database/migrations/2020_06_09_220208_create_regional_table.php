<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regional', function (Blueprint $table) {
            $table->integerIncrements('id_regional');
            $table->string('nombre_regional','60');
            $table->string('nombre_director_regional','60')->nullable();
            $table->integer('telefono_regional')->nullable();
            $table->string('direccion_regional','60')->nullable();
            $table->string('correo_regional','60')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regional');
    }
}
