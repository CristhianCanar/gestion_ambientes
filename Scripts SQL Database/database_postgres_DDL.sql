/*
		Centro de Teleinformatica y producción Industrial
		Fabrica de software CTPI
		Octubre 22 de 2020
		Versión estable DB 1.0
		Grupo de Desarrollo:
			Aprendiz: Brayan Aux Mate => bryaux98@gmail.com
					  Cristian Cañar  => cristhian.canar.m@gmail.com
			Instructor: Henry Eduardo Bastidas Paruma => hbastidasp@sena.edu.co
						Peter Emerson Pinchao  => pepinchao@sena.edu.co
*/



	--
	create type booleano as enum('SI', 'NO');
	create type t_estado as enum('Bueno','Regular','Malo');
	create type t_permiso as enum('L','E');
	create type t_estado_ra  as  enum('Disponible','Programado') ;

	CREATE SEQUENCE actividades_aprendizaje_seq;

	CREATE TABLE actividades_aprendizaje (
	  id_actividad_aprendizaje smallint NOT NULL DEFAULT NEXTVAL ('actividades_aprendizaje_seq'),
	  programa_formacion_id smallint check (programa_formacion_id > 0) NOT NULL,
	  actividad_aprendizaje varchar(200) NOT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_actividad_aprendizaje) 
	)  ;

	ALTER SEQUENCE actividades_aprendizaje_seq RESTART WITH 20;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX ix_programa_formacion_id ON actividades_aprendizaje (programa_formacion_id);

	--
	-- SQLINES DEMO *** or table ambiente
	--

	--  ambiente;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE ambiente_seq;

	CREATE TABLE ambiente (
	  id_ambiente int NOT NULL DEFAULT NEXTVAL ('ambiente_seq'),
	  id_tenencia smallint check (id_tenencia > 0) NOT NULL,
	  id_tipo_ambiente smallint check (id_tipo_ambiente > 0) NOT NULL,
	  id_guardaescoba smallint NOT NULL,
	  id_piso smallint NOT NULL,
	  id_muro smallint NOT NULL,
	  id_cieloraso smallint NOT NULL,
	  id_ventaneria smallint NOT NULL,
	  id_puerta smallint NOT NULL,
	  id_centro_formacion int NOT NULL,
	  id_sede smallint check (id_sede > 0) NOT NULL,
	  nombre_ambiente varchar(45) DEFAULT NULL,
	  cantidad_aprendices smallint DEFAULT NULL,
	  dimension_alto double precision DEFAULT NULL,
	  dimension_largo double precision DEFAULT NULL,
	  dimension_ancho double precision DEFAULT NULL,
	  area_ambiente double precision DEFAULT NULL ,
	  puerta_alto double precision DEFAULT NULL ,
	  puerta_ancho double precision DEFAULT NULL ,
	  otro_piso varchar(50) DEFAULT NULL,
	  otro_muro varchar(50) DEFAULT NULL,
	  otro_guardaescoba varchar(50) DEFAULT NULL,
	  otro_cieloraso varchar(50) DEFAULT NULL,
	  otra_ventaneria varchar(50) DEFAULT NULL,
	  otra_puerta varchar(50) DEFAULT NULL,
	  produccion_centros booleano DEFAULT NULL,
	  otro_tipo_riesgo varchar(45) DEFAULT NULL,
	  iluminacion_natural booleano  DEFAULT NULL,
	  iluminacion_artificial varchar(45) DEFAULT NULL,
	  ventilacion_natural booleano DEFAULT NULL,
	  ventilacion_mecanica varchar(50) DEFAULT NULL,
	  gas_natural booleano DEFAULT NULL,
	  gas_propano booleano DEFAULT NULL,
	  electrica_bifasica booleano DEFAULT NULL,
	  electrica_trifasica booleano DEFAULT NULL,
	  electrica_regulada booleano DEFAULT NULL,
	  red_datos booleano DEFAULT NULL,
	  hidro_aguafria booleano DEFAULT NULL,
	  hidro_aguacaliente booleano DEFAULT NULL,
	  hidro_banco_hielo booleano DEFAULT NULL,
	  hidro_vapor booleano DEFAULT NULL,
	  hidro_lavamanos booleano DEFAULT NULL,
	  hidro_lavapies booleano DEFAULT NULL,
	  hidro_lavaojos booleano DEFAULT NULL,
	  hidro_trampa_grasas booleano DEFAULT NULL,
	  hidro_desarenador booleano DEFAULT NULL,
	  hidro_desagues booleano DEFAULT NULL,
	  hidro_otro varchar(50) DEFAULT NULL,
	  esquema_ambiente varchar(50) DEFAULT NULL,
	  foto_ambiente varchar(50) DEFAULT NULL,
	  observaciones_generales bytea DEFAULT NULL,
	  fecha_creacion_ambiente date DEFAULT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_ambiente)
	 
	)  ;

	ALTER SEQUENCE ambiente_seq RESTART WITH 17;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX fk_ambiente_tenencia_idx ON ambiente (id_tenencia);
	CREATE INDEX fk_ambiente_tipo_ambiente1_idx ON ambiente (id_tipo_ambiente);
	CREATE INDEX fk_ambiente_piso1_idx ON ambiente (id_piso);
	CREATE INDEX fk_ambiente_guardaescoba1_idx ON ambiente (id_guardaescoba);
	CREATE INDEX fk_ambiente_muro1_idx ON ambiente (id_muro);
	CREATE INDEX fk_ambiente_cieloraso1_idx ON ambiente (id_cieloraso);
	CREATE INDEX fk_ambiente_ventaneria1_idx ON ambiente (id_ventaneria);
	CREATE INDEX fk_ambiente_puerta1_idx ON ambiente (id_puerta);
	CREATE INDEX fk_ambiente_Centro_formacion1_idx ON ambiente (id_centro_formacion);
	CREATE INDEX id_sede ON ambiente (id_sede);

	--
	-- SQLINES DEMO *** or table ambiente_programa
	--

	--  ambiente_programa;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE TABLE ambiente_programa (
	  id_ambiente int NOT NULL,
	  id_programa smallint check (id_programa > 0) NOT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_ambiente,id_programa)
	 
	) ;

	CREATE INDEX fk_ambiente_has_programa_programa1_idx ON ambiente_programa (id_programa);
	CREATE INDEX fk_ambiente_has_programa_ambiente1_idx ON ambiente_programa (id_ambiente);
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table categoria_elemento
	--

	--  categoria_elemento;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE categoria_elemento_seq;

	CREATE TABLE categoria_elemento (
	  id_categoria_elemento smallint NOT NULL DEFAULT NEXTVAL ('categoria_elemento_seq'),
	  categoria_elemento varchar(50) NOT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_categoria_elemento)
	)  ;

	ALTER SEQUENCE categoria_elemento_seq RESTART WITH 5;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table centro_formacion
	--

	--  centro_formacion;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE centro_formacion_seq;

	CREATE TABLE centro_formacion (
	  id_centro_formacion int NOT NULL DEFAULT NEXTVAL ('centro_formacion_seq'),
	  id_regional smallint check (id_regional > 0) NOT NULL,
	  id_municipio smallint check (id_municipio > 0) DEFAULT NULL,
	  centro_formacion varchar(100) DEFAULT NULL,
	  direccion_centro varchar(40) DEFAULT NULL,
	  telefono_centro varchar(15) DEFAULT NULL,
	  email_centro varchar(50) DEFAULT NULL,
	  subdirector varchar(80) DEFAULT NULL,
	  contacto_subdirector varchar(50) DEFAULT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_centro_formacion)

	)  ;

	ALTER SEQUENCE centro_formacion_seq RESTART WITH 353;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX fk_Centro_formacion_Regional1_idx ON centro_formacion (id_regional);
	CREATE INDEX id_municipio ON centro_formacion (id_municipio);

	--
	-- SQLINES DEMO *** or table cieloraso
	--

	--  cieloraso;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE cieloraso_seq;

	CREATE TABLE cieloraso (
	  id_cieloraso smallint NOT NULL DEFAULT NEXTVAL ('cieloraso_seq'),
	  cieloraso varchar(45) DEFAULT NULL,
	  PRIMARY KEY (id_cieloraso)
	)   ;

	ALTER SEQUENCE cieloraso_seq RESTART WITH 128;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table competencia_laboral_programa
	--

	--  competencia_laboral_programa;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE TABLE competencia_laboral_programa (
	  competencia_laboral_id int NOT NULL,
	  programa_id smallint check (programa_id > 0) NOT NULL,
	  PRIMARY KEY (competencia_laboral_id,programa_id)
	 
	) ;

	CREATE INDEX fk_Competencia_laboral_has_programa_programa1_idx ON competencia_laboral_programa (programa_id);
	CREATE INDEX fk_Competencia_laboral_has_programa_Competencia_laboral1_idx ON competencia_laboral_programa (competencia_laboral_id);
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table competencias_laborales
	--

	--  competencias_laborales;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE competencias_laborales_seq;

	CREATE TABLE competencias_laborales (
	  id_competencia_laboral int NOT NULL DEFAULT NEXTVAL ('competencias_laborales_seq'),
	  programa_formacion_id smallint check (programa_formacion_id > 0) NOT NULL,
	  codigo_competencia_laboral varchar(10) DEFAULT NULL,
	  version_competencia_laboral varchar(3) DEFAULT NULL,
	  competencia_laboral varchar(200) DEFAULT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_competencia_laboral)

	)  ;

	ALTER SEQUENCE competencias_laborales_seq RESTART WITH 5;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX programa_formacion_id ON competencias_laborales (programa_formacion_id);

	--
	-- SQLINES DEMO *** or table departamentos
	--

	--  departamentos;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE departamentos_seq;

	CREATE TABLE departamentos (
	  id_departamento smallint check (id_departamento > 0) NOT NULL DEFAULT NEXTVAL ('departamentos_seq'),
	  departamento varchar(50) NOT NULL,
	  regional_id smallint check (regional_id > 0) NOT NULL,
	  created_at timestamp(0) NULL DEFAULT NULL,
	  updated_at timestamp(0) NULL DEFAULT NULL,
	  PRIMARY KEY (id_departamento)
	 
	)  ;

	ALTER SEQUENCE departamentos_seq RESTART WITH 35;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX regional_id ON departamentos (regional_id);

	--
	-- SQLINES DEMO *** or table elemento_ambiente
	--

	--  elemento_ambiente;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE elemento_ambiente_seq;

	CREATE TABLE elemento_ambiente (
	  id_elemento_ambiente int check (id_elemento_ambiente > 0) NOT NULL DEFAULT NEXTVAL ('elemento_ambiente_seq'),
	  id_ambiente int NOT NULL,
	  subcategoria_elemento_id smallint NOT NULL,
	  origen_elemento_id smallint NOT NULL,
	  numero_inventario varchar(45) DEFAULT NULL,
	  serial varchar(20) NOT NULL,
	  cantidad smallint DEFAULT NULL,
	  elemento_ambiente varchar(45) DEFAULT NULL,
	  estado t_estado DEFAULT NULL,
	  fecha_adquisicion date DEFAULT NULL,
	  tiempo_vida_util smallint DEFAULT NULL,
	  precio float NOT NULL,
	  observacion_general VARCHAR(2000) DEFAULT NULL,
	  created_at timestamp NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_elemento_ambiente)
	) ;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table espacio_ambiente
	--

	--  espacio_ambiente;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE espacio_ambiente_seq;

	CREATE TABLE espacio_ambiente (
	  id_espacio int check (id_espacio > 0) NOT NULL DEFAULT NEXTVAL ('espacio_ambiente_seq'),
	  id_ambiente int NOT NULL,
	  espacio varchar(150) DEFAULT NULL,
	  area double precision DEFAULT NULL ,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_espacio,id_ambiente)
	 
	)  ;

	ALTER SEQUENCE espacio_ambiente_seq RESTART WITH 9;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX fk_espacio_ambiente1_idx ON espacio_ambiente (id_ambiente);

	--
	-- SQLINES DEMO *** or table festivos
	--

	--  festivos;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE TABLE festivos (
	  id_festivos date NOT NULL,
	  PRIMARY KEY (id_festivos)
	) ;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table ficha
	--

	--  ficha;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE ficha_seq;

	CREATE TABLE ficha (
	  id_ficha int NOT NULL DEFAULT NEXTVAL ('ficha_seq'),
	  programa_id smallint check (programa_id > 0) NOT NULL,
	  centro_formacion_id int NOT NULL,
	  numero_ficha int DEFAULT NULL,
	  cupo_aprendices smallint DEFAULT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_ficha)
	 
	)  ;

	ALTER SEQUENCE ficha_seq RESTART WITH 5;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX fk_Ficha_programa1_idx ON ficha (programa_id);
	CREATE INDEX fk_Ficha_Centro_formacion1_idx ON ficha (centro_formacion_id);

	--
	-- SQLINES DEMO *** or table guardaescoba
	--

	--  guardaescoba;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE guardaescoba_seq;

	CREATE TABLE guardaescoba (
	  id_guardaescoba smallint NOT NULL DEFAULT NEXTVAL ('guardaescoba_seq'),
	  guardaescoba varchar(45) DEFAULT NULL,
	  PRIMARY KEY (id_guardaescoba)
	)  ;

	ALTER SEQUENCE guardaescoba_seq RESTART WITH 128;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table inventarios
	--

	--  inventarios;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE inventarios_seq;

	CREATE TABLE inventarios (
	  id_inventario integer check (id_inventario > 0) NOT NULL DEFAULT NEXTVAL ('inventarios_seq'),
	  centro_formacion_id smallint DEFAULT NULL,
	  subcategoria_elemento_id smallint DEFAULT NULL,
	  origen_elemento_id smallint DEFAULT NULL,
	  numero_inventario int DEFAULT NULL,
	  serial varchar(20) DEFAULT NULL,
	  elemento_ambiente varchar(45) DEFAULT NULL,
	  cantidad smallint DEFAULT NULL,
	  estado t_estado DEFAULT NULL,
	  fecha_adquisicion date DEFAULT NULL,
	  tiempo_vida_util smallint DEFAULT NULL,
	  precio integer DEFAULT NULL,
	  observacion_general text DEFAULT NULL,
	  created_at timestamp NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_inventario)
	) ;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table linea_tematica
	--

	--  linea_tematica;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE linea_tematica_seq;

	CREATE TABLE linea_tematica (
	  id_linea_tematica integer NOT NULL DEFAULT NEXTVAL ('linea_tematica_seq'),
	  id_redconocimiento smallint NOT NULL,
	  linea_tematica varchar(60) DEFAULT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_linea_tematica)
	 
	)  ;

	ALTER SEQUENCE linea_tematica_seq RESTART WITH 22;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX fk_linea_tematica_redconocimiento1_idx ON linea_tematica (id_redconocimiento);

	--
	-- SQLINES DEMO *** or table migrations
	--

	--  migrations;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE migrations_seq;

	CREATE TABLE migrations (
	  id int check (id > 0) NOT NULL DEFAULT NEXTVAL ('migrations_seq'),
	  migration varchar(255) NOT NULL,
	  batch int NOT NULL,
	  PRIMARY KEY (id)
	)  ;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table modulo
	--

	--  modulo;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE modulo_seq;

	CREATE TABLE modulo (
	  id_modulo smallint NOT NULL DEFAULT NEXTVAL ('modulo_seq'),
	  id_modulo_padre smallint DEFAULT NULL,
	  modulo varchar(45) DEFAULT NULL,
	  url_modulo varchar(80) DEFAULT NULL,
	  icono varchar(36) NOT NULL,
	  orden smallint DEFAULT NULL,
	  hijos smallint DEFAULT NULL,
	  PRIMARY KEY (id_modulo)
	 
	)  ;

	ALTER SEQUENCE modulo_seq RESTART WITH 33;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX id_modulo_padre ON modulo (id_modulo_padre);

	--
	-- SQLINES DEMO *** or table moduloxperfil
	--

	--  moduloxperfil;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE moduloxperfil_seq;

	CREATE TABLE moduloxperfil (
	  id_moduloxperfil smallint NOT NULL DEFAULT NEXTVAL ('moduloxperfil_seq'),
	  id_perfil_usuario smallint NOT NULL,
	  id_modulo smallint NOT NULL,
	  permiso t_permiso DEFAULT NULL,
	  PRIMARY KEY (id_moduloxperfil)

	)  ;

	ALTER SEQUENCE moduloxperfil_seq RESTART WITH 202;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX fk_Moduloxperfil_perfil_usuario1_idx ON moduloxperfil (id_perfil_usuario);
	CREATE INDEX fk_Moduloxperfil_Modulo1_idx ON moduloxperfil (id_modulo);

	--
	-- SQLINES DEMO *** or table mueble_ambiente
	--

	--  mueble_ambiente;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE mueble_ambiente_seq;

	CREATE TABLE mueble_ambiente (
	  id_mueble_ambiente int NOT NULL DEFAULT NEXTVAL ('mueble_ambiente_seq'),
	  id_ambiente int NOT NULL,
	  numero_inventario_mueble varchar(45) DEFAULT NULL,
	  nombre_mueble varchar(45) DEFAULT NULL,
	  cantidad_mueble integer DEFAULT NULL,
	  PRIMARY KEY (id_mueble_ambiente)

	) ;

	CREATE INDEX id_ambiente ON mueble_ambiente (id_ambiente);
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table municipios
	--

	--  municipios;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE municipios_seq;

	CREATE TABLE municipios (
	  id_municipio smallint check (id_municipio > 0) NOT NULL DEFAULT NEXTVAL ('municipios_seq'),
	  municipio varchar(100) NOT NULL,
	  departamento_id smallint check (departamento_id > 0) NOT NULL,
	  created_at timestamp(0) NULL DEFAULT NULL,
	  updated_at timestamp(0) NULL DEFAULT NULL,
	  PRIMARY KEY (id_municipio)
	 
	)   ;

	ALTER SEQUENCE municipios_seq RESTART WITH 1125;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX municipios_departamento_id_foreign ON municipios (departamento_id);

	--
	-- SQLINES DEMO *** or table muro
	--

	--  muro;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE muro_seq;

	CREATE TABLE muro (
	  id_muro smallint NOT NULL DEFAULT NEXTVAL ('muro_seq'),
	  muro varchar(45) DEFAULT NULL,
	  PRIMARY KEY (id_muro)
	)  ;

	ALTER SEQUENCE muro_seq RESTART WITH 128;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table nivel_programa
	--

	--  nivel_programa;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE nivel_programa_seq;

	CREATE TABLE nivel_programa (
	  id_nivel_programa integer NOT NULL DEFAULT NEXTVAL ('nivel_programa_seq'),
	  nivel_programa varchar(80) DEFAULT NULL,
	  PRIMARY KEY (id_nivel_programa)
	)  ;

	ALTER SEQUENCE nivel_programa_seq RESTART WITH 9;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table origen_elemento
	--

	--  origen_elemento;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE origen_elemento_seq;

	CREATE TABLE origen_elemento (
	  id_origen_elemento smallint NOT NULL DEFAULT NEXTVAL ('origen_elemento_seq'),
	  origen_elemento varchar(50) NOT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_origen_elemento)
	)  ;

	ALTER SEQUENCE origen_elemento_seq RESTART WITH 5;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table perfiles
	--

	--  perfiles;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE perfiles_seq;

	CREATE TABLE perfiles (
	  id_perfil_usuario smallint NOT NULL DEFAULT NEXTVAL ('perfiles_seq'),
	  perfil_usuario varchar(45) DEFAULT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_perfil_usuario)
	)  ;

	ALTER SEQUENCE perfiles_seq RESTART WITH 35;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table piso
	--

	--  piso;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE piso_seq;

	CREATE TABLE piso (
	  id_piso smallint NOT NULL DEFAULT NEXTVAL ('piso_seq'),
	  piso varchar(45) DEFAULT NULL,
	  PRIMARY KEY (id_piso)
	)  ;

	ALTER SEQUENCE piso_seq RESTART WITH 128;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table productos
	--

	--  productos;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE productos_seq;

	CREATE TABLE productos (
	  id_producto smallint NOT NULL DEFAULT NEXTVAL ('productos_seq'),
	  producto varchar(80) NOT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_producto)
	)  ;

	ALTER SEQUENCE productos_seq RESTART WITH 4;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table productos_ambiente
	--

	--  productos_ambiente;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE productos_ambiente_seq;

	CREATE TABLE productos_ambiente (
	  id_producto_ambiente smallint NOT NULL DEFAULT NEXTVAL ('productos_ambiente_seq'),
	  producto_id smallint NOT NULL,
	  ambiente_id int NOT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_producto_ambiente)
	 
	)  ;

	ALTER SEQUENCE productos_ambiente_seq RESTART WITH 17;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX ix_producto_id ON productos_ambiente (producto_id);
	CREATE INDEX ix_ambiente_id ON productos_ambiente (ambiente_id);

	--
	-- SQLINES DEMO *** or table programa
	--

	--  programa;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE programa_seq;

	CREATE TABLE programa (
	  id_programa smallint check (id_programa > 0) NOT NULL DEFAULT NEXTVAL ('programa_seq'),
	  idredconocimiento smallint NOT NULL,
	  id_linea_tematica integer NOT NULL,
	  id_nivel_programa integer NOT NULL,
	  codigo_programa varchar(7) DEFAULT NULL,
	  version_programa integer DEFAULT NULL,
	  nombre_programa varchar(80) DEFAULT NULL,
	  duracion_total varchar(45) DEFAULT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_programa)
	 
	)  ;

	ALTER SEQUENCE programa_seq RESTART WITH 17;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX fk_programa_nivel_programa1_idx1 ON programa (id_nivel_programa);
	CREATE INDEX fk_programa_linea_tematica1_idx2 ON programa (id_linea_tematica);
	CREATE INDEX idredconocimiento ON programa (idredconocimiento);

	--
	-- SQLINES DEMO *** or table programacion_ambientes
	--

	--  programacion_ambientes;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE programacion_ambientes_seq;

	CREATE TABLE programacion_ambientes (
	  id_programacion_ambiente bigint NOT NULL DEFAULT NEXTVAL ('programacion_ambientes_seq'),
	  ambiente_id int NOT NULL,
	  ficha_id int NOT NULL,
	  instructor_id int NOT NULL,
	  actividad_aprendizaje text NOT NULL,
	  fecha_inicio date NOT NULL,
	  fecha_fin date NOT NULL,
	  hora_inicio time(0) NOT NULL,
	  hora_fin time(0) NOT NULL,
	  domingo varchar(12) DEFAULT NULL,
	  lunes varchar(12) DEFAULT NULL,
	  martes varchar(12) DEFAULT NULL,
	  miercoles varchar(12) DEFAULT NULL,
	  jueves varchar(12) DEFAULT NULL,
	  viernes varchar(12) DEFAULT NULL,
	  sabado varchar(12) DEFAULT NULL,
	  created_at timestamp NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_programacion_ambiente)
	 
	)  ;

	CREATE INDEX  ix2_ambiente_id ON programacion_ambientes(ambiente_id);
	CREATE INDEX  ix_instructor_id ON programacion_ambientes(instructor_id);
	CREATE INDEX  ix_ficha_id ON programacion_ambientes(ficha_id);
	--
	-- SQLINES DEMO *** or table programacion_ficha
	--

	--  programacion_ficha;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE programacion_ficha_seq;

	CREATE TABLE programacion_ficha (
	  id_programacion_ficha int NOT NULL DEFAULT NEXTVAL ('programacion_ficha_seq'),
	  programacion_ambiente_id bigint NOT NULL,
	  resultado_aprendizaje_id int NOT NULL,
	  estado t_estado_ra NOT NULL DEFAULT 'Disponible',
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_programacion_ficha)

	)  ;

	ALTER SEQUENCE programacion_ficha_seq RESTART WITH 2;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX ix_programacion_ambiente_id ON programacion_ficha (programacion_ambiente_id);
	CREATE INDEX ix_resultado_aprendizaje_id ON programacion_ficha (resultado_aprendizaje_id);

	--
	-- SQLINES DEMO *** or table programacion_ficha_resultado
	--

	--  programacion_ficha_resultado;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE programacion_ficha_resultado_seq;

	CREATE TABLE programacion_ficha_resultado (
	  id_programacion_ficha_resultado int NOT NULL DEFAULT NEXTVAL ('programacion_ficha_resultado_seq'),
	  programacion_ficha_id int NOT NULL,
	  resultado_aprendizaje_id int NOT NULL,
	  estado varchar(20) NOT NULL DEFAULT 'DISPONIBLE',
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_programacion_ficha_resultado)

	) ;

	CREATE INDEX programacion_ficha_id ON programacion_ficha_resultado (programacion_ficha_id);
	CREATE INDEX resultado_aprendizaje_id ON programacion_ficha_resultado (resultado_aprendizaje_id);
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table puerta
	--

	--  puerta;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE puerta_seq;

	CREATE TABLE puerta (
	  id_puerta smallint NOT NULL DEFAULT NEXTVAL ('puerta_seq'),
	  puerta varchar(45) DEFAULT NULL,
	  PRIMARY KEY (id_puerta)
	)  ;

	ALTER SEQUENCE puerta_seq RESTART WITH 128;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table redconocimiento
	--

	--  redconocimiento;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE redconocimiento_seq;

	CREATE TABLE redconocimiento (
	  idredconocimiento smallint NOT NULL DEFAULT NEXTVAL ('redconocimiento_seq'),
	  redconocimiento varchar(100) DEFAULT NULL,
	  nombre_gestor varchar(80) DEFAULT NULL,
	  tel_gestor varchar(15) DEFAULT NULL,
	  mail_gestor varchar(50) DEFAULT NULL,
	  nombre_asesor varchar(80) DEFAULT NULL,
	  tel_asesor varchar(15) DEFAULT NULL,
	  mail_asesor varchar(50) DEFAULT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (idredconocimiento)
	)  ;

	ALTER SEQUENCE redconocimiento_seq RESTART WITH 44;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table regional
	--

	--  regional;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE regional_seq;

	CREATE TABLE regional (
	  id_regional smallint check (id_regional > 0) NOT NULL DEFAULT NEXTVAL ('regional_seq'),
	  regional varchar(80) NOT NULL,
	  direccion varchar(250) NOT NULL,
	  telefono varchar(80) NOT NULL,
	  PRIMARY KEY (id_regional)
	)  ;

	ALTER SEQUENCE regional_seq RESTART WITH 36;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table resultados_aprendizaje
	--

	--  resultados_aprendizaje;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE resultados_aprendizaje_seq;

	CREATE TABLE resultados_aprendizaje (
	  id_resultado_aprendizaje int NOT NULL DEFAULT NEXTVAL ('resultados_aprendizaje_seq'),
	  competencia_laboral_id int NOT NULL,
	  resultado_aprendizaje text NOT NULL,
	  fecha_terminacion date NOT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_resultado_aprendizaje)

	)  ;

	ALTER SEQUENCE resultados_aprendizaje_seq RESTART WITH 8;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX competencia_laboral_id ON resultados_aprendizaje (competencia_laboral_id);

	--
	-- SQLINES DEMO *** or table sedes
	--

	--  sedes;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE sedes_seq;

	CREATE TABLE sedes (
	  id_sede smallint check (id_sede > 0) NOT NULL DEFAULT NEXTVAL ('sedes_seq'),
	  id_centro_formacion int NOT NULL,
	  sede varchar(50) NOT NULL,
	  direccion varchar(40) NOT NULL,
	  telefono varchar(15) DEFAULT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_sede)

	)  ;

	ALTER SEQUENCE sedes_seq RESTART WITH 13;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX id_centro_formacion ON sedes (id_centro_formacion);

	--
	-- SQLINES DEMO *** or table subcategoria_elemento
	--

	--  subcategoria_elemento;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE subcategoria_elemento_seq;

	CREATE TABLE subcategoria_elemento (
	  id_subcategoria_elemento smallint NOT NULL DEFAULT NEXTVAL ('subcategoria_elemento_seq'),
	  categoria_elemento_id smallint NOT NULL,
	  subcategoria_elemento varchar(50) NOT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_subcategoria_elemento)

	)  ;

	ALTER SEQUENCE subcategoria_elemento_seq RESTART WITH 10;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX categoria_elemento_id ON subcategoria_elemento (categoria_elemento_id);

	--
	-- SQLINES DEMO *** or table tenencia
	--

	--  tenencia;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE tenencia_seq;

	CREATE TABLE tenencia (
	  id_tenencia smallint check (id_tenencia > 0) NOT NULL DEFAULT NEXTVAL ('tenencia_seq'),
	  tenencia varchar(45) DEFAULT NULL,
	  PRIMARY KEY (id_tenencia)
	)  ;

	ALTER SEQUENCE tenencia_seq RESTART WITH 5;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table tipo_ambiente
	--

	--  tipo_ambiente;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE tipo_ambiente_seq;

	CREATE TABLE tipo_ambiente (
	  id_tipo_ambiente smallint check (id_tipo_ambiente > 0) NOT NULL DEFAULT NEXTVAL ('tipo_ambiente_seq'),
	  tipo_ambiente varchar(45) DEFAULT NULL,
	  detalle_tipo_ambiente text DEFAULT NULL,
	  PRIMARY KEY (id_tipo_ambiente)
	)  ;

	ALTER SEQUENCE tipo_ambiente_seq RESTART WITH 8;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table tipos_riesgo
	--

	--  tipos_riesgo;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE tipos_riesgo_seq;

	CREATE TABLE tipos_riesgo (
	  id_tipo_riesgo smallint NOT NULL DEFAULT NEXTVAL ('tipos_riesgo_seq'),
	  tipo_riesgo varchar(45) DEFAULT NULL,
	  PRIMARY KEY (id_tipo_riesgo)
	)  ;

	ALTER SEQUENCE tipos_riesgo_seq RESTART WITH 128;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	--
	-- SQLINES DEMO *** or table tipos_riesgo_ambiente
	--

	--  tipos_riesgo_ambiente;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE tipos_riesgo_ambiente_seq;

	CREATE TABLE tipos_riesgo_ambiente (
	  id_tipo_riesgo_ambiente smallint NOT NULL DEFAULT NEXTVAL ('tipos_riesgo_ambiente_seq'),
	  tipo_riesgo_id smallint NOT NULL,
	  ambiente_id int NOT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id_tipo_riesgo_ambiente)
	 
	)  ;

	ALTER SEQUENCE tipos_riesgo_ambiente_seq RESTART WITH 16;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */;

	CREATE INDEX tipo_riesgo_id ON tipos_riesgo_ambiente (tipo_riesgo_id);
	CREATE INDEX ambiente_id ON tipos_riesgo_ambiente (ambiente_id);

	--
	-- SQLINES DEMO *** or table users
	--

	--  users;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE users_seq;

	CREATE TABLE users (
	  id int NOT NULL DEFAULT NEXTVAL ('users_seq'),
	  id_perfil_usuario smallint NOT NULL,
	  id_centro_formacion int DEFAULT NULL,
	  identificacion varchar(10) NOT NULL,
	  nombres varchar(50) DEFAULT NULL,
	  apellidos varchar(50) DEFAULT NULL,
	  telefono varchar(10) DEFAULT NULL,
	  email varchar(50)  DEFAULT NULL,
	  password varchar(255)  DEFAULT NULL,
	  created_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  updated_at timestamp(0) NOT NULL DEFAULT current_timestamp,
	  PRIMARY KEY (id)
	 
	)  ;

	ALTER SEQUENCE users_seq RESTART WITH 66;
	/* SQLINES DEMO *** er_set_client = @saved_cs_client */

	CREATE INDEX fk_Usuario_perfil_usuario1_idx ON users (id_perfil_usuario);
	CREATE INDEX fk_Usuario_Centro_formacion1_idx ON users (id_centro_formacion);

	--
	-- SQLINES DEMO *** or table ventaneria
	--

	--  ventaneria;
	/* SQLINES DEMO *** cs_client     = @@character_set_client */;
	/* SQLINES DEMO *** er_set_client = utf8 */;
	CREATE SEQUENCE ventaneria_seq;

	CREATE TABLE ventaneria (
	  id_ventaneria smallint NOT NULL DEFAULT NEXTVAL ('ventaneria_seq'),
	  ventaneria varchar(45) DEFAULT NULL,
	  PRIMARY KEY (id_ventaneria)
	)  ;

	ALTER SEQUENCE ventaneria_seq RESTART WITH 128;



	ALTER TABLE  actividades_aprendizaje ADD CONSTRAINT actividades_aprendizaje_ibfk_1 FOREIGN KEY (programa_formacion_id) REFERENCES programa (id_programa);




	ALTER TABLE  ambiente ADD CONSTRAINT ambiente_ibfk_1 FOREIGN KEY (id_sede) REFERENCES sedes (id_sede);
	ALTER TABLE  ambiente ADD CONSTRAINT fk_ambiente_Centro_formacion1 FOREIGN KEY (id_centro_formacion) REFERENCES centro_formacion (id_centro_formacion) ON DELETE NO ACTION ON UPDATE NO ACTION;
	ALTER TABLE  ambiente ADD CONSTRAINT fk_ambiente_cieloraso1 FOREIGN KEY (id_cieloraso) REFERENCES cieloraso (id_cieloraso) ON DELETE NO ACTION ON UPDATE NO ACTION;
	ALTER TABLE  ambiente ADD CONSTRAINT fk_ambiente_guardaescoba1 FOREIGN KEY (id_guardaescoba) REFERENCES guardaescoba (id_guardaescoba) ON DELETE NO ACTION ON UPDATE NO ACTION;
	ALTER TABLE  ambiente ADD CONSTRAINT fk_ambiente_muro1 FOREIGN KEY (id_muro) REFERENCES muro (id_muro) ON DELETE NO ACTION ON UPDATE NO ACTION;
	ALTER TABLE  ambiente ADD CONSTRAINT fk_ambiente_piso1 FOREIGN KEY (id_piso) REFERENCES piso (id_piso) ON DELETE NO ACTION ON UPDATE NO ACTION;
	ALTER TABLE  ambiente ADD  CONSTRAINT fk_ambiente_puerta1 FOREIGN KEY (id_puerta) REFERENCES puerta (id_puerta) ON DELETE NO ACTION ON UPDATE NO ACTION;
	ALTER TABLE  ambiente ADD  CONSTRAINT fk_ambiente_tenencia FOREIGN KEY (id_tenencia) REFERENCES tenencia (id_tenencia) ON DELETE NO ACTION ON UPDATE NO ACTION;
	ALTER TABLE  ambiente ADD CONSTRAINT fk_ambiente_tipo_ambiente1 FOREIGN KEY (id_tipo_ambiente) REFERENCES tipo_ambiente (id_tipo_ambiente) ON DELETE NO ACTION ON UPDATE NO ACTION;
	ALTER TABLE  ambiente ADD CONSTRAINT fk_ambiente_ventaneria1 FOREIGN KEY (id_ventaneria) REFERENCES ventaneria (id_ventaneria) ON DELETE NO ACTION ON UPDATE NO ACTION;


	ALTER TABLE  ambiente_programa ADD     CONSTRAINT ambiente_programa_ibfk_1 FOREIGN KEY (id_programa) REFERENCES programa (id_programa);
	ALTER TABLE   ambiente_programa ADD     CONSTRAINT fk_ambiente_has_programa_ambiente1 FOREIGN KEY (id_ambiente) REFERENCES ambiente (id_ambiente) ON DELETE NO ACTION ON UPDATE NO ACTION;

	 
	 ALTER TABLE centro_formacion ADD  CONSTRAINT centro_formacion_ibfk_1 FOREIGN KEY (id_regional) REFERENCES regional (id_regional);
	ALTER TABLE centro_formacion ADD   CONSTRAINT centro_formacion_ibfk_2 FOREIGN KEY (id_municipio) REFERENCES municipios (id_municipio);
	  
		
	  ALTER TABLE competencia_laboral_programa ADD CONSTRAINT fk_Competencia_laboral_has_programa_Competencia_laboral1 FOREIGN KEY (competencia_laboral_id) REFERENCES competencias_laborales (id_competencia_laboral) ON DELETE NO ACTION ON UPDATE NO ACTION;
	  ALTER TABLE competencia_laboral_programa ADD CONSTRAINT fk_Competencia_laboral_has_programa_programa1 FOREIGN KEY (programa_id) REFERENCES programa (id_programa) ON DELETE NO ACTION ON UPDATE NO ACTION;
	  
	  
	  
	  
	  
	  ALTER TABLE competencias_laborales ADD CONSTRAINT competencias_laborales_ibfk_1 FOREIGN KEY (programa_formacion_id) REFERENCES programa (id_programa);
	  
	  
	  ALTER TABLE departamentos ADD CONSTRAINT departamentos_ibfk_1 FOREIGN KEY (regional_id) REFERENCES regional (id_regional);
	  
	  
	  ALTER TABLE elemento_ambiente ADD CONSTRAINT elemento_ambiente_ibfk_1 FOREIGN KEY (id_ambiente) REFERENCES ambiente (id_ambiente);
	  ALTER TABLE elemento_ambiente ADD  CONSTRAINT origen_elemento_id FOREIGN KEY (origen_elemento_id) REFERENCES origen_elemento (id_origen_elemento);
	  ALTER TABLE elemento_ambiente ADD  CONSTRAINT subcategoria_elemento_id FOREIGN KEY (subcategoria_elemento_id) REFERENCES subcategoria_elemento (id_subcategoria_elemento);
	  
	  
	  ALTER TABLE espacio_ambiente ADD CONSTRAINT fk_espacio_ambiente1 FOREIGN KEY (id_ambiente) REFERENCES ambiente (id_ambiente) ON DELETE NO ACTION ON UPDATE NO ACTION;
	  
	  
	  ALTER TABLE ficha ADD CONSTRAINT fk_Ficha_Centro_formacion1 FOREIGN KEY (centro_formacion_id) REFERENCES centro_formacion (id_centro_formacion) ON DELETE NO ACTION ON UPDATE NO ACTION;
	  ALTER TABLE ficha ADD CONSTRAINT fk_Ficha_programa1 FOREIGN KEY (programa_id) REFERENCES programa (id_programa) ON DELETE NO ACTION ON UPDATE NO ACTION;
	  
	  
	  
	  ALTER TABLE linea_tematica ADD CONSTRAINT fk_linea_tematica_redconocimiento1 FOREIGN KEY (id_redconocimiento) REFERENCES redconocimiento (idredconocimiento) ON DELETE NO ACTION ON UPDATE NO ACTION;
	  
	  
	  
	  ALTER TABLE  modulo ADD  CONSTRAINT modulo_ibfk_1 FOREIGN KEY (id_modulo_padre) REFERENCES modulo (id_modulo);
	  
	   
	  ALTER TABLE moduloxperfil ADD CONSTRAINT fk_Moduloxperfil_Modulo1 FOREIGN KEY (id_modulo) REFERENCES modulo (id_modulo) ON DELETE NO ACTION ON UPDATE NO ACTION;
	  ALTER TABLE moduloxperfil ADD CONSTRAINT fk_Moduloxperfil_perfil_usuario1 FOREIGN KEY (id_perfil_usuario) REFERENCES perfiles (id_perfil_usuario) ON DELETE NO ACTION ON UPDATE NO ACTION;
	  
	  
		
	  ALTER TABLE mueble_ambiente ADD CONSTRAINT mueble_ambiente_ibfk_1 FOREIGN KEY (id_ambiente) REFERENCES ambiente (id_ambiente);
	  
	  
	  ALTER TABLE municipios ADD CONSTRAINT municipios_ibfk_1 FOREIGN KEY (departamento_id) REFERENCES departamentos (id_departamento);
	  
	  
	  ALTER TABLE productos_ambiente ADD CONSTRAINT productos_ambiente_ibfk_1 FOREIGN KEY (producto_id) REFERENCES productos (id_producto);
	  ALTER TABLE productos_ambiente ADD  CONSTRAINT productos_ambiente_ibfk_2 FOREIGN KEY (ambiente_id) REFERENCES ambiente (id_ambiente);
	  
	  
	  


	  
	  ALTER TABLE programa ADD CONSTRAINT fk_programa_linea_tematica12 FOREIGN KEY (id_linea_tematica) REFERENCES linea_tematica (id_linea_tematica);
	  ALTER TABLE programa ADD CONSTRAINT fk_programa_nivel_programa14 FOREIGN KEY (id_nivel_programa) REFERENCES nivel_programa (id_nivel_programa);
	  ALTER TABLE programa ADD CONSTRAINT idredconocimiento FOREIGN KEY (idredconocimiento) REFERENCES redconocimiento (idredconocimiento);
	  
	  
	  
		 
	  ALTER TABLE programacion_ambientes ADD CONSTRAINT programacion_ambientes_ibfk_1 FOREIGN KEY (ambiente_id) REFERENCES ambiente (id_ambiente);
	  ALTER TABLE programacion_ambientes ADD CONSTRAINT programacion_ambientes_ibfk_3 FOREIGN KEY (instructor_id) REFERENCES users (id);
	  ALTER TABLE programacion_ambientes ADD CONSTRAINT programacion_ambientes_ibfk_4 FOREIGN KEY (ficha_id) REFERENCES ficha (id_ficha);
	  
	  
	  ALTER TABLE programacion_ficha ADD CONSTRAINT programacion_ficha_ibfk_2 FOREIGN KEY (programacion_ambiente_id) REFERENCES programacion_ambientes (id_programacion_ambiente);
	  ALTER TABLE programacion_ficha ADD CONSTRAINT programacion_ficha_ibfk_3 FOREIGN KEY (resultado_aprendizaje_id) REFERENCES resultados_aprendizaje (id_resultado_aprendizaje);
	  
	  
	  
	  
	  ALTER TABLE programacion_ficha_resultado ADD CONSTRAINT programacion_ficha_resultado_ibfk_1 FOREIGN KEY (programacion_ficha_id) REFERENCES programacion_ficha (id_programacion_ficha); 
	  ALTER TABLE programacion_ficha_resultado ADD CONSTRAINT programacion_ficha_resultado_ibfk_2 FOREIGN KEY (resultado_aprendizaje_id) REFERENCES resultados_aprendizaje (id_resultado_aprendizaje); 
	  
	  
	  
	  ALTER TABLE resultados_aprendizaje ADD CONSTRAINT resultados_aprendizaje_ibfk_1 FOREIGN KEY (competencia_laboral_id) REFERENCES competencias_laborales (id_competencia_laboral);
	  
	  
	  
	  ALTER TABLE sedes ADD CONSTRAINT sedes_ibfk_1 FOREIGN KEY (id_centro_formacion) REFERENCES centro_formacion (id_centro_formacion);
	  
	  
		   ALTER TABLE subcategoria_elemento ADD CONSTRAINT categoria_elemento_id FOREIGN KEY (categoria_elemento_id) REFERENCES categoria_elemento (id_categoria_elemento);
		   
		   
		   
		   

	   
	  
	  
	  ALTER TABLE tipos_riesgo_ambiente ADD CONSTRAINT tipos_riesgo_ambiente_ibfk_1 FOREIGN KEY (tipo_riesgo_id) REFERENCES tipos_riesgo (id_tipo_riesgo);
	  ALTER TABLE tipos_riesgo_ambiente ADD CONSTRAINT tipos_riesgo_ambiente_ibfk_2 FOREIGN KEY (ambiente_id) REFERENCES ambiente (id_ambiente);
	  
	  
	   -- CONSTRAINT email_usuario UNIQUE  (email)

	  ALTER TABLE users ADD CONSTRAINT fk_Usuario_Centro_formacion1 FOREIGN KEY (id_centro_formacion) REFERENCES centro_formacion (id_centro_formacion) ON DELETE NO ACTION ON UPDATE NO ACTION;
	  ALTER TABLE users ADD CONSTRAINT fk_Usuario_perfil_usuario1 FOREIGN KEY (id_perfil_usuario) REFERENCES perfiles (id_perfil_usuario) ON DELETE NO ACTION ON UPDATE NO ACTION; 
	  
	  
	  
