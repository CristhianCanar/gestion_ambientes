PGDMP     *                    x         	   gambiente    12.4 (Debian 12.4-1.pgdg100+1)    12.4     D           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            E           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            F           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            G           1262    16385 	   gambiente    DATABASE     {   CREATE DATABASE gambiente WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE gambiente;
                postgres    false                       1259    16695    programas_formacion    TABLE     �  CREATE TABLE public.programas_formacion (
    id_programa smallint DEFAULT nextval('public.programa_seq'::regclass) NOT NULL,
    idredconocimiento smallint NOT NULL,
    id_linea_tematica integer NOT NULL,
    id_nivel_programa integer NOT NULL,
    codigo_programa character varying(7) DEFAULT NULL::character varying,
    version_programa numeric,
    nombre_programa character varying(80) DEFAULT NULL::character varying,
    duracion_total character varying(45) DEFAULT NULL::character varying,
    created_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT programa_id_programa_check CHECK ((id_programa > 0))
);
 '   DROP TABLE public.programas_formacion;
       public         heap    postgres    false            A          0    16695    programas_formacion 
   TABLE DATA           �   COPY public.programas_formacion (id_programa, idredconocimiento, id_linea_tematica, id_nivel_programa, codigo_programa, version_programa, nombre_programa, duracion_total, created_at, updated_at) FROM stdin;
    public          postgres    false    258   �       �           2606    16872 !   programas_formacion programa_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.programas_formacion
    ADD CONSTRAINT programa_pkey PRIMARY KEY (id_programa);
 K   ALTER TABLE ONLY public.programas_formacion DROP CONSTRAINT programa_pkey;
       public            postgres    false    258            �           1259    16924     fk_programa_linea_tematica1_idx2    INDEX     m   CREATE INDEX fk_programa_linea_tematica1_idx2 ON public.programas_formacion USING btree (id_linea_tematica);
 4   DROP INDEX public.fk_programa_linea_tematica1_idx2;
       public            postgres    false    258            �           1259    16925     fk_programa_nivel_programa1_idx1    INDEX     m   CREATE INDEX fk_programa_nivel_programa1_idx1 ON public.programas_formacion USING btree (id_nivel_programa);
 4   DROP INDEX public.fk_programa_nivel_programa1_idx1;
       public            postgres    false    258            �           1259    16932    idredconocimiento    INDEX     ^   CREATE INDEX idredconocimiento ON public.programas_formacion USING btree (idredconocimiento);
 %   DROP INDEX public.idredconocimiento;
       public            postgres    false    258            �           2606    17080 0   programas_formacion fk_programa_linea_tematica12    FK CONSTRAINT     �   ALTER TABLE ONLY public.programas_formacion
    ADD CONSTRAINT fk_programa_linea_tematica12 FOREIGN KEY (id_linea_tematica) REFERENCES public.linea_tematica(id_linea_tematica);
 Z   ALTER TABLE ONLY public.programas_formacion DROP CONSTRAINT fk_programa_linea_tematica12;
       public          postgres    false    258            �           2606    17085 0   programas_formacion fk_programa_nivel_programa14    FK CONSTRAINT     �   ALTER TABLE ONLY public.programas_formacion
    ADD CONSTRAINT fk_programa_nivel_programa14 FOREIGN KEY (id_nivel_programa) REFERENCES public.nivel_programa(id_nivel_programa);
 Z   ALTER TABLE ONLY public.programas_formacion DROP CONSTRAINT fk_programa_nivel_programa14;
       public          postgres    false    258            �           2606    17100 %   programas_formacion idredconocimiento    FK CONSTRAINT     �   ALTER TABLE ONLY public.programas_formacion
    ADD CONSTRAINT idredconocimiento FOREIGN KEY (idredconocimiento) REFERENCES public.redes_conocimiento(idredconocimiento);
 O   ALTER TABLE ONLY public.programas_formacion DROP CONSTRAINT idredconocimiento;
       public          postgres    false    258            A   �   x���1
1D�S�+�����	�bmkc�@6�$���a/�~��B�)�)f@��7�=2��������z�Ֆ�c����)��'[%�-�ɺ0?�;4�&ܢ�[�XYd�\Va�y� �olb#�_��h�S����}H6�����RJ� �,B�     