@extends('public.layouts.app')
@section('title','Inicio de Sesión')
@section('content')
<div class="container mt-4">
    <div class="row justify-content-center card-login">
        <div class="col-12 col-sm-10 col-lg-8">
            <div class="card text-sm-center text-lg-center text-md-center">
                <div class="card-body justify-content-center">
                    <h3 class="card-title mt-3 text-center">¡Hola!, Ingrese con sus datos</h3>
                    <div class="card-content mt-md-4">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group row justify-content-center">
                                <div class="col-10 col-sm-8 col-lg-6">
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}" autocomplete="email" placeholder="{{ __('Correo Electrónico') }}" autofocus required>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row justify-content-center">
                                <div class="col-10 col-sm-8 col-lg-6">
                                    <div class="input-group">
                                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="{{ __('Contraseña') }}" autocomplete="current-password" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <i class="fas fa-eye-slash color-naranja" id="mostrar-password"></i>
                                            </div>
                                        </div>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center mb-5">
                                <div class="col-10 col-sm-8 col-lg-6">
                                    <button type="submit" class="btn btn-block mt-3 btn-card">{{ __('Iniciar sesión') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/login.js') }}"></script>
@endsection
