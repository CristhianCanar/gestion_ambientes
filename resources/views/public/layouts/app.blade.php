<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SIGPA - @yield('title')</title>
    <link rel="shortcut icon" href="{{ asset('img/logotipo_sena_orange.png') }}">
    
    <!-- Iconos Font Awesome -->
    <link href="{{ asset('font-awesome/css/all.css')}}" rel="stylesheet">

    <!-- Bootstrap y Estilos CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles_login.css')}}" rel="stylesheet">

    <!-- JQuery -->
    <script src="{{asset('js/jquery.js')}}"></script>

    <!-- Bootstrap JS -->
    <script src="{{ asset('js/app.js') }}" defer></script>

</head>
<body>
    <div id="app">
        <nav class="navbar-login">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-2 col-lg-1">
                        <a class="" href="{{ url('/') }}">
                            <img src="{{ asset('img/logo_sena.png') }}" id="logotipo_sena">
                        </a>
                    </div>
                    <div class="col-8 col-lg-10 d-block d-lg-none">
                        <h3 class="text-center" id="title-login">S I G P A</h3>
                    </div>
                    <div class="col-8 col-lg-10 d-none d-lg-block">
                        <h3 class="text-center" id="title-login">Sistema de Información para la Gestión y Programación de Ambientes</h3>
                    </div>
                    <div class="col-2 col-lg-1">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('img/logotipo_sigpa.png') }}" id="logotipo-gestion-ambientes">
                        </a>
                    </div>
                </div>
            </div>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto"></ul>
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->nombres }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </nav>
        <main class="py-4">
            <br>
            @yield('content')
        </main>

        @extends('public.layouts.footer')
    </div>
</body>
</html>
