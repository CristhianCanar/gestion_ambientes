<footer class="footer mt-auto">
    <div class="container">
        <div class="row">
            <div class="col-12 texto text-center mt-4">
                <span>Servicio Nacional de Aprendizaje SENA | Centro de Teleinformática y Producción Industrial CTPI - Regional Cauca</span>
            </div>
        </div>
        <div class="row">
            <div class="col-12 texto text-center">
                <span>fabricasoftwarectpi@misena.edu.co</span>                    
            </div>
        </div>
        <div class="row">
            <div class="col-12 texto text-center">
                <span>Copyright©2021</span>
            </div>
        </div>
        <div class="row">
            <div class="col-4 offset-4 text-center mt-2 mb-2">
                <a href="https://www.facebook.com/SENA/" target="_blank"><i class="fab fa-facebook-square fa-2x icono"></i></a>
                <a href="https://twitter.com/SENAComunica?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"><i class="fab fa-twitter-square fa-2x icono"></i></a>
                <a href="https://www.instagram.com/senacomunica/?hl=es-la" target="_blank"><i class="fab fa-instagram fa-2x icono"></i></a>
            </div>
            <div class="col-4 nuestra-autoria mt-2 mb-2">
                <span>Brayan & Cristhian FDS©2021</span>
            </div>
        </div>
    </div>
    
</footer>
