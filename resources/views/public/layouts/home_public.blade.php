<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SIGPA</title>
    <link rel="shortcut icon" href="{{asset('img/logotipo_sena_orange.png')}}" />
    <!-- Font Awesome Icons -->
    <link href="{{asset('font-awesome/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <!-- Plugin CSS -->
    <link href="{{asset('css/magnific-popup.css')}}" rel="stylesheet">
    <!-- Theme CSS - Includes Bootstrap -->
    <link href="{{asset('css/creative.css')}}" rel="stylesheet">
    <link href="{{asset('css/styles_home_public.css')}}" rel="stylesheet">
    <link href="{{asset('css/styles_footer.css')}}" rel="stylesheet">
</head>
<body id="page-top">

    <!-- Menu de Navegacion -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <img class="navbar-brand" id="logo" src="{{asset('img/logotipo_sena_white.png')}}" width="4%" alt="Logotipo SENA">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand js-scroll-trigger text-title mt-1 mt-lg-0" href="#page-top"> S I G P A </a>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#about"><i class="fas fa-question-circle"></i> S I G P A</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#services"><i class="fas fa-lightbulb"></i> Ventajas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#como_funciona"><i class="fas fa-book-open"></i> Como funciona</a>
                </li>
                @guest
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{ route('login') }}"><i class="fas fa-user"></i> Ingresar</a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{ url('/tablero') }}"><i class="fas fa-tachometer-alt"></i> Tablero</a>
                </li>
                @endguest
            </ul>
        </div>
    </nav>

    <!-- Subtitulo Imagen Inicial -->
    <header class="masthead">
        <div class="h-100">
            <div class="row h-100 align-items-center m-0">
                <div class="llamado_accion col-12 col-sm-8 col-lg-4 ml-lg-5">
                    <p id="title_llamado" class="text-center">Gestionar los ambientes de aprendizaje nunca fue tan fácil!</p>
                    <a id="btn_llamado" class="btn btn-success btn-lg js-scroll-trigger w-100" href="{{ route('login') }}">Consultar Ambientes</a>
                </div>
            </div>
        </div>
    </header>

    <!-- Seccion Que es Oportunidades -->
    <section class="page-section bg-primary" id="about">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <h2 class="text-white mt-0 title-target">Sistema de Información para la Gestión y Programación de Ambientes</h2>
                    <hr class="divider light my-4">
                    <p class="text-white-75 mb-4 text-target contenido-seccion">
                        El sistema de información para la gestión y programación de ambientes proporciona la información necesaria de las diferentes áreas,
                        elementos esenciales, que propicien una enseñanza que estimule el desarrollo de habilidades y competencias
                        valiosas para la vida de nuestros aprendices.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- Seccion Ventajas, Propositos, Beneficios -->
    <section class="page-section" id="services">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h2 class="mt-0 title-target">Ventajas</h2>
                    <hr class="divider my-4">
                </div>
            </div>
            <div class="row text-center text-lg-left mt-4">
                <div class="col-12 col-lg-10 offset-lg-2">
                    <div class="row">
                        <div class="col-2 col-lg-1">
                            <i class="fas fa-leaf fa-3x" id="iconos-seccion2"></i>
                        </div>
                        <div class="col-8 col-lg-4">
                            <span class="items-seccion2">Ecológico</span>
                        </div>
                        <div class="col-12 col-lg-6">
                            <h5 class="contenido-seccion">Evitamos más  de 1 tonelada en uso de papel al año</h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-2 col-lg-1">
                            <i class="fas fa-laugh fa-3x" id="iconos-seccion2"></i>
                        </div>
                        <div class="col-8 col-lg-4">
                            <span class="items-seccion2">Entretenido</span>
                        </div>
                        <div class="col-12 col-lg-6">
                            <h5 class="contenido-seccion">La rápidez y eficiencia en resultados y peticiones</h5>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-2 col-lg-1">
                            <i class="fas fa-search fa-3x" id="iconos-seccion2"></i>
                        </div>
                        <div class="col-8 col-lg-4">
                            <span class="items-seccion2">Eficiente</span>
                        </div>
                        <div class="col-12 col-lg-6">
                            <h5 class="contenido-seccion">
                                Sus búsquedas y estados de disponibilidad lo hacen el perfecto
                                gestor de información
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Seccion Como Funciona -->
    <section class="page-section" id="como_funciona">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <h2 class="mt-0 title-target">Como funciona</h2>
                    <hr class="divider my-4">
                    </p>
                </div>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/9zDLP6VMXlg" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    @extends('public.layouts.footer')
    
    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/bootstrap.bundle.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{asset('js/jquery.easing.js')}}"></script>
    <script src="{{asset('js/jquery.magnific-popup.js')}}"></script>
    <script src="{{asset('js/creative.js')}}"></script>
    <script src="{{asset('js/change-img-scroll.js')}}"></script>
</body>
</html>
