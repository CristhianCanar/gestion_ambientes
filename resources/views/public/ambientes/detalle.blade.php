@extends('layouts.componentes_home_public')
@section('title','Detalle')
@section('content')
<div class="row">
    <div class="col s12">
        <h4 class="titulo-detalle">TBT</h4>
    </div>
</div>
<div class="row">
    <div class="col s12 center">
        <picture>
            <img src="{{ asset('img/ambiente1.png') }}" alt="#" class="img-card">
        </picture>
    </div>
</div>
<div class="row">
    <div class="col s4 center">
        <h5 class="subtitulo-detalle">Información</h5>
        <h6 class="texto-detalle">Ambiente del area de sistemas de información</h6>
    </div>

    <div class="col s4 center">
        <h5 class="subtitulo-detalle">Capacidad</h5>
        <h6 class="texto-detalle">35 personas</h6>
    </div>

    <div class="col s4 center">
        <h5 class="subtitulo-detalle">Disponibilidad</h5>
        <h6 class="texto-detalle">Disponible</h6>
        <i class="fas fa-map-marker-alt fa-lg" id="iconos-detalle" title="Ubicación"></i>
    </div>
</div>

<div class="row">
    <div class="col s12">
        <h4 class="titulo-detalle2">Agendar Ambiente</h4>
    </div>
</div>

<div class="row">
    <div class="col s6">
        <input type="text" id="calendario" class="datepicker">
        <label for="calendario">Selecciona Fecha</label>
    </div>

    <div class="col s6">

    </div>
</div>
@endsection
