@extends('layouts.componentes_home_public')
@section('title', 'Consultar')
@section('content')
<div class="row">
    <div class="col s12 col m8 offset-m2">
        <h5 class="titulo-ambientes">Búsqueda de Ambientes de Aprendizaje</h5>
    </div>
</div>
<div class="row">
    <div class="col s12 col m8 offset-m2">
        <h6 class="subtitulo-ambientes">Ambientes en su centro:</h6>
    </div>
</div>
<div class="divider"></div>
<div class="row">
    <div class="col s12 m8 offset-m2">
        <div class="card horizontal hoverable">
            <div class="card-image">
                <picture>
                    <img src="{{ asset('img/ambiente1.png') }}">
                </picture>
            </div>
            <div class="card-stacked">
                <div class="card-content">
                    <h5>TBT</h5>
                    <h6>Ambiente del area de sistemas de información</h6>
                    <div class="row">
                        <div class="col s6">
                            <span>Capacidad:</span>
                            <span>35 personas</span>
                        </div>
                        <div class="col s6">
                            <span>Disponibilidad:</span>
                            <span style="color: green">Disponible</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6">
                            <a href="{{url('#')}}">
                                <i class="fas fa-map-marker-alt fa-lg" id="iconos-ambientes" title="Ubicación"></i>
                            </a>
                        </div>
                        <div class="col s6">
                            <a href="{{route('ambientes.create')}}">
                                <i class="fas fa-expand fa-lg" id="iconos-ambientes" title="Ver más"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="divider"></div>

<div class="row">
    <div class="col s12 m8 offset-m2">
        <div class="card horizontal hoverable">
            <div class="card-image">
                <picture>
                    <img src="{{ asset('img/ambiente2.png') }}">
                </picture>
            </div>
            <div class="card-stacked">
                <div class="card-content">
                    <h5>Impresiones 3D</h5>
                    <h6>Ambiente del area de sistemas de información</h6>
                    <div class="row">
                        <div class="col s6">
                            <span>Capacidad:</span>
                            <span>35 personas</span>
                        </div>
                        <div class="col s6">
                            <span>Disponibilidad:</span>
                            <span style="color: green">Disponible</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6">
                            <a href="{{url('#')}}">
                                <i class="fas fa-map-marker-alt fa-lg" id="iconos-ambientes" title="Ubicación"></i>
                            </a>
                        </div>
                        <div class="col s6">
                            <a href="{{route('ambientes.create')}}">
                                <i class="fas fa-expand fa-lg" id="iconos-ambientes" title="Ver más"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="divider"></div>

<div class="row">
    <div class="col s12 m8 offset-m2">
        <div class="card horizontal hoverable">
            <div class="card-image">
                <picture>
                    <img src="{{ asset('img/ambiente3.png') }}">
                </picture>
            </div>
            <div class="card-stacked">
                <div class="card-content">
                    <h5>Pasteleria</h5>
                    <h6>Ambiente del area de sistemas de información</h6>
                    <div class="row">
                        <div class="col s6">
                            <span>Capacidad:</span>
                            <span>35 personas</span>
                        </div>
                        <div class="col s6">
                            <span>Disponibilidad:</span>
                            <span style="color: red">Ocupado</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6">
                            <a href="{{url('#')}}">
                                <i class="fas fa-map-marker-alt fa-lg" id="iconos-ambientes" title="Ubicación"></i>
                            </a>
                        </div>
                        <div class="col s6">
                            <a href="{{route('ambientes.create')}}">
                                <i class="fas fa-expand fa-lg" id="iconos-ambientes" title="Ver más"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="divider"></div>

<div class="row">
    <div class="col s12 m8 offset-m2">
        <div class="card horizontal hoverable">
            <div class="card-image">
                <picture>
                    <img src="{{ asset('img/ambiente1.png') }}">
                </picture>
            </div>
            <div class="card-stacked">
                <div class="card-content">
                    <h5>Mecatronica</h5>
                    <h6>Ambiente del area de sistemas de información</h6>
                    <div class="row">
                        <div class="col s6">
                            <span>Capacidad:</span>
                            <span>35 personas</span>
                        </div>
                        <div class="col s6">
                            <span>Disponibilidad:</span>
                            <span style="color: green">Disponible</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6">
                            <a href="{{url('#')}}">
                                <i class="fas fa-map-marker-alt fa-lg" id="iconos-ambientes" title="Ubicación"></i>
                            </a>
                        </div>
                        <div class="col s6">
                            <a href="{{route('ambientes.create')}}">
                                <i class="fas fa-expand fa-lg" id="iconos-ambientes" title="Ver más"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="divider"></div>
@endsection