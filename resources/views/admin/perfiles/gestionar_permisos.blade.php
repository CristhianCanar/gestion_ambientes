@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center mb-4">
    <div class="col-12">
        <h3 class="card-title">Gestionar Permisos de Perfiles</h3>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-12">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="table-header">
                            <tr>
                                <th>Modulo</th>
                                @foreach($perfiles as $perfil)
                                <th class="text-center">{{$perfil->perfil_usuario}}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody class="table-body" id="roles_profiles">
                            @foreach($modulos as $modulo)
                            <tr>
                                <td class="align-bottom">
                                    <div class="row">
                                        @if ($modulo->id_modulo_padre == null)
                                            <div class="col-lg-1">
                                                <i class="{{$modulo->icono}}"></i>
                                            </div>
                                            <div class="col-lg-8">
                                                <h6>{{$modulo->modulo}}</h6>
                                            </div>
                                        @elseif($modulo->id_modulo_padre != null && $modulo->url_modulo == "#")
                                            <div class="col-lg-1 ml-lg-3">
                                                <i class="{{$modulo->icono}}"></i>
                                            </div>
                                            <div class="col-lg-8">
                                                <h6>{{$modulo->modulo}}</h6>
                                            </div>
                                        @else
                                            <div class="col-lg-1 ml-lg-5">
                                                <i class="{{$modulo->icono}}"></i>
                                            </div>
                                            <div class="col-lg-8">
                                                <h6>{{$modulo->modulo}}</h6>
                                            </div>
                                        @endif
                                    </div>
                                </td>
                                @foreach($perfiles as $perfil)
                                <td class="text-center">
                                    <div class="icheck-orange">
                                        <input type="checkbox" name="{{$perfil->perfil_usuario}}_{{$modulo->id_modulo}}" id="{{$perfil->perfil_usuario}}_{{$modulo->id_modulo}}" value="{{$modulo->id_modulo}}|{{$perfil->id_perfil_usuario}}"
                                            @foreach ($moduloxperfil as $mp)
                                                @if ($mp->id_perfil_usuario == $perfil->id_perfil_usuario &&  $mp->id_modulo == $modulo->id_modulo)
                                                    checked
                                                @endif
                                            @endforeach
                                        >
                                        <label for="{{$perfil->perfil_usuario}}_{{$modulo->id_modulo}}">
                                        </label>
                                    </div>
                                </td>
                                @endforeach
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@CSRF
<script>
    $(function(){

        $('#roles_profiles input[type="checkbox"]').on('change',function(){

                $.post("{{route('perfiles.gestionpermisos')}}",
                {
                    'id_modulo':    $(this).val().split('|')[0],
                    'id_perfil':    $(this).val().split('|')[1],
                    'oper':         $(this).prop('checked')?'add':'del',
                    '_token' :      $('[name="_token"]').val()
                },
                function(json){
                        $('[name="_token"]').val(json._token);
                        if(json.isOK){

                        }
                },
                'json');
        });
    });
</script>
@endsection