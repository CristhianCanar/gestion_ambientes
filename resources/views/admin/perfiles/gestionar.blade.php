@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center mb-4">
    <div class="col-12">
        <h3 class="card-title">Gestionar Perfiles</h3>
    </div>
</div>

@if (count($perfiles) > 0)
    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th>N°</th>
                                    <th>Perfil</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($perfiles as $perfil)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$perfil->perfil_usuario}}</td>
                                        <td class="text-center ">
                                            <div class="row justify-content-center">
                                                <div class="col-3">
                                                    <a data-toggle="tooltip" href="{{ route('perfiles.edit',$perfil->id_perfil_usuario) }}" type="button" title="Actualizar Perfil">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $perfiles->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">No hay perfiles registrados</h4>
@endif
@endsection
