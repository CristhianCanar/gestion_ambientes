@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-sm-10 col-lg-8">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Registrar Perfil</h3>
                </div>
                <form method="POST" action="{{route('perfiles.store')}}" class="needs-validation" novalidate>
                    @csrf
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="perfil_usuario"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Perfil</label>
                            <input type="text" class="form-control" name="perfil_usuario" id="perfil_usuario" placeholder="Ej: Instructor" maxlength="50" autofocus required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Perfil
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn w-100 btn-naranja" type="submit">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
