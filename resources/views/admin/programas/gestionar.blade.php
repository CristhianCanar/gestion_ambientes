@extends('admin.layouts.app')
@section('content')
<div class="row mb-4">
    <div class="col-12 col-lg-6 offset-lg-3">
        <h3 class="card-title">Gestionar Programas de Formación</h3>
    </div>
    <div class="col-12 col-sm-10 col-lg-3 offset-sm-1 offset-lg-0 mt-4 mt-lg-0">
        <form action="{{route('buscar.programa_formacion')}}" method="POST">
            @csrf
            <div class="input-group">
                <input type="text" name="buscar_programa_formacion" class="form-control" placeholder="Buscar Programa Formación">
                <div class="input-group-append">
                    <div class="input-group-text pt-0 pb-0">
                        <button class="btn p-0" type="submit">
                            <i class="fas fa-search color-naranja"></i>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
    @if (count($programas) > 0)
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card card-outline card-sena elevation-2">
                    <div class="card-body">
                        <!-- Tabla de registros -->
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="table-header">
                                    <tr>
                                        <th>N°</th>
                                        <th>Código</th>
                                        <th>Programa Formación</th>
                                        <th>Nivel Formación</th>
                                        <th class="text-truncate" style="max-width: 200px;">Linea Tecnológica</th>
                                        <th class="text-center">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody class="table-body">
                                    @foreach ($programas as $programa)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{$programa->codigo_programa}}</td>
                                            <td class="text-truncate" style="max-width: 250px;">{{$programa->nombre_programa}}</td>
                                            <td class="text-truncate" style="max-width: 150px;">{{$programa->nivel_programa->nivel_programa}}</td>
                                            <td class="text-truncate" style="max-width: 200px;">{{$programa->linea_tematica->linea_tematica}}</td>
                                            <td class="text-center">
                                                <div class="row justify-content-center">
                                                    <div class="col-4 col-lg-3">
                                                        <a href="{{route('programas_formacion.show',$programa->id_programa)}}">
                                                            <i class="fas fa-eye" data-toggle="tooltip" title="Detalles Programa"></i>
                                                        </a>
                                                    </div>
                                                    @if(Auth::user()->id_perfil_usuario == 1)
                                                        <div class="col-4 col-lg-3">
                                                            <a href="{{route('programas_formacion.edit',$programa->id_programa)}}">
                                                                <i class="fas fa-edit" data-toggle="tooltip" title="Actualizar Programa"></i>
                                                            </a>
                                                        </div>
                                                    @endif
                                                    <div class="col-4 col-lg-3">
                                                        <a href="{{route('programas_formacion.fichas.index',$programa->id_programa)}}">
                                                            <i class="fas fa-list-alt" data-toggle="tooltip" title="Gestionar Fichas"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="float-right">
                                {{ $programas ->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <h4 class="mt-5" style="text-align: center">No hay programas de formación registrados</h4>
    @endif
@endsection
