@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-sm-10 col-lg-8">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Registro Programa de Formación</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('programas_formacion.store')}}" novalidate>
                    @csrf
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="red_conocimiento_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Red de Conocimiento</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="red_conocimiento_id" id="red_conocimiento_id" autofocus required style="width: 100%;" data-select2-id="red_conocimiento_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled>Seleccione Red de Conocimiento</option>
                                @foreach($redes_conocimiento as $red)
                                    <option value="{{$red->idredconocimiento}}">{{$red->redconocimiento}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_linea_tematica"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Linea Tecnológica</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_linea_tematica" id="id_linea_tematica" required src="{{route('redes.getlinea','#')}}" style="width: 100%;" data-select2-id="id_linea_tematica" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">    
                                <option value="" selected disabled></option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una Linea Tecnológica
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_nivel_programa"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nivel de Formación</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_nivel_programa" id="id_nivel_programa" required style="width: 100%;" data-select2-id="id_nivel_programa" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled>Seleccione Nivel de Formación</option>
                                @foreach($niveles_programa as $nivel)
                                    <option value="{{$nivel->id_nivel_programa}}">{{$nivel->nivel_programa}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Nivel de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="codigo_programa"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Código Programa de Formación</label>
                            <input type="text" class="form-control" name="codigo_programa" id="codigo_programa" placeholder="Ej: 217303" maxlength="20" required>
                            <div class="invalid-feedback">
                                Escriba el Código del Programa de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="nombre_programa"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Programa de Formación</label>
                            <input type="text" class="form-control" name="nombre_programa" id="nombre_programa" placeholder="Ej: Programación de Software" maxlength="200" required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Programa de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="version_programa"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Versión del Programa de Formación</label>
                            <input type="number" class="form-control" name="version_programa" id="version_programa" placeholder="Ej: 102" min="1" max="9999" required>
                            <div class="invalid-feedback">
                                Escriba la Versión del Programa de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="duracion_total"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Duración Total del Programa de Formación (meses)</label>
                            <input type="number" class="form-control" name="duracion_total" id="duracion_total" placeholder="Ej: 12" min="1" max="99" required>
                            <div class="invalid-feedback">
                                Escriba la Duración Total (meses) del Programa de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn w-100 btn-naranja" type="submit">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/programas_formacion.js')}}"></script>
@endsection
