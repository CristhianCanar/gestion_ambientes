@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('programas_formacion.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Detalles Programa de Formación</h3>
                </div>
                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="codigo_programa">Código del Programa de Formación</label>
                        <h6>{{$programa->codigo_programa}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="nombre_programa">Programa de Formación</label>
                        <h6>{{$programa->nombre_programa}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="id_nivel_programa">Nivel de Formación</label>
                        <h6>{{$programa->nivel_programa->nivel_programa}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="idredconocimiento">Red de Conocimiento</label>
                        <h6>{{$programa->redconocimiento->redconocimiento}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="id_linea_tematica">Linea Tecnológica</label>
                        <h6>{{$programa->linea_tematica->linea_tematica}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="version_programa">Versión del Programa de Formación</label>
                        <h6>{{$programa->version_programa}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="duracion_total">Duración Total del Programa de Formación</label>
                        @if ($programa->duracion_total < 2)
                            <h6>{{$programa->duracion_total}} mes</h6>
                        @else 
                            <h6>{{$programa->duracion_total}} meses</h6>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
