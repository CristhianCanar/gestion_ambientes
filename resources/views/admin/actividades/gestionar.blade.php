@extends('admin.layouts.app')
@section('content')
<div class="row align-items-center mb-3">
    <div class="col-1 d-none d-lg-flex text-left">
        <a href="{{route('programas_formacion.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-8 offset-2 offset-lg-1">
        <h3 class="card-title">Gestionar Actividades de Aprendizaje</h3>
    </div>
    <div class="col-1 offset-1">
        <a href="{{route('programas_formacion.actividades.create', $id_programa)}}" class="btn btn-lg float-right">
            <i class="fas fa-plus-circle fa-2x" data-toggle="tooltip" id="icono-v" title="Registrar Actividad de Aprendizaje"></i>
        </a>
    </div>
</div>
@if (count($actividades_aprendizaje) >= 1)
    <div class="row justify-content-center">
        <div class="col-12 col-sm-10 col-lg-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <!-- Tabla de registros -->
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th class="align-top align-lg-middle">N°</th>
                                    <th class="align-top align-lg-middle">Actividad de Aprendizaje</th>
                                    <th class="align-top align-lg-middle text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($actividades_aprendizaje as $actividad)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$actividad->actividad_aprendizaje}}</td>
                                        <td class="text-center align-top align-lg-middle">
                                            <div class="row justify-content-center">
                                                <div class="col-5 col-lg-5">
                                                    <a href="{{ route('programas_formacion.actividades.show', [$id_programa, $actividad->id_actividad_aprendizaje]) }}">
                                                        <i class="fas fa-eye" data-toggle="tooltip" title="Detalles Actividad"></i>
                                                    </a>
                                                </div>
                                                <div class="col-5 col-lg-5">
                                                    <a href="#" type="button">
                                                        <i class="fas fa-edit" data-toggle="tooltip" title="Actualizar Actividad"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $actividades_aprendizaje ->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">El programa no tiene actividades de aprendizaje registradas</h4>
@endif
@endsection
