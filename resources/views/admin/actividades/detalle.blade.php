@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('programas_formacion.actividades.index',$id_programa)}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Detalles Actividad de Aprendizaje</h3>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label for="nombre" class="form-label">Actividad Aprendizaje</label>
                        <h6>{{$actividad_aprendizaje->actividad_aprendizaje}}</h6>
                    </div>
                </div>

                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

                    <ol class="carousel-indicators">
                        @for ($i = 0; $i < count($resultados_aprendizaje); $i++)
                            <li data-target="#carouselExampleControls" data-slide-to="{{$i}}" class="{{$i ? 'active' : ''}}" style="background-color: #FC7323"></li>
                        @endfor
                    </ol>
                    <div class="carousel-inner">
                        @for ($i = 0; $i < count($resultados_aprendizaje); $i++)
                            <div class="carousel-item {{$i ? 'active' : ''}}">
                                <div class="form-row justify-content-center">
                                    <div class="form-group col-10 col-lg-8">
                                        <label for="codigo_competencia" class="form-label">Código Competencia</label>
                                        <h6>{{$resultados_aprendizaje[$i][0]->competencia_laboral->codigo_competencia_laboral}}</h6>
                                    </div>

                                    <div class="form-group col-10 col-lg-8">
                                        <label for="version_competencia" class="form-label">Versión Competencia</label>
                                        <h6>{{$resultados_aprendizaje[$i][0]->competencia_laboral->version_competencia_laboral}}</h6>
                                    </div>

                                    <div class="form-group col-10 col-lg-8">
                                        <label for="competencia" class="form-label">Competencia</label>
                                        <h6>{{$resultados_aprendizaje[$i][0]->competencia_laboral->competencia_laboral}}</h6>
                                    </div>

                                    @for ($j = 0; $j < count($resultados_aprendizaje[$i]); $j++)
                                        <div class="form-group col-10 col-lg-8">
                                            <label for="resultado_aprendizaje" class="form-label">Resultado Aprendizaje</label>
                                            <h6>{{$resultados_aprendizaje[$i][$j]->resultado_aprendizaje}}</h6>
                                        </div>
                                        <div class="form-group col-10 col-lg-8">
                                            <label for="fecha_resultado_aprendizaje" class="form-label">Fecha Terminación</label>
                                            <h6>{{$resultados_aprendizaje[$i][$j]->fecha_terminacion}}</h6>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                        @endfor

                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span><i class="fas fa-chevron-circle-left fa-3x" aria-hidden="true"></i></span>
                            <span class="sr-only">Anterior</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span><i class="fas fa-chevron-circle-right fa-3x" aria-hidden="true"></i></span>
                            <span class="sr-only">Siguiente</span>
                        </a>
                    </div>
                </div>
                           
            </div>
        </div>
    </div>
</div>
@endsection
