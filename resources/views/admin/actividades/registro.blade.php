@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a class="btn btn-lg" href="{{route('programas_formacion.actividades.index',$id_programa)}}">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" id="icono-v" title="Regresar"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Registro Actividad de Aprendizaje</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('programas_formacion.actividades.store',$id_programa)}}" novalidate>
                    @csrf
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="actividad_aprendizaje"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Actividad Aprendizaje</label>
                            <textarea class="form-control" name="actividad_aprendizaje" id="actividad_aprendizaje" rows="2"
                            placeholder="Ej: Realizar el levantamiento de los requerimientos funcionales y no funcionales para el desarrollo del proyecto"
                            maxlength="200" autofocus required></textarea>
                            <div class="invalid-feedback">
                                Escriba el nombre de la Actividad de Aprendizaje
                            </div>
                        </div>
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="codigo_competencia_laboral"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Código Competencia</label>
                            <input type="number" class="form-control" name="codigo_competencia_laboral" id="codigo_competencia_laboral" placeholder="Ej: 1008025" min="0" max="99999999" required>
                            <div class="invalid-feedback">
                                Escriba el Código de la Competencia
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="version_competencia_laboral"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Versión Competencia</label>
                            <input type="number" step="any" class="form-control" name="version_competencia_laboral" id="version_competencia_laboral" placeholder="Ej: 102" min="0" max="999" required>
                            <div class="invalid-feedback">
                                Escriba la Versión de la Competencia
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="competencia_laboral"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Competencia</label>
                            <textarea class="form-control" name="competencia_laboral" id="competencia_laboral" rows="4"
                            placeholder="Ej: PARTICIPAR EN EL PROCESO DE NEGOCIACIÓN DE TECNOLOGÍA INFORMÁTICA PARA PERMITIR LA IMPLEMENTACIÓN DEL SISTEMA DE INFORMACIÓN"
                            maxlength="200" required></textarea>
                            <div class="invalid-feedback">
                                Escriba el nombre de la Competencia
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="resultado_aprendizaje"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Resultado Aprendizaje</label>
                            <a class="float-right" href="#">
                                <i class="fas fa-plus-circle fa-2x icon_resultado_aprendizaje" data-toggle="tooltip" id="icono-v" title="Registrar más resultados"></i>
                            </a>
                            <textarea class="form-control" name="resultado_aprendizaje" id="resultado_aprendizaje" rows="6"
                            placeholder="Ej: ELABORAR EL INFORME SOBRE EL CUMPLIMIENTO DE LOS TÉRMINOS DE REFERENCIA PREVISTOS EN LA NEGOCIACIÓN, DE ACUERDO CON LA PARTICIPACIÓN DE CADA UNO DE LOS ACTORES EN RELACIÓN CON LA SATISFACCIÓN DE LOS BIENES INFORMÁTICOS CONTRATADOS Y RECIBIDOS, SEGÚN NORMAS Y PROTOCOLOS DE LA ORGANIZACIÓN"
                            maxlength="200" required></textarea>
                            <div class="invalid-feedback">
                                Escriba el Resultado de Aprendizaje
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="fecha_terminacion"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Fecha Terminación de Resultado Aprendizaje</label>
                            <input type="date" class="form-control" name="fecha_terminacion" id="fecha_terminacion" required>
                            <div class="invalid-feedback">
                                Seleccione la Fecha de Terminación del Resultado de Aprendizaje
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 resultado_aprendizaje">
                        </div>

                        <div class="form-group col-10 col-lg-8 fecha_resultado_aprendizaje">
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3">
                            <button class="btn btn-naranja w-100" type="submit">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(".icon_resultado_aprendizaje").on("click", function () {
        var label_resultado_aprendizaje = $("<label></label>").attr({
            class: "form-label"
        }).text("Resultado Aprendizaje");
        var resultado_aprendizaje = $("<textarea></textarea>").attr({
            class: "form-control", name: "resultado_aprendizaje", id: "resultado_aprendizaje", rows: "4",
            placeholder: "Ej: ELABORAR EL INFORME SOBRE EL CUMPLIMIENTO DE LOS TÉRMINOS DE REFERENCIA PREVISTOS EN LA NEGOCIACIÓN, DE ACUERDO CON LA PARTICIPACIÓN DE CADA UNO DE LOS ACTORES EN RELACIÓN CON LA SATISFACCIÓN DE LOS BIENES INFORMÁTICOS CONTRATADOS Y RECIBIDOS, SEGÚN NORMAS Y PROTOCOLOS DE LA ORGANIZACIÓN"
        });

        var icono_eliminar = $("<a></a>").attr({
            class: "float-right", href: "#"
        }).html("<i class='fas fa-minus-circle fa-2x icon_resultado_aprendizaje' id='icono-v'></i>");

        $(".resultado_aprendizaje").append(label_resultado_aprendizaje, icono_eliminar, resultado_aprendizaje);

        var label_fecha_resultado_aprendizaje = $("<label></label>").attr({
            class: "form-label"
        }).text("Fecha Terminación de Resultado Aprendizaje");
        var fecha_resultado_aprendizaje = $("<input/>").attr({
            type: "date", class: "form-control", name: "fecha_resultado_aprendizaje",
            id: "fecha_resultado_aprendizaje"
        });
        $(".fecha_resultado_aprendizaje").append(label_fecha_resultado_aprendizaje, fecha_resultado_aprendizaje);

    });

</script>
@endsection
