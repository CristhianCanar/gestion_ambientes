@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center">
	<div class="col-12 mb-4 text-center">
		<h3 class="card-title">Reportes</h3>
	</div>
</div>
<div class="row justify-content-center">
	<div class="col-11 col-sm-10 col-lg-10">
		<div class="card card-outline card-sena elevation-2">
			<div class="card-body">
				<div class="card-content mt-md-4">
					<!-- Formulario reportes-->
					<div class="row">
						<div class="col-12 col-sm-8 offset-sm-2 col-lg-5 offset-lg-1">
							<form action="{{ route('reportes.store_ambientes') }}" method="POST">
								@csrf
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="titulo-reportes">
											<span class="float-left color-naranja">Ambientes</span>
											<div class="icheck-orange">
												<input type="checkbox" name="ambientes" id="ambientes">
												<label class="float-right" for="ambientes" data-toggle="tooltip" data-placement="right" id="subtitulo-reportes" title="Seleccionar Todo"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Información general</span>
											<div class="icheck-orange">
												<input type="checkbox" name="ambiente[]" id="informacion_general" value="informacion_general">
												<label class="checkbox-reportes float-right" for="informacion_general"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Descripción fisica</span>
											<div class="icheck-orange">
												<input type="checkbox" name="ambiente[]" id="descripcion_fisica" value="descripcion_fisica">
												<label class="checkbox-reportes float-right" for="descripcion_fisica"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Caracteristicas</span>
											<div class="icheck-orange">
												<input type="checkbox" name="ambiente[]" id="caracteristicas" value="caracteristicas">
												<label class="checkbox-reportes float-right" for="caracteristicas"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Condicion de Confort</span>
											<div class="icheck-orange">
												<input type="checkbox" name="ambiente[]" id="condiciones_confort" value="condiciones_confort">
												<label class="checkbox-reportes float-right" for="condiciones_confort"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Programas de Formación</span>
											<div class="icheck-orange">
												<input type="checkbox" name="ambiente[]" id="programa_formacion" value="programa_formacion">
												<label class="checkbox-reportes float-right" for="programa_formacion"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Espacios Complementarios</span>
											<div class="icheck-orange">
												<input type="checkbox" name="ambiente[]" id="espacio_complementario" value="espacio_complementario">
												<label class="checkbox-reportes float-right" for="espacio_complementario"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Elementos</span>
											<div class="icheck-orange">
												<input type="checkbox" name="ambiente[]" id="elemento" value="elemento">
												<label class="checkbox-reportes float-right" for="elemento"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-lg-10 mt-2 mb-4" id="btn-reportes">
										<button class="btn w-100 btn-naranja" type="submit">Descargar</button>
									</div>
								</div>
							</form>
						</div>

						<div class="col-12 col-sm-8 offset-sm-2 col-lg-5 offset-lg-1 mt-5 mt-lg-0">
							<form action="{{ route('reportes.store_centros_formacion') }}" method="POST">
								@csrf
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="titulo-reportes">
											<span class="float-left color-naranja">Centros Formación</span>
											<div class="icheck-orange">
												<input type="checkbox" name="centros_formacion" id="centros_formacion">
												<label class="float-right" for="centros_formacion" data-toggle="tooltip" data-placement="right" id="subtitulo-reportes" title="Seleccionar Todo"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Centro de Formación</span>
											<div class="icheck-orange">
												<input type="checkbox" name="centro_formacion[]" id="centro_formacion" value="centro_formacion">
												<label class="checkbox-reportes float-right" for="centro_formacion"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Dirección</span>
											<div class="icheck-orange">
												<input type="checkbox" name="centro_formacion[]" id="direccion_centro" value="direccion_centro">
												<label class="checkbox-reportes float-right" for="direccion_centro"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Teléfono</span>
											<div class="icheck-orange">
												<input type="checkbox" name="centro_formacion[]" id="telefono_centro" value="telefono_centro">
												<label class="checkbox-reportes float-right" for="telefono_centro"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Correo Electrónico</span>
											<div class="icheck-orange">
												<input type="checkbox" name="centro_formacion[]" id="email_centro" value="email_centro">
												<label class="checkbox-reportes float-right" for="email_centro"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Regional</span>
											<div class="icheck-orange">
												<input type="checkbox" name="centro_formacion[]" id="regional" value="regional">
												<label class="checkbox-reportes float-right" for="regional"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Nombre Subdirector</span>
											<div class="icheck-orange">
												<input type="checkbox" name="centro_formacion[]" id="subdirector" value="subdirector">
												<label class="checkbox-reportes float-right" for="subdirector"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Contacto Subdirector</span>
											<div class="icheck-orange">
												<input type="checkbox" name="centro_formacion[]" id="contacto_subdirector" value="contacto_subdirector">
												<label class="checkbox-reportes float-right" for="contacto_subdirector"></label>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-12 col-lg-10 mt-2 mb-4">
										<button class="btn w-100 btn-naranja" type="submit">Descargar</button>
									</div>
								</div>
							</form>
						</div>
					</div>

					<div class="row mt-5 mb-lg-5">
						<div class="col-12 col-sm-8 offset-sm-2 col-lg-5 offset-lg-1">
							<form action="{{ route('reportes.store_redes_conocimiento') }}" method="POST">
								@csrf
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="titulo-reportes">
											<span class="float-left color-naranja">Redes Conocimiento</span>
											<div class="icheck-orange">
												<input type="checkbox" name="red_conocimiento" id="red_conocimiento">
												<label class="float-right" for="red_conocimiento" data-toggle="tooltip" data-placement="right" id="subtitulo-reportes" title="Seleccionar Todo"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Nombre Gestor</span>
											<div class="icheck-orange">
												<input type="checkbox" name="red_conocimiento[]" id="nombre_gestor" value="nombre_gestor">
												<label class="checkbox-reportes float-right" for="nombre_gestor"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Teléfono Gestor</span>
											<div class="icheck-orange">
												<input type="checkbox" name="red_conocimiento[]" id="tel_gestor" value="tel_gestor">
												<label class="checkbox-reportes float-right" for="tel_gestor"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Correo Electrónico Gestor</span>
											<div class="icheck-orange">
												<input type="checkbox" name="red_conocimiento[]" id="mail_gestor" value="mail_gestor">
												<label class="checkbox-reportes float-right" for="mail_gestor"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Nombre Dinamizador</span>
											<div class="icheck-orange">
												<input type="checkbox" name="red_conocimiento[]" id="nombre_asesor" value="nombre_asesor">
												<label class="checkbox-reportes float-right" for="nombre_asesor"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Teléfono Dinamizador</span>
											<div class="icheck-orange">
												<input type="checkbox" name="red_conocimiento[]" id="tel_asesor" value="tel_asesor">
												<label class="checkbox-reportes float-right" for="tel_asesor"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Correo Electrónico Dinamizador</span>
											<div class="icheck-orange">
												<input type="checkbox" name="red_conocimiento[]" id="mail_asesor" value="mail_asesor">
												<label class="checkbox-reportes float-right" for="mail_asesor"></label>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-12 col-lg-10 mt-2 mb-4">
										<button class="btn w-100 btn-naranja" type="submit">Descargar</button>
									</div>
								</div>
							</form>
						</div>

						<div class="col-12 col-sm-8 offset-sm-2 col-lg-5 offset-lg-1 mt-5 mt-lg-0">
							<form action="{{ route('reportes.store_programas_formacion') }}" method="POST">
								@csrf
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="titulo-reportes">
											<span class="float-left color-naranja">Programas Formación</span>
											<div class="icheck-orange">
												<input type="checkbox" name="programas_formacion" id="programas_formacion">
												<label class="float-right" for="programas_formacion" data-toggle="tooltip" data-placement="right" id="subtitulo-reportes" title="Seleccionar Todo"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Código del Programa</span>
											<div class="icheck-orange">
												<input type="checkbox" name="programa_formacion[]" id="codigo_programa" value="codigo_programa">
												<label class="checkbox-reportes float-right" for="codigo_programa"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Versión del Programa</span>
											<div class="icheck-orange">
												<input type="checkbox" name="programa_formacion[]" id="version_programa" value="version_programa">
												<label class="checkbox-reportes float-right" for="version_programa"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Duración del Programa</span>
											<div class="icheck-orange">
												<input type="checkbox" name="programa_formacion[]" id="duracion_total" value="duracion_total">
												<label class="checkbox-reportes float-right" for="duracion_total"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Nivel de Formación</span>
											<div class="icheck-orange">
												<input type="checkbox" name="programa_formacion[]" id="nivel_programa" value="nivel_programa">
												<label class="checkbox-reportes float-right" for="nivel_programa"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Red de Conocimiento</span>
											<div class="icheck-orange">
												<input type="checkbox" name="programa_formacion[]" id="redconocimiento" value="redconocimiento">
												<label class="checkbox-reportes float-right" for="redconocimiento"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-10">
										<div class="custom-control custom-checkbox" id="items-reportes">
											<span class="float-left">Linea Tecnológica</span>
											<div class="icheck-orange">
												<input type="checkbox" name="programa_formacion[]" id="linea_tematica" value="linea_tematica">
												<label class="checkbox-reportes float-right" for="linea_tematica"></label>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-12 col-lg-10 mt-2 mb-4" id="btn-reportes">
										<button class="btn w-100 btn-naranja" type="submit">Descargar</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{{asset('js/reportes.js')}}"></script>
@endsection
