@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('redes_conocimiento.lineas_tematicas.index',$id_red_conocimiento)}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Detalles Linea Tecnológica</h3>
                </div>
                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label for="numero_ficha" class="form-label">Linea Tecnológica</label>
                        <h6>{{$red_linea->linea_tematica->linea_tematica}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label for="fecha_inicio" class="form-label">Descripción</label>
                        <h6>{{$red_linea->linea_tematica->descripcion_linea_tematica}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
