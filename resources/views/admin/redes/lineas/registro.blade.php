@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('redes_conocimiento.lineas_tematicas.index', $id_red_conocimiento)}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Asociar Linea Tecnológica</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('redes_conocimiento.lineas_tematicas.store', $id_red_conocimiento)}}" novalidate>
                    @csrf
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <input id="idredconocimiento" name="idredconocimiento" value="{{$id_red_conocimiento}}" type="hidden">
                            <label class="form-label" for="linea_tematica"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Linea Tecnológica</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="linea_tematica" id="linea_tematica" required autofocus style="width: 100%;" data-select2-id="linea_tematica" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled>Seleccione Linea Tecnológica</option>
                                @foreach($lineas_tematicas as $linea)
                                    <option value="{{$linea->id_linea_tematica}}">{{$linea->linea_tematica}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una Linea Tecnológica
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn w-100 btn-naranja" type="submit">Asociar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
