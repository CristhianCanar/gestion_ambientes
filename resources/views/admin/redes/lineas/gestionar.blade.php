@extends('admin.layouts.app')
@section('content')
<div class="row align-items-center mb-3">
    <div class="col-1 d-none d-lg-flex text-left">
        <a href="{{route('redes_conocimiento.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-8 offset-2 offset-lg-1">
        <h3 class="card-title">Gestionar Lineas Tecnológicas</h3>
    </div>
    <div class="col-1 offset-1">
        <a href="{{route('redes_conocimiento.lineas_tematicas.create', $id_red_conocimiento)}}" class="btn btn-lg float-right">
            <i class="fas fa-plus-circle fa-2x" data-toggle="tooltip" title="Asociar Linea Tecnológica" id="icono-v"></i>
        </a>
    </div>
</div>
@if (count($redes_lineas) > 0)
    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th>N°</th>
                                    <th class="text-truncate" style="max-width: 100px;">Linea Tecnológica</th>
                                    <th>Descripción</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($redes_lineas as $red_linea)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td class="text-truncate" style="max-width: 300px;">{{$red_linea->linea_tematica->linea_tematica}}</td>
                                        <td class="text-truncate" style="max-width: 350px;">{{$red_linea->linea_tematica->descripcion_linea_tematica}}</td>
                                        <td class="text-center">
                                            <div class="row justify-content-center">
                                                <div class="col-5 col-lg-3">
                                                    <a href="{{route('redes_conocimiento.lineas_tematicas.show', [$red_linea->red_conocimiento_id, $red_linea->linea_tematica_id])}}" type="button">
                                                        <i class="fas fa-eye" data-toggle="tooltip" title="Detalles Linea"></i>
                                                    </a>
                                                </div>
                                                <div class="col-5 col-lg-3">
                                                    <form action="{{ route('redes_conocimiento.lineas_tematicas.destroy',[$red_linea->red_conocimiento_id, $red_linea->linea_tematica_id]) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn p-0" onclick="return confirm('¿Seguro que desea Eliminar La Linea de la Red de Conocimiento?')">
                                                            <i class="fas fa-trash-alt" id="icono-v" data-toggle="tooltip" title="Eliminar Linea"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">No hay lineas tecnológicas asociadas a la red de conocimiento</h4>
@endif
@endsection
