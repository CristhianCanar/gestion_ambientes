@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('redes_conocimiento.lineas_tematicas.index', $id_red_conocimiento)}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Actualizar Linea Tecnológica</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('redes_conocimiento.lineas_tematicas.update', [$id_red_conocimiento, $linea->id_linea_tematica])}}" novalidate>
                    @csrf
                    @method('PUT')
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label  class="form-label" for="text"> <span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Linea Tecnológica</label>
                            <input type="text" class="form-control" name="linea_tematica" id="linea_tematica" value="{{$linea->linea_tematica}}" placeholder="Ej: Software" maxlength="60" required>
                            <div class="invalid-feedback">
                                Escriba el Nombre de la Linea Tecnológica
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn w-100 btn-naranja" type="submit">Actualizar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
