@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-sm-10 col-lg-8">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Registro Red de Conocimiento</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('redes_conocimiento.store')}}" novalidate>
                    @csrf
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="redconocimiento"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Red de Conocimiento</label>
                            <input type="text" class="form-control" name="redconocimiento" id="redconocimiento" placeholder="Ej: Mecánica Industrial" maxlength="100" autofocus required>
                            <div class="invalid-feedback">
                                Escriba el Nombre de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="nombre_gestor"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Gestor</label>
                            <input type="text" class="form-control" name="nombre_gestor" id="nombre_gestor" placeholder="Ej: William Andres Suarez Benavides" maxlength="80" required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Gestor de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="tel_gestor"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Teléfono Gestor</label>
                            <input type="number" class="form-control" name="tel_gestor" id="tel_gestor" placeholder="Ej: 0324353787" min="1" max="9999999999" required>
                            <div class="invalid-feedback">
                                Escriba el Teléfono del Gestor de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="mail_gestor"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Correo Electrónico Gestor</label>
                            <input type="email" class="form-control" name="mail_gestor" id="mail_gestor" placeholder="Ej: william@gmail.com" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba el Correo Electrónico del Gestor de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="nombre_asesor"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Dinamizador</label>
                            <input type="text" class="form-control" name="nombre_asesor" id="nombre_asesor" placeholder="Ej: Cristhian David Gomez Muñoz" maxlength="80" required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Dinamizador de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="tel_asesor"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Teléfono Dinamizador</label>
                            <input type="number" class="form-control" name="tel_asesor" id="tel_asesor" placeholder="Ej: 0324352238" min="1" max="9999999999" required>
                            <div class="invalid-feedback">
                                Escriba el Teléfono del Dinamizador de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="mail_asesor"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Correo Electrónico Dinamizador</label>
                            <input type="email" class="form-control" name="mail_asesor" id="mail_asesor" placeholder="Ej: cristhian@gmail.com" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba el Correo Electrónico del Dinamizador de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn w-100 btn-naranja" type="submit">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
