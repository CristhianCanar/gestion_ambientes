@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('redes_conocimiento.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Detalles Red de Conocimiento</h3>
                </div>
                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label  class="form-label" for="text">Red de Conocimiento</label>
                        @if ($red->redconocimiento != null)
                            <h6>{{$red->redconocimiento}}</h6>
                        @else
                            <h6>Sin registrar</h6>
                        @endif
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="nombre_gestor">Nombre Gestor</label>
                        @if ($red->nombre_gestor != null)
                            <h6>{{$red->nombre_gestor}}</h6>
                        @else
                            <h6>Sin registrar</h6>
                        @endif
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="telefono_gestor">Teléfono Gestor</label>
                        @if ($red->tel_gestor != null)
                            <h6>{{$red->tel_gestor}}</h6>
                        @else
                            <h6>Sin registrar</h6>
                        @endif
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="correo_gestor">Correo Electrónico Gestor</label>
                        @if ($red->mail_gestor != null)
                            <h6>{{$red->mail_gestor}}</h6>
                        @else
                            <h6>Sin registrar</h6>
                        @endif
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="nombre_asesor">Nombre Dinamizador</label>
                        @if ($red->nombre_asesor != null)
                            <h6>{{$red->nombre_asesor}}</h6>
                        @else
                            <h6>Sin registrar</h6>
                        @endif
                    </div>
                    
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="telefono_asesor">Teléfono Dinamizador</label>
                        @if ($red->tel_asesor != null)
                            <h6>{{$red->tel_asesor}}</h6>
                        @else
                            <h6>Sin registrar</h6>
                        @endif
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="correo_asesor">Correo Electrónico Dinamizador</label>
                        @if ($red->mail_asesor != null)
                            <h6>{{$red->mail_asesor}}</h6>
                        @else
                            <h6>Sin registrar</h6>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
