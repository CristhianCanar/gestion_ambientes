@extends('admin.layouts.app')
@section('content')
<div class="row mb-4">
    <div class="col-12 col-lg-6 offset-lg-3">
        <h3 class="card-title">Gestionar Redes de Conocimiento</h3>
    </div>
    <div class="col-12 col-sm-10 col-lg-3 offset-sm-1 offset-lg-0 mt-4 mt-lg-0">
        <form action="{{route('buscar.red_conocimiento')}}" method="POST">
            @csrf
            <div class="input-group">
                <input type="text" name="buscar_red_conocimiento" class="form-control" placeholder="Buscar Red Conocimiento">
                <div class="input-group-append">
                    <div class="input-group-text pt-0 pb-0">
                        <button class="btn p-0" type="submit">
                            <i class="fas fa-search color-naranja"></i>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@if (count($redes) > 0)
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <!-- Tabla de registros -->
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th>N°</th>
                                    <th>Red Conocimiento</th>
                                    <th>Gestor</th>
                                    <th>Teléfono</th>
                                    <th>Correo Electrónico</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($redes as $r)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td class="text-truncate" style="max-width: 250px;">{{$r->redconocimiento}}</td>
                                    <td class="text-truncate" style="max-width: 200px;">{{$r->nombre_gestor}}</td>
                                    <td>{{$r->tel_gestor}}</td>
                                    <td>{{$r->mail_gestor}}</td>
                                    <td class="text-center">
                                        <div class="row justify-content-center">
                                            <div class="col-4 col-lg-3">
                                                <a href="{{route('redes_conocimiento.show',$r->idredconocimiento)}}" type="button">
                                                    <i class="fas fa-eye" data-toggle="tooltip" title="Detalles Red"></i>
                                                </a>
                                            </div>
                                            <div class="col-4 col-lg-3">
                                                <a href="{{route('redes_conocimiento.edit',$r->idredconocimiento)}}" type="button">
                                                    <i class="fas fa-edit" data-toggle="tooltip" title="Actualizar Red"></i>
                                                </a>
                                            </div>
                                            <div class="col-4 col-lg-3">
                                                <a href="{{route('redes_conocimiento.lineas_tematicas.index',$r->idredconocimiento)}}" type="button">
                                                    <i class="fas fa-list" data-toggle="tooltip" title="Gestionar Lineas Tecnológicas"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right mt-2">
                            {{ $redes->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">No hay redes de conocimiento registradas</h4>
@endif
@endsection
