@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('redes_conocimiento.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Actualizar Red de Conocimiento</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('redes_conocimiento.update',$red->idredconocimiento)}}" novalidate>
                    @csrf
                    @method('PUT')
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="redconocimiento"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Red de Conocimiento</label>
                            <input type="text" class="form-control" name="redconocimiento" id="redconocimiento" value="{{$red->redconocimiento}}" placeholder="Ej: Mecánica Industrial" maxlength="100" required>
                            <div class="invalid-feedback">
                                Escriba el Nombre de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="nombre_gestor"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Gestor</label>
                            <input type="text" class="form-control" name="nombre_gestor" id="nombre_gestor" value="{{$red->nombre_gestor}}" placeholder="Ej: William Andres Suarez Benavides" maxlength="80" required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Gestor de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="tel_gestor"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Teléfono Gestor</label>
                            <input type="number" class="form-control" name="tel_gestor" id="tel_gestor" value="{{$red->tel_gestor}}" placeholder="Ej: 0324353787" min="1" max="9999999999" required>
                            <div class="invalid-feedback">
                                Escriba el Teléfono del Gestor de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="mail_gestor"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Correo Electrónico Gestor</label>
                            <input type="email" class="form-control" name="mail_gestor" id="mail_gestor" value="{{$red->mail_gestor}}" placeholder="Ej: william@gmail.com" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba el Correo Electrónico del Gestor de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="text"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Dinamizador</label>
                            <input type="text" class="form-control" name="nombre_asesor" id="nombre_asesor" value="{{$red->nombre_asesor}}" placeholder="Ej: Cristhian David Gomez Muñoz" maxlength="80" required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Dinamizador de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="text"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Teléfono Dinamizador</label>
                            <input type="number" class="form-control" name="tel_asesor" id="tel_asesor" value="{{$red->tel_asesor}}" placeholder="Ej: 0324352238" min="1" max="9999999999" required>
                            <div class="invalid-feedback">
                                Escriba el Teléfono del Dinamizador de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="text"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Correo Electrónico Dinamizador</label>
                            <input type="email" class="form-control" name="mail_asesor" id="mail_asesor" value="{{$red->mail_asesor}}" placeholder="Ej: cristhian@gmail.com" maxlength="50" required>
                             <div class="invalid-feedback">
                                Escriba el Correo Electrónico del Dinamizador de la Red de Conocimiento
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn w-100 btn-naranja" type="submit">Actualizar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
