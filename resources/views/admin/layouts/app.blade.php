<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIGPA @yield('title')</title>
    <link rel="shortcut icon" href="{{ asset('img/logotipo_sena_orange.png') }}">

    <!-- Iconos Font Awesome -->
    <link href="{{asset('font-awesome/css/all.css')}}" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="{{ asset('adminlte/css/adminlte.css') }}" rel="stylesheet">

    <!-- CSS Local -->
    <link href="{{ asset('css/styles_home_auth.css') }}" rel="stylesheet">

    <!-- iCheck Radio-Checkbox CSS -->
    <link href="{{asset('adminlte/plugins/icheck-bootstrap/icheck-bootstrap.css')}}" rel="stylesheet">

    <!-- Select2 CSS -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/select2/css/select2.css')}}">

    <!-- JQuery-UI -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/jquery-ui/jquery-ui.css')}}">

    <!-- FullCalendar 5.3.2 CSS -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/fullcalendar/main.css')}}">

    <!-- JQuery, JQuery-UI -->
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('adminlte/plugins/jquery-ui/jquery-ui.js')}}"></script>

    <!-- Bootstrap JS -->
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('adminlte/js/adminlte.js')}}"></script>
    <script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- JS Local -->
    <script src="{{asset('js/home_auth.js')}}"></script>

    <!-- Select2 JS -->
    <script src="{{asset('adminlte/plugins/select2/js/select2.full.js')}}"></script>

    <!-- FullCalendar 5.3.2 JS -->
    <script src="{{asset('adminlte/plugins/fullcalendar/main.js')}}"></script>
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-light elevation-1" id="navbar-h">
            <!-- Left navbar links -->

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars" id="iconos-sidenav"></i></a>
                </li>
            </ul>

            <!-- SEARCH FORM

            <form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                    </button>
                </div>
                </div>
            </form>
            -->
            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Notifications Dropdown Menu 
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                      <i class="far fa-bell"></i>
                      <span class="badge badge-warning navbar-badge"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                      <span class="dropdown-header"> Notificaciones</span>
                      <div class="dropdown-divider"></div>
                      <a href="#" class="dropdown-item">
                        <i class="fas fa-envelope mr-2"></i> 2 Nuevos Mensajes
                        <span class="float-right text-muted text-sm">3 mins</span>
                      </a>
                      <div class="dropdown-divider"></div>
                      <div class="dropdown-divider"></div>
                      <a href="#" class="dropdown-item">
                        <i class="fas fa-envelope mr-2"></i> 3 Nuevas Lineas

                        <span class="float-right text-muted text-sm">2 days</span>
                      </a>

                      <div class="dropdown-divider"></div>
                      <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                    </div>
                </li>
                -->
                <!--
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i class="fas fa-th-large"></i></a>
                </li>
                -->

                <!-- Item informacion de usuario -->
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 text-gray-600 small">{{Auth::User()->nombres}} {{Auth::User()->apellidos}}</span>
                        <i class="fas fa-user-circle fa-lg color-naranja" id="icono-a"></i>
                    </a>
                    <!-- Desplegable informacion de usuario -->
                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" id="opciones-usuario" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="{{route('usuario.perfil',Auth::User()->id)}}" id="items-usuario">
                            <i class="fas fa-user fa-lg fa-fw mr-2 text-gray-400"></i>Perfil
                        </a>

                        <div class="dropdown-divider">
                        </div>

                        <a class="dropdown-item" id="items-usuario" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt fa-lg fa-fw mr-2 text-gray-400"></i>
                            {{ __('Cerrar Sesión') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-orange elevation-2">
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
                <img src="{{asset('img/logotipo_sigpa.png')}}" class="brand-image">
                <span class="brand-text text-md" id="text-sidenav" data-toggle="tooltip" title="Sistema de Información para la Gestión y Programación de Ambientes">
                    S I G P A
                </span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    {!! $shtml !!}
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header)
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Starter Page</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Starter Page</li>
                            </ol>
                        </div>
                        </div>
                    </div>
                </div>
            -->
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <!--Alertas-->
                    @include('sweetalert::alert')
                    <br>
                    @yield('content')
                    <br>
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar
        <aside class="control-sidebar control-sidebar-light">
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
         -->
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer footer-auth">
            <div class="row">
                <div class="col-12 copyright text-center my-auto">
                    <span>Servicio Nacional de Aprendizaje SENA | Centro de Teleinformática y Producción Industrial CTPI - Regional Cauca</span>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-12 copyright text-center my-auto">
                    <span class="text-bold">fabricasoftwarectpi@misena.edu.co</span>                    
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-6 iconos my-auto">
                    <a href="https://www.facebook.com/SENA/" target="_blank"><i class="fab fa-facebook-square iconos"></i></a>
                    <a href="https://twitter.com/SENAComunica?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"><i class="fab fa-twitter-square iconos"></i></a>
                    <a href="https://www.instagram.com/senacomunica/?hl=es-la" target="_blank"><i class="fab fa-instagram iconos"></i></a>
                </div>

                <div class="col-6 copyright_two float-right mt-1">
                    <span class="text-bold">Copyright©2021</span>
                </div>
            </div>

            <div class="row">
                <div class="col-12 nuestra-autoria">
                    <span>Brayan & Cristhian FDS©2021</span>
                </div>
            </div>
        </footer>
    </div>
</body>
</html>

<script>
    $("#userDropdown").mouseenter(function () {
        $("#opciones-usuario").css("display", "block");
    })
    $("#navbar-h").mouseleave(function () {
        $("#opciones-usuario").css("display", "none");
    })
</script>
