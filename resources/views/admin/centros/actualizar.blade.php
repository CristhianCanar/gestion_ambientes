@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('centros_formacion.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Actualizar Centro de Formación</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('centros_formacion.update',$centro->id_centro_formacion)}}" novalidate>
                    @csrf
                    @method('PUT')
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label for="id_regional" class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Regional</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_regional" id="id_regional" required style="width: 100%;" data-select2-id="id_regional" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="{{$centro->id_regional}}">{{$centro->regional->regional}}</option>
                                @foreach($regional as $r)
                                    <option value="{{$r->id_regional}}">{{$r->regional}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una Regional
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label for="id_municipio" class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Municipio</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_municipio" id="id_municipio" required src="{{route('regionales.getmunicipio','#')}}" style="width: 100%;" data-select2-id="id_municipio" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="{{$centro->id_municipio}}">{{$centro->municipios->municipio}}</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Municipio
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="centro_formacion"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Centro de Formación</label>
                            <input type="text" class="form-control" name="centro_formacion" id="centro_formacion" value="{{$centro->centro_formacion}}" placeholder="Ej: Centro de Comercio y Servicios" maxlength="100" required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Centro de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="direccion_centro"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Dirección</label>
                            <input type="text" class="form-control" name="direccion_centro" id="direccion_centro" value="{{$centro->direccion_centro}}" placeholder="Ej: Cra 27B #1-22" maxlength="40" required>
                            <div class="invalid-feedback">
                                Escriba la Dirección del Centro de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="telefono_centro"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Teléfono</label>
                            <input type="number" class="form-control" name="telefono_centro" id="telefono_centro" value="{{$centro->telefono_centro}}" placeholder="Ej: 0328721772" min="1" max="9999999999" required>
                            <div class="invalid-feedback">
                                Escriba el Teléfono del Centro de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="email_centro"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Correo Electrónico</label>
                            <input type="email" class="form-control" name="email_centro" id="email_centro" value="{{$centro->email_centro}}" placeholder="Ej: comercio_servicios@gmail.com" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba el Correo Electrónico del Centro de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="subdirector"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Subdirector</label>
                            <input type="text" class="form-control" name="subdirector" id="subdirector" value="{{$centro->subdirector}}" placeholder="Ej: William Andres Suarez Benavides" maxlength="80" required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Subdirector del Centro de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="contacto_subdirector"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Correo Electrónico Subdirector</label>
                            <input type="email" class="form-control" name="contacto_subdirector" id="contacto_subdirector" value="{{$centro->contacto_subdirector}}" placeholder="Ej: william@gmail.com" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba el Correo Electrónico del Subdirector del Centro de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn w-100 btn-naranja" type="submit">Actualizar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/registro_centro_formacion.js')}}"></script>
@endsection
