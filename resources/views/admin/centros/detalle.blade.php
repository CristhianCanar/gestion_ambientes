@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('centros_formacion.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Detalles Centro de Formación</h3>
                </div>
                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="text">Centro de Formación</label>
                        <h6>{{$centro->centro_formacion}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label for="id_regional" class="form-label">Regional</label>
                        <h6>{{$centro->regional->regional}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label for="id_municipio" class="form-label">Municipio</label>
                        <h6>{{$centro->municipios->municipio}}</h6>
                    </div>
                    
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="text">Dirección</label>
                        <h6>{{$centro->direccion_centro}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="text">Teléfono</label>
                        <h6>{{$centro->telefono_centro}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="text">Correo Electrónico</label>
                        <h6>{{$centro->email_centro}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="text">Subdirector</label>
                        <h6>{{$centro->subdirector}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="text">Contacto Subdirector</label>
                        <h6>{{$centro->contacto_subdirector}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
