@extends('admin.layouts.app')
@section('content')
@if (Auth::user()->id_perfil_usuario == 1)
    <div class="row mb-4">
        <div class="col-12 col-lg-6 offset-lg-3">
            <h3 class="card-title">Gestionar Centros de Formación</h3>
        </div>
        <div class="col-12 col-sm-10 col-lg-3 offset-sm-1 offset-lg-0 mt-4 mt-lg-0">
            <form action="{{route('buscar.centro_formacion')}}" method="POST">
                @csrf
                <div class="input-group">
                    <input type="text" name="buscar_centro_formacion" class="form-control" placeholder="Buscar Centro Formación">
                    <div class="input-group-append">
                        <div class="input-group-text pt-0 pb-0">
                            <button class="btn p-0" type="submit">
                                <i class="fas fa-search color-naranja"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@else
    <div class="row justify-content-center mb-4">
        <div class="col-12">
            <h3 class="card-title">Gestionar Centros de Formación</h3>
        </div>
    </div>
@endif

@if (count($centros) > 0)
    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <!-- Tabla de registros -->
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th>N°</th>
                                    <th>Centro Formación</th>
                                    <th>Dirección</th>
                                    <th>Teléfono</th>
                                    <th>Subdirector</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($centros as $centro)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td class="text-truncate" style="max-width: 200px;">{{$centro->centro_formacion}}</td>
                                    <td class="text-truncate" style="max-width: 150px;">{{$centro->direccion_centro}}</td>
                                    <td>{{$centro->telefono_centro}}</td>
                                    <td class="text-truncate" style="max-width: 150px;">{{$centro->subdirector}}</td>
                                    <td class="text-center">
                                        <div class="row justify-content-center">
                                            <div class="col-4 col-lg-3">
                                                <a href="{{route('centros_formacion.show',$centro->id_centro_formacion)}}" type="button">
                                                    <i class="fas fa-eye" data-toggle="tooltip" title="Detalles Centro"></i>
                                                </a>
                                            </div>
                                            <div class="col-4 col-lg-3">
                                                <a href="{{route('centros_formacion.edit',$centro->id_centro_formacion)}}" type="button">
                                                    <i class="fas fa-edit" data-toggle="tooltip" title="Actualizar Centro"></i>
                                                </a>
                                            </div>
                                            <div class="col-4 col-lg-3">
                                                <a href="{{route('centros_formacion.sedes.index',$centro->id_centro_formacion)}}" type="button">
                                                    <i class="fas fa-building" data-toggle="tooltip" title="Gestionar Sedes"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $centros->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">No hay centros de formación registrados</h4>
@endif
@endsection
