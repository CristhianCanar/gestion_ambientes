@extends('admin.layouts.app')
@section('content')
<div class="row align-items-center mb-3">
    <div class="col-8 offset-2">
        <h3 class="card-title">Gestionar Tipos de Ambiente</h3>
    </div>
    <div class="col-1 offset-1">
        <a href="{{ route('tipos_ambiente.create') }}" type="button" class="btn btn-lg float-right">
            <i class="fas fa-plus-circle fa-2x" data-toggle="tooltip" title="Registrar Tipo" id="icono-v"></i>
        </a>
    </div>
</div>

@if (count($tipos_ambiente) > 0)
    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th>N°</th>
                                    <th>Tipo Ambiente</th>
                                    <th>Descripción</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($tipos_ambiente as $tipo_ambiente)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td class="text-truncate" style="max-width: 200px;">{{$tipo_ambiente->tipo_ambiente}}</td>
                                        <td class="text-truncate" style="max-width: 600px;">{{$tipo_ambiente->detalle_tipo_ambiente}}</td>
                                        <td class="text-center">
                                            <div class="row justify-content-center">
                                                <div class="col-3">
                                                    <a href="{{ route('tipos_ambiente.edit',$tipo_ambiente->id_tipo_ambiente) }}" type="button">
                                                        <i class="fas fa-edit" data-toggle="tooltip" title="Actualizar Tipo"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $tipos_ambiente->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">No hay tipos de ambiente registrados</h4>
@endif
@endsection
