@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('tipos_ambiente.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Actualizar Tipo de Ambiente</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{ route('tipos_ambiente.update',$tipo_ambiente->id_tipo_ambiente) }}" novalidate>
                    @csrf
                    @method('PATCH')
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="numero_ficha"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Tipo de Ambiente</label>
                            <input type="text" class="form-control" name="tipo_ambiente" id="tipo_ambiente" placeholder="Ej: Laboratorio" maxlength="45"
                                value="{{$tipo_ambiente->tipo_ambiente}}" required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Tipo de Ambiente
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="numero_ficha"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Descripción</label>
                            <textarea class="form-control" name="detalle_tipo_ambiente" id="detalle_tipo_ambiente" rows="4"
                            placeholder="Ej: Espacio cerrado con condiciones ambientales controladas para desarrollo de prácticas e investigación"
                            maxlength="200" required>{{$tipo_ambiente->detalle_tipo_ambiente}}</textarea>
                            <div class="invalid-feedback">
                                Escriba la Descripción del Tipo de Ambiente
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn btn-naranja w-100" type="submit">Actualizar</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
