@extends('admin.layouts.app')
@section('content')
<div class="row align-items-center mb-3">
    <div class="col-8 offset-2">
        <h3 class="card-title">Gestionar Productos de Producción de Centros</h3>
    </div>
    <div class="col-1 offset-1">
        <a href="{{ route('produccion_centros.create') }}" type="button" class="btn btn-lg float-right">
            <i class="fas fa-plus-circle fa-2x" data-toggle="tooltip" title="Registrar Producto" id="icono-v"></i>
        </a>
    </div>
</div>

@if (count($productos) > 0)
    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th>N°</th>
                                    <th>Producto</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($productos as $producto)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td class="text-truncate" style="max-width: 500px;">{{$producto->producto}}</td>
                                        <td class="text-center">
                                            <div class="row justify-content-center">
                                                <div class="col-3">
                                                    <a href="{{ route('produccion_centros.edit',$producto->id_producto) }}" type="button">
                                                        <i class="fas fa-edit" data-toggle="tooltip" title="Actualizar Producto"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $productos->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">No hay productos registrados</h4>
@endif
@endsection
