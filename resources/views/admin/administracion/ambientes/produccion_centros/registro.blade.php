@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('produccion_centros.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Registro Producto</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('produccion_centros.store')}}" novalidate>
                    @csrf
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="producto"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Producto</label>
                            <input type="text" class="form-control" name="producto" id="producto" placeholder="Ej: Aplicación Movil de Domicilios" maxlength="80" autofocus required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Producto
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn btn-naranja w-100" type="submit">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection