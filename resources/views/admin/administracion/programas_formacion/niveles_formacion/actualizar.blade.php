@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('niveles_formacion.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Actualizar Nivel de Formación</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('niveles_formacion.update', $nivel->id_nivel_programa)}}" novalidate>
                    @csrf
                    @method('PATCH')
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="numero_ficha"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Nivel de Formación</label>
                            <input type="text" class="form-control" name="nivel_programa" id="nivel_programa" placeholder="Ej: Tecnologo" maxlength="80"
                            value="{{$nivel->nivel_programa}}" required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Nivel de Formación
                            </div>
                        </div>
                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn btn-naranja w-100" type="submit">Actualizar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
