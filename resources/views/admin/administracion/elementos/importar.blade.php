@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-sm-10 col-lg-8">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mt-2 mb-4 text-center">
                        <h3 class="card-title">Importar Elementos</h3>
                    </div>
                </div>
                <form class="needs-validation" method="POST" action="{{route('elementos.store_importacion')}}" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="form-row justify-content-center mt-4">
                        <div class="form-group col-9 col-lg-7">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="elementos" id="elementos" accept=".xls,.xlsx">
                                <label class="custom-file-label" for="elementos" data-browse="Seleccionar">Archivo Excel</label>
                            </div>
                        </div>
                        <div class="form-group col-1 col-lg-1">
                            <a class="btn btn-lg" href="{{route('elementos.exportar_plantilla')}}">
                                <i class="fas fa-file-excel fa-lg" data-toggle="tooltip" title="Descargar Plantilla de Inventarios" id="icono-v"></i>
                            </a>
                        </div>
                    </div>

                    <div class="form-row justify-content-center mt-3 mb-4">
                        <div class="form-group col-10 col-lg-8">
                            <button type="submit" class="btn btn-naranja w-100">Importar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/registro_elemento.js')}}"></script>
@endsection
