@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-sm-10 col-lg-8">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mt-2 mb-4 text-center">
                        <h3 class="card-title">Registro Elemento</h3>
                    </div>
                </div>
                <form class="needs-validation" method="POST" action="{{route('elementos.store')}}" novalidate>
                    @csrf
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="centro_formacion_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Centro de Formación</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="centro_formacion_id" id="centro_formacion_id" autofocus required style="width: 100%;" data-select2-id="centro_formacion_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange"> 
                                <option value="" selected disabled>Seleccione Centro de Formación</option>
                                @foreach ($centros_formacion as $centro)
                                    <option value="{{$centro->id_centro_formacion}}">{{$centro->centro_formacion}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Centro de Formación
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="numero_inventario"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Número de Inventario</label>
                            <input type="number" class="form-control" id="numero_inventario" name="numero_inventario" placeholder="Ej: 100529210" min="1" required>
                            <div class="invalid-feedback">
                                Escriba el Número de Inventario del Elemento
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="serial"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Serial</label>
                            <input type="number" class="form-control" id="serial" name="serial" placeholder="Ej: 998852572" min="1" required>
                            <div class="invalid-feedback">
                                Escriba el Serial del Elemento
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_categoria_elemento"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Categoria</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_categoria_elemento" id="id_categoria_elemento" required style="width: 100%;" data-select2-id="id_categoria_elemento" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled>Seleccione Categoria</option>
                                @foreach ($categorias as $categoria)
                                    <option value="{{$categoria->id_categoria_elemento}}">{{$categoria->categoria_elemento}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione la Categoria del Elemento
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="subcategoria_elemento_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Subcategoria</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="subcategoria_elemento_id" id="subcategoria_elemento_id" required src="{{route('elementos_subcategorias.getsubcategorias','#')}}" style="width: 100%;" data-select2-id="subcategoria_elemento_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">                             
                                <option value="" selected disabled></option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione la Subcategoria del Elemento
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="elemento_ambiente"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Elemento</label>
                            <input type="text" class="form-control" id="elemento_ambiente" name="elemento_ambiente" maxlength="45" placeholder="Ej: Lector de codigos QR" required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Elemento
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="cantidad">Cantidad</label>
                            <input type="hidden" class="form-control" id="cantidad" name="cantidad" value="1">
                            <input type="number" class="form-control" id="cantidad" name="cantidad" value="1" disabled>
                        </div>

                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="estado"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Estado de Funcionamiento</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="estado" id="estado" required style="width: 100%;" data-select2-id="estado" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">                              
                                <option value="" selected disabled>Seleccione Estado de Funcionamiento</option>
                                <option value="Bueno">Bueno</option>
                                <option value="Regular">Regular</option>
                                <option value="Malo">Malo</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione el Estado de Funcionamiento del Elemento
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="fecha_adquisicion"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Fecha de Adquisición</label>
                            <input type="date" class="form-control" id="fecha_adquisicion" name="fecha_adquisicion" required>
                            <div class="invalid-feedback">
                                Seleccione la Fecha de Adquisición del Elemento
                            </div>
                        </div>
                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="origen_elemento_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Origen de Adquisición</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="origen_elemento_id" id="origen_elemento_id" required style="width: 100%;" data-select2-id="origen_elemento_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">                                                           
                                <option value="" selected disabled>Seleccione Origen de Adquisición</option>
                                @foreach ($origenes as $origen)
                                    <option value="{{$origen->id_origen_elemento }}">{{$origen->origen_elemento }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione el Origen de Adquisición del Elemento
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="tiempo_vida_util"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Tiempo de Vida Util (años)</label>
                            <input type="number" class="form-control" id="tiempo_vida_util" name="tiempo_vida_util" placeholder="Ej: 3" min="1" max="99" required>
                            <div class="invalid-feedback">
                                Escriba el Tiempo de Vida Util (años) del Elemento
                            </div>
                        </div>
                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="precio"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Precio</label>
                            <input type="number" step="any" class="form-control" id="precio" name="precio" placeholder="Ej: 300000" required>
                            <div class="invalid-feedback">
                                Escriba el Precio del Elemento
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="observacion_general">Observaciones Generales</label>
                            <textarea type="text" class="form-control" id="observacion_general" name="observacion_general" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="form-row justify-content-center mt-3 mb-4">
                        <div class="form-group col-10 col-lg-8">
                            <button type="submit" class="btn btn-naranja w-100">Registrar</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/registro_elemento.js')}}"></script>
@endsection
