@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('usuarios.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Detalles Usuario</h3>
                </div>
                <div class="form-row justify-content-center">
                    <!-- identificación usuario -->
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="identificacion">Identificación</label>
                        <h6>{{$usuario->identificacion}}</h6>
                    </div>

                    <!-- nombres usuario -->
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="nombres">Nombres</label>
                        <h6>{{$usuario->nombres}}</h6>
                    </div>

                    <!-- apellidos usuario -->
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="apellidos">Apellidos</label>
                        <h6>{{$usuario->apellidos}}</h6>
                    </div>

                    <!-- identificación usuario -->
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="telefono">Teléfono Movil</label>
                        <h6>{{$usuario->telefono}}</h6>
                    </div>

                    <!-- correo electronico usuario -->
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="email">Correo Electrónico</label>
                        <h6>{{$usuario->email}}</h6>
                    </div>

                    <!-- perfil usuario -->
                    <div class="form-group col-10 col-lg-8">
                        <label for="id_perfil_usuario" class="form-label">Perfil</label>
                        <h6>{{$usuario->perfiles->perfil_usuario}}</h6>
                    </div>

                    <!-- centro de formación usuario -->
                    <div class="form-group col-10 col-lg-8">
                        <label for="id_centro_formacion" class="form-label">Centro de Formación</label>
                        <h6>{{$usuario->centro_formacion->centro_formacion}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
