@extends('admin.layouts.app')
@section('content')
<div class="row mb-4">
    <div class="col-12 col-lg-6 offset-lg-3">
        <h3 class="card-title">Gestionar Usuarios</h3>
    </div>
    <div class="col-12 col-sm-10 col-lg-3 offset-sm-1 offset-lg-0 mt-4 mt-lg-0">
        <form action="{{route('buscar.usuario')}}" method="POST">
            @csrf
            <div class="input-group">
                <input type="text" name="buscar_usuario" class="form-control" placeholder="Buscar Usuario">
                <div class="input-group-append">
                    <div class="input-group-text pt-0 pb-0">
                        <button class="btn p-0" type="submit">
                            <i class="fas fa-search color-naranja"></i>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@if (count($usuarios) > 0)
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <!-- Tabla de registros -->
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th>N°</th>
                                    <th>Identificación</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Perfil</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($usuarios as $usuario)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$usuario->identificacion}}</td>
                                    <td class="text-truncate" style="max-width: 200px;">{{$usuario->nombres}}</td>
                                    <td class="text-truncate" style="max-width: 200px;">{{$usuario->apellidos}}</td>
                                    <td>{{$usuario->perfiles->perfil_usuario}}</td>
                                    <td class="text-center">
                                        <div class="row justify-content-center">
                                            <div class="col-4 col-lg-3">
                                                <a href="{{route('usuarios.show',$usuario->id)}}" type="button">
                                                    <i class="fas fa-eye" data-toggle="tooltip" title="Detalles Usuario"></i>
                                                </a>
                                            </div>
                                            <div class="col-4 col-lg-3">
                                                <a href="{{route('usuarios.edit',$usuario->id)}}" type="button">
                                                    <i class="fas fa-edit" data-toggle="tooltip" title="Actualizar Usuario"></i>
                                                </a>
                                            </div>
                                            @if ($usuario->estado)
                                                <div class="col-4 col-lg-3">
                                                    <form action="{{route('usuarios.usuarios_acceso',["id" => $usuario->id, "estado" => 0])}}" id="formEliminar" method="POST">
                                                        @csrf
                                                        @method('PATCH')
                                                        <button type="submit" class="btn p-0">
                                                            <i class="fas fa-user-check icono-denegar" data-toggle="tooltip" title="Denegar Acceso"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            @else
                                                <div class="col-4 col-lg-3">
                                                    <form action="{{route('usuarios.usuarios_acceso',["id" => $usuario->id,"estado" => 1])}}" id="formEliminar" method="POST">
                                                        @csrf
                                                        @method('PATCH')
                                                        <button type="submit" class="btn p-0">
                                                            <i class="fas fa-user-times icono-permitir" data-toggle="tooltip" title="Permitir acceso"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $usuarios->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">No hay usuarios registrados</h4>
@endif
@endsection
