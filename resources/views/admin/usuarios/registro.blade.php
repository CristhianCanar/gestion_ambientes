@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-sm-10 col-lg-8">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Registro Usuario</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('usuarios.store')}}" novalidate>
                    @csrf
                    <div class="form-row justify-content-center">
                        <!-- perfil usuario -->
                        <div class="form-group col-10 col-lg-8">
                            
                            <label for="id_perfil_usuario" class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Perfil</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_perfil_usuario" id="id_perfil_usuario" required style="width: 100%;" data-select2-id="id_perfil_usuario" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled>Seleccione Perfil</option>
                                @foreach($perfiles as $p)
                                    <option value="{{$p->id_perfil_usuario}}">{{$p->perfil_usuario}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Perfil
                            </div>
                        </div>

                        <!-- centro de formación usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label for="id_centro_formacion" class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Centro Formación</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_centro_formacion" id="id_centro_formacion" required style="width: 100%;" data-select2-id="id_centro_formacion" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled>Seleccione Centro de Formación</option>
                                @foreach($centros_formacion as $cf)
                                    <option value="{{$cf->id_centro_formacion}}">{{$cf->centro_formacion}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Centro de Formación
                            </div>
                        </div>

                        <!-- identificación usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="identificacion"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Identificación</label>
                            <input type="number" class="form-control" name="identificacion" id="identificacion" placeholder="Ej: 1085234214" min="1" max="9999999999" required>
                            <div class="invalid-feedback">
                                Escriba el Número de Identificación del Usuario
                            </div>
                        </div>

                        <!-- nombres usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="nombres"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombres</label>
                            <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Ej: Carlos Andres" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba los Nombres del Usuario
                            </div>
                        </div>

                        <!-- apellidos usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="apellidos"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Apellidos</label>
                            <input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="Ej: Rodriguez Suarez" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba los Apellidos del Usuario
                            </div>
                        </div>

                        <!-- telefono movil usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="telefono"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Teléfono Movil</label>
                            <input type="number" class="form-control" name="telefono" id="telefono" placeholder="Ej: 3108432390" min="1" max="9999999999" required>
                            <div class="invalid-feedback">
                                Escriba el Número de Teléfono Movil del Usuario
                            </div>
                        </div>

                        <!-- correo electronico usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="email"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Correo Electrónico</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Ej: carlos@sena.edu.co"  maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba el Correo Electrónico del Usuario
                            </div>
                        </div>

                        <!-- contraseña usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="password"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Contraseña</label>
                            <div class="input-group">
                                <input type="password" class="form-control" name="password" id="password" maxlength="20" required>
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <i class="fas fa-eye-slash color-naranja" id="mostrar-password" data-toggle="tooltip" title="Mostrar Contraseña"></i>
                                    </div>
                                </div>
                                <div class="invalid-feedback">
                                    Escriba la Contraseña para el Usuario
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn w-100 btn-naranja" type="submit">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/usuario.js') }}"></script>
@endsection
