@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-sm-10 col-lg-8">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Mi Perfil</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('usuario.update_perfil', $usuario->id)}}" novalidate>
                    @csrf
                    @method('PUT')
                    <div class="form-row justify-content-center">
                        <!-- perfil usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label for="id_perfil_usuario" class="form-label">Perfil</label>
                            <select class="custom-select" name="id_perfil_usuario" id="id_perfil_usuario" disabled>
                                <option value="{{$usuario->id_perfil_usuario}}" selected>{{$usuario->perfiles->perfil_usuario}}</option>
                            </select>
                        </div>
                    </div>

                    <!-- centro de formación usuario -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label for="id_centro_formacion" class="form-label">Centro Formación</label>
                            <select class="custom-select" name="id_centro_formacion" id="id_centro_formacion" disabled>
                                <option value="{{$usuario->id_centro_formacion}}" selected>{{$usuario->centro_formacion->centro_formacion}}</option>
                            </select>
                        </div>
                    </div>

                    <!-- identificación usuario -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="identificacion"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Identificación</label>
                            <input type="number" class="form-control" name="identificacion" id="identificacion" value="{{$usuario->identificacion}}" min="1" max="9999999999" required>
                            <div class="invalid-feedback">
                                Escriba su Número de Identificación
                            </div>
                        </div>
                    </div>
                
                    <!-- nombres usuario -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="nombres"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombres</label>
                            <input type="text" class="form-control" name="nombres" id="nombres" value="{{$usuario->nombres}}" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba sus Nombres
                            </div>
                        </div>
                    </div>

                    <!-- apellidos usuario -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="apellidos"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Apellidos</label>
                            <input type="text" class="form-control" name="apellidos" id="apellidos" value="{{$usuario->apellidos}}" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba sus Apellidos
                            </div>
                        </div>
                    </div>
                    
                    <!-- telefono movil usuario -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="telefono"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Teléfono Movil</label>
                            <input type="number" class="form-control" name="telefono" id="telefono" value="{{$usuario->telefono}}" min="1" max="9999999999" required>
                            <div class="invalid-feedback">
                                Escriba su Número de Teléfono
                            </div>
                        </div>
                    </div>

                    <!-- correo electronico usuario -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="email"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Correo Electrónico</label>
                            <input type="email" class="form-control" name="email" id="email" value="{{$usuario->email}}" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba su Correo Electrónico
                            </div>
                        </div>
                    </div>
   
                    <!-- contraseña usuario -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8 offset-1">
                            <label class="form-label" for="password"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Confirmar Contraseña Actual</label>
                            <div class="input-group">
                                <input type="password" class="form-control" name="password" id="password" maxlength="20" required>
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <i class="fas fa-eye-slash color-naranja" id="mostrar-password" data-toggle="tooltip" title="Mostrar Contraseña"></i>
                                    </div>
                                </div>
                                <div class="invalid-feedback">
                                    Confirme su Contraseña Actual
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-1">
                            <i class="fas fa-key color-naranja ml-2 mt-5" id="nueva_contrasena" data-toggle="tooltip" title="Cambiar Contraseña"></i>
                        </div>
                    </div>

                    <!-- nueva contraseña usuario -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8 offset-1 d-none input_nueva_contasena">
                            <label class="form-label" for="new_password"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nueva Contraseña</label>
                            <div class="input-group">
                                <input type="password" class="form-control" name="new_password" id="new_password" maxlength="20">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <i class="fas fa-eye-slash color-naranja" id="mostrar-new-password" data-toggle="tooltip" title="Mostrar Contraseña"></i>
                                    </div>
                                </div>
                                <div class="invalid-feedback">
                                    Escriba su Nueva Contraseña
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-1 d-none input_nueva_contasena">
                            <i class="fas fa-minus-circle color-naranja ml-2 mt-5" id="ocultar_nueva_contrasena" data-toggle="tooltip" title="No Cambiar Contraseña"></i>
                        </div>
                    </div>

                    <!-- confirmacion nueva contraseña usuario -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8 d-none input_nueva_contasena">
                            <label class="form-label" for="confirm_new_password"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Confirmar Nueva Contraseña</label>
                            <div class="input-group">
                                <input type="password" class="form-control" name="confirm_new_password" id="confirm_new_password" maxlength="20">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <i class="fas fa-eye-slash color-naranja" id="mostrar-confirm-new-password" data-toggle="tooltip" title="Mostrar Contraseña"></i>
                                    </div>
                                </div>
                                <div class="invalid-feedback">
                                    Confirme su Nueva Contraseña
                                </div>
                                <div class="invalid-input" id="invalid-contrasena">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn w-100 btn-naranja" type="submit">Actualizar</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/perfil_usuario.js') }}"></script>
@endsection
