@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('usuarios.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Actualizar Usuario</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('usuarios.update', $usuario->id)}}" novalidate>
                    @csrf
                    @method('PUT')
                    <div class="form-row justify-content-center">
                        <!-- perfil usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label for="id_perfil_usuario" class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Perfil</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_perfil_usuario" id="id_perfil_usuario" required style="width: 100%;" data-select2-id="id_perfil_usuario" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="{{$usuario->id_perfil_usuario}}" selected>{{$usuario->perfiles->perfil_usuario}}</option>
                                @foreach($perfiles as $p)
                                    <option value="{{$p->id_perfil_usuario}}">{{$p->perfil_usuario}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Perfil
                            </div>
                        </div>

                        <!-- centro de formación usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label for="id_centro_formacion" class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Centro Formación</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_centro_formacion" id="id_centro_formacion" required style="width: 100%;" data-select2-id="id_centro_formacion" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="{{$usuario->id_centro_formacion}}" selected>{{$usuario->centro_formacion->centro_formacion}}</option>
                                @foreach($centros_formacion as $cf)
                                    <option value="{{$cf->id_centro_formacion}}">{{$cf->centro_formacion}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Centro de Formación
                            </div>
                        </div>

                        <!-- identificación usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="identificacion"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Identificación</label>
                            <input type="number" class="form-control" name="identificacion" id="identificacion" value="{{$usuario->identificacion}}" min="1" max="9999999999" required>
                            <div class="invalid-feedback">
                                Escriba el Número de Identificación del Usuario
                            </div>
                        </div>

                        <!-- nombres usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="nombres"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombres</label>
                            <input type="text" class="form-control" name="nombres" id="nombres" value="{{$usuario->nombres}}" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba los Nombres del Usuario
                            </div>
                        </div>

                        <!-- apellidos usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="apellidos"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Apellidos</label>
                            <input type="text" class="form-control" name="apellidos" id="apellidos" value="{{$usuario->apellidos}}" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba los Apellidos del Usuario
                            </div>
                        </div>

                        <!-- telefono movil usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="telefono"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Teléfono Movil</label>
                            <input type="number" class="form-control" name="telefono" id="telefono" value="{{$usuario->telefono}}" min="1" max="9999999999" required>
                            <div class="invalid-feedback">
                                Escriba el Número de Teléfono Movil del Usuario
                            </div>
                        </div>

                        <!-- correo electronico usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="email"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Correo Electrónico</label>
                            <input type="email" class="form-control" name="email" id="email" value="{{$usuario->email}}" maxlength="50" required>
                            <div class="invalid-feedback">
                                Escriba el Correo Electrónico del Usuario
                            </div>
                        </div>

                        <!-- contraseña usuario -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="password">Contraseña</label>
                            <div class="input-group">
                                <input type="password" class="form-control" name="password" id="password" maxlength="20">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <i class="fas fa-eye-slash color-naranja" id="mostrar-password" data-toggle="tooltip" title="Mostrar Contraseña"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn w-100 btn-naranja" type="submit">Actualizar</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/usuario.js') }}"></script>
@endsection
