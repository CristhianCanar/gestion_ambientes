@extends('admin.layouts.app')
@section('content')
<script src="{{ asset('js/Chart.js') }}"></script>
<script src="{{ asset('js/Chart-bundle.js') }}"></script>
<script src="{{ asset('js/graficas.js') }}"></script>

<div class="container-fluid">
    <div class="row">
        <!-- Card Ambientes Registrados -->
        <div class="col-12 col-lg-4">
            <div class="small-box bg-azul">
                <div class="inner">
                    <h3 class="contador-ambientes-registrados">0</h3>
                    <p>Ambientes Registrados</p>
                </div>
                <div class="icon">
                    <i class="fas fa-chalkboard-teacher"></i>
                </div>
                <a href="{{route('home')}}" class="small-box-footer background-naranja">
                    Más información <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>

        <!-- Card Ambientes Programados -->
        <div class="col-12 col-lg-4">
            <div class="small-box bg-olive">
                <div class="inner">
                    <h3 class="contador-ambientes-programados">0</h3>
                    <p>Ambientes Programados</p>
                </div>
                <div class="icon">
                    <i class="fas fa-calendar-alt"></i>
                </div>
                <a href="{{route('ambientes_programados')}}" class="small-box-footer background-naranja">
                    Más información <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>

        <!-- Card Usuarios Registrados -->
        <div class="col-12 col-lg-4">
            <div class="small-box bg-amarillo">
                <div class="inner">
                    <h3 class="contador-usuarios-registrados">0</h3>
                    <p>Usuarios Registrados</p>
                </div>
                <div class="icon">
                    <i class="fas fa-users"></i>
                </div>
                <a href="{{route('usuarios_registrados')}}" class="small-box-footer background-naranja">
                    Más información <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>


    <div class="row">
        <!-- Grafica Usuarios Registrados por Perfiles -->
        <div class="col-12"> 
            <div class="card shadow mb-4">
                <div class="card-header bg-amarillo py-3">
                    <h6 class="m-0 font-weight-bold">Usuarios por Perfil</h6>
                </div>
                <div class="card-body">
                    <div class="chart-bar">
                        <canvas id="usuarios_perfil"></canvas>
                        <script>
                            var ctx = document.getElementById("usuarios_perfil");
                            var usuarios_perfil = new Chart(ctx, {
                                type: 'bar',
                                data: {
                                    labels: [
                                        @foreach ($perfiles_usuario as $per)
                                            "{{ substr($per->perfil_usuario, 0, 52) }}", 
                                        @endforeach
                                    ],
                                    datasets: [{
                                        label: 'Usuarios',
                                        backgroundColor: "#ffd149",
                                        hoverBackgroundColor: "#ffa000",
                                        borderColor: "#ffa000",
                                        data: [
                                            @foreach ($cantidad_usuarios_perfil as $cantidad_usuario)
                                                "{{ $cantidad_usuario }}", 
                                            @endforeach
                                        ]
                                    }]
                                },
                                options: {
                                    maintainAspectRatio: false,
                                    layout: {
                                        padding: {
                                            left: 10,
                                            right: 25,
                                            top: 25,
                                            bottom: 0
                                        }
                                    },
                                    scales: {
                                        xAxes: [{
                                            time: {
                                                unit: 'month'
                                            },
                                            gridLines: {
                                                display: false,
                                                drawBorder: false
                                            },
                                            ticks: {
                                                maxTicksLimit: 12
                                            },
                                            maxBarThickness: 30,
                                        }],
                                        yAxes: [{
                                            ticks: {
                                                maxTicksLimit: 3,
                                                padding: 10,
                                                callback: function(value, index, values) {
                                                    return number_format(value);
                                                }
                                            },
                                            gridLines: {
                                                color: "rgb(234, 236, 244)",
                                                zeroLineColor: "rgb(234, 236, 244)",
                                                drawBorder: false,
                                                borderDash: [2],
                                                zeroLineBorderDash: [2]
                                            }
                                        }],
                                    },
                                    legend: {
                                        display: false
                                    },
                                    tooltips: {
                                        titleMarginBottom: 10,
                                        titleFontColor: '#6e707e',
                                        titleFontSize: 14,
                                        backgroundColor: "rgb(255,255,255)",
                                        bodyFontColor: "#858796",
                                        borderColor: '#dddfeb',
                                        borderWidth: 1,
                                        xPadding: 15,
                                        yPadding: 15,
                                        displayColors: false,
                                        caretPadding: 10,
                                        callbacks: {
                                            label: function(tooltipItem, chart) {
                                                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                                                return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                                            }
                                        }
                                    },
                                }
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery({
        ambientes_registrados:  0,
        ambientes_programados:  0,
        usuarios_registrados:   0
    }).animate({
        ambientes_registrados:  '{{$cantidad_ambientes_registrados}}',
        ambientes_programados:  '{{$cantidad_ambientes_programados}}',
        usuarios_registrados:   '{{$cantidad_usuarios_registrados}}'
    }, 
    {
        duration: 2000,
        easing:'swing', 
        step: function() { 
            $('.contador-ambientes-registrados').text(Math.ceil(this.ambientes_registrados));
            $('.contador-ambientes-programados').text(Math.ceil(this.ambientes_programados));
            $('.contador-usuarios-registrados').text(Math.ceil(this.usuarios_registrados));
        }
    });
</script>
@endsection
