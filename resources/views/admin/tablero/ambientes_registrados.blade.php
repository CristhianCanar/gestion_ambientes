@extends('admin.layouts.app')
@section('content')
<script src="{{ asset('js/Chart.js') }}"></script>
<script src="{{ asset('js/Chart-bundle.js') }}"></script>
<script src="{{ asset('js/graficas.js') }}"></script>

<div class="container-fluid">
    <div class="row">
        <!-- Card Ambientes Registrados -->
        @if (Auth::user()->id_perfil_usuario == 1 || Auth::user()->id_perfil_usuario == 2)
            <div class="col-12 col-lg-4">
        @else
            <div class="col-12 col-lg-6">
        @endif
                <div class="small-box bg-azul">
                    <div class="inner">
                        <h3 class="contador-ambientes-registrados">0</h3>
                        <p>Ambientes Registrados</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-chalkboard-teacher"></i>
                    </div>
                    <a href="{{route('home')}}" class="small-box-footer background-naranja">
                        Más información <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

        <!-- Card Ambientes Programados -->
        @if (Auth::user()->id_perfil_usuario == 1 || Auth::user()->id_perfil_usuario == 2)
            <div class="col-12 col-lg-4">
        @else
            <div class="col-12 col-lg-6">
        @endif
                <div class="small-box bg-olive">
                    <div class="inner">
                        <h3 class="contador-ambientes-programados">0</h3>
                        <p>Ambientes Programados</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-calendar-alt"></i>
                    </div>
                    <a href="{{route('ambientes_programados')}}" class="small-box-footer background-naranja">
                        Más información <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        
        <!-- Card Usuarios Registrados -->
        @if (Auth::user()->id_perfil_usuario == 1 || Auth::user()->id_perfil_usuario == 2)
            <div class="col-12 col-lg-4">
                <div class="small-box bg-amarillo">
                    <div class="inner">
                        <h3 class="contador-usuarios-registrados">0</h3>
                        <p>Usuarios Registrados</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-users"></i>
                    </div>
                    <a href="{{route('usuarios_registrados')}}" class="small-box-footer background-naranja">
                        Más información <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        @endif
    </div>

    <div class="row">
        <!-- Grafica Ambientes Registrados por Tipos -->
        <div class="col-12 col-lg-6">
            <div class="card shadow mb-4" style="height: 100%">
                <div class="card-header bg-azul py-3">
                    <h6 class="m-0 font-weight-bold">Ambientes por Tipo</h6>
                </div>
                <div class="card-body">
                    <div class="chart-pie pt-4">
                        <canvas id="ambientes_tipo"></canvas>
                        <script>
                            var ctx = document.getElementById("ambientes_tipo");
                            var ambientes_tipo = new Chart(ctx, {
                                type: 'doughnut',
                                data: {
                                    labels: [
                                        @foreach ($tipos_ambiente as $tipo)
                                            ["{{ $tipo->tipo_ambiente }}"], 
                                        @endforeach
                                    ],
                                    datasets: [{
                                        data: [
                                            @foreach ($cantidad_tipos_ambiente as $cantidad_tipo)
                                                ["{{ $cantidad_tipo }}"], 
                                            @endforeach
                                        ],
                                        backgroundColor: ['#86a1ff', '#36b9cc', '#1cc88a', '#ffa453', '#ffd149', '#ff96b3'],
                                        hoverBackgroundColor: ['#4e73df','#2c9faf', '#17a673', '#fc7323', '#ffa000', '#ff6384'],
                                        hoverBorderColor: "rgba(234, 236, 244)",
                                    }],
                                },
                                options: {
                                    maintainAspectRatio: false,
                                    tooltips: {
                                        backgroundColor: "rgb(255,255,255)",
                                        bodyFontColor: "#858796",
                                        borderColor: '#dddfeb',
                                        borderWidth: 1,
                                        xPadding: 15,
                                        yPadding: 15,
                                        displayColors: false,
                                        caretPadding: 10,
                                    },
                                    legend: {
                                        display: true,
                                        position: 'right',
                                        align: 'start'
                                    },
                                    cutoutPercentage: 50,
                                },
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>

        <!-- Grafica Ambientes Registrados por Tenencias -->
        <div class="col-12 col-lg-6"> 
            <div class="card shadow mb-4" style="height: 100%">
                <div class="card-header py-3 bg-azul">
                    <h6 class="m-0 font-weight-bold">Ambientes por Tenencia</h6>
                </div>
                <div class="card-body">
                    <div class="chart-pie pt-4">
                        <canvas id="ambientes_tenencia"></canvas>
                        <script>
                            var ctx = document.getElementById("ambientes_tenencia");
                            var ambientes_tenencia = new Chart(ctx, {
                                type: 'doughnut',
                                data: {
                                    labels: [
                                        @foreach ($tenencia_ambiente as $tenencia)
                                            ["{{ $tenencia->tenencia }}"], 
                                        @endforeach
                                    ],
                                    datasets: [{
                                        data: [
                                            @foreach ($cantidad_tenencia_ambiente as $cantidad_tenencia)
                                                ["{{ $cantidad_tenencia }}"], 
                                            @endforeach
                                        ],
                                        backgroundColor: ['#ff96b3', '#86a1ff', '#36b9cc', '#ffa453', '#1cc88a'],
                                        hoverBackgroundColor: ['#ff6384', '#4e73df', '#2c9faf', '#fc7323', '#17a673'],
                                        hoverBorderColor: "rgba(234, 236, 244)",
                                    }],
                                },
                                options: {
                                    maintainAspectRatio: false,
                                    tooltips: {
                                        backgroundColor: "rgb(255,255,255)",
                                        bodyFontColor: "#858796",
                                        borderColor: '#dddfeb',
                                        borderWidth: 1,
                                        xPadding: 15,
                                        yPadding: 15,
                                        displayColors: false,
                                        caretPadding: 10,
                                    },
                                    legend: {
                                        display: true,
                                        position: 'right',
                                        align: 'start'
                                    },
                                    cutoutPercentage: 50,
                                },
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Auth::user()->id_perfil_usuario == 1)
        <div class="row mt-4">
            <!-- Grafica Top 10 de Ambientes Registrados por Centros Formación -->
            <div class="col-12">
                <div class="card shadow mb-4">
                    <div class="card-header bg-azul py-3">
                        <h6 class="m-0 font-weight-bold">Top 10 de Ambientes por Centro Formación</h6>
                    </div>
                    <div class="card-body">
                        <div class="chart-bar">
                            <canvas id="ambientes_centro"></canvas>
                            <script>
                                var ctx = document.getElementById("ambientes_centro");
                                var ambientes_centro = new Chart(ctx, {
                                    type: 'bar',
                                    data: {
                                        labels: [
                                            @foreach ($ambientes_centro_formacion as $ambiente_centro)
                                                "{{ substr($ambiente_centro->centro_formacion, 0, 55) }}", 
                                            @endforeach
                                        ],
                                        datasets: [{
                                            label: 'Ambientes',
                                            backgroundColor: "#86a1ff",
                                            hoverBackgroundColor: "#4e73df",
                                            borderColor: "#4e73df",
                                            data: [
                                                @foreach ($ambientes_centro_formacion as $cantidad_ambiente_centro)
                                                    "{{ $cantidad_ambiente_centro->cantidad_ambientes }}", 
                                                @endforeach
                                            ]
                                        }]
                                    },
                                    options: {
                                        maintainAspectRatio: false,
                                        layout: {
                                            padding: {
                                                left: 10,
                                                right: 25,
                                                top: 25,
                                                bottom: 0
                                            }
                                        },
                                        scales: {
                                            xAxes: [{
                                                time: {
                                                    unit: 'month'
                                                },
                                                gridLines: {
                                                    display: false,
                                                    drawBorder: false
                                                },
                                                ticks: {
                                                    maxTicksLimit: 12
                                                },
                                                maxBarThickness: 30,
                                            }],
                                            yAxes: [{
                                                ticks: {
                                                    maxTicksLimit: 3,
                                                    padding: 10,
                                                    callback: function(value, index, values) {
                                                        return number_format(value);
                                                    }
                                                },
                                                gridLines: {
                                                    color: "rgb(234, 236, 244)",
                                                    zeroLineColor: "rgb(234, 236, 244)",
                                                    drawBorder: false,
                                                    borderDash: [2],
                                                    zeroLineBorderDash: [2]
                                                }
                                            }],
                                        },
                                        legend: {
                                            display: false
                                        },
                                        tooltips: {
                                            titleMarginBottom: 10,
                                            titleFontColor: '#6e707e',
                                            titleFontSize: 14,
                                            backgroundColor: "rgb(255,255,255)",
                                            bodyFontColor: "#858796",
                                            borderColor: '#dddfeb',
                                            borderWidth: 1,
                                            xPadding: 15,
                                            yPadding: 15,
                                            displayColors: false,
                                            caretPadding: 10,
                                            callbacks: {
                                                label: function(tooltipItem, chart) {
                                                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                                                    return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                                                }
                                            }
                                        },
                                    }
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="row mt-4">
            <!-- Grafica Ambientes Registrados por Sedes -->
            <div class="col-12">
                <div class="card shadow mb-4">
                    <div class="card-header bg-azul py-3">
                        <h6 class="m-0 font-weight-bold">Ambientes por Sede</h6>
                    </div>
                    <div class="card-body">
                        <div class="chart-bar">
                            <canvas id="ambientes_sede"></canvas>
                            <script>
                                var ctx = document.getElementById("ambientes_sede");
                                var ambientes_sede = new Chart(ctx, {
                                    type: 'bar',
                                    data: {
                                        labels: [
                                            @foreach ($ambientes_sede as $sede)
                                                "{{ $sede->sede }}", 
                                            @endforeach
                                        ],
                                        datasets: [{
                                            label: 'Ambientes',
                                            backgroundColor: "#86a1ff",
                                            hoverBackgroundColor: "#4e73df",
                                            borderColor: "#4e73df",
                                            data: [
                                                @foreach ($cantidad_ambientes_sede as $cantidad_sede)
                                                    "{{ $cantidad_sede }}", 
                                                @endforeach
                                            ]
                                        }]
                                    },
                                    options: {
                                        maintainAspectRatio: false,
                                        layout: {
                                            padding: {
                                                left: 10,
                                                right: 25,
                                                top: 25,
                                                bottom: 0
                                            }
                                        },
                                        scales: {
                                            xAxes: [{
                                                time: {
                                                    unit: 'month'
                                                },
                                                gridLines: {
                                                    display: false,
                                                    drawBorder: false
                                                },
                                                ticks: {
                                                    maxTicksLimit: 12
                                                },
                                                maxBarThickness: 30,
                                            }],
                                            yAxes: [{
                                                ticks: {
                                                    maxTicksLimit: 3,
                                                    padding: 10,
                                                    callback: function(value, index, values) {
                                                        return number_format(value);
                                                    }
                                                },
                                                gridLines: {
                                                    color: "rgb(234, 236, 244)",
                                                    zeroLineColor: "rgb(234, 236, 244)",
                                                    drawBorder: false,
                                                    borderDash: [2],
                                                    zeroLineBorderDash: [2]
                                                }
                                            }],
                                        },
                                        legend: {
                                            display: false
                                        },
                                        tooltips: {
                                            titleMarginBottom: 10,
                                            titleFontColor: '#6e707e',
                                            titleFontSize: 14,
                                            backgroundColor: "rgb(255,255,255)",
                                            bodyFontColor: "#858796",
                                            borderColor: '#dddfeb',
                                            borderWidth: 1,
                                            xPadding: 15,
                                            yPadding: 15,
                                            displayColors: false,
                                            caretPadding: 10,
                                            callbacks: {
                                                label: function(tooltipItem, chart) {
                                                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                                                    return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                                                }
                                            }
                                        },
                                    }
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
<script>
    jQuery({
        ambientes_registrados:  0,
        ambientes_programados:  0,
        usuarios_registrados:   0
    }).animate({
        ambientes_registrados:  '{{$cantidad_ambientes_registrados}}',
        ambientes_programados:  '{{$cantidad_ambientes_programados}}',
        usuarios_registrados:   '{{$cantidad_usuarios_registrados}}'
    }, 
    {
        duration: 2000,
        easing:'swing', 
        step: function() { 
            $('.contador-ambientes-registrados').text(Math.ceil(this.ambientes_registrados));
            $('.contador-ambientes-programados').text(Math.ceil(this.ambientes_programados));
            $('.contador-usuarios-registrados').text(Math.ceil(this.usuarios_registrados));
        }
    });
</script>
@endsection
