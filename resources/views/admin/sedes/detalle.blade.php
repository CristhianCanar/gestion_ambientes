@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('centros_formacion.sedes.index',$id_centro)}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Detalles Sede</h3>
                </div>
                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label for="nombre" class="form-label">Sede</label>
                        <h6>{{$sede->sede}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label for="direccion" class="form-label">Dirección</label>
                        <h6>{{$sede->direccion}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label for="telefono" class="form-label">Teléfono</label>
                        <h6>{{$sede->telefono}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
