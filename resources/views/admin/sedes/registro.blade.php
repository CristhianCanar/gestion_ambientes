@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('centros_formacion.sedes.index',$id_centro ?? '')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Registro Sede</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('centros_formacion.sedes.store',$id_centro ?? '')}}" novalidate>
                    @csrf
                    <input type="hidden" id="id_centro_formacion" name="id_centro_formacion" value="{{$id_centro ?? ''}}">
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="sede"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Sede</label>
                            <input type="text" class="form-control" name="sede" id="sede" placeholder="Ej: Teleinformatica Norte" maxlength="50" autofocus required>
                            <div class="invalid-feedback">
                                Escriba el Nombre de la Sede
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="direccion"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Dirección</label>
                            <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Ej: Cll 50A #2-14" maxlength="40" required>
                            <div class="invalid-feedback">
                                Escriba la Dirección de la Sede
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="telefono"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Teléfono</label>
                            <input type="number" class="form-control" name="telefono" id="telefono" placeholder="Ej: 0323434543" min="1" max="9999999999" required>
                            <div class="invalid-feedback">
                                Escriba el Teléfono de la Sede
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button type="submit" class="btn btn-naranja w-100">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
