@extends('admin.layouts.app')
@section('content')
<div class="row align-items-center mb-3">
    <div class="col-1 d-none d-lg-flex text-left">
        <a href="{{route('centros_formacion.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-8 offset-2 offset-lg-1">
        <h3 class="card-title">Gestionar Sedes</h3>
    </div>
    <div class="col-1 offset-1">
        <a href="{{ route('centros_formacion.sedes.create',$id_centro) }}" type="button" class="btn btn-lg float-right">
            <i class="fas fa-plus-circle fa-2x" data-toggle="tooltip" title="Registrar Sede" id="icono-v"></i>
        </a>
    </div>
</div>
@if (count($sedes) > 0)
    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th>N°</th>
                                    <th>Sede</th>
                                    <th>Dirección</th>
                                    <th>Teléfono</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($sedes as $sede)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td class="text-truncate" style="max-width: 200px;">{{$sede->sede}}</td>
                                        <td class="text-truncate" style="max-width: 200px;">{{$sede->direccion}}</td>
                                        <td class="text-truncate" style="max-width: 150px;">{{$sede->telefono}}</td>
                                        <td class="text-center">
                                            <div class="row justify-content-center">
                                                <div class="col-5 col-lg-3">
                                                    <a href="{{ route('centros_formacion.sedes.show', [$sede->id_centro_formacion, $sede->id_sede]) }}" type="button">
                                                        <i class="fas fa-eye" data-toggle="tooltip" title="Detalles Sede"></i>
                                                    </a>
                                                </div>
                                                <div class="col-5 col-lg-3">
                                                    <a href="{{ route('centros_formacion.sedes.edit', [$sede->id_centro_formacion, $sede->id_sede]) }}" type="button">
                                                        <i class="fas fa-edit" data-toggle="tooltip" title="Actualizar Sede"></i>
                                                    </a>
                                                </div>
                                                <!--
                                                <div class="col-4 col-sm-3 col-lg-2">
                                                    <a href="#" type="button" class="align-middle">
                                                        <i class="fas fa-trash-alt" id="icono-v" data-toggle="tooltip" title="Eliminar Sede"></i>
                                                    </a>       
                                                </div>
                                                -->
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">No hay sedes registradas</h4>
@endif
@endsection
