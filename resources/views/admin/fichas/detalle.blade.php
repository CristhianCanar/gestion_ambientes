@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('programas_formacion.fichas.index',$id_programa)}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Detalles Ficha de Formación</h3>
                </div>
                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label for="numero_ficha" class="form-label">Número de Ficha</label>
                        <h6>{{$ficha->numero_ficha}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label for="fecha_inicio" class="form-label">Fecha de Apertura</label>
                        <h6>{{$ficha->fecha_inicio}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label for="fecha_fin" class="form-label">Fecha de Finalización</label>
                        <h6>{{$ficha->fecha_fin}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label for="jornada" class="form-label">Jornada</label>
                        <h6>{{$ficha->jornada->jornada}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label for="cupo_aprendices" class="form-label">Cupo de Aprendices</label>
                        <h6>{{$ficha->cupo_aprendices}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
