@extends('admin.layouts.app')
@section('content')
<div class="row align-items-center mb-3">
    <div class="col-1 d-none d-lg-flex text-left">
        <a href="{{route('programas_formacion.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-8 offset-2 offset-lg-1">
        <h3 class="card-title">Gestionar Fichas de Formación</h3>
    </div>
    <div class="col-1 offset-1">
        <a href="{{ route('programas_formacion.fichas.create', $id_programa) }}" class="btn btn-lg float-right">
            <i class="fas fa-plus-circle fa-2x" data-toggle="tooltip" title="Registrar Ficha de Formación" id="icono-v"></i>
        </a>
    </div>
</div>
@if (count($fichas) > 0)
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <!-- Tabla de registros -->
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th>N°</th>
                                    <th class="text-truncate" style="max-width: 150px;">Número Ficha</th>
                                    <th class="text-truncate" style="max-width: 150px;">Fecha Inicio</th>
                                    <th class="text-truncate" style="max-width: 150px;">Fecha Finalización</th>
                                    <th>Jornada</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($fichas as $ficha)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$ficha->numero_ficha}}</td>
                                        <td>{{$ficha->fecha_inicio}}</td>
                                        <td>{{$ficha->fecha_fin}}</td>
                                        <td>{{$ficha->jornada->jornada}}</td>
                                        <td class="text-center">
                                            <div class="row justify-content-center">
                                                <div class="col-5 col-lg-3">
                                                    <a href="{{ route('programas_formacion.fichas.show', [$ficha->programa_id, $ficha->id_ficha]) }}" type="button">
                                                        <i class="fas fa-eye" data-toggle="tooltip" title="Detalles Ficha"></i>
                                                    </a>
                                                </div>
                                                <div class="col-5 col-lg-3">
                                                    <a href="{{ route('programas_formacion.fichas.edit', [$ficha->programa_id, $ficha->id_ficha]) }}" type="button">
                                                        <i class="fas fa-edit" data-toggle="tooltip" title="Actualizar Ficha"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $fichas ->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">No hay fichas de formación registradas</h4>
@endif
@endsection
