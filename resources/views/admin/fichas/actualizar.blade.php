@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('programas_formacion.fichas.index',$id_programa)}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Actualizar Ficha de Formación</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('programas_formacion.fichas.update',[$ficha->programa_id, $ficha->id_ficha])}}" novalidate>
                    @csrf
                    @method('PATCH')
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="numero_ficha"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Número de Ficha</label>
                            <input type="number" class="form-control" name="numero_ficha" id="numero_ficha" value="{{$ficha->numero_ficha}}" min="1" max="9999999" placeholder="Ej: 1750305" required>
                            <div class="invalid-feedback">
                                Escriba el Número de Ficha
                            </div>
                        </div>
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="fecha_inicio"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Fecha de Apertura</label>
                            <input type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" value="{{$ficha->fecha_inicio}}" required>
                            <div class="invalid-feedback">
                                Seleccione la Fecha de Apertura
                            </div>
                        </div>
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="fecha_fin"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Fecha de Finalización</label>
                            <input type="date" class="form-control" name="fecha_fin" id="fecha_fin" value="{{$ficha->fecha_fin}}" required>
                            <div class="invalid-feedback">
                                Seleccione la Fecha de Finalización
                            </div>
                            <div class="invalid-input" id="invalid-date">
                            </div>
                        </div>
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="jornada_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Jornada</label>
                            <select id="jornada_id" name="jornada_id" class="custom-select" required>
                                <option value="{{$ficha->jornada_id}}">{{$ficha->jornada->jornada}}</option>
                                @foreach($jornadas as $jornada)
                                    <option value="{{$jornada->id_jornada}}">{{$jornada->jornada}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una Jornada
                            </div>
                        </div>
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="cupo_aprendices"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Cupo de Aprendices</label>
                            <input type="number" class="form-control" name="cupo_aprendices" id="cupo_aprendices" value="{{$ficha->cupo_aprendices}}" min="1" max="99" placeholder="Ej: 30" required>
                            <div class="invalid-feedback">
                                Escriba el Cupo de Aprendices
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn btn-naranja w-100" type="submit">Actualizar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/fichas_formacion.js')}}"></script>
@endsection
