@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('programaciones_ambiente.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Detalles Programación de Ambiente</h3>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-5 col-lg-4">
                        <label class="form-label" for="nombre_programa">Fecha Inicio</label>
                        <h6>{{date('d-m-Y', strtotime($programacion_ambiente->fecha_inicio))}}</h6>
                    </div>

                    <div class="form-group col-5 col-lg-4">
                        <label class="form-label" for="version_programa">Fecha Fin</label>
                        <h6>{{date('d-m-Y', strtotime($programacion_ambiente->fecha_fin))}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="id_nivel_programa">Días Programados</label>
                        <h6>{{$programacion_ambiente->lunes}}</h6> <h6>{{$programacion_ambiente->martes}}</h6>
                        <h6>{{$programacion_ambiente->miercoles}}</h6> <h6>{{$programacion_ambiente->jueves}}</h6>
                        <h6>{{$programacion_ambiente->viernes}}</h6> <h6>{{$programacion_ambiente->sabado}}</h6>
                    </div>
                </div>
                
                <div class="form-row justify-content-center">
                    <div class="form-group col-5 col-lg-4">
                        <label class="form-label" for="duracion_total">Hora Inicio</label>
                        <h6>{{date('h:i a', strtotime($programacion_ambiente->hora_inicio))}}</h6>
                    </div>

                    <div class="form-group col-5 col-lg-4">
                        <label class="form-label" for="duracion_total">Hora Fin</label>
                        <h6>{{date('h:i a', strtotime($programacion_ambiente->hora_fin))}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="idredconocimiento">Ambiente de Aprendizaje</label>
                        <h6>{{$programacion_ambiente->ambiente->nombre_ambiente}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="id_linea_tematica">Programa de Formación</label>
                        <h6>{{$programacion_ambiente->ficha->programa_formacion->nombre_programa}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="id_linea_tematica">Número de Ficha</label>
                        <h6>{{$programacion_ambiente->ficha->numero_ficha}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="id_nivel_programa">Actividad de Aprendizaje</label>
                        <h6>{{$programacion_ambiente->actividad_aprendizaje}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="duracion_total">Competencia Laboral o Transversal</label>
                        <h6>{{$programacion_resultados[0]->resultado_aprendizaje->competencia_laboral_programa->competencia_laboral->competencia_laboral}}</h6>  
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="duracion_total">Resultados de Aprendizaje</label>
                        @foreach ($programacion_resultados as $programacion)
                            <h6 class="mb-2">-{{$programacion->resultado_aprendizaje->resultado_aprendizaje}}</h6>   
                        @endforeach
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="id_nivel_programa">Instructor</label>
                        @foreach ($programacion_instructores as $instructor)
                            <h6 class="mb-2">{{$instructor->usuario->nombres}} {{$instructor->usuario->apellidos}}</h6>   
                        @endforeach
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="id_nivel_programa">Horas Programadas</label>
                        <h6>{{$programacion_ambiente->horas_programadas}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
