@extends('admin.layouts.app')
@section('content')
    <div class="row mb-4">
        <div class="col-12 col-lg-6 offset-lg-3">
            <h3 class="card-title">Gestionar Programación de Ambientes</h3>
        </div>
        <div class="col-12 col-sm-10 col-lg-3 offset-sm-1 offset-lg-0 mt-4 mt-lg-0">
            <form action="{{route('buscar.programacion_ambiente')}}" method="POST">
                @csrf
                <div class="input-group">
                    <input type="text" name="buscar_programacion_ambiente" class="form-control" placeholder="Buscar Programacion Ambiente">
                    <div class="input-group-append">
                        <div class="input-group-text pt-0 pb-0">
                            <button class="btn p-0" type="submit">
                                <i class="fas fa-search color-naranja"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if (count($programacion_ambientes) > 0)
        <div class="row justify-content-center">
            <div class="col-12 col-sm-10 col-lg-12">
                <div class="card card-outline card-sena elevation-2">
                    <div class="card-body">
                        <!-- Tabla de registros -->
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="table-header">
                                    <tr>
                                        <th>N°</th>
                                        <th class="text-truncate" style="max-width: 200px;">Ambiente de Aprendizaje</th>
                                        <th class="text-truncate" style="max-width: 150px;">Número de Ficha</th>
                                        <th>Actividad de Aprendizaje</th>
                                        <th class="text-truncate" style="max-width: 150px;">Fecha Inicio</th>
                                        <th>Fecha Fin</th>
                                        <th class="text-center">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody class="table-body">
                                    @foreach ($programacion_ambientes as $programacion_ambiente)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td class="text-truncate" style="max-width: 300px;">{{$programacion_ambiente->ambiente->nombre_ambiente}}</td>
                                            <td class="text-truncate" style="max-width: 100px;">{{$programacion_ambiente->ficha->numero_ficha}}</td>
                                            <td class="text-truncate" style="max-width: 300px;">{{$programacion_ambiente->actividad_aprendizaje}}</td>
                                            <td class="text-truncate" style="max-width: 100px;">{{date('d-m-Y', strtotime($programacion_ambiente->fecha_inicio))}}</td>
                                            <td class="text-truncate" style="max-width: 100px;">{{date('d-m-Y', strtotime($programacion_ambiente->fecha_fin))}}</td>
                                            <td class="text-center">
                                                <div class="row justify-content-center">
                                                    <div class="col-5 col-lg-4">
                                                        <a href="{{ route('programaciones_ambiente.show', $programacion_ambiente->id_programacion_ambiente) }}" type="button">
                                                            <i class="fas fa-eye" data-toggle="tooltip" title="Detalles Programación"></i>
                                                        </a>
                                                    </div>
                                                    <div class="col-5 col-lg-4">
                                                        <a href="{{ route('programaciones_ambiente.edit', $programacion_ambiente->id_programacion_ambiente) }}" type="button">
                                                            <i class="fas fa-edit" data-toggle="tooltip" title="Actualizar Programación"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="float-right">
                                {{ $programacion_ambientes ->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <h4 class="mt-5" style="text-align: center">No hay programaciones de ambiente registradas</h4>
    @endif
@endsection
