<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Programacion de Ambientes</title>
    <style>
        .table {
          font-family: Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        .table td, .table th {
          border: 1px solid #ddd;
          padding: 8px;
        }
        
        .table tr:hover {
            background-color: #ddd;
        }
        
        .table th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #FC7323;
          color: white;
        }
        h2 {
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
            margin-bottom: 5%;
        }
    </style>
</head>
<body>
    @if ($tipo_programacion == 1)
        <h2>Programación Ambiente {{$programaciones_ambiente[0]->nombre_ambiente}}</h2>
    @elseif ($tipo_programacion == 2)
        <h2>Programación Número de Ficha {{$programaciones_ambiente[0]->numero_ficha}}</h2>
    @else
        <h2>Programación Instructor {{$programaciones_ambiente[0]->nombres}} {{$programaciones_ambiente[0]->apellidos}}</h2>
    @endif
    <table class="table">
        <tr>
            <th>Fecha Inicio</th>
            <th>Fecha Fin</th>
            <th>Hora Inicio</th>
            <th>Hora Fin</th>
            <th>Ambiente de Aprendizaje</th>
            <th>Número de Ficha</th>
            <th>Instructor</th>
            <th>Horas</th>
        </tr>
       
        @foreach($programaciones_ambiente as $programacion)
            <tr>
                <td>{{date('d-m-Y', strtotime($programacion->fecha_inicio))}}</td>
                <td>{{date('d-m-Y', strtotime($programacion->fecha_fin))}}</td>
                <td>{{date('h:i a', strtotime($programacion->hora_inicio))}}</td>
                <td>{{date('h:i a', strtotime($programacion->hora_fin))}}</td>
                <td>{{$programacion->nombre_ambiente}}</td>
                <td>{{$programacion->numero_ficha}}</td>
                <td>{{$programacion->nombres}} {{$programacion->apellidos}}</td>
                <td>{{$programacion->horas_programadas}}</td>
            </tr>
        @endforeach
    </table>
</body>
</html>