@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-sm-10 col-lg-8">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Registro Programación Ambiente</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('programaciones_ambiente.store')}}" novalidate>
                    @csrf
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="fecha_inicio"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Fecha Inicio</label>
                            <input type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" autofocus required>
                            <div class="invalid-feedback">
                                Seleccione una Fecha Inicio
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="fecha_fin"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Fecha Fin</label>
                            <input type="date" class="form-control" name="fecha_fin" id="fecha_fin" required>
                            <div class="invalid-input" id="invalid-date">
                            </div>
                            <div class="invalid-feedback">
                                Seleccione una Fecha Fin
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label d-block"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Días a Programar</label>
                            <div class="d-inline">
                                <label>Dom</label>
                            </div>
                            <div class="icheck-orange d-inline">
                                <input type="checkbox" name="domingo" id="domingo" value="domingo">
                                <label for="domingo"></label>
                            </div>
                            
                            <div class="d-inline">
                                <label>Lun</label>
                            </div>
                            <div class="icheck-orange d-inline">
                                <input type="checkbox" name="lunes" id="lunes" value="lunes">
                                <label for="lunes"></label>
                            </div>
               
                            <div class="d-inline">
                                <label class="ml-2">Mar</label>
                            </div>
                            <div class="icheck-orange d-inline">
                                <input type="checkbox" name="martes" id="martes" value="martes">
                                <label for="martes"></label>
                            </div>
               
                            <div class="d-inline">
                                <label class="ml-2">Mié</label>
                            </div>
                            <div class="icheck-orange d-inline">
                                <input type="checkbox" name="miercoles" id="miercoles" value="miércoles">
                                <label for="miercoles"></label>
                            </div>
                        
                            <div class="d-inline">
                                <label class="ml-2">Jue</label>
                            </div>
                            <div class="icheck-orange d-inline">
                                <input type="checkbox" name="jueves" id="jueves" value="jueves">
                                <label for="jueves"></label>
                            </div>
                        
                            <div class="d-inline">
                                <label class="ml-2">Vie</label>
                            </div>
                            <div class="icheck-orange d-inline">
                                <input type="checkbox" name="viernes" id="viernes" value="viernes">
                                <label for="viernes"></label>
                            </div>
                        
                            <div class="d-inline">
                                <label class="ml-2">Sáb</label>
                            </div>
                            <div class="icheck-orange d-inline">
                                <input type="checkbox" name="sabado" id="sabado" value="sábado">
                                <label for="sabado"></label>
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <div class="row">
                                <div class="form-group col-12 col-lg-6">
                                    <label class="form-label" for="hora_inicio"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Hora Inicio</label>
                                    <input type="time" class="form-control" name="hora_inicio" id="hora_inicio" required> 
                                    <div class="invalid-feedback">
                                        Seleccione una Hora Inicio
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <label class="form-label" for="hora_fin"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Hora Fin</label>
                                    <input type="time" class="form-control" name="hora_fin" id="hora_fin" required>
                                    <div class="invalid-input" id="invalid-hour">
                                    </div>
                                    <div class="invalid-feedback">
                                        Seleccione una Hora Fin
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8"> 
                            <label class="form-label" for="ambiente_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Ambiente de Aprendizaje</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="ambiente_id" id="ambiente_id" required style="width: 100%;" data-select2-id="ambiente_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled>Seleccione Ambiente de Aprendizaje</option>
                                @foreach ($ambientes as $ambiente)
                                    <option value="{{$ambiente->id_ambiente}}">{{$ambiente->nombre_ambiente}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Ambiente de Aprendizaje
                            </div>
                        </div>
    
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="programa_formacion_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Programa de Formación</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="programa_formacion_id" id="programa_formacion_id" required src="{{route('programas_ambiente.getprogramas','#')}}" style="width: 100%;" data-select2-id="programa_formacion_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled></option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Programa de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="ficha_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Número de Ficha</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="ficha_id" id="ficha_id" required src="{{route('fichas_programa.getfichas','#')}}" style="width: 100%;" data-select2-id="ficha_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled></option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Número de Ficha
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="actividad_aprendizaje"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Actividad de Aprendizaje</label>
                            <textarea class="form-control" name="actividad_aprendizaje" id="actividad_aprendizaje" rows="3" 
                            placeholder="Ej: Realizar el levantamiento de los requerimientos funcionales y no funcionales para el desarrollo del proyecto"
                            maxlength="200" required></textarea>
                            <div class="invalid-feedback">
                                Escriba la Actividad de Aprendizaje
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="competencia_laboral_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Competencia Laboral o Transversal</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="competencia_laboral_id" id="competencia_laboral_id" required src="{{route('competencias_programa.getcompetencias','#')}}" style="width: 100%;" data-select2-id="competencia_laboral_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled></option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una Competencia Laboral o Transversal
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="resultado_aprendizaje_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Resultados de Aprendizaje</label>
                            <div class="select2-orange">
                                <select class="select2" name="resultado_aprendizaje_id[]" id="resultado_aprendizaje_id" multiple="multiple" required src="{{route('resultados_programa.getresultados',['#','*'])}}" style="width: 100%;" data-dropdown-css-class="select2-orange">
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione un Resultado de Aprendizaje
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="instructor_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Instructor</label>
                            <div class="select2-orange">
                                <select class="select2" name="instructor_id[]" id="instructor_id" multiple="multiple" data-placeholder="Seleccione Instructor" required style="width: 100%;" data-dropdown-css-class="select2-orange">
                                @foreach ($usuarios as $usuario)
                                    <option value="{{$usuario->id}}">{{$usuario->nombres}} {{$usuario->apellidos}}</option>
                                @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione un Instructor
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn w-100 btn-naranja" type="submit">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/programacion_ambiente.js')}}"></script>
@endsection
