@extends('admin.layouts.app')
@section('content')
<!-- modal detalles programacion de ambiente -->
<div class="modal fade" id="detalle-programacion" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="registro-sede-titulo" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-10 offset-1">
                    <h5 class="modal-title text-center">Detalles Programación de Ambiente</h5>
                </div>
                <div class="col-1">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrar-modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class="form-row justify-content-center">
                    <div class="form-group col-5 col-lg-4">
                        <label class="form-label" for="fecha_inicio">Fecha Inicio</label>
                        <h6 id="fecha_inicio"></h6>
                    </div>

                    <div class="form-group col-5 col-lg-4">
                        <label class="form-label" for="fecha_fin">Fecha Fin</label>
                        <h6 id="fecha_fin"></h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="dias">Días Programados</label>
                        <h6 id="dias"></h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-5 col-lg-4">
                        <label class="form-label" for="hora_inicio">Hora Inicio</label>
                        <h6 id="hora_inicio"></h6>
                    </div>

                    <div class="form-group col-5 col-lg-4">
                        <label class="form-label" for="hora_fin">Hora Fin</label>
                        <h6 id="hora_fin"></h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="ambiente_aprendizaje">Ambiente de Aprendizaje</label>
                        <h6 id="ambiente_aprendizaje"></h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="ambiente_aprendizaje">Programa de Formación</label>
                        <h6 id="programa_formacion"></h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="numero_ficha">Número de Ficha</label>
                        <h6 id="numero_ficha"></h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="actividad_aprendizaje">Actividad de Aprendizaje</label>
                        <h6 id="actividad_aprendizaje"></h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="competencia_laboral">Competencia Laboral o Transversal</label>
                        <h6 class="mb-2" id="competencia_laboral"></h6>  
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="resultado_aprendizaje">Resultados de Aprendizaje</label>
                        <div id="resultados_aprendizaje">
                        </div>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="resultado_aprendizaje">Instructor</label>
                        <div id="instructores">
                        </div>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="resultado_aprendizaje">Horas Programadas</label>
                        <div id="horas_programadas">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendario');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            /* Tipo Calendario por Defecto */
            initialView: 'dayGridMonth',

            /* Idioma */
            locale: 'es',

            /* Estilo Calendario */
            themeSystem: 'bootstrap',

            /* Boton Ambiente */
            customButtons: {
                filtro: {
                    text: '{{substr($filtro_programacion->nombre, 0, 30)}}'
                },
                reporte: {
                    bootstrapFontAwesome: 'fas fa-file-download',
                    click: function() {
                        location.href = "{{ route('programaciones_ambiente.exportar', ['id' => $filtro_programacion->id, 'filtro' => $id_filtro]) }}"
                    }
                }
            },
            /* Encabezado Navegacion */
            headerToolbar: {
                left: 'prev,next today filtro',
                center: 'title',
                right: 'reporte dayGridMonth,timeGridWeek,timeGridDay'
            },

            /* Formato Eventos de Tiempo */
            eventTimeFormat: {
                hour: 'numeric',
                minute: '2-digit',
                meridiem: 'short',
                hour12: true
            },

            /* Persistencia de Eventos */
            lazyFetching: false,

            /* Fuente de Eventos */
            eventSources: [
                {
                    url: "{{route('programaciones_ambiente.festivos')}}"
                },
                
                {
                    url: "{{route('programaciones_ambiente.eventos')}}",
                    extraParams: {
                        id: '{{$filtro_programacion->id}}',
                        filtro: '{{$id_filtro}}'
                    }
                }
            ],

            /*eventRender: function(info) {
                var tooltip = new Tooltip(info.el, {
                    title: info.event.extendedProps.ambiente,
                    placement: 'top',
                    trigger: 'hover',
                    container: 'body'
                });
            },*/

            /* Botones de Navegacion Superior */
            buttonText: {
                today:    'Hoy',
                month:    'Mes',
                week:     'Semana',
                day:      'Día',
                list:     'Agenda'
            },

            /* Numero Maximo de Eventos por Dia */
            dayMaxEventRows: true,
            views: {
                dayGridMonth: {
                    dayMaxEventRows: 2
                }
            },

            /* Data para Modal Detalles Programacion de Ambiente */
            eventClick: function(info) {
                $('#detalle-programacion').modal('show');
                $('#fecha_inicio').text(info.event.extendedProps.fecha_inicio);
                $('#fecha_fin').text(info.event.extendedProps.fecha_fin);
                $('#dias').text(info.event.extendedProps.dias);
                $('#hora_inicio').text(info.event.extendedProps.hora_inicio);
                $('#hora_fin').text(info.event.extendedProps.hora_fin);
                $('#ambiente_aprendizaje').text(info.event.extendedProps.ambiente);
                $('#programa_formacion').text(info.event.extendedProps.programa_formacion);
                $('#numero_ficha').text((info.event.title).substr(7,15));
                $('#actividad_aprendizaje').text(info.event.extendedProps.actividad_aprendizaje);
                $('#competencia_laboral').text(info.event.extendedProps.competencia_laboral);
                                
                let rap = info.event.extendedProps.resultados_aprendizaje;
                for (let r=0; r<rap.length; r++) {
                    $('#resultados_aprendizaje').prepend($("<h6></h6>").text("-"+rap[r].resultado_aprendizaje)); 
                }

                let instructores = info.event.extendedProps.instructor;
                for (let i=0; i<instructores.length; i++) {
                    $('#instructores').prepend($("<h6></h6>").text(instructores[i].nombres + ' ' + instructores[i].apellidos)); 
                }

                $('#horas_programadas').text(info.event.extendedProps.horas_programadas);

                $("#cerrar-modal").click(function () {
                    $("#resultados_aprendizaje").empty();
                    $("#instructores").empty();
                });
            }
            
        });
        calendar.render();
    });
</script>

<div id='calendario'>
</div>
<script>
    $(document).ready(function(){
        $(".fc-reporte-button").attr({ "data-toggle": "tooltip",  "title": "Descargar Reporte de Programación" }).tooltip();
    });
</script>
@endsection
