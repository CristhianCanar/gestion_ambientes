@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-sm-10 col-lg-8">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Visualizar Programacion</h3>
                </div>
                <form class="needs-validation" method="POST" action="{{route('programaciones_ambiente.calendario')}}" id="form-filtro" novalidate>
                    @csrf
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8"> 
                            <label class="form-label" for="red_conocimiento_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Red de Conocimiento</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="red_conocimiento_id" id="red_conocimiento_id" required style="width: 100%;" data-select2-id="red_conocimiento_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled>Seleccione Red de Conocimiento</option>
                                @foreach ($redes_conocimiento as $red)
                                    <option value="{{$red->idredconocimiento}}">{{$red->redconocimiento}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una Red de Conocimiento
                            </div>
                        </div>
                        
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="linea_tematica_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Linea Tecnologica</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="linea_tematica_id" id="linea_tematica_id" required src="{{route('redes.getlinea','#')}}" style="width: 100%;" data-select2-id="linea_tematica_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled></option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una Linea Tecnologica
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="programa_formacion_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Programa de Formación</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="programa_formacion_id" id="programa_formacion_id" required src="{{route('redes.getprograma','#')}}" style="width: 100%;" data-select2-id="programa_formacion_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled></option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Programa de Formación
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="ambiente_id">Ambiente de Aprendizaje</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="ambiente_id" id="ambiente_id" src="{{route('ambientes_programa.getambientes','#')}}" style="width: 100%;" data-select2-id="ambiente_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled></option>
                            </select>
                            <div class="invalid-input" id="validacion-filtro">
                            </div>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="ficha_id">Número de Ficha</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="ficha_id" id="ficha_id" src="{{route('fichas_programa.getfichas','#')}}" style="width: 100%;" data-select2-id="ficha_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled></option>
                            </select>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="instructor_id">Instructor</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="instructor_id" id="instructor_id" src="{{route('redes.getprograma','#')}}" style="width: 100%;" data-select2-id="instructor_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled>Seleccione Instructor</option>
                                @foreach ($instructores_programados as $instructor_programado)
                                    <option value="{{$instructor_programado->id}}">{{$instructor_programado->nombres}} {{$instructor_programado->apellidos}}</option>
                                @endforeach                                
                            </select>
                        </div>

                        <div class="form-group col-10 col-lg-8 mt-3 mb-4">
                            <button class="btn w-100 btn-naranja" type="submit">Visualizar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/filtro_programacion.js')}}"></script>
@endsection
