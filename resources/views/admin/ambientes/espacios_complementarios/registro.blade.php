@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('espacios_complementarios.gestionar_espacios_complementarios',$id_ambiente)}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mt-2 mb-4 text-center">
                        <h3 class="card-title">Asociar Espacio Complementario</h3>
                    </div>
                </div>
                <form class="needs-validation" method="POST" action="{{route('espacios_complementarios.store')}}" novalidate>
                    @csrf
                    <input type="hidden" class="form-control" id="id_ambiente" name="id_ambiente" value="{{$id_ambiente}}">

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="espacio"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Espacio Complementario</label>
                            <input type="text" class="form-control" name="espacio" id="espacio" placeholder="Ej: Bodega" maxlength="150" autofocus required>
                            <div class="invalid-feedback">
                                Escriba el Nombre del Espacio Complementario
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="area"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Area Aproximada (m²)</label>
                            <input type="number" step="any" class="form-control" name="area" id="area" placeholder="Ej: 80" min="1" max="1000" required>
                            <div class="invalid-feedback">
                                Escriba la Area del Espacio Complementario
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center mt-3 mb-4">
                        <div class="form-group col-10 col-lg-8">
                            <button type="submit" class="btn btn-naranja w-100">Asociar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
