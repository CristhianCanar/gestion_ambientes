@extends('admin.layouts.app')
@section('content')
<div class="row align-items-center mb-3">
    <div class="col-1 d-none d-lg-flex text-left">
        <a href="{{route('ambientes.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-8 offset-2 offset-lg-1">
        <h3 class="card-title">Gestionar Espacios Complementarios del Ambiente</h3>
    </div>
    <div class="col-1 offset-1">
        <a href="{{ route('espacios_complementarios.create_espacios_complementarios',$id_ambiente) }}" type="button" class="btn btn-lg float-right">
            <i class="fas fa-plus-circle fa-2x" data-toggle="tooltip" title="Asociar Espacio Complementario" id="icono-v"></i>
        </a>
    </div>
</div>
@if (count($espacios_complementarios) > 0)
    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th>N°</th>
                                    <th class="text-truncate" style="max-width: 200px;">Espacio Complementario</th>
                                    <th class="text-truncate" style="max-width: 100px;">Area (m²)</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($espacios_complementarios as $espacio_complementario)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td class="text-truncate" style="max-width: 600px;">{{$espacio_complementario->espacio}}</td>
                                        <td>{{$espacio_complementario->area}}</td>
                                        <td class="text-center">
                                            <div class="row justify-content-center">
                                                <div class="col-5 col-lg-2">
                                                    <a  href="{{route('espacios_complementarios.edit', $espacio_complementario->id_espacio)}}">
                                                        <i class="fas fa-edit" data-toggle="tooltip" title="Actualizar Espacio"></i>
                                                    </a>
                                                </div>
                                                <div class="col-5 col-lg-2">
                                                    <form action="{{ route('espacios_complementarios.delete_espacios_complementarios',[ 'id_espacio'=>$espacio_complementario->id_espacio, 'id_ambiente' => $id_ambiente]) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button  type="submit" class="btn p-0"
                                                            onclick="return confirm('¿Seguro que desea Eliminar el Espacio Complementario del Ambiente?')">
                                                            <i class="fas fa-trash-alt" id="icono-v" data-toggle="tooltip" title="Eliminar Espacio"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $espacios_complementarios->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">No hay espacios complementarios asociados al ambiente</h4>
@endif
@endsection
