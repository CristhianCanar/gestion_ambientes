@extends('admin.layouts.app')
@section('content')
<div class="row align-items-center mb-3">
    <div class="col-1 d-none d-lg-flex text-left">
        <a href="{{route('ambientes.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-8 offset-2 offset-lg-1">
        <h3 class="card-title">Gestionar Programas de Formación del Ambiente</h3>
    </div>
    <div class="col-1 offset-1">
        <a href="{{route('programas_formacion_ambiente.create_programas_ambiente', $id_ambiente)}}" type="button" class="btn btn-lg float-right">
            <i class="fas fa-plus-circle fa-2x" data-toggle="tooltip" title="Asociar Programa de Formación" id="icono-v"></i>
        </a>
    </div>
</div>
@if (count($programas_formacion) > 0)
    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <!--Nueva linea-->
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th>N°</th>
                                    <th>Código</th>
                                    <th>Programa de Formación</th>
                                    <th class="text-truncate" style="max-width: 200px;">Nivel de Formación</th>
                                    <th class="text-center">Versión</th>
                                    <th>Duración</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($programas_formacion as $programa_formacion)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$programa_formacion->programa_formacion->codigo_programa}}</td>
                                        <td class="text-truncate" style="max-width: 300px;">{{$programa_formacion->programa_formacion->nombre_programa}}</td>
                                        <td class="text-truncate" style="max-width: 200px;">{{$programa_formacion->programa_formacion->nivel_programa->nivel_programa}}</td>
                                        <td class="text-center">{{$programa_formacion->programa_formacion->version_programa}}</td>
                                        <td class="text-truncate" style="max-width: 200px;">{{$programa_formacion->programa_formacion->duracion_total}} meses</td>
                                        <td class="text-center">
                                            <div class="row justify-content-center">
                                                <div class="col-5 col-lg-3">
                                                    <a href="{{route('programas_formacion_ambiente.show',$programa_formacion->programa_formacion->id_programa)}}" type="button">
                                                        <i class="fas fa-eye" data-toggle="tooltip" title="Detalles Programa"></i>
                                                    </a>
                                                </div>
                                                <div class="col-5 col-lg-3">
                                                    <form action="{{ route('programas_formacion_ambiente.delete_programas_ambiente',[ 'id_programa'=>$programa_formacion->programa_formacion->id_programa, 'id_ambiente' => $id_ambiente]) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn p-0" onclick="return confirm('¿Seguro que desea Eliminar el Programa del Ambiente?')">
                                                            <i class="fas fa-trash-alt" id="icono-v" data-toggle="tooltip" title="Eliminar Programa"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $programas_formacion->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">No hay programas de formación asociados al ambiente</h4>
@endif
@endsection
