@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{ route('programas_formacion_ambiente.gestionar_programas_ambiente',$id_ambiente) }}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mt-2 mb-4 text-center">
                        <h3 class="card-title">Asociar Programa de Formación</h3>
                    </div>
                </div>
                <form class="needs-validation" method="POST" action="{{route('programas_formacion_ambiente.store',$id_ambiente)}}" novalidate>
                    @csrf
                    <input type="hidden" class="form-control" id="id_ambiente" name="id_ambiente" value="{{$id_ambiente}}">
                
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="idredconocimiento"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Red de Conocimiento</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="idredconocimiento" id="idredconocimiento" required autofocus style="width: 100%;" data-select2-id="idredconocimiento" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled>Seleccione Red de Conocimiento</option>
                                @foreach ($redes as $r)
                                    <option value="{{$r->idredconocimiento}}">{{$r->redconocimiento}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una Red de Conocimiento
                            </div>
                        </div>
                    </div>
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_linea_tematica"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Linea Tecnológica</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_linea_tematica" id="id_linea_tematica" required src="{{route('redes.getlinea','#')}}" style="width: 100%;" data-select2-id="id_linea_tematica" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                <option value="" selected disabled></option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una Linea Tecnológica
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_programa"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Programa de Formación</label>
                            <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_programa" id="id_programa" required src="{{route('redes.getprograma','#')}}" style="width: 100%;" data-select2-id="id_programa" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">          
                                <option value="" selected disabled></option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Programa de Formación
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center mt-3 mb-4">
                        <div class="form-group col-10 col-lg-8">
                            <button class="btn btn-naranja w-100" type="submit">Asociar</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/gestionar_ambiente.js')}}"></script>
@endsection
