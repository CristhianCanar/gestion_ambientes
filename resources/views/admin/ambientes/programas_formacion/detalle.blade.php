@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('programas_formacion_ambiente.gestionar_programas_ambiente',$programa_ambiente->id_ambiente)}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Detalles Programa de Formación</h3>
                </div>
                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="idredconocimiento">Red de Conocimiento</label>
                        <h6>{{$programa_ambiente->programa_formacion->redconocimiento->redconocimiento}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="id_linea_tematica">Linea Tecnológica</label>
                        <h6>{{$programa_ambiente->programa_formacion->linea_tematica->linea_tematica}}</h6>
                    </div>
                    
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="nombre_programa">Código del Programa</label>
                        <h6>{{$programa_ambiente->programa_formacion->codigo_programa}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="nombre_programa">Programa de Formación</label>
                        <h6>{{$programa_ambiente->programa_formacion->nombre_programa}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="id_nivel_programa">Nivel de Formación</label>
                        <h6>{{$programa_ambiente->programa_formacion->nivel_programa->nivel_programa}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="version_programa">Versión</label>
                        <h6>{{$programa_ambiente->programa_formacion->version_programa}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="duracion_programa">Duración Total</label>
                        <h6>{{$programa_ambiente->programa_formacion->duracion_total}} meses</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
