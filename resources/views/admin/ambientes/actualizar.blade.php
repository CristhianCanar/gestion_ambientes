@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('ambientes.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2 card-ambiente">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Actualizar Ambiente de Aprendizaje</h3>
                </div>
                <form id="form-actualizar" action="{{route('ambientes.update',$ambiente->id_ambiente)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <ul id="progressbar">
                        <li class="step" id="informacion_general"><strong>Información General</strong></li>
                        <li class="step" id="descripcion_fisica"><strong>Descripción Fisica</strong></li>
                        <li class="step" id="caracteristicas"><strong>Caracteristicas</strong></li>
                        <li class="step" id="condiciones_confort"><strong>Condiciones de Confort</strong></li>
                    </ul>

                    <!-- información general -->
                    <div class="tab">
                        <div class="form-row justify-content-center mt-2">
                            <!-- nombre ambiente -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="nombre_ambiente"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Ambiente</label>
                                <input type="text" class="form-control" name="nombre_ambiente" id="nombre_ambiente" value="{{$ambiente->nombre_ambiente}}" placeholder="Ej: Desarrollo de software 2" maxlength="50" required>
                                <span id="nombre_ambienteHelpBlock" class="form-text text-muted">Nombre dado al ambiente en el centro de formación</span>
                            </div>

                            <!-- cantidad de aprendices -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="cantidad_aprendices"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Número de Aprendices</label>
                                <input type="number" class="form-control" name="cantidad_aprendices" id="cantidad_aprendices" value="{{$ambiente->cantidad_aprendices}}" placeholder="Ej: 35" min="1" max="99" required>
                            </div>

                            <!-- area de cualificacion -->
                            <div class="form-group col-10 col-lg-8 offset-1">
                                <label class="form-label" for="area_cualificacion_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Area de Cualificación</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="area_cualificacion_id" id="area_cualificacion_id" required style="width: 100%;" data-select2-id="area_cualificacion_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="{{$ambiente->area_cualificacion->id_area_cualificacion}}" data-descripcion-area="{{$ambiente->area_cualificacion->descripcion_area_cualificacion}}" selected>{{$ambiente->area_cualificacion->area_cualificacion}}</option>                                    
                                    @foreach ($areas_cualificacion as $area)
                                        <option value="{{$area->id_area_cualificacion}}" data-descripcion-area="{{$area->descripcion_area_cualificacion}}">{{$area->area_cualificacion}}</option>   
                                    @endforeach
                                </select>
                            </div>

                            <!-- boton modal descripcion area de cualificacion -->
                            <div class="form-group col-1">
                                <a href="#" class="btn btn-lg pl-0 pt-3 mt-4" data-toggle="modal" data-target="#modal-area-cualificacion">
                                    <i class="fas fa-question-circle fa-lg color-naranja" data-toggle="tooltip" title="Haga click para visualizar la descripción del área de cualificación que selecciono"></i>
                                </a>
                            </div>

                            <!-- tipo de ambiente -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Tipo de Ambiente</label>
                                <div class="icheck-orange">
                                    <input type="radio" name="id_tipo_ambiente" id="id_tipo_ambiente_{{$ambiente->tipo_ambiente->id_tipo_ambiente}}" value="{{$ambiente->tipo_ambiente->id_tipo_ambiente}}" checked>
                                    <label for="id_tipo_ambiente_{{$ambiente->tipo_ambiente->id_tipo_ambiente}}">{{$ambiente->tipo_ambiente->tipo_ambiente}}
                                        <small>({{$ambiente->tipo_ambiente->detalle_tipo_ambiente}})</small>
                                    </label>
                                </div>
                                @foreach ($tipos_ambientes as $tipo_ambiente)
                                    <div class="icheck-orange">
                                        <input type="radio" name="id_tipo_ambiente" id="id_tipo_ambiente_{{$tipo_ambiente->id_tipo_ambiente}}" value="{{$tipo_ambiente->id_tipo_ambiente}}" required>
                                        <label for="id_tipo_ambiente_{{$tipo_ambiente->id_tipo_ambiente}}">{{$tipo_ambiente->tipo_ambiente}}
                                            <small>({{$tipo_ambiente->detalle_tipo_ambiente}})</small>
                                        </label>
                                    </div>
                                @endforeach
                            </div>

                            <!-- tipo de tenencia -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_tenencia"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Tenencia</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_tenencia" id="id_tenencia" required style="width: 100%;" data-select2-id="id_tenencia" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="{{$ambiente->tenencia->id_tenencia}}" selected>{{$ambiente->tenencia->tenencia}}</option>
                                    @foreach ($tipos_tenencia as $tipo_tenencia)
                                        <option value="{{$tipo_tenencia->id_tenencia}}">{{$tipo_tenencia->tenencia}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!-- regional -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_regional"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Regional</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_regional" id="id_regional" required style="width: 100%;" data-select2-id="id_regional" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="{{$ambiente->centro_formacion->regional->id_regional}}" selected disabled>{{$ambiente->centro_formacion->regional->regional}}</option>
                                    @foreach ($regionales as $r)
                                        <option value="{{$r->id_regional}}">{{$r->regional}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!-- municipio -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_municipio"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Municipio</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_municipio" id="id_municipio" required src="{{route('regionales.getmunicipio','#')}}" style="width: 100%;" data-select2-id="id_municipio" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="{{$ambiente->centro_formacion->municipios->id_municipio}}" selected>{{$ambiente->centro_formacion->municipios->municipio}}</option>
                                </select>
                            </div>

                            <!-- centro de formación -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_centro_formacion"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Centro de Formación</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_centro_formacion" id="id_centro_formacion" required src="{{route('regionales.getcentro','#')}}" style="width: 100%;" data-select2-id="id_centro_formacion" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="{{$ambiente->centro_formacion->id_centro_formacion}}" selected>{{$ambiente->centro_formacion->centro_formacion}}</option>
                                </select>
                            </div>

                             <!-- sede -->
                             <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_sede"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Sede</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_sede" id="id_sede" required src="{{route('regionales.getsede','#')}}" style="width: 100%;" data-select2-id="id_sede" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">   
                                    <option value="{{$ambiente->sedes->id_sede}}" selected>{{$ambiente->sedes->sede}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- fin información general -->

                    <!-- descripción fisica -->
                    <div class="tab">
                        <!-- dimensiones -->
                        <div class="form-row justify-content-center mb-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Dimensiones</label>
                            </div>
                        </div>
                        <div class="form-row justify-content-center mb-2">
                            <div class="form-group col-5 col-lg-4">
                                <label class="form-label" for="dimension_largo"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Largo-Profundo (m)</label>
                                <input type="number" class="form-control" name="dimension_largo" id="dimension_largo" value="{{$ambiente->dimension_largo}}" step="any" min="1" max="1000" placeholder="Ej: 10.40" onchange="cal()" onkeyup="cal()" required>
                            </div>

                            <div class="form-group col-5 col-lg-4">
                                <label class="form-label" for="dimension_ancho"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Ancho-Frente (m)</label>
                                <input type="number" class="form-control" name="dimension_ancho" id="dimension_ancho" value="{{$ambiente->dimension_ancho}}" step="any" min="1" max="1000" placeholder="Ej: 7" onchange="cal()" onkeyup="cal()" required>
                            </div>
                        </div>

                        <div class="form-row justify-content-center mb-2">
                            <div class="form-group col-5 col-lg-4">
                                <label class="form-label" for="dimension_alto"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Altura (m)</label>
                                <input type="number" class="form-control" name="dimension_alto" id="dimension_alto" value="{{$ambiente->dimension_alto}}" step="any" min="1" max="100" placeholder="Ej: 4.35" required>
                            </div>

                            <div class="form-group col-5 col-lg-4">
                                <label class="form-label" for="area_ambiente">Area (m²)</label>
                                <input type="number" class="form-control" name="area_ambiente" id="area_ambiente" value="{{$ambiente->area_ambiente}}" step="any" readonly="readonly">
                            </div>
                        </div>

                        <!-- puerta acceso -->
                        <div class="row justify-content-center mb-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Puerta de Acceso</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center mb-4">
                            <div class="form-group col-5 col-lg-4">
                                <label class="form-label" for="puerta_ancho"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Ancho (m)</label>
                                <input type="number" class="form-control" name="puerta_ancho" id="puerta_ancho" value="{{$ambiente->puerta_ancho}}"
                                    step="any" min="1" max="100" maxlength="5" placeholder="Ej: 1.15" onchange="validacion_puerta()" onkeyup="validacion_puerta()" required>
                                <div id="validacion_ancho">
                                </div>
                            </div>

                            <div class="form-group col-5 col-lg-4">
                                <label class="form-label" for="puerta_alto"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Altura (m)</label>
                                <input type="number" class="form-control" name="puerta_alto" id="puerta_alto" value="{{$ambiente->puerta_alto}}"
                                    step="any" min="1" max="100" maxlength="5" placeholder="Ej: 2" onchange="validacion_puerta()" onkeyup="validacion_puerta()" required>
                                <div id="validacion_altura">
                                </div>
                            </div>
                        </div>

                        <!-- acabados del ambiente -->
                        <div class="row justify-content-center mb-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Acabados del Ambiente</label>
                            </div>
                        </div>

                        <!-- pisos -->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_piso"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Pisos</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_piso" id="id_piso" required style="width: 100%;" data-select2-id="id_piso" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="{{$ambiente->piso->id_piso}}" selected>{{$ambiente->piso->piso}}</option>
                                    @foreach ($pisos as $p)
                                        <option value="{{$p->id_piso}}">{{$p->piso}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_piso_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otro_piso">Otro Tipo de Piso</label>
                                @if ($ambiente->otro_piso != null)
                                    <input type="text" class="form-control" name="otro_piso" id="otro_piso" value="{{$ambiente->otro_piso}}" placeholder="Especifique" maxlength="50">
                                @else 
                                    <input type="text" class="form-control" name="otro_piso" id="otro_piso" placeholder="Especifique" maxlength="50">
                                @endif
                            </div>
                        </div>
                          
                        <!-- muros -->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_muro"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Muros</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_muro" id="id_muro" required style="width: 100%;" data-select2-id="id_muro" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="{{$ambiente->muro->id_muro}}" selected>{{$ambiente->muro->muro}}</option>
                                    @foreach ($muros as $m)
                                        <option value="{{$m->id_muro}}">{{$m->muro}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_muro_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otro_muro">Otro Tipo de Muro</label>
                                @if ($ambiente->otro_muro != null)
                                    <input type="text" class="form-control" name="otro_muro" id="otro_muro" value="{{$ambiente->otro_muro}}" placeholder="Especifique" maxlength="50">
                                @else 
                                    <input type="text" class="form-control" name="otro_muro" id="otro_muro" placeholder="Especifique" maxlength="50">
                                @endif
                            </div>
                        </div>

                        <!-- guardaescobas -->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_guardaescoba"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Guardaescobas</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_guardaescoba" id="id_guardaescoba" required style="width: 100%;" data-select2-id="id_guardaescoba" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="{{$ambiente->guardaescoba->id_guardaescoba}}" selected>{{$ambiente->guardaescoba->guardaescoba}}</option>
                                    @foreach ($guardaescobas as $g)
                                        <option value="{{$g->id_guardaescoba}}">{{$g->guardaescoba}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_guardaescoba_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otro_guardaescoba">Otro Tipo de Guardaescoba</label>
                                @if ($ambiente->otro_guardaescoba != null)
                                    <input type="text" class="form-control" name="otro_guardaescoba" id="otro_guardaescoba" value="{{$ambiente->otro_guardaescoba}}" placeholder="Especifique" maxlength="50">
                                @else 
                                    <input type="text" class="form-control" name="otro_guardaescoba" id="otro_guardaescoba" placeholder="Especifique" maxlength="50">
                                @endif
                            </div>
                        </div>

                        <!-- cielorasos -->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_cieloraso"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Cielorasos</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_cieloraso" id="id_cieloraso" required style="width: 100%;" data-select2-id="id_cieloraso" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="{{$ambiente->cieloraso->id_cieloraso}}" selected>{{$ambiente->cieloraso->cieloraso}}</option>
                                    @foreach ($cielorasos as $c)
                                        <option value="{{$c->id_cieloraso}}">{{$c->cieloraso}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_cieloraso_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otro_cieloraso">Otro Tipo de Cieloraso</label>
                                @if ($ambiente->otro_cieloraso != null)
                                    <input type="text" class="form-control" name="otro_cieloraso" id="otro_cieloraso" value="{{$ambiente->otro_cieloraso}}" placeholder="Especifique" maxlength="50">
                                @else 
                                    <input type="text" class="form-control" name="otro_cieloraso" id="otro_cieloraso" placeholder="Especifique" maxlength="50">
                                @endif
                            </div>
                        </div>

                        <!-- ventanerias -->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_ventaneria"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Ventanas</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_ventaneria" id="id_ventaneria" required style="width: 100%;" data-select2-id="id_ventaneria" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="{{$ambiente->ventaneria->id_ventaneria}}" selected>{{$ambiente->ventaneria->ventaneria}}</option>
                                    @foreach ($ventanerias as $v)
                                        <option value="{{$v->id_ventaneria}}">{{$v->ventaneria}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_ventaneria_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otra_ventaneria">Otro Tipo de Ventana</label>
                                @if ($ambiente->otra_ventaneria != null)
                                    <input type="text" class="form-control" name="otra_ventaneria" id="otra_ventaneria" value="{{$ambiente->otra_ventaneria}}" placeholder="Especifique" maxlength="50">
                                @else 
                                    <input type="text" class="form-control" name="otra_ventaneria" id="otra_ventaneria" placeholder="Especifique" maxlength="50">
                                @endif
                            </div>
                        </div>

                        <!-- puertas -->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_puerta"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Puertas</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_puerta" id="id_puerta" required style="width: 100%;" data-select2-id="id_puerta" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="{{$ambiente->puerta->id_puerta}}" selected>{{$ambiente->puerta->puerta}}</option>
                                    @foreach ($puertas as $p)
                                        <option value="{{$p->id_puerta}}">{{$p->puerta}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_puerta_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otra_puerta">Otro Tipo de Puerta</label>
                                @if ($ambiente->otra_puerta != null)
                                    <input type="text" class="form-control" name="otra_puerta" id="otra_puerta" value="{{$ambiente->otra_puerta}}" placeholder="Especifique" maxlength="50">
                                @else 
                                    <input type="text" class="form-control" name="otra_puerta" id="otra_puerta" placeholder="Especifique" maxlength="50">
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- fin descripción fisica -->

                    <!-- caracteristicas -->
                    <div class="tab">
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Producción de Centros</label>
                            </div>
                        </div>
                        <div class="form-row justify-content-center mt-2">
                            <div class="col-10 col-lg-6">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> ¿El ambiente realiza producción de centros?</label>
                            </div>
                            <div class="form-group col-10 col-lg-2">
                                @if ($ambiente->produccion_centros == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="produccion_centros" id="produccion_centros_1" value="SI" checked>
                                        <label for="produccion_centros_1"></label>
                                    </div>
                                    <div class="d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="produccion_centros" id="produccion_centros_2" value="NO">
                                        <label for="produccion_centros_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="produccion_centros" id="produccion_centros_1" value="SI">
                                        <label for="produccion_centros_1"></label>
                                    </div>
                                    <div class="d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="produccion_centros" id="produccion_centros_2" value="NO" checked>
                                        <label for="produccion_centros_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center" id="productos_ambiente">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_producto"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Productos Generados en el Ambiente</label>
                                <div class="select2-orange">
                                    <select class="select2" name="id_producto[]" id="id_producto" multiple="multiple" data-placeholder="Autocomplete Productos" data-dropdown-css-class="select2-orange" style="width: 100%;">
                                        @foreach ($productos_ambiente as $pa)
                                            <option value="{{$pa->producto->id_producto}}" selected>{{$pa->producto->producto}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
            
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Condiciones Relacionadas con la Seguridad</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center mt-2">
                            <div class="col-10 col-lg-6">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> ¿El ambiente tiene algun tipo de riesgo?</label>
                            </div>
                            <div class="form-group col-10 col-lg-2">
                                @if ($tipos_riesgo_ambiente != "[]")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="riesgos" id="riesgos_1" value="SI" checked>
                                        <label for="riesgos_1"></label>
                                    </div>
                                    <div class="d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="riesgos" id="riesgos_2" value="NO">
                                        <label for="riesgos_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="riesgos" id="riesgos_1" value="SI">
                                        <label for="riesgos_1"></label>
                                    </div>
                                    <div class="d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="riesgos" id="riesgos_2" value="NO" checked>
                                        <label for="riesgos_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center" id="riesgos_ambiente">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_tipo_riesgo"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Tipos de Riesgo</label>
                                <div class="select2-orange">
                                    <select class="select2" name="id_tipo_riesgo[]" id="id_tipo_riesgo" multiple="multiple" data-placeholder="Seleccione Tipos de Riesgo" data-dropdown-css-class="select2-orange" style="width: 100%;">
                                        @foreach ($tipos_riesgo_ambiente as $tra)
                                            <option value="{{$tra->tipo_riesgo->id_tipo_riesgo}}" selected>{{$tra->tipo_riesgo->tipo_riesgo}}</option>
                                        @endforeach
                                        @foreach ($tipos_riesgo as $tr)
                                            <option value="{{$tr->id_tipo_riesgo}}">{{$tr->tipo_riesgo}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_tipo_riesgo_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otro_tipo_riesgo">Otro Tipo de Riesgo</label>
                                <input type="text" class="form-control" name="otro_tipo_riesgo" id="otro_tipo_riesgo" value="{{$ambiente->otro_tipo_riesgo}}" placeholder="Especifique" maxlength="50">
                            </div>
                        </div>

                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Esquemas del Ambiente</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="esquema_ambiente">Esquema Funcional Real</label>
                                @if ($ambiente->esquema_ambiente != null)
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="esquema_ambiente" id="esquema_ambiente" value="{{$ambiente->esquema_ambiente}}" accept="image/*">
                                        <label class="custom-file-label" for="esquema_ambiente" data-browse="Seleccionar">{{$ambiente->esquema_ambiente}}</label>
                                    </div>
                                @else
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="esquema_ambiente" id="esquema_ambiente" accept="image/*">
                                        <label class="custom-file-label" for="esquema_ambiente" data-browse="Seleccionar">No se ha registrado esquema funcional real</label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="foto_ambiente">Foto del Ambiente</label>
                                @if ($ambiente->foto_ambiente != null)
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="foto_ambiente" id="foto_ambiente" value="{{$ambiente->foto_ambiente}}" accept="image/*">
                                        <label class="custom-file-label" for="foto_ambiente" data-browse="Seleccionar">{{$ambiente->foto_ambiente}}</label>
                                    </div>
                                @else
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="foto_ambiente" id="foto_ambiente" accept="image/*">
                                        <label class="custom-file-label" for="foto_ambiente" data-browse="Seleccionar">No se ha registrado foto</label>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- fin caracteristicas -->

                    <!-- condiciones de confort -->
                    <div class="tab">
                        <!-- iluminación -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Iluminación</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Natural</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->iluminacion_natural == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="iluminacion_natural" id="iluminacion_natural_1" value="SI" checked>
                                        <label for="iluminacion_natural_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="iluminacion_natural" id="iluminacion_natural_2" value="NO">
                                        <label for="iluminacion_natural_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="iluminacion_natural" id="iluminacion_natural_1" value="SI">
                                        <label for="iluminacion_natural_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="iluminacion_natural" id="iluminacion_natural_2" value="NO" checked>
                                        <label for="iluminacion_natural_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label">Artificial</label>
                                <input type="text" class="form-control" name="iluminacion_artificial" id="iluminacion_artificial" value="{{$ambiente->iluminacion_artificial}}"
                                placeholder="Ej: Fluorecente T8" maxlength="50">
                            </div>
                        </div>
                        <!-- fin iluminación -->

                        <!-- ventilación -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Ventilación</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Natural</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->ventilacion_natural == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="ventilacion_natural" id="ventilacion_natural_1" value="SI" checked>
                                        <label for="ventilacion_natural_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="ventilacion_natural" id="ventilacion_natural_2" value="NO">
                                        <label for="ventilacion_natural_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="ventilacion_natural" id="ventilacion_natural_1" value="SI">
                                        <label for="ventilacion_natural_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="ventilacion_natural" id="ventilacion_natural_2" value="NO" checked>
                                        <label for="ventilacion_natural_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label">Mecánica</label>
                                <input type="text" class="form-control" name="ventilacion_mecanica" id="ventilacion_mecanica" value="{{$ambiente->ventilacion_mecanica}}"
                                placeholder="Ej: Ventilador" maxlength="50">
                            </div>
                        </div>
                        <!-- fin ventilación -->

                        <!-- instalaciones de gas -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Instalaciones de Gas</label>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Gas Natural</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->gas_natural == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="gas_natural" id="gas_natural_1" value="SI" checked>
                                        <label for="gas_natural_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="gas_natural" id="gas_natural_2" value="NO">
                                        <label for="gas_natural_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="gas_natural" id="gas_natural_1" value="SI">
                                        <label for="gas_natural_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="gas_natural" id="gas_natural_2" value="NO" checked>
                                        <label for="gas_natural_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Gas Propano</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->gas_propano == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="gas_propano" id="gas_propano_1" value="SI" checked>
                                        <label for="gas_propano_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="gas_propano" id="gas_propano_2" value="NO">
                                        <label for="gas_propano_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="gas_propano" id="gas_propano_1" value="SI">
                                        <label for="gas_propano_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="gas_propano" id="gas_propano_2" value="NO" checked>
                                        <label for="gas_propano_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <!-- fin instalaciones de gas -->

                        <!-- instalaciones eléctricas -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Instalaciones Eléctricas</label>
                            </div>
                        </div>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Bifásica</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->electrica_bifasica == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="electrica_bifasica" id="electrica_bifasica_1" value="SI" checked>
                                        <label for="electrica_bifasica_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="electrica_bifasica" id="electrica_bifasica_2" value="NO">
                                        <label for="electrica_bifasica_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="electrica_bifasica" id="electrica_bifasica_1" value="SI">
                                        <label for="electrica_bifasica_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="electrica_bifasica" id="electrica_bifasica_2" value="NO" checked>
                                        <label for="electrica_bifasica_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Trifásica</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->electrica_trifasica == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="electrica_trifasica" id="electrica_trifasica_1" value="SI" checked>
                                        <label for="electrica_trifasica_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="electrica_trifasica" id="electrica_trifasica_2" value="NO">
                                        <label for="electrica_trifasica_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="electrica_trifasica" id="electrica_trifasica_1" value="SI">
                                        <label for="electrica_trifasica_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="electrica_trifasica" id="electrica_trifasica_2" value="NO" checked>
                                        <label for="electrica_trifasica_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Red Regulada</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->electrica_regulada == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="electrica_regulada" id="electrica_regulada_1" value="SI" checked>
                                        <label for="electrica_regulada_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="electrica_regulada" id="electrica_regulada_2" value="NO">
                                        <label for="electrica_regulada_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="electrica_regulada" id="electrica_regulada_1" value="SI">
                                        <label for="electrica_regulada_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="electrica_regulada" id="electrica_regulada_2" value="NO" checked>
                                        <label for="electrica_regulada_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Red de Datos</label>
                            </div>

                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->red_datos == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="red_datos" id="red_datos_1" value="SI" checked>
                                        <label for="red_datos_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="red_datos" id="red_datos_2" value="NO">
                                        <label for="red_datos_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="red_datos" id="red_datos_1" value="SI">
                                        <label for="red_datos_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="red_datos" id="red_datos_2" value="NO" checked>
                                        <label for="red_datos_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <!-- fin instalaciones eléctricas -->

                        <!-- instalaciones hidrosanitarias -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Instalaciones Hidrosanitarias</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Agua Fría</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_aguafria == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_aguafria" id="hidro_aguafria_1" value="SI" checked>
                                        <label for="hidro_aguafria_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_aguafria" id="hidro_aguafria_2" value="NO">
                                        <label for="hidro_aguafria_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_aguafria" id="hidro_aguafria_1" value="SI">
                                        <label for="hidro_aguafria_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_aguafria" id="hidro_aguafria_2" value="NO" checked>
                                        <label for="hidro_aguafria_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Agua Caliente</label>
                            </div>

                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_aguacaliente == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_aguacaliente" id="hidro_aguacaliente_1" value="SI" checked>
                                        <label for="hidro_aguacaliente_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_aguacaliente" id="hidro_aguacaliente_2" value="NO">
                                        <label for="hidro_aguacaliente_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_aguacaliente" id="hidro_aguacaliente_1" value="SI">
                                        <label for="hidro_aguacaliente_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_aguacaliente" id="hidro_aguacaliente_2" value="NO" checked>
                                        <label for="hidro_aguacaliente_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Lavaojos</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_lavaojos == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_lavaojos" id="hidro_lavaojos_1" value="SI" checked>
                                        <label for="hidro_lavaojos_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_lavaojos" id="hidro_lavaojos_2" value="NO">
                                        <label for="hidro_lavaojos_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_lavaojos" id="hidro_lavaojos_1" value="SI">
                                        <label for="hidro_lavaojos_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_lavaojos" id="hidro_lavaojos_2" value="NO" checked>
                                        <label for="hidro_lavaojos_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Banco de Hielo</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_banco_hielo == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_banco_hielo" id="hidro_banco_hielo_1" value="SI" checked>
                                        <label for="hidro_banco_hielo_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_banco_hielo" id="hidro_banco_hielo_2" value="NO">
                                        <label for="hidro_banco_hielo_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_banco_hielo" id="hidro_banco_hielo_1" value="SI">
                                        <label for="hidro_banco_hielo_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_banco_hielo" id="hidro_banco_hielo_2" value="NO" checked>
                                        <label for="hidro_banco_hielo_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Trampa de Grasas</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_trampa_grasas == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_trampa_grasas" id="hidro_trampa_grasas_1" value="SI" checked>
                                        <label for="hidro_trampa_grasas_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_trampa_grasas" id="hidro_trampa_grasas_2" value="NO">
                                        <label for="hidro_trampa_grasas_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_trampa_grasas" id="hidro_trampa_grasas_1" value="SI">
                                        <label for="hidro_trampa_grasas_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_trampa_grasas" id="hidro_trampa_grasas_2" value="NO" checked>
                                        <label for="hidro_trampa_grasas_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Vapor</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_vapor == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_vapor" id="hidro_vapor_1" value="SI" checked>
                                        <label for="hidro_vapor_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_vapor" id="hidro_vapor_2" value="NO">
                                        <label for="hidro_vapor_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_vapor" id="hidro_vapor_1" value="SI">
                                        <label for="hidro_vapor_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_vapor" id="hidro_vapor_2" value="NO" checked>
                                        <label for="hidro_vapor_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Desarenador</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_desarenador == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_desarenador" id="hidro_desarenador_1" value="SI" checked>
                                        <label for="hidro_desarenador_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_desarenador" id="hidro_desarenador_2" value="NO">
                                        <label for="hidro_desarenador_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_desarenador" id="hidro_desarenador_1" value="SI">
                                        <label for="hidro_desarenador_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_desarenador" id="hidro_desarenador_2" value="NO" checked>
                                        <label for="hidro_desarenador_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Lavamanos</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_lavamanos == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_lavamanos" id="hidro_lavamanos_1" value="SI" checked>
                                        <label for="hidro_lavamanos_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_lavamanos" id="hidro_lavamanos_2" value="NO">
                                        <label for="hidro_lavamanos_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_lavamanos" id="hidro_lavamanos_1" value="SI">
                                        <label for="hidro_lavamanos_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_lavamanos" id="hidro_lavamanos_2" value="NO" checked>
                                        <label for="hidro_lavamanos_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Desagües</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_desagues == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_desagues" id="hidro_desagues_1" value="SI" checked>
                                        <label for="hidro_desagues_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_desagues" id="hidro_desagues_2" value="NO">
                                        <label for="hidro_desagues_2"></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_desagues" id="hidro_desagues_1" value="SI">
                                        <label for="hidro_desagues_1"></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" name="hidro_desagues" id="hidro_desagues_2" value="NO" checked>
                                        <label for="hidro_desagues_2"></label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otras_instalaciones_hidrosanitarias">Otras Instalaciones Hidrosanitarias</label>
                                <input type="text" class="form-control" name="hidro_otro" id="hidro_otro" value="{{$ambiente->hidro_otro}}" placeholder="Especifique" maxlength="50">
                            </div>
                        </div>
                        
                        <!-- fin condiciones hidrosanitarias-->

                        <!-- observaciones generales-->
                            <div class="form-row justify-content-center mt-2">
                                <div class="form-group col-10 col-lg-8">
                                    <label class="form-label color-naranja" for="observaciones_generales">Observaciones Generales</label>
                                    @if ($ambiente->observaciones_generales != null)
                                        <textarea class="form-control" name="observaciones_generales" id="observaciones_generales" placeholder="Descripción de Observaciones al Ambiente" rows="3">{{$ambiente->observaciones_generales}}</textarea>
                                    @else
                                        <textarea class="form-control" name="observaciones_generales" id="observaciones_generales" placeholder="Descripción de Observaciones al Ambiente" rows="3"></textarea>
                                    @endif
                                </div>
                            </div>
                        <!-- fin observaciones generales-->
                    </div>
                    <!-- fin condiciones de confort -->

                    <div class="row justify-content-center mt-3">
                        <div class="form-group col-10 col-lg-8">
                            <div style="overflow:auto;">
                                <button type="button" class="btn w-100 btn-naranja" id="nextBtn" onclick="nextPrev(1)">Siguiente</button>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <div style="overflow:auto;">
                                <button type="button" class="btn w-100 btn-naranja mb-3" id="prevBtn" onclick="nextPrev(-1)">Anterior</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- modal descripcion area de cualificacion -->
        <div class="modal fade" id="modal-area-cualificacion" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modal-area-cualificacion-titulo" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-10 offset-1">
                            <h5 class="modal-title text-center" id="modal-area-cualificacion-titulo">Descripción Area de Cualificación</h5>
                        </div>
                        <div class="col-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-modal">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <p id="descripcion-area-cualificacion"></p>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<script src="{{asset('js/actualizar_ambiente.js')}}"></script>
<script>
	$(document).ready(function() {
        $("#id_producto").select2({
            ajax: {
                url: "{{ route('autocompletar.productos') }}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        autocompletar_producto: params.term,
                    };
                },

                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj) {
                            return {
                                id: obj.value,
                                text: obj.label
                            };
                        })
                    };
                    
                },
                cache: true
            }
        });
	});
</script>
@endsection
