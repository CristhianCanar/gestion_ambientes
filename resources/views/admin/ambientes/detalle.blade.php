@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('ambientes.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2 card-ambiente">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Detalles Ambiente de Aprendizaje</h3>
                </div>

                <ul id="progressbar">
                    <li class="step" id="informacion_general"><strong>Información General</strong></li>
                    <li class="step" id="descripcion_fisica"><strong>Descripción Fisica</strong></li>
                    <li class="step" id="caracteristicas"><strong>Caracteristicas</strong></li>
                    <li class="step" id="condiciones_confort"><strong>Condiciones de Confort</strong></li>
                </ul>
                <!-- información general -->
                <div class="tab">
                    <div class="form-row justify-content-center mt-2">
                        <!-- codigo ambiente -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="codigo_ambiente">Código Ambiente</label>
                            <h6>{{$ambiente->codigo_ambiente}}</h6>
                        </div>

                        <!-- nombre ambiente -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="nombre_ambiente">Ambiente</label>
                            <h6>{{$ambiente->nombre_ambiente}}</h6>
                        </div>

                        <!-- cantidad de aprendices -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="cantidad_aprendices">Número de Aprendices</label>
                            <h6>{{$ambiente->cantidad_aprendices}}</h6>
                        </div>

                        <!-- area de cualificacion-->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="area_cualificacion">Area de Cualificación</label>
                            <h6>{{$ambiente->area_cualificacion->area_cualificacion}}</h6>
                        </div>

                        <!-- tipo de ambiente -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="tipo_ambiente">Tipo de Ambiente</label>
                            <h6>{{$ambiente->tipo_ambiente->tipo_ambiente}}
                                <small>({{$ambiente->tipo_ambiente->detalle_tipo_ambiente}})</small>
                            </h6>
                        </div>

                        <!-- tipo de tenencia -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_tenencia">Tenencia</label>
                            <h6>{{$ambiente->tenencia->tenencia}}</h6>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_regional">Regional</label>
                            <h6>{{$ambiente->centro_formacion->regional->regional}}</h6>
                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_municipio">Municipio</label>
                            <h6>{{$ambiente->centro_formacion->municipios->municipio}}</h6>

                        </div>

                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_centro_formacion">Centro de Formación</label>
                            <h6>{{$ambiente->centro_formacion->centro_formacion}}</h6>
                        </div>

                        <!-- centro de formación -->
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_sede">Sede</label>
                            <h6>{{$ambiente->sedes->sede}}</h6>
                        </div>

                    </div>
                </div>
                <!-- fin información general -->

                <!-- descripción fisica -->
                <div class="tab">
                    <!-- dimensiones -->
                    <div class="form-row justify-content-center">
                        <div class="col-10 col-lg-8">
                            <label class="form-label color-naranja">Dimensiones</label>
                        </div>
                    </div>
                    <div class="form-row justify-content-center">
                        <div class="form-group col-5 col-lg-4">
                            <label class="form-label" for="dimension_largo">Largo-Profundo</label>
                            <h6>{{$ambiente->dimension_largo}} m</h6>
                        </div>

                        <div class="form-group col-5 col-lg-4">
                            <label class="form-label" for="dimension_ancho">Ancho-Frente</label>
                            <h6>{{$ambiente->dimension_ancho}} m</h6>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-5 col-lg-4">
                            <label class="form-label" for="dimension_alto">Altura</label>
                            <h6>{{$ambiente->dimension_alto}} m</h6>
                        </div>

                        <div class="form-group col-5 col-lg-4">
                            <label class="form-label" for="area_ambiente">Area</label>
                            <h6>{{$ambiente->area_ambiente}} m²</h6>
                        </div>
                    </div>

                    <!-- puerta acceso -->
                    <div class="row justify-content-center mt-2">
                        <div class="col-10 col-lg-8">
                            <label class="form-label color-naranja">Puerta de Acceso</label>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-5 col-lg-4">
                            <label class="form-label" for="puerta_ancho">Ancho</label>
                            <h6>{{$ambiente->puerta_ancho}} m</h6>
                        </div>
                        <div class="form-group col-5 col-lg-4">
                            <label class="form-label" for="puerta_alto">Altura</label>
                            <h6>{{$ambiente->puerta_alto}} m</h6>
                        </div>
                    </div>

                    <!-- acabados del ambiente -->
                    <div class="row justify-content-center mt-2">
                        <div class="col-10 col-lg-8">
                            <label class="form-label color-naranja">Acabados del Ambiente</label>
                        </div>
                    </div>

                    <!-- pisos -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_piso">Pisos</label>
                            @if ($ambiente->id_piso == 127)
                                <h6><label class="form-label">{{$ambiente->piso->piso}}:</label> {{$ambiente->otro_piso}}</h6>
                            @else
                                <h6>{{$ambiente->piso->piso}}</h6>
                            @endif
                        </div>
                    </div>

                    <!-- muros -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_muro">Muros</label>
                            @if ($ambiente->id_muro == 127)
                                <h6><label class="form-label">{{$ambiente->muro->muro}}:</label> {{$ambiente->otro_muro}}</h6>
                            @else
                                <h6>{{$ambiente->muro->muro}}</h6>
                            @endif
                        </div>
                    </div>

                    <!-- guardaescobas -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_guardaescoba">Guardaescobas</label>
                            @if ($ambiente->id_guardaescoba == 127)
                                <h6><label class="form-label">{{$ambiente->guardaescoba->guardaescoba}}:</label> {{$ambiente->otro_guardaescoba}}</h6>
                            @else
                                <h6>{{$ambiente->guardaescoba->guardaescoba}}</h6>
                            @endif
                        </div>
                    </div>

                    <!-- cielorasos -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_cieloraso">Cielorasos</label>
                            @if ($ambiente->id_cieloraso == 127)
                                <h6><label class="form-label">{{$ambiente->cieloraso->cieloraso}}:</label> {{$ambiente->otro_cieloraso}}</h6>
                            @else
                                <h6>{{$ambiente->cieloraso->cieloraso}}</h6>
                            @endif
                        </div>
                    </div>

                    <!-- ventanerias -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_ventaneria">Ventanas</label>
                            @if ($ambiente->id_ventaneria == 127)
                                <h6><label class="form-label">{{$ambiente->ventaneria->ventaneria}}:</label> {{$ambiente->otra_ventaneria}}</h6>
                            @else
                                <h6>{{$ambiente->ventaneria->ventaneria}}</h6>
                            @endif
                        </div>
                    </div>

                    <!-- puertas -->
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="id_puerta">Puertas</label>
                            @if ($ambiente->id_puerta == 127)
                                <h6><label class="form-label">{{$ambiente->puerta->puerta}}:</label> {{$ambiente->otra_puerta}}</h6>
                            @else
                                <h6>{{$ambiente->puerta->puerta}}</h6>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- fin descripción fisica -->

                <!-- caracteristicas -->
                <div class="tab">
                    <div class="row justify-content-center mt-2">
                        <div class="col-10 col-lg-8">
                            <label class="form-label color-naranja">Producción de Centros</label>
                        </div>
                    </div>
                    <div class="form-row justify-content-center mt-2">
                        <div class="col-10 col-lg-6">
                            <label class="form-label">¿El ambiente realiza producción de centros?</label>
                        </div>
                        <div class="form-group col-10 col-lg-2">
                            @if ($ambiente->produccion_centros == "SI")
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" value="SI" checked>
                                    <label></label>
                                </div>
                                <div class="d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" value="NO" disabled>
                                    <label></label>
                                </div>
                            @else
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" value="SI" disabled>
                                    <label></label>
                                </div>
                                <div class="d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" value="NO" checked>
                                    <label></label>
                                </div>
                            @endif
                        </div>
                    </div>

                    @if ($productos_ambiente != "[]")
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label color-naranja" for="id_producto">Productos Generados en el Ambiente</label>
                                @foreach ($productos_ambiente as $pa)
                                    <h6>{{$pa->producto->producto}}</h6>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    <div class="row justify-content-center mt-2">
                        <div class="col-10 col-lg-8">
                            <label class="form-label color-naranja">Condiciones Relacionadas con la Seguridad</label>
                        </div>
                    </div>
                    <div class="form-row justify-content-center mt-2">
                        <div class="col-10 col-lg-6">
                            <label class="form-label">¿El ambiente tiene algun tipo de riesgo?</label>
                        </div>
                        <div class="form-group col-10 col-lg-2">
                            @if ($tipos_riesgo_ambiente != "[]")
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" value="SI" checked>
                                    <label></label>
                                </div>
                                <div class="d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" value="NO" disabled>
                                    <label></label>
                                </div>
                            @else
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" value="SI" disabled>
                                    <label></label>
                                </div>
                                <div class="d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" value="NO" checked>
                                    <label></label>
                                </div>
                            @endif
                        </div>
                    </div>

                    @if ($tipos_riesgo_ambiente != "[]")
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label color-naranja" for="id_tipo_riesgo">Tipos de Riesgo</label>
                                @if ($ambiente->otro_tipo_riesgo == null)
                                    @foreach ($tipos_riesgo_ambiente as $tra)
                                        <h6>{{$tra->tipo_riesgo->tipo_riesgo}}</h6>
                                    @endforeach
                                @else 
                                    <h6><label class="form-label">Otro:</label> {{$ambiente->otro_tipo_riesgo}}</h6>  
                                @endif
                            </div>
                        </div>
                    @endif
                  
                    <div class="row justify-content-center mt-2">
                        <div class="col-10 col-lg-8">
                            <label class="form-label color-naranja">Esquemas del Ambiente</label>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="esquema_ambiente">Esquema Funcional Real</label>
                            @if ($ambiente->esquema_ambiente != null)
                                <img style="width:500px; height:350px;" src="../imagenes_ambiente/{{$ambiente->esquema_ambiente}}" class="card-img img-fluid" alt="Esquema Funcional Real">
                            @else
                                <h6>No se ha registrado esquema funcional real</h6>
                            @endif
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="foto_ambiente">Foto del Ambiente</label>
                            @if ($ambiente->foto_ambiente != null)
                                <img style="width:500px; height:350px;" src="../imagenes_ambiente/{{$ambiente->foto_ambiente}}" class="card-img img-fluid" alt="Foto del Ambiente">
                            @else
                                <h6>No se ha registrado foto</h6>
                            @endif
                        </div>
                    </div>

                </div>
                <!-- fin caracteristicas -->

                <!-- condiciones de confort -->
                <div class="tab">
                    <div id="step-4">
                        <!-- iluminación -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Iluminación</label>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-4">
                                <label class="form-label">Natural</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->iluminacion_natural == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label">Artificial</label>
                                @if ($ambiente->iluminacion_artificial != null)
                                    <h6>{{$ambiente->iluminacion_artificial}}</h6>
                                @else
                                    <h6>No se ha registrado iluminación artificial</h6>
                                @endif
                            </div>
                        </div>
                        
                        <!-- fin iluminación -->

                        <!-- ventilación -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Ventilación</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="col-4">
                                <label class="form-label">Natural</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->ventilacion_natural == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                            
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label">Mecánica</label>
                                @if ($ambiente->ventilacion_mecanica != null)
                                    <h6>{{$ambiente->ventilacion_mecanica}}</h6>
                                @else
                                    <h6>No se ha registrado ventilación mecánica</h6>
                                @endif
                            </div>
                        </div>
                        <!-- fin ventilación -->

                        <!-- instalaciones de gas -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Instalaciones de Gas</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Gas Natural</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->gas_natural == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Gas Propano</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->gas_propano == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <!-- fin instalaciones de gas -->

                        <!-- instalaciones eléctricas -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Instalaciones Eléctricas</label>
                            </div>
                        </div>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Bifásica</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->electrica_bifasica == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Trifásica</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->electrica_trifasica == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Red Regulada</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->electrica_regulada == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Red de Datos</label>
                            </div>

                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->red_datos == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <!-- fin instalaciones eléctricas -->

                        <!-- instalaciones hidrosanitarias -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Instalaciones Hidrosanitarias</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Agua Fría</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_aguafria == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Agua Caliente</label>
                            </div>

                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_aguacaliente == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Lavaojos</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_lavaojos == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Banco de Hielo</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_banco_hielo == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Trampa de Grasas</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_trampa_grasas == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Vapor</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_vapor == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Desarenador</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_desarenador == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Lavamanos</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_lavamanos == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label">Desagües</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                @if ($ambiente->hidro_desagues == "SI")
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" checked>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" disabled>
                                        <label></label>
                                    </div>
                                @else
                                    <div class="d-inline">
                                        <label>Si</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="SI" disabled>
                                        <label></label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <label>No</label>
                                    </div>
                                    <div class="icheck-orange d-inline">
                                        <input type="radio" value="NO" checked>
                                        <label></label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        @if ($ambiente->hidro_otro != null)
                            <div class="form-row justify-content-center">
                                <div class="form-group col-10 col-lg-8">
                                    <label class="form-label" for="observaciones_generales">Otras Instalaciones Hidrosanitarias</label>
                                    <p>{{$ambiente->hidro_otro}}</p>
                                </div>
                            </div>
                        @endif
                        <!-- fin condiciones hodhidrosanitarias-->

                        <!-- observaciones generales-->

                            <div class="form-row justify-content-center mt-2">
                                <div class="form-group col-10 col-lg-8">
                                    <label class="form-label color-naranja" for="observaciones_generales">Observaciones Generales</label>
                                    @if ($ambiente->observaciones_generales != null)
                                        <p>{{$ambiente->observaciones_generales}}</p>
                                    @else
                                        <h6>No se han registrado observaciones</h6>
                                    @endif
                                </div>
                            </div>
                        <!-- fin observaciones generales-->

                        <!-- fin condiciones de confort -->
                    </div>
                </div>
                <!-- fin condiciones de confort -->

                <div class="row justify-content-center mt-3">
                    <div class="form-group col-10 col-lg-8">
                        <div style="overflow:auto;">
                            <button type="button" class="btn w-100 btn-naranja" id="nextBtn" onclick="nextPrev(1)">Siguiente</button>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <div style="overflow:auto;">
                            <button type="button" class="btn w-100 btn-naranja mb-3" id="prevBtn" onclick="nextPrev(-1)">Anterior</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/detalles_ambiente.js')}}"></script>
@endsection
