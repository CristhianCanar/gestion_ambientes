@extends('admin.layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-sm-10 col-lg-8">
        <div class="card card-outline card-sena elevation-2 card-ambiente">
            <div class="card-body">
                <div class="col-12 mt-2 mb-4 text-center">
                    <h3 class="card-title">Registro Ambiente de Aprendizaje</h3>
                </div>
                <form id="regForm" method="POST" action="{{route('ambientes.store')}}" enctype="multipart/form-data">
                    @csrf
                    <ul id="progressbar">
                        <li class="step" id="informacion_general"><strong>Información General</strong></li>
                        <li class="step" id="descripcion_fisica"><strong>Descripción Fisica</strong></li>
                        <li class="step" id="caracteristicas"><strong>Caracteristicas</strong></li>
                        <li class="step" id="condiciones_confort"><strong>Condiciones de Confort</strong></li>
                    </ul>
                    <!-- información general -->
                    <div class="tab">
                        <div class="form-row justify-content-center mt-2">
                            <!-- nombre ambiente -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="nombre_ambiente"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Ambiente</label>
                                <input type="text" class="form-control" name="nombre_ambiente" id="nombre_ambiente" placeholder="Ej: Desarrollo de software 2" maxlength="50" autofocus required>
                                <span id="nombre_ambienteHelpBlock" class="form-text text-muted">Nombre dado al ambiente en el centro de formación</span>
                            </div>

                            <!-- cantidad de aprendices -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="cantidad_aprendices"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Número de Aprendices</label>
                                <input type="number" class="form-control" name="cantidad_aprendices" id="cantidad_aprendices" placeholder="Ej: 35" min="1" max="99" required>
                            </div>

                            <!-- area de cualificacion -->
                            <div class="form-group col-10 col-lg-8 offset-1">
                                <label class="form-label" for="area_cualificacion_id"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Area de Cualificación</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="area_cualificacion_id" id="area_cualificacion_id" required style="width: 100%;" data-select2-id="area_cualificacion_id" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="" selected disabled>Seleccione Area de Cualificación</option>
                                    @foreach ($areas_cualificacion as $area)
                                        <option value="{{$area->id_area_cualificacion}}" data-descripcion-area="{{$area->descripcion_area_cualificacion}}">{{$area->area_cualificacion}}</option>   
                                    @endforeach
                                </select>
                            </div>

                            <!-- boton modal descripcion area de cualificacion -->
                            <div class="form-group col-1">
                                <a href="#" class="btn btn-lg pl-0 pt-3 mt-4" data-toggle="modal" data-target="#modal-area-cualificacion">
                                    <i class="fas fa-question-circle fa-lg color-naranja" data-toggle="tooltip" title="Haga click para visualizar la descripción del área de cualificación que selecciono"></i>
                                </a>
                            </div>

                            <!-- tipo de ambiente -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Tipo de Ambiente</label>
                                @foreach ($tipos_ambientes as $tipo_ambiente)
                                    <div class="icheck-orange">
                                        <input type="radio" name="id_tipo_ambiente" id="id_tipo_ambiente_{{$tipo_ambiente->id_tipo_ambiente}}" value="{{$tipo_ambiente->id_tipo_ambiente}}" required>
                                        <label for="id_tipo_ambiente_{{$tipo_ambiente->id_tipo_ambiente}}">{{$tipo_ambiente->tipo_ambiente}}
                                            <small>({{$tipo_ambiente->detalle_tipo_ambiente}})</small>
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                                                      
                            <!-- tipo de tenencia -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_tenencia"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Tenencia</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_tenencia" id="id_tenencia" required style="width: 100%;" data-select2-id="id_tenencia" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="" selected disabled>Seleccione Tenencia</option>
                                    @foreach ($tipos_tenencia as $tipo_tenencia)
                                        <option value="{{$tipo_tenencia->id_tenencia}}">{{$tipo_tenencia->tenencia}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!-- regional -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_regional"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Regional</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_regional" id="id_regional" required style="width: 100%;" data-select2-id="id_regional" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="" selected disabled>Seleccione Regional</option>
                                    @foreach ($regionales as $r)
                                        <option value="{{$r->id_regional}}">{{$r->regional}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!-- municipio -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_municipio"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Municipio</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_municipio" id="id_municipio" required src="{{route('regionales.getmunicipio','#')}}" style="width: 100%;" data-select2-id="id_municipio" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                </select>
                            </div>

                            <!-- centro de formación -->
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_centro_formacion"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Centro de Formación</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_centro_formacion" id="id_centro_formacion" required src="{{route('regionales.getcentro','#')}}" style="width: 100%;" data-select2-id="id_centro_formacion" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                </select>
                            </div>

                            <!-- sede -->
                            <div class="form-group col-10 col-lg-8 offset-1">
                                <label class="form-label" for="id_sede"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Sede</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_sede" id="id_sede" required src="{{route('regionales.getsede','#')}}" style="width: 100%;" data-select2-id="id_sede" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">   
                                </select>
                            </div>
                            <!-- boton modal registro sede -->
                            <div class="form-group col-1">
                                <a href="#" class="btn btn-lg pl-0 pt-3 mt-4" data-toggle="modal" data-target="#registro-sede">
                                    <i class="fas fa-question-circle fa-lg color-naranja" data-toggle="tooltip" title="No tiene sedes para seleccionar, haga click para registrarlas aquí"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- fin información general -->

                    <!-- descripción fisica -->
                    <div class="tab">
                        <!-- dimensiones -->
                        <div class="form-row justify-content-center mb-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Dimensiones</label>
                            </div>
                        </div>
                        <div class="form-row justify-content-center mb-2">
                            <div class="form-group col-5 col-lg-4">
                                <label class="form-label" for="dimension_largo"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Largo-Profundo (m)</label>
                                <input type="number" class="form-control" name="dimension_largo" id="dimension_largo" step="any" min="1" max="1000" placeholder="Ej: 10.40" onchange="cal()" onkeyup="cal()" required>
                            </div>

                            <div class="form-group col-5 col-lg-4">
                                <label class="form-label" for="dimension_ancho"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Ancho-Frente (m)</label>
                                <input type="number" class="form-control" name="dimension_ancho" id="dimension_ancho" step="any" min="1" max="1000" placeholder="Ej: 7" onchange="cal()" onkeyup="cal()" required>
                            </div>
                        </div>

                        <div class="form-row justify-content-center mb-2">
                            <div class="form-group col-5 col-lg-4">
                                <label class="form-label" for="dimension_alto"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Altura (m)</label>
                                <input type="number" class="form-control" name="dimension_alto" id="dimension_alto" step="any" min="1" max="100" placeholder="Ej: 4.35" required>
                            </div>

                            <div class="form-group col-5 col-lg-4">
                                <label class="form-label" for="area_ambiente">Area (m²)</label>
                                <input type="number" class="form-control" name="area_ambiente" id="area_ambiente" step="any" readonly="readonly">
                            </div>
                        </div>


                        <!-- puerta acceso -->
                        <div class="row justify-content-center mb-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Puerta de Acceso</label>
                            </div>
                        </div>
                        <div class="form-row justify-content-center mb-4">
                            <div class="form-group col-5 col-lg-4">
                                <label class="form-label" for="puerta_ancho"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Ancho (m)</label>
                                <input type="number" class="form-control" name="puerta_ancho" id="puerta_ancho" step="any" min="1" max="100" placeholder="Ej: 1.15" onchange="validacion_puerta()" onkeyup="validacion_puerta()" required>
                                <div id="validacion_ancho">
                                </div>
                            </div>
                            <div class="form-group col-5 col-lg-4">
                                <label class="form-label" for="puerta_alto"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Altura (m)</label>
                                <input type="number" class="form-control" name="puerta_alto" id="puerta_alto" step="any" min="1" max="100" placeholder="Ej: 2" onchange="validacion_puerta()" onkeyup="validacion_puerta()" required>
                                <div id="validacion_altura">
                                </div>
                            </div>
                        </div>

                        <!-- acabados del ambiente -->
                        <div class="row justify-content-center mb-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Acabados del Ambiente</label>
                            </div>
                        </div>

                        <!-- pisos -->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_piso"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Pisos</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_piso" id="id_piso" required style="width: 100%;" data-select2-id="id_piso" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="" selected disabled>Seleccione Tipo de Piso</option>
                                    @foreach ($pisos as $p)
                                    <option value="{{$p->id_piso}}">{{$p->piso}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_piso_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otro_piso">Otro Tipo de Piso</label>
                                <input type="text" class="form-control" name="otro_piso" id="otro_piso" placeholder="Especifique" maxlength="50">
                            </div>
                        </div>

                        <!-- muros -->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_muro"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Muros</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_muro" id="id_muro" required style="width: 100%;" data-select2-id="id_muro" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="" selected disabled>Seleccione Tipo de Muro</option>
                                    @foreach ($muros as $m)
                                    <option value="{{$m->id_muro}}">{{$m->muro}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_muro_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otro_muro">Otro Tipo de Muro</label>
                                <input type="text" class="form-control" name="otro_muro" id="otro_muro" placeholder="Especifique" maxlength="50">
                            </div>
                        </div>

                        <!-- guardaescobas -->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_guardaescoba"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Guardaescobas</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_guardaescoba" id="id_guardaescoba" required style="width: 100%;" data-select2-id="id_guardaescoba" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="" selected disabled>Seleccione Tipo de Guardaescobas</option>
                                    @foreach ($guardaescobas as $g)
                                    <option value="{{$g->id_guardaescoba}}">{{$g->guardaescoba}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_guardaescoba_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otro_guardaescoba">Otro Tipo de Guardaescoba</label>
                                <input type="text" class="form-control" name="otro_guardaescoba" id="otro_guardaescoba" placeholder="Especifique" maxlength="50">
                            </div>
                        </div>

                        <!-- cielorasos -->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_cieloraso"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Cielorasos</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_cieloraso" id="id_cieloraso" required style="width: 100%;" data-select2-id="id_cieloraso" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="" selected disabled>Seleccione Tipo de Cieloraso</option>
                                    @foreach ($cielorasos as $c)
                                    <option value="{{$c->id_cieloraso}}">{{$c->cieloraso}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_cieloraso_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otro_cieloraso">Otro Tipo de Cieloraso</label>
                                <input type="text" class="form-control" name="otro_cieloraso" id="otro_cieloraso" placeholder="Especifique" maxlength="50">
                            </div>
                        </div>

                        <!-- ventanerias -->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_ventaneria"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Ventanas</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_ventaneria" id="id_ventaneria" required style="width: 100%;" data-select2-id="id_ventaneria" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="" selected disabled>Seleccione Tipo de Ventana</option>
                                    @foreach ($ventanerias as $v)
                                    <option value="{{$v->id_ventaneria}}">{{$v->ventaneria}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_ventaneria_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otra_ventaneria">Otro Tipo de Ventana</label>
                                <input type="text" class="form-control" name="otra_ventaneria" id="otra_ventaneria" placeholder="Especifique" maxlength="50">
                            </div>
                        </div>

                        <!-- puertas -->
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_puerta"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Puertas</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="id_puerta" id="id_puerta" required style="width: 100%;" data-select2-id="id_puerta" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">
                                    <option value="" selected disabled>Seleccione Tipo de Puerta</option>
                                    @foreach ($puertas as $p)
                                    <option value="{{$p->id_puerta}}">{{$p->puerta}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_puerta_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otra_puerta">Otro Tipo de Puerta</label>
                                <input type="text" class="form-control" name="otra_puerta" id="otra_puerta" placeholder="Especifique" maxlength="50">
                            </div>
                        </div>
                    </div>
                    <!-- fin descripción fisica -->

                    <!-- caracteristicas -->
                    <div class="tab">
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Producción de Centros</label>
                            </div>
                        </div>
                        <div class="form-row justify-content-center mt-2">
                            <div class="col-10 col-sm-7 col-lg-6">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> ¿El ambiente realiza producción de centros?</label>
                            </div>
                            <div class="form-group col-10 col-sm-3 col-lg-2">
                                <div class="ml-sm-3 d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="produccion_centros" id="produccion_centros_1" value="SI" required>
                                    <label for="produccion_centros_1"></label>
                                </div>
                                <div class="d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="produccion_centros" id="produccion_centros_2" value="NO" required>
                                    <label for="produccion_centros_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center" id="productos_ambiente">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_producto"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Productos Generados en el Ambiente</label>
                                <div class="select2-orange">
                                    <select class="select2" name="id_producto[]" id="id_producto" multiple="multiple" data-placeholder="Autocomplete Productos" data-dropdown-css-class="select2-orange" style="width: 100%;">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Condiciones Relacionadas con la Seguridad</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center mt-2">
                            <div class="col-10 col-sm-7 col-lg-6">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> ¿El ambiente tiene algun tipo de riesgo?</label>
                            </div>
                            <div class="form-group col-10 col-sm-3 col-lg-2">
                                <div class="ml-sm-3 d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="riesgos" id="riesgos_1" value="SI" required>
                                    <label for="riesgos_1"></label>
                                </div>
                                <div class="d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="riesgos" id="riesgos_2" value="NO" required>
                                    <label for="riesgos_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center" id="riesgos_ambiente">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_tipo_riesgo"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Tipos de Riesgo</label>
                                <div class="select2-orange">
                                    <select class="select2" name="id_tipo_riesgo[]" id="id_tipo_riesgo" multiple="multiple" data-placeholder="Seleccione Tipos de Riesgo" data-dropdown-css-class="select2-orange" style="width: 100%;">
                                        @foreach ($tipos_riesgo as $tp)
                                        <option value="{{$tp->id_tipo_riesgo}}">{{$tp->tipo_riesgo}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- selecciono otro -->
                        <div class="form-row justify-content-center" id="otro_tipo_riesgo_row">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otro_tipo_riesgo">Otro Tipo de Riesgo</label>
                                <input type="text" class="form-control" name="otro_tipo_riesgo" id="otro_tipo_riesgo" placeholder="Especifique" maxlength="50">
                            </div>
                        </div>

                        <div class="row justify-content-center mt-4">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Esquemas del Ambiente</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="esquema_ambiente">Esquema Funcional Real</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="esquema_ambiente" id="esquema_ambiente" accept="image/*">
                                    <label class="custom-file-label" for="esquema_ambiente" data-browse="Seleccionar">Seleccione Imagen</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="foto_ambiente">Foto del Ambiente</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="foto_ambiente" id="foto_ambiente" accept="image/*">
                                    <label class="custom-file-label" for="foto_ambiente" data-browse="Seleccionar">Seleccione Imagen</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- fin caracteristicas -->

                    <!-- condiciones de confort -->
                    <div class="tab">
                        <!-- iluminación -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Iluminación</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Natural</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="iluminacion_natural" id="iluminacion_natural_1" value="SI" required>
                                    <label for="iluminacion_natural_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="iluminacion_natural" id="iluminacion_natural_2" value="NO" required>
                                    <label for="iluminacion_natural_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label">Artificial</label>
                                <input type="text" class="form-control" name="iluminacion_artificial" id="iluminacion_artificial" placeholder="Ej: Fluorecente T8" maxlength="50">
                            </div>
                        </div>
                        <!-- fin iluminación -->

                        <!-- ventilación -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Ventilación</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Natural</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="ventilacion_natural" id="ventilacion_natural_1" value="SI" required>
                                    <label for="ventilacion_natural_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="ventilacion_natural" id="ventilacion_natural_2" value="NO" required>
                                    <label for="ventilacion_natural_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label">Mecánica</label>
                                <input type="text" class="form-control" name="ventilacion_mecanica" id="ventilacion_mecanica" placeholder="Ej: Ventilador" maxlength="50">
                            </div>
                        </div>
                        <!-- fin ventilación -->

                        <!-- instalaciones de gas -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Instalaciones de Gas</label>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Gas Natural</h5>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="gas_natural" id="gas_natural_1" value="SI" required>
                                    <label for="gas_natural_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="gas_natural" id="gas_natural_2" value="NO" required>
                                    <label for="gas_natural_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Gas Propano</h5>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="gas_propano" id="gas_propano_1" value="SI" required>
                                    <label for="gas_propano_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="gas_propano" id="gas_propano_2" value="NO" required>
                                    <label for="gas_propano_2"></label>
                                </div>
                            </div>
                        </div>
                        <!-- fin instalaciones de gas -->

                        <!-- instalaciones eléctricas -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Instalaciones Eléctricas</label>
                            </div>
                        </div>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Bifásica</h5>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="electrica_bifasica" id="electrica_bifasica_1" value="SI" required>
                                    <label for="electrica_bifasica_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="electrica_bifasica" id="electrica_bifasica_2" value="NO" required>
                                    <label for="electrica_bifasica_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Trifásica</h5>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="electrica_trifasica" id="electrica_trifasica_1" value="SI" required>
                                    <label for="electrica_trifasica_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="electrica_trifasica" id="electrica_trifasica_2" value="NO" required>
                                    <label for="electrica_trifasica_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Red Regulada</h5>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="electrica_regulada" id="electrica_regulada_1" value="SI" required>
                                    <label for="electrica_regulada_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="electrica_regulada" id="electrica_regulada_2" value="NO" required>
                                    <label for="electrica_regulada_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Red de Datos</h5>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="red_datos" id="red_datos_1" value="SI" required>
                                    <label for="red_datos_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="red_datos" id="red_datos_2" value="NO" required>
                                    <label for="red_datos_2"></label>
                                </div>
                            </div>
                        </div>
                        <!-- fin instalaciones eléctricas -->

                        <!-- instalaciones hidrosanitarias -->
                        <div class="row justify-content-center mt-2">
                            <div class="col-10 col-lg-8">
                                <label class="form-label color-naranja">Instalaciones Hidrosanitarias</label>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4 col-lg-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Agua Fría</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_aguafria" id="hidro_aguafria_1" value="SI" required>
                                    <label for="hidro_aguafria_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_aguafria" id="hidro_aguafria_2" value="NO" required>
                                    <label for="hidro_aguafria_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4 col-lg-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Agua Caliente</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_aguacaliente" id="hidro_aguacaliente_1" value="SI" required>
                                    <label for="hidro_aguacaliente_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_aguacaliente" id="hidro_aguacaliente_2" value="NO" required>
                                    <label for="hidro_aguacaliente_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4 col-lg-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Lavaojos</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_lavaojos" id="hidro_lavaojos_1" value="SI" required>
                                    <label for="hidro_lavaojos_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_lavaojos" id="hidro_lavaojos_2" value="NO" required>
                                    <label for="hidro_lavaojos_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4 col-lg-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Banco de Hielo</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_banco_hielo" id="hidro_banco_hielo_1" value="SI" required>
                                    <label for="hidro_banco_hielo_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_banco_hielo" id="hidro_banco_hielo_2" value="NO" required>
                                    <label for="hidro_banco_hielo_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4 col-lg-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Trampa de Grasas</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_trampa_grasas" id="hidro_trampa_grasas_1" value="SI" required>
                                    <label for="hidro_trampa_grasas_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_trampa_grasas" id="hidro_trampa_grasas_2" value="NO" required>
                                    <label for="hidro_trampa_grasas_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4 col-lg-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Vapor</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_vapor" id="hidro_vapor_1" value="SI" required>
                                    <label for="hidro_vapor_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_vapor" id="hidro_vapor_2" value="NO" required>
                                    <label for="hidro_vapor_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4 col-lg-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Desarenador</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_desarenador" id="hidro_desarenador_1" value="SI" required>
                                    <label for="hidro_desarenador_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_desarenador" id="hidro_desarenador_2" value="NO" required>
                                    <label for="hidro_desarenador_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4 col-lg-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Lavamanos</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_lavamanos" id="hidro_lavamanos_1" value="SI" required>
                                    <label for="hidro_lavamanos_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_lavamanos" id="hidro_lavamanos_2" value="NO" required>
                                    <label for="hidro_lavamanos_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-4 col-lg-4">
                                <label class="form-label"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Desagües</label>
                            </div>
                            <div class="form-group col-6 col-lg-4">
                                <div class="d-inline">
                                    <label>Si</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_desagues" id="hidro_desagues_1" value="SI" required>
                                    <label for="hidro_desagues_1"></label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <label>No</label>
                                </div>
                                <div class="icheck-orange d-inline">
                                    <input type="radio" name="hidro_desagues" id="hidro_desagues_2" value="NO" required>
                                    <label for="hidro_desagues_2"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="otras_instalaciones_hidrosanitarias">Otras Instalaciones Hidrosanitarias</label>
                                <input type="text" class="form-control" name="hidro_otro" id="hidro_otro" placeholder="Especifique" maxlength="50">
                            </div>
                        </div>
                        <!-- fin condiciones hidrosanitarias-->

                        <!-- observaciones generales-->
                        <div class="form-row justify-content-center mt-2">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label color-naranja" for="observaciones_generales">Observaciones Generales</label>
                                <textarea class="form-control" name="observaciones_generales" id="observaciones_generales" placeholder="Descripción de Observaciones al Ambiente" rows="3"></textarea>
                            </div>
                        </div>
                        <!-- fin observaciones generales-->
                    </div>
                    <!-- fin condiciones de confort -->

                    <div class="row justify-content-center mt-3">
                        <div class="form-group col-10 col-lg-8">
                            <div style="overflow:auto;">
                                <button type="button" class="btn w-100 btn-naranja" id="nextBtn" onclick="nextPrev(1)">Siguiente</button>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <div style="overflow:auto;">
                                <button type="button" class="btn w-100 btn-naranja mb-3" id="prevBtn" onclick="nextPrev(-1)">Anterior</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- modal descripcion area de cualificacion -->
        <div class="modal fade" id="modal-area-cualificacion" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modal-area-cualificacion-titulo" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-10 offset-1">
                            <h5 class="modal-title text-center" id="modal-area-cualificacion-titulo">Descripción Area de Cualificación</h5>
                        </div>
                        <div class="col-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-modal">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <p class="text-center" id="descripcion-sin-area-cualificacion">Seleccione una área de cualificación para visualizar su descripción</p>
                        <p id="descripcion-area-cualificacion"></p>
                    </div>
                </div>
            </div>
        </div>

        <!-- modal registro sede -->
        <div class="modal fade" id="registro-sede" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="registro-sede-titulo" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-10 offset-1">
                            <h5 class="modal-title text-center" id="registro-sede-titulo">Registro Sede</h5>
                        </div>
                        <div class="col-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form class="needs-validation" method="POST" action="{{route('registro.sedes')}}" novalidate>
                            @csrf
                            <div class="form-row justify-content-center">
                                <div class="form-group col-10 col-lg-8">
                                    <label class="form-label" for="sede"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Nombre Sede</label>
                                    <input type="text" class="form-control" name="sede" id="sede" placeholder="Ej: Teleinformatica Norte" maxlength="50" autofocus required>
                                    <div class="invalid-feedback">
                                        Escriba el Nombre de la Sede
                                    </div>
                                </div>
        
                                <div class="form-group col-10 col-lg-8">
                                    <label class="form-label" for="direccion"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Dirección</label>
                                    <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Ej: Cll 50A #2-14" maxlength="40" required>
                                    <div class="invalid-feedback">
                                        Escriba la Dirección de la Sede
                                    </div>
                                </div>
        
                                <div class="form-group col-10 col-lg-8">
                                    <label class="form-label" for="telefono"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Teléfono</label>
                                    <input type="number" class="form-control" name="telefono" id="telefono" placeholder="Ej: 0323434543" min="1" max="999999999" required>
                                    <div class="invalid-feedback">
                                        Escriba el Teléfono de la Sede
                                    </div>
                                </div>
                                
                                <div class="form-group col-10 col-lg-8 mt-3 mb-5">
                                    <button type="submit" class="btn btn-naranja w-100">Registrar</button>
                                </div>       
                            </div>
                        </form>      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('js/registro_ambiente.js')}}"></script>
<script>
	$(document).ready(function() {
        $("#id_producto").select2({
            ajax: {
                url: "{{ route('autocompletar.productos') }}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        autocompletar_producto: params.term,
                    };
                },

                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj) {
                            return {
                                id: obj.value,
                                text: obj.label
                            };
                        })
                    };
                    
                },
                cache: true
            }
        });
	});
</script>
@endsection
