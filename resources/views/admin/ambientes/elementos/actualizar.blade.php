@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 text-left">
        <a class="btn btn-lg" data-toggle="tooltip" href="{{route('ambientes.elementos.index',$elemento->id_ambiente)}}" title="Regresar">
            <i class="fas fa-arrow-alt-circle-left fa-2x" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-1 justify-content-center">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mt-2 mb-4 text-center">
                        <h3 class="card-title">Actualizar Elemento</h3>
                    </div>
                </div>
                <form class="needs-validation" method="POST" action="{{route('ambientes.elementos.update',[$elemento->id_elemento_ambiente, $elemento->id_ambiente])}}" novalidate>
                    @csrf
                    @method('PATCH')
                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="numero_inventario">Número Inventario</label>
                            <input type="text" class="form-control" id="numero_inventario" name="numero_inventario" maxlength="20" value="{{$elemento->numero_inventario}}" autofocus>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="serial">Serial</label>
                            <input type="text" class="form-control" id="serial" name="serial" maxlength="20" value="{{$elemento->serial}}">
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-8">
                            <label class="form-label" for="id_categoria_elemento">Categoria</label>
                            <select class="custom-select" name="id_categoria_elemento" id="id_categoria_elemento">
                                <option value="{{$elemento->subcategoria_elemento->categoria_elemento->id_categoria_elemento}}" selected>{{$elemento->subcategoria_elemento->categoria_elemento->categoria_elemento}}</option>
                                @foreach ($categorias as $categoria)
                                    <option value="{{$categoria->id_categoria_elemento}}">{{$categoria->categoria_elemento}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-8">
                            <label class="form-label" for="subcategoria_elemento_id">Subcategoria</label>
                            <select class="custom-select" name="subcategoria_elemento_id" id="subcategoria_elemento_id" src="{{route('elementos_subcategorias.getsubcategorias','#')}}">
                                <option value="{{$elemento->subcategoria_elemento_id}}" selected>{{$elemento->subcategoria_elemento->subcategoria_elemento}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="elemento_ambiente">Nombre Elemento</label>
                            <input type="text" class="form-control" id="elemento_ambiente" name="elemento_ambiente" maxlength="40" value="{{$elemento->elemento_ambiente}}">
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="cantidad">Cantidad</label>
                            <input type="text" class="form-control" id="cantidad" maxlength="3" name="cantidad" value="{{$elemento->cantidad}}">
                        </div>

                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="estado">Estado Funcionamiento</label>
                            <select class="custom-select" id="estado" name="estado">
                                <option value="{{$elemento->estado}}" selected>{{$elemento->estado}}</option>
                                @foreach ($estados as $estado)
                                    <option value="{{$estado}}">{{$estado}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="fecha_adquisicion">Fecha Adquisición</label>
                            <input type="date" class="form-control" id="fecha_adquisicion" name="fecha_adquisicion" value="{{$elemento->fecha_adquisicion}}">
                        </div>
                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="tiempo_vida_util">Tiempo Vida Util (años)</label>
                            <input type="text" class="form-control" id="tiempo_vida_util" maxlength="3" name="tiempo_vida_util" value="{{$elemento->tiempo_vida_util}}">
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="origen_elemento_id">Origen</label>
                            <select class="custom-select" name="origen_elemento_id" id="origen_elemento_id">
                                <option value="{{$elemento->origen_elemento->id_origen_elemento}}" selected>{{$elemento->origen_elemento->origen_elemento}}</option>
                                @foreach ($origenes as $origen)
                                    <option value="{{$origen->id_origen_elemento}}">{{$origen->origen_elemento}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-10 col-lg-4">
                            <label class="form-label" for="precio">Precio</label>
                            <input type="int" class="form-control" id="precio" name="precio" value="{{$elemento->precio}}">
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group col-10 col-lg-8">
                            <label class="form-label" for="observacion_general">Observaciones Generales</label>
                            <textarea class="form-control" id="observacion_general" name="observacion_general" rows="2">{{$elemento->observacion_general}}</textarea>
                        </div>
                    </div>

                    <div class="form-row justify-content-center mt-3 mb-4">
                        <div class="form-group col-10 col-lg-8">
                            <button type="submit" class="btn btn-naranja w-100">Actualizar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/registro.js')}}"></script>
@endsection
