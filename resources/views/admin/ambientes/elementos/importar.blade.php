@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 text-left">
        <a class="btn btn-lg" href="{{route('ambientes.elementos.index',$id_ambiente)}}">
            <i class="fas fa-arrow-alt-circle-left fa-2x" id="icono-v" data-toggle="tooltip" title="Regresar"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-1 justify-content-center">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mt-2 mb-4 text-center">
                        <h3 class="card-title">Importar Elementos</h3>
                    </div>
                </div>
                <!-- formulario Equipos-->
                <form class="needs-validation" method="POST" action="{{route('almacenar.elementos',$id_ambiente)}}" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="form-row justify-content-center mt-4">
                        <div class="form-group col-8">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="elementos" id="elementos" accept=".xls,.xlsx">
                                <label class="custom-file-label" for="elementos" data-browse="Seleccionar">Archivo Excel</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center mt-2 mb-4">
                        <div class="form-group col-10 col-lg-8">
                            <button type="submit" class="btn btn-naranja w-100">Importar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/registro.js')}}"></script>
@endsection
