@extends('admin.layouts.app')
@section('content')
<div class="row align-items-center mb-3">
    <div class="col-1 d-none d-lg-flex text-left">
        <a href="{{route('ambientes.index')}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-8 offset-2 offset-lg-1">
        <h3 class="card-title">Gestionar Elementos del Ambiente</h3>
    </div>
    <div class="col-1 offset-1">
        <a href="{{ route('ambientes.elementos.create',$id_ambiente) }}" type="button" class="btn btn-lg float-right">
            <i class="fas fa-plus-circle fa-2x" id="icono-v" data-toggle="tooltip" title="Asociar Elemento"></i>
        </a>
    </div>
</div>
@if (count($elementos) > 0)
    <div class="row">
        <div class="col-12">
            <div class="card card-outline card-sena elevation-2">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="table-header">
                                <tr>
                                    <th>N°</th>
                                    <th class="text-truncate" style="max-width: 150px;">Número Inventario</th>
                                    <th>Elemento</th>
                                    <th class="text-center">Cantidad</th>
                                    <th>Estado</th>
                                    <th class="text-truncate" style="max-width: 150px;">Fecha Adquisición</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($elementos as $elemento)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td class="text-truncate" style="max-width: 150px;">{{$elemento->numero_inventario}}</td>
                                        <td class="text-truncate" style="max-width: 300px;">{{$elemento->elemento_ambiente}}</td>
                                        <td class="text-center">{{$elemento->cantidad}}</td>
                                        <td>{{$elemento->estado}}</td>
                                        <td>{{$elemento->fecha_adquisicion}}</td>
                                        <td class="text-center">
                                            <div class="row justify-content-center">
                                                <div class="col-5 col-lg-3">
                                                    <a href="{{ route('ambientes.elementos.show', [$elemento->id_elemento_ambiente,$elemento->id_ambiente]) }}" type="button">
                                                        <i class="fas fa-eye" data-toggle="tooltip" title="Detalles Elemento"></i>
                                                    </a>
                                                </div>
                                                <div class="col-5 col-lg-3">
                                                    <form action="{{ route('ambientes.elementos.destroy', [$elemento->id_elemento_ambiente, $id_ambiente]) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn p-0" onclick="return confirm('¿Seguro que desea Eliminar el Elemento del Ambiente?')">
                                                            <i class="fas fa-trash-alt" id="icono-v" data-toggle="tooltip" title="Eliminar Elemento"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {{ $elementos->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <h4 class="mt-5" style="text-align: center">No hay elementos asociados al ambiente</h4>
@endif
@endsection
