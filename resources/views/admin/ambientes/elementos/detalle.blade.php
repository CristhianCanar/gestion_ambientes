@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('ambientes.elementos.index', $elemento->id_ambiente)}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mt-2 mb-4 text-center">
                        <h3 class="card-title">Detalles Elemento</h3>
                    </div>
                </div>
                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="numero_inventario">Número de Inventario</label>
                        <h6>{{$elemento->numero_inventario}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="serial">Serial</label>
                        <h6>{{$elemento->serial}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="id_categoria_elemento">Categoria</label>
                        <h6>{{$elemento->subcategoria_elemento->categoria_elemento->categoria_elemento}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="subcategoria_elemento_id">Subcategoria</label>
                        <h6>{{$elemento->subcategoria_elemento->subcategoria_elemento}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="elemento_ambiente">Elemento</label>
                        <h6>{{$elemento->elemento_ambiente}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-4">
                        <label class="form-label" for="cantidad">Cantidad</label>
                        <h6>{{$elemento->cantidad}}</h6>
                    </div>

                    <div class="form-group col-10 col-lg-4">
                        <label class="form-label" for="estado">Estado Funcionamiento</label>
                        <h6>{{$elemento->estado}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-4">
                        <label class="form-label" for="fecha_adquisicion">Fecha de Adquisición</label>
                        <h6>{{$elemento->fecha_adquisicion}}</h6>
                    </div>
                    <div class="form-group col-10 col-lg-4">
                        <label class="form-label" for="tiempo_vida_util">Tiempo de Vida Util</label>
                        <h6>{{$elemento->tiempo_vida_util}} años</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-4">
                        <label class="form-label" for="origen_elemento_id">Origen</label>
                        <h6>{{$elemento->origen_elemento->origen_elemento}}</h6>
                    </div>
                    <div class="form-group col-10 col-lg-4">
                        <label class="form-label" for="precio">Precio</label>
                        <h6>${{$elemento->precio}}</h6>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <div class="form-group col-10 col-lg-8">
                        <label class="form-label" for="observacion_general">Observaciones Generales</label>
                        @if ($elemento->observacion_general != null)
                            <p>{{$elemento->observacion_general}}</p>
                        @else
                            <h6>No se han registrado observaciones</h6>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
