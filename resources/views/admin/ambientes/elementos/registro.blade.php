@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-1 d-none d-lg-flex">
        <a href="{{route('ambientes.elementos.index',$id_ambiente)}}" class="btn btn-lg">
            <i class="fas fa-arrow-alt-circle-left fa-2x" data-toggle="tooltip" title="Regresar" id="icono-v"></i>
        </a>
    </div>
    <div class="col-12 col-sm-10 col-lg-8 offset-sm-1">
        <div class="card card-outline card-sena elevation-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 mt-2 mb-4 text-center">
                        <h3 class="card-title">Asociar Elemento</h3>
                    </div>
                </div>
                @if ($validacion == 1)
                    <form class="needs-validation" method="POST" action="{{route('validacion.elemento',$id_ambiente)}}" novalidate>
                        @csrf
                        <div class="form-row justify-content-center mb-4">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="numero_inventario"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Número de Inventario</label>
                                <input type="number" class="form-control" id="numero_inventario" name="numero_inventario" placeholder="Ej: 922111874" min="1" max="9999999999" autofocus required>
                                <div class="invalid-feedback">
                                    Escriba el Número de Inventario del Elemento
                                </div>
                            </div>
                            <div class="form-group col-1 mt-2">
                                <label for=""></label>
                                <button class="btn btn-lg" type="submit">
                                    <i class="fas fa-search" id="icono-v" data-toggle="tooltip" title="Buscar"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                @else
                    <form class="needs-validation" method="POST" action="{{route('ambientes.elementos.store',$id_ambiente)}}" novalidate>
                        @csrf
                        <input type="hidden" class="form-control" id="id_ambiente" name="id_ambiente" value="{{$id_ambiente}}">
                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="numero_inventario">Número de Inventario</label>
                                <input type="hidden" class="form-control" id="numero_inventario" name="numero_inventario" value="{{$elemento->numero_inventario}}">
                                <input type="text" class="form-control" value="{{$elemento->numero_inventario}}" disabled>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="serial">Serial</label>
                                <input type="hidden" class="form-control" id="serial" name="serial" value="{{$elemento->serial}}">
                                <input type="text" class="form-control" id="serial" name="serial" maxlength="20" value="{{$elemento->serial}}" disabled>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="id_categoria_elemento">Categoria</label>
                                <input type="hidden" class="form-control" id="id_categoria_elemento" name="id_categoria_elemento" value="{{$categoria->id_categoria_elemento}}">
                                <input type="text" class="form-control" value="{{$categoria->categoria_elemento}}" disabled>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="subcategoria_elemento_id">Subcategoria</label>
                                <input type="hidden" class="form-control" id="subcategoria_elemento_id" name="subcategoria_elemento_id" value="{{$elemento->subcategoria_elemento_id}}">
                                <input type="text" class="form-control" value="{{$subcategoria->subcategoria_elemento}}" disabled>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="elemento_ambiente">Nombre Elemento</label>
                                <input type="hidden" class="form-control" id="elemento_ambiente" name="elemento_ambiente" maxlength="45" value="{{$elemento->elemento_ambiente}}">
                                <input type="text" class="form-control" maxlength="45" value="{{$elemento->elemento_ambiente}}" disabled>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-4">
                                <label class="form-label" for="cantidad">Cantidad</label>
                                <input type="hidden" class="form-control" maxlength="3" id="cantidad" name="cantidad" value="{{$elemento->cantidad}}">
                                <input type="number" class="form-control" id="cantidad" name="cantidad" value="{{$elemento->cantidad}}" min="1" max="99" disabled>
                            </div>

                            <div class="form-group col-10 col-lg-4">
                                <label class="form-label" for="estado"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Estado de Funcionamiento</label>
                                <select class="form-control select2 select2-hidden-accessible select2-orange" name="estado" id="estado" required style="width: 100%;" data-select2-id="estado" tabindex="-1" aria-hidden="true" data-dropdown-css-class="select2-orange">                              
                                    <option value="{{$elemento->estado}}" selected>{{$elemento->estado}}</option>
                                    @foreach ($estados as $estado)
                                        <option value="{{$estado}}">{{$estado}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione el Estado de Funcionamiento del Elemento
                                </div>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-4">
                                <label class="form-label" for="fecha_adquisicion"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Fecha de Adquisición</label>
                                <input type="date" class="form-control" id="fecha_adquisicion" name="fecha_adquisicion" value="{{$elemento->fecha_adquisicion}}" required>
                                <div class="invalid-feedback">
                                    Seleccione la Fecha de Adquisición del Elemento
                                </div>
                            </div>
                            <div class="form-group col-10 col-lg-4">
                                <label class="form-label" for="origen_elemento_id">Origen de Adquisición</label>
                                <input type="hidden" class="form-control" id="origen_elemento_id" name="origen_elemento_id" value="{{$origen->id_origen_elemento}}">
                                <input type="text" class="form-control" value="{{$origen->origen_elemento}}" disabled>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-4">
                                <label class="form-label" for="tiempo_vida_util"><span class="obligatorio" data-toggle="tooltip" title="Campo Obligatorio">*</span> Tiempo de Vida Util (años)</label>
                                <input type="number" class="form-control" id="tiempo_vida_util" name="tiempo_vida_util" value="{{$elemento->tiempo_vida_util}}" placeholder="Ej: 3" min="1" max="99" required>
                                <div class="invalid-feedback">
                                    Escriba el Tiempo de Vida Util (años) del Elemento
                                </div>
                            </div>
                            <div class="form-group col-10 col-lg-4">
                                <label class="form-label" for="precio">Precio</label>
                                <input type="hidden" class="form-control" id="precio" name="precio" value="{{$elemento->precio}}">
                                <input type="number" class="form-control" value="{{$elemento->precio}}" disabled>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-10 col-lg-8">
                                <label class="form-label" for="observacion_general">Observaciones Generales</label>
                                <textarea class="form-control" id="observacion_general" name="observacion_general" rows="3">{{$elemento->observacion_general}}</textarea>
                            </div>
                        </div>

                        <div class="form-row justify-content-center mt-3 mb-4">
                            <div class="form-group col-10 col-lg-8">
                                <button type="submit" class="btn btn-naranja w-100">Asociar</button>
                            </div>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/registro_elemento.js')}}"></script>
@endsection
