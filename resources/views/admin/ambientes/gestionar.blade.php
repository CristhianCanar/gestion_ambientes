@extends('admin.layouts.app')
@section('content')
    <div class="row mb-4">
        <div class="col-12 col-lg-6 offset-lg-3">
            <h3 class="card-title">Gestionar Ambientes de Aprendizaje</h3>
        </div>
        <div class="col-12 col-sm-10 col-lg-3 offset-sm-1 offset-lg-0 mt-4 mt-lg-0">
            <form action="{{route('buscar.ambiente')}}" method="POST">
                @csrf
                <div class="input-group">
                    <input type="text" name="buscar_ambiente" class="form-control" placeholder="Buscar Ambiente Aprendizaje">
                    <div class="input-group-append">
                        <div class="input-group-text pt-0 pb-0">
                            <button class="btn p-0" type="submit">
                                <i class="fas fa-search color-naranja"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if (count($ambientes) > 0)
        <div class="row">
            <div class="col-12">
                <div class="card card-outline card-sena elevation-1">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="table-header">
                                    <tr>
                                        <th>N°</th>
                                        <th>Ambiente Aprendizaje</th>
                                        <th>Tipo</th>
                                        <th class="text-center text-truncate" style="max-width: 120px;">N° Aprendices</th>
                                        <th>Centro Formación</th>
                                        <th class="text-center">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody class="table-body">
                                    @foreach ($ambientes as $ambiente)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td class="text-truncate" style="max-width: 130px;">{{$ambiente->nombre_ambiente}}</td>
                                            <td>{{$ambiente->tipo_ambiente->tipo_ambiente}}</td>
                                            <td class="text-center">{{$ambiente->cantidad_aprendices}}</td>
                                            <td class="text-truncate" style="max-width: 330px;">{{$ambiente->centro_formacion->centro_formacion}}</td>
                                            <td class="align-top align-lg-middle">
                                                <div class="form-row justify-content-lg-center">
                                                    <div class="col-4 col-lg-2">
                                                        <a  href="{{route('ambientes.show',$ambiente->id_ambiente)}}">
                                                            <i class="fas fa-eye" data-toggle="tooltip" title="Detalles Ambiente"></i>
                                                        </a>
                                                    </div>

                                                    <div class="col-4 col-lg-2">
                                                        <a  href="{{route('ambientes.edit',$ambiente->id_ambiente)}}">
                                                            <i class="fas fa-edit" data-toggle="tooltip" title="Actualizar Ambiente"></i>
                                                        </a>
                                                    </div>

                                                    <div class="col-4 col-lg-2">
                                                        <a href="{{route('programas_formacion_ambiente.gestionar_programas_ambiente',$ambiente->id_ambiente)}}">
                                                            <i class="fas fa-graduation-cap" data-toggle="tooltip" title="Gestionar Programas"></i>
                                                        </a>
                                                    </div>

                                                    <div class="col-4 col-lg-2">
                                                        <a href="{{route('espacios_complementarios.gestionar_espacios_complementarios',$ambiente->id_ambiente)}}">
                                                            <i class="fas fa-kaaba" data-toggle="tooltip" title="Gestionar Espacios"></i>
                                                        </a>
                                                    </div>
                                                    
                                                    <div class="col-4 col-lg-2">
                                                        <a href="{{route('ambientes.elementos.index',$ambiente->id_ambiente)}}">
                                                            <i class="fas fa-tools" data-toggle="tooltip" title="Gestionar Elementos"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="float-right">
                                {{ $ambientes->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <h4 class="mt-5" style="text-align: center">No hay ambientes de formación registrados</h4>
    @endif
@endsection
