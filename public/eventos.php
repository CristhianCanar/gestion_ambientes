<?php 

$inicio = new DateTime($_POST['start']);
$fin    = new DateTime($_POST['end']);

/**
 * 
 * Hacer la consulta con sacando los datos de los Objetos
 * $inicio
 * $fin
 * ejemplo:
 * ->Where('inicio','>=', $inicio->format('YYYY-mm-dd H:i:s'))  // https://www.php.net/manual/es/function.date.php
 */

$eventos = array();

for( $i =0; $i< rand(2,5); $i++){

    $eventos[$i] = new stdClass();
    $eventos[$i]->title  = "Evento ".($i+1);
    $h = rand(0,23);
    $m = rand(0,30);
    $eventos[$i]->start  = $inicio->format('Y-m-d '.$h.':'.$m.':00');
    $eventos[$i]->end    = $inicio->format("Y-m-d $h:".($m+rand(10,30).':00'));
    $eventos[$i]->allDay = false;
    $eventos[$i]->color  = 'red';
    $eventos[$i]->textColor = 'black';

}

echo  json_encode($eventos);
