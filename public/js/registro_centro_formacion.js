/* municipios */
$("#id_regional").on('change', function () {
    if ($(this).val().length == 0) {
      return false;
    }
    else {
      $("#id_municipio")
        .load($("#id_municipio").attr('src').replace('#', $(this).val()), function () {
          $("#id_municipio").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Municipio'))
        });
    }
}); 