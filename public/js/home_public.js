document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems,{
        coverTrigger: false,
        hover : true
    });
    
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems);
    
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems);

    var elems = document.querySelectorAll('.parallax');
    var instances = M.Parallax.init(elems);

    var elems = document.querySelectorAll('.datepicker');
    var instances = M.Datepicker.init(elems, options);
});

window.addEventListener('scroll', Navbar);

function Navbar() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        document.getElementById("encabezado").style.display="none";
    } 
    else {
        document.getElementById("encabezado").style.display="block";
    }
}


window.addEventListener('scroll', Animaciones);

function Animaciones(){
    var seccion2 = document.getElementById("animation-text");
    var posicion_seccion2 = seccion2.getBoundingClientRect().top;
    var tamano_pantalla = window.innerHeight/3;
    if (posicion_seccion2 < tamano_pantalla){
        seccion2.style.animation = 'mover 3s';
    }
}
