$(document).ready(function(){
    /* lineas tematicas de la red de conocimiento */
    $("#red_conocimiento_id").on('change', function () {
        $("#id_linea_tematica").empty()
        if ($(this).val().length == 0) {
            return false;
        } 
        else {
            $("#id_linea_tematica")
            .load($("#id_linea_tematica").attr('src').replace('#', $(this).val()), function () {
                $("#id_linea_tematica").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Linea Tecnológica'))
            });
        }
    });
});
