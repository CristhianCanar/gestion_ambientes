$("#fecha_fin,#fecha_inicio").change(function () {
    if ($("#fecha_fin").val() <= $("#fecha_inicio").val()) {
        $("#invalid-date").text("La Fecha de Finalización debe ser Mayor a la Fecha de Apertura");
    } 
    else {
        $("#invalid-date").text("");
    } 
});
