/* lineas tematicas */
$("#idredconocimiento").on('change', function () {
    $("#id_linea_tematica, #id_programa").empty();
    if ($(this).val().length == 0) {
      return false;
    }
    else {
      $("#id_linea_tematica")
        .load($("#id_linea_tematica").attr('src').replace('#', $(this).val()), function () {
          $("#id_linea_tematica").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Linea Tecnológica'))
        });
    }
  });
    
  /* programas de formacion */
  $("#id_linea_tematica").on('change', function () {
    $("#id_programa").empty()
    if ($(this).val().length == 0) {
      return false;
    } else {
      $("#id_programa")
        .load($("#id_programa").attr('src').replace('#', $(this).val()), function () {
          $("#id_programa").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Programa de Formación'))
        });
    }
  });
  
  /* subcategorias de elementos */
  $("#id_categoria_elemento").on('change', function () {
    $("#subcategoria_elemento_id").empty()
    if ($(this).val().length == 0) {
      return false;
    } else {
      $("#subcategoria_elemento_id")
        .load($("#subcategoria_elemento_id").attr('src').replace('#', $(this).val()), function () {
          $("#subcategoria_elemento_id").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Tipo de Subcategoria'))
        });
    }
});
