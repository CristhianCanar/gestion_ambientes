var checkbox_ambiente = document.getElementById("ambientes");
var checkbox_centro_formacion = document.getElementById("centros_formacion");
var checkbox_red_conocimiento = document.getElementById("red_conocimiento");
var checkbox_programa_formacion = document.getElementById("programas_formacion");
checkbox_ambiente.addEventListener('click', Seleccionar);
checkbox_centro_formacion.addEventListener('click', Seleccionar);
checkbox_red_conocimiento.addEventListener('click', Seleccionar);
checkbox_programa_formacion.addEventListener('click', Seleccionar);

function Seleccionar() {
    var items_ambiente = document.getElementsByName("ambiente[]");
    for (i = 0; i < items_ambiente.length; i++) {
        if (checkbox_ambiente.checked == true) {
            items_ambiente[i].checked = true;
        }
        else {
            items_ambiente[i].checked = false;
        }
    }

    var items_centro_formacion = document.getElementsByName("centro_formacion[]");
    for (i = 0; i < items_centro_formacion.length; i++) {
        if (checkbox_centro_formacion.checked == true) {
            items_centro_formacion[i].checked = true;
        }
        else {
            items_centro_formacion[i].checked = false;
        }
    }

    var items_red_conocimiento = document.getElementsByName("red_conocimiento[]");
    for (i = 0; i < items_red_conocimiento.length; i++) {
        if (checkbox_red_conocimiento.checked == true) {
            items_red_conocimiento[i].checked = true;
        }
        else {
            items_red_conocimiento[i].checked = false;
        }
    }

    var items_programa_formacion = document.getElementsByName("programa_formacion[]");
    for (i = 0; i < items_programa_formacion.length; i++) {
        if (checkbox_programa_formacion.checked == true) {
            items_programa_formacion[i].checked = true;
        }
        else {
            items_programa_formacion[i].checked = false;
        }
    }

}