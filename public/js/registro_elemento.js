/* validacion input file imagen */
$(".custom-file-input").on("change", function() {
    var archivo = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(archivo);
  });
  /* fin validacion input file imagen */

/* subcategorias de elementos */
$("#id_categoria_elemento").on('change', function() {
    $("#subcategoria_elemento_id").empty()
    if ($(this).val().length == 0) {
      return false;
    } else {
      $("#subcategoria_elemento_id")
        .load($("#subcategoria_elemento_id").attr('src').replace('#', $(this).val()), function () {
          $("#subcategoria_elemento_id").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Subcategoria'))
        });
    }
});
$("#precio").on('keyup', function() {
  //var precioMiles = new Intl.NumberFormat("es-CO").format(parseInt($(this).val(), 10))
  //$("#precio").val(precioMiles)
  //var n = new Number($(this).val());
  //var fin = n.toLocaleString("es-CO", n)
  var precios = ($(this).val()/1000).toFixed(3)
  console.log(precios)
});
