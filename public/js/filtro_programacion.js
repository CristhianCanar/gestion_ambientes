$(document).ready(function(){
    /* lineas tematicas de la red de conocimiento */
    $("#red_conocimiento_id").on('change', function () {
        $("#linea_tematica_id").empty()
        if ($(this).val().length == 0) {
            return false;
        } 
        else {
            $("#linea_tematica_id")
            .load($("#linea_tematica_id").attr('src').replace('#', $(this).val()), function () {
                $("#linea_tematica_id").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Linea Tecnologica'))
            });
        }
    });

    /* programas de formacion de la linea tematica */
    $("#linea_tematica_id").on('change', function () {
        $("#programa_formacion_id").empty()
        if ($(this).val().length == 0) {
            return false;
        } 
        else {
            $("#programa_formacion_id")
            .load($("#programa_formacion_id").attr('src').replace('#', $(this).val()), function () {
                $("#programa_formacion_id").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Programa de Formación'))
            });
        }
    });

    /* ambientes del formacion del programa */
    $("#programa_formacion_id").on('change', function () {
        $("#ambiente_id").empty()
        if ($(this).val().length == 0) {
            return false;
        } 
        else {
            $("#ambiente_id")
            .load($("#ambiente_id").attr('src').replace('#', $(this).val()), function () {
                $("#ambiente_id").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Ambiente de Aprendizaje'))
            });
        }
    });

    /* fichas del programa de formacion */
    $("#programa_formacion_id").on('change', function () {
        if ($(this).val().length == 0) {
            return false;
        } 
        else {
            $("#ficha_id")
            .load($("#ficha_id").attr('src').replace('#', $(this).val()), function () {
                $("#ficha_id").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Número de Ficha'))
            });
        }
    });
   
    /* validacion de campos no obligatorios */
    $("#form-filtro").submit(function (event) {
        if ($("#ambiente_id").val() == null && $("#ficha_id").val() == null && $("#instructor_id").val() == null) {
            $("#validacion-filtro").text("Seleccione un Ambiente de Aprendizaje o Número de Ficha o Instructor para Filtrar la Programación");
            event.preventDefault();
        }
    }); 

    $("#ambiente_id, #ficha_id, #instructor_id").on('change', function () {
        if ($("#ambiente_id").val() != null || $("#ficha_id").val() != null || $("#instructor_id").val() != null) {
            $("#validacion-filtro").text("");
        }
    });
    
});
