$("#nueva_contrasena").click(function (){
    $(".input_nueva_contasena").removeClass("d-none");
    $("#new_password").attr("required", "true");
    $("#confirm_new_password").attr("required", "true");
    
    $("#confirm_new_password").keyup(function (){
        if ($("#new_password").val() !== $("#confirm_new_password").val()) {
            $("#invalid-contrasena").text("La Nueva Contraseña y Confirmar Nueva Contraseña No Coinciden");
        }
        else {
            $("#invalid-contrasena").text("");
        }
    });
});

$("#ocultar_nueva_contrasena").click(function (){
    $(".input_nueva_contasena").addClass("d-none");
    $("#new_password").removeAttr("required");
    $("#confirm_new_password").removeAttr("required");
});

$("#mostrar-password").click(function (){
    if ($(this).hasClass("fa-eye-slash") == true) {
        $(this).removeClass("fa-eye-slash").addClass("fa-eye");
        $("#password").attr("type", "text");
    }
    else {
        $(this).removeClass("fa-eye").addClass("fa-eye-slash");
        $("#password").attr("type", "password");
    }
});  

$("#mostrar-new-password").click(function (){
    if ($(this).hasClass("fa-eye-slash") == true) {
        $(this).removeClass("fa-eye-slash").addClass("fa-eye");
        $("#new_password").attr("type", "text");
    }
    else {
        $(this).removeClass("fa_eye").addClass("fa-eye-slash");
        $("#new_password").attr("type", "password");
    }
}); 

$("#mostrar-confirm-new-password").click(function (){
    if ($(this).hasClass("fa-eye-slash") == true) {
        $(this).removeClass("fa-eye-slash").addClass("fa-eye");
        $("#confirm_new_password").attr("type", "text");
    }
    else {
        $(this).removeClass("fa-eye").addClass("fa-eye-slash");
        $("#confirm_new_password").attr("type", "password");
    }
});
 