$(document).ready(function(){
    /* validacion fecha inicio y fin */
    $("#fecha_fin").change(function () {
        if ($(this).val() < $("#fecha_inicio").val()) {
            $("#invalid-date").text("La Fecha Final debe ser Mayor o Igual a la Fecha Inicial");
        } 
        else {
            $("#invalid-date").text("");
        } 
    });
    /* fin validacion fecha inicio y fin */

    /* validacion hora inicio y fin */
    $("#hora_fin").change(function () {
        if ($(this).val() <= $("#hora_inicio").val()) {
            $("#invalid-hour").text("La Hora Final debe ser Mayor a la Hora Inicial");
        } 
        else {
            $("#invalid-hour").text("");
        } 
    });
    /* fin validacion hora inicio y fin */

    /* programas de formacion del ambiente */
    $("#ambiente_id").on('change', function () {
        if ($(this).val().length == 0) {
            return false;
        } 
        else {
            $("#programa_formacion_id")
            .load($("#programa_formacion_id").attr('src').replace('#', $(this).val()), function () {
                $("#programa_formacion_id").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Programa de Formación'))
            });
        }
    });
    /* fin programas de formacion del ambiente */

    /* fichas del programa de formacion */
    $("#programa_formacion_id").on('change', function () {
        if ($(this).val().length == 0) {
            return false;
        } 
        else {
            $("#ficha_id")
            .load($("#ficha_id").attr('src').replace('#', $(this).val()), function () {
                $("#ficha_id").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Número de Ficha'))
            });
        }
    });
    /* fin fichas del programa de formacion */

    /* competencias del programa de formacion */
    $("#programa_formacion_id").on('change', function () {
        if ($(this).val().length == 0) {
            return false;
        } 
        else {
            $("#competencia_laboral_id")
            .load($("#competencia_laboral_id").attr('src').replace('#', $(this).val()), function () {
                $("#competencia_laboral_id").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Competencia Laboral o Transversal'))
            });
        }
    });
    /* fin competencias del programa de formacion */

    /* resultados de aprendizaje del programa de formacion */
    $("#competencia_laboral_id").on('change', function () {
        if ($(this).val().length == 0) {
            return false;
        } 
        else {
            $("#resultado_aprendizaje_id")
            .load($("#resultado_aprendizaje_id").attr('src').replace('#', $(this).val()).replace('*', $("#ficha_id").val()), function () {
                $("#resultado_aprendizaje_id").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Resultado de Aprendizaje'))
            });
        }
    });
    /* fin resultados de aprendizaje del programa de formacion */
});
