var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Actualizar";
  } else {
    document.getElementById("nextBtn").innerHTML = "Siguiente";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("form-actualizar").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  var fisrt = false;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].querySelectorAll("input,select,textarea");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].checkValidity() == false) {
      if (valid) {
        fisrt = y[i];
      }
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  if (fisrt) {
    fisrt.reportValidity();
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    if (i>=n){
    x[i].className = x[i].className.replace(" active", "");
    }
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}
  
/* municipios */
$("#id_regional").on('change', function () {
  if ($(this).val().length == 0) {
    return false;
  }
  else {
    $("#id_municipio")
      .load($("#id_municipio").attr('src').replace('#', $(this).val()), function () {
        $("#id_municipio").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Municipio'))
      });
  }
}); 
  
/* centros de formacion */
$("#id_municipio").on('change', function () {
  if ($(this).val().length == 0) {
    return false;
  } 
  else {
    $("#id_centro_formacion")
      .load($("#id_centro_formacion").attr('src').replace('#', $(this).val()), function () {
        $("#id_centro_formacion").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Centro de Formación'))
      });
  }
});
  
/* sedes */
$("#id_centro_formacion").on('change', function () {
  if ($(this).val().length == 0) {
    return false;
  } 
  else {
    $("#id_sede")
      .load($("#id_sede").attr('src').replace('#', $(this).val()), function () {
        $("#id_sede").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Sede'))
      });
  }
});
  
/* agregar input otro tipo */
$("#id_piso, #id_muro, #id_guardaescoba, #id_cieloraso, #id_ventaneria, #id_puerta, #id_tipo_riesgo").on('change', function () {
  new_row = '#otro_' + $(this).attr('id').replace('id_', '') + '_row';

  if ($(this).val() != null && $(this).val().length) {
    if ($(this).val() == '127') {
      $(new_row).show();

      $(new_row + ' input').prop('required', true).focus();
    } 
    else {
      $(new_row).hide();
      $(new_row + ' input').prop('required', false)
    }
  } 
  else {
    $(new_row).hide();
    $(new_row + ' input').prop('required', false)
  }
}).trigger('change');
  
/* validacion input file imagen */
$(".custom-file-input").on("change", function () {
  var archivo = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(archivo);
});
/* fin validacion input file imagen */

/* validacion productos del ambiente */
$("#produccion_centros_1").click(function (){
  $("#productos_ambiente").show();
});

$("#produccion_centros_2").click(function (){
  $("#productos_ambiente").hide();
});
/* fin validacion productos del ambiente */
  
/* validacion riesgos del ambiente */
$("#riesgos_1").click(function (){
  $("#riesgos_ambiente").show();
});

$("#riesgos_2").click(function (){
  $("#riesgos_ambiente").hide();
});
/* fin validacion riesgos del ambiente */
  
/* programas formacion de ambiente */
$("#ambiente_id").on('change', function () {
  $("#programa_formacion_id").empty()
  if ($(this).val().length == 0) {
    return false;
  } else {
    $("#programa_formacion_id")
      .load($("#programa_formacion_id").attr('src').replace('#', $(this).val()), function () {
        $("#programa_formacion_id").prepend($("<option/>").attr({ selected: true, disabled: true }).html('Seleccione Tipo de Subcategoria'))
      });
  }
});
/* fin programas formacion de ambiente */

/* validacion puerta ambiente ancho y alto */
function validacion_puerta(){
  var dimension_alto = document.getElementById('dimension_alto'),
  dimension_ancho = document.getElementById('dimension_ancho'),

  puerta_alto = document.getElementById('puerta_alto'),
  puerta_ancho = document.getElementById('puerta_ancho'),

  validacion_altura = document.getElementById('validacion_altura'),
  validacion_ancho = document.getElementById('validacion_ancho');

  dimension_alto = parseFloat(dimension_alto.value);
  dimension_ancho = parseFloat(dimension_ancho.value);

  puerta_alto = parseFloat(puerta_alto.value);
  puerta_ancho = parseFloat(puerta_ancho.value);

  if(puerta_alto >= dimension_alto){
      validacion_altura.innerHTML = "Esta Altura debe ser Menor a la Altura del Ambiente";
      validacion_altura.style.color = "#e3342f";
      validacion_altura.style.fontSize = "90%";
  }
  else {
      validacion_altura.innerHTML = "";
  }

  if(puerta_ancho >= dimension_ancho){
      validacion_ancho.innerHTML = "Este Ancho debe ser Menor al Ancho del Ambiente";
      validacion_ancho.style.color = "#e3342f";
      validacion_ancho.style.fontSize = "90%";
  }
  else {
      validacion_ancho.innerHTML = "";
  }

}
function cal() {
  var dimension_alto = document.getElementById('dimension_largo'),
  dimension_ancho = document.getElementById('dimension_ancho'),
  area_ambiente = document.getElementById('area_ambiente');

  try {
      dimension_alto = parseFloat(dimension_alto.value);
      dimension_ancho = parseFloat(dimension_ancho.value);

      area_ambiente.value = (dimension_alto * dimension_ancho).toFixed(2);
  } catch (e) {

  }
}
/* fin validacion puerta ambiente ancho y alto */


/* descripcion area de cualificacion */
$("#area_cualificacion_id").on('change', function () {
  if ($(this).val().length == 0) {
    return false;
  }
  else {
    $("#descripcion-area-cualificacion").prepend($("#area_cualificacion_id option:selected").data("descripcion-area"));
    $("#close-modal").click(function(){
      $("#descripcion-area-cualificacion").empty();
    });
  }
}); 
/* fin descripcion area de cualificacion */