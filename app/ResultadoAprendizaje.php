<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultadoAprendizaje extends Model
{
    protected $table = "resultados_aprendizaje";
    protected $guarded = [];
    protected $primaryKey = "id_resultado_aprendizaje";

    public function competencia_laboral_programa(){
        return $this->belongsTo('App\CompetenciaLaboralPrograma', 'competencia_programa_id', 'id_competencia_programa');
    }

    public function programacion_ficha(){
        return $this->hasMany('App\ProgramacionFicha', 'id_resultado_aprendizaje', 'resultado_aprendizaje_id');
    }
}
