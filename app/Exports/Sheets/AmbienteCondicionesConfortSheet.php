<?php

namespace App\Exports\Sheets;

use App\Ambiente;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;

class AmbienteCondicionesConfortSheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithEvents

{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $campos_condiciones = ['codigo_ambiente', 'iluminacion_natural', 'iluminacion_artificial',
        'ventilacion_natural', 'ventilacion_mecanica', 'gas_natural', 'gas_propano',
        'electrica_bifasica', 'electrica_trifasica', 'electrica_regulada', 'red_datos',
        'hidro_aguafria', 'hidro_aguacaliente', 'hidro_lavaojos', 'hidro_banco_hielo',
        'hidro_trampa_grasas', 'hidro_vapor', 'hidro_desarenador', 'hidro_lavamanos',
        'hidro_desagues', 'hidro_otro', 'observaciones_generales'];

        if(Auth::user()->id_perfil_usuario == 1){
            $reporte_ambiente = Ambiente::select($campos_condiciones)
            ->orderBy('ambiente.id_ambiente', 'desc')
            ->get();
        }
        else{
            $reporte_ambiente = Ambiente::select($campos_condiciones)
            /* filtro centro de formacion */
            ->where('ambiente.id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->orderBy('ambiente.id_ambiente', 'desc')
            ->get();
        }
        return $reporte_ambiente;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Condiciones Confort';
    }


    public function headings(): array
    {
        return [
            array_map('strtoupper', ['codigo_ambiente', 'iluminacion_natural', 'iluminacion_artificial',
            'ventilacion_natural', 'ventilacion_mecanica', 'gas_natural', 'gas_propano',
            'electrica_bifasica', 'electrica_trifasica', 'electrica_regulada', 'red_datos',
            'hidro_aguafria', 'hidro_aguacaliente', 'hidro_lavaojos', 'hidro_banco_hielo',
            'hidro_trampa_grasas', 'hidro_vapor', 'hidro_desarenador', 'hidro_lavamanos',
            'hidro_desagues', 'hidro_otro', 'observaciones_generales'])
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:V1',
                    [
                        'font' => [
                            'name' => 'Calibri',
                            'bold' => true,
                            'size' => 12,
                            'color' => ['argb' => 'FFFFFFFF'],
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'FF078F7E'],
                        ]
                    ]
                );
            },
        ];
    }

}

