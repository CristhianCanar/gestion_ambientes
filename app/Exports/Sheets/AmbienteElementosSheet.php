<?php

namespace App\Exports\Sheets;

use App\Ambiente;
use App\ElementoAmbiente;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;

class AmbienteElementosSheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithEvents

{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $campos_elementos = ['ambiente.codigo_ambiente', 'numero_inventario', 'serial', 'elemento_ambiente', 
        'categoria_elemento', 'subcategoria_elemento', 'origen_elemento', 'estado', 
        'cantidad', 'precio', 'tiempo_vida_util', 'fecha_adquisicion', 'observacion_general'];

        if(Auth::user()->id_perfil_usuario == 1){
            $reporte_ambiente = ElementoAmbiente::select($campos_elementos)
            /* elementos */
            ->join('ambiente', 'elemento_ambiente.id_ambiente', '=', 'ambiente.id_ambiente')
            ->join('subcategoria_elemento', 'elemento_ambiente.subcategoria_elemento_id', '=', 'subcategoria_elemento.id_subcategoria_elemento')
            ->join('categoria_elemento', 'subcategoria_elemento.categoria_elemento_id', '=', 'categoria_elemento.id_categoria_elemento')
            ->join('origen_elemento', 'elemento_ambiente.origen_elemento_id', '=', 'origen_elemento.id_origen_elemento')
            ->orderBy('elemento_ambiente.id_ambiente', 'desc')
            ->get();
        }
        else{
            $reporte_ambiente = ElementoAmbiente::select($campos_elementos)
            /* elementos */
            ->join('ambiente', 'elemento_ambiente.id_ambiente', '=', 'ambiente.id_ambiente')
            ->join('subcategoria_elemento', 'elemento_ambiente.subcategoria_elemento_id', '=', 'subcategoria_elemento.id_subcategoria_elemento')
            ->join('categoria_elemento', 'subcategoria_elemento.categoria_elemento_id', '=', 'categoria_elemento.id_categoria_elemento')
            ->join('origen_elemento', 'elemento_ambiente.origen_elemento_id', '=', 'origen_elemento.id_origen_elemento')
            /* filtro centro de formacion */
            ->where('ambiente.id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->orderBy('elemento_ambiente.id_ambiente', 'desc')
            ->get();
        }

        return $reporte_ambiente;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Elementos';
    }


    public function headings(): array
    {
        return [
            array_map('strtoupper', ['codigo_ambiente', 'numero_inventario', 'serial', 'elemento', 
            'categoria', 'subcategoria', 'origen_elemento', 'estado', 'cantidad', 
            'precio', 'tiempo_vida_util', 'fecha_adquisicion', 'observaciones_generales'])
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:M1',
                    [
                        'font' => [
                            'name' => 'Calibri',
                            'bold' => true,
                            'size' => 12,
                            'color' => ['argb' => 'FFFFFFFF'],
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'FF078F7E'],
                        ]
                    ]
                );
            },
        ];
    }

}

