<?php

namespace App\Exports\Sheets;

use App\AmbientePrograma;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;

class AmbienteProgramasFormacionSheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithEvents

{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $campos_programas = ['ambiente.codigo_ambiente', 'nombre_programa', 'version_programa', 'duracion_total',
        'redconocimiento', 'nivel_programa', 'linea_tematica'];

        if(Auth::user()->id_perfil_usuario == 1){
            $reporte_ambiente = AmbientePrograma::select($campos_programas)
            /* programas de formación */
            ->join('ambiente', 'ambiente_programa.id_ambiente', '=', 'ambiente.id_ambiente')
            ->join('programas_formacion', 'ambiente_programa.id_programa', '=', 'programas_formacion.id_programa')
            ->join('redes_conocimiento', 'programas_formacion.red_conocimiento_id', '=', 'redes_conocimiento.idredconocimiento')
            ->join('linea_tematica', 'programas_formacion.id_linea_tematica', '=', 'linea_tematica.id_linea_tematica')
            ->join('nivel_programa', 'programas_formacion.id_nivel_programa', '=', 'nivel_programa.id_nivel_programa')
            ->orderBy('ambiente_programa.id_ambiente', 'desc')
            ->get();
        }
        else{
            $reporte_ambiente = AmbientePrograma::select($campos_programas)
            /* programas de formación */
            ->join('ambiente', 'ambiente_programa.id_ambiente', '=', 'ambiente.id_ambiente')
            ->join('programas_formacion', 'ambiente_programa.id_programa', '=', 'programas_formacion.id_programa')
            ->join('redes_conocimiento', 'programas_formacion.red_conocimiento_id', '=', 'redes_conocimiento.idredconocimiento')
            ->join('linea_tematica', 'programas_formacion.id_linea_tematica', '=', 'linea_tematica.id_linea_tematica')
            ->join('nivel_programa', 'programas_formacion.id_nivel_programa', '=', 'nivel_programa.id_nivel_programa')
            /* filtro centro de formacion */
            ->where('ambiente.id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->orderBy('ambiente_programa.id_ambiente', 'desc')
            ->get();
        }
        return $reporte_ambiente;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Programas Formacion';
    }


    public function headings(): array
    {
        return [
            array_map('strtoupper', ['codigo_ambiente', 'nombre_programa', 'version_programa', 'duracion_total',
            'red_conocimiento', 'nivel_programa', 'linea_tematica'])
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:G1',
                    [
                        'font' => [
                            'name' => 'Calibri',
                            'bold' => true,
                            'size' => 12,
                            'color' => ['argb' => 'FFFFFFFF'],
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'FF078F7E'],
                        ]
                    ]
                );
            },
        ];
    }

}

