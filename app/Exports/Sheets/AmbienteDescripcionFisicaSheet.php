<?php

namespace App\Exports\Sheets;

use App\Ambiente;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;

class AmbienteDescripcionFisicaSheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithEvents

{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $campos_descripcion = ['codigo_ambiente','dimension_largo', 'dimension_ancho',
        'dimension_alto', 'area_ambiente', 'puerta_alto',
        'puerta_ancho', 'piso', 'otro_piso', 'muro', 'otro_muro',
        'guardaescoba', 'otro_guardaescoba', 'cieloraso',
        'otro_cieloraso', 'ventaneria', 'otra_ventaneria',
        'puerta', 'otra_puerta'];

        if(Auth::user()->id_perfil_usuario == 1){
            $reporte_ambiente = Ambiente::select($campos_descripcion)
            /* descripción general */
            ->join('piso', 'ambiente.id_piso', '=', 'piso.id_piso')
            ->join('muro', 'ambiente.id_muro', '=', 'muro.id_muro')
            ->join('guardaescoba', 'ambiente.id_guardaescoba', '=', 'guardaescoba.id_guardaescoba')
            ->join('cieloraso', 'ambiente.id_cieloraso', '=', 'cieloraso.id_cieloraso')
            ->join('ventaneria', 'ambiente.id_ventaneria', '=', 'ventaneria.id_ventaneria')
            ->join('puerta', 'ambiente.id_puerta', '=', 'puerta.id_puerta')
            ->orderBy('ambiente.id_ambiente', 'desc')
            ->get();
        }
        else{
            $reporte_ambiente = Ambiente::select($campos_descripcion)
            /* descripción general */
            ->join('piso', 'ambiente.id_piso', '=', 'piso.id_piso')
            ->join('muro', 'ambiente.id_muro', '=', 'muro.id_muro')
            ->join('guardaescoba', 'ambiente.id_guardaescoba', '=', 'guardaescoba.id_guardaescoba')
            ->join('cieloraso', 'ambiente.id_cieloraso', '=', 'cieloraso.id_cieloraso')
            ->join('ventaneria', 'ambiente.id_ventaneria', '=', 'ventaneria.id_ventaneria')
            ->join('puerta', 'ambiente.id_puerta', '=', 'puerta.id_puerta')
            /* filtro centro de formacion */
            ->where('ambiente.id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->orderBy('ambiente.id_ambiente', 'desc')
            ->get();
        }

        return $reporte_ambiente;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Descripcion Fisica';
    }


    public function headings(): array
    {
        return [
            array_map('strtoupper', ['codigo_ambiente','dimension_largo', 'dimension_ancho',
            'dimension_alto', 'area_ambiente', 'puerta_alto',
            'puerta_ancho', 'piso', 'otro_piso', 'muro', 'otro_muro',
            'guardaescoba', 'otro_guardaescoba', 'cieloraso',
            'otro_cieloraso', 'ventaneria', 'otra_ventaneria',
            'puerta', 'otra_puerta'])
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:S1',
                    [
                        'font' => [
                            'name' => 'Calibri',
                            'bold' => true,
                            'size' => 12,
                            'color' => ['argb' => 'FFFFFFFF'],
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'FF078F7E'],
                        ]
                    ]
                );
            },
        ];
    }

}

