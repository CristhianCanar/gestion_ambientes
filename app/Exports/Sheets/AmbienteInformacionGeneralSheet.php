<?php

namespace App\Exports\Sheets;

use App\Ambiente;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;

class AmbienteInformacionGeneralSheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithEvents

{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $campos_informacion = ['codigo_ambiente', 'nombre_ambiente', 'cantidad_aprendices',
        'area_cualificacion', 'tipo_ambiente', 'tenencia', 'regional',
        'municipio', 'centro_formacion', 'fecha_creacion_ambiente'];
        
        if(Auth::user()->id_perfil_usuario == 1){
            $reporte_ambiente = Ambiente::select($campos_informacion)
            /* información general */
            ->join('areas_cualificacion', 'ambiente.area_cualificacion_id', '=', 'areas_cualificacion.id_area_cualificacion')
            ->join('tipo_ambiente', 'ambiente.id_tipo_ambiente', '=', 'tipo_ambiente.id_tipo_ambiente')
            ->join('tenencia', 'ambiente.id_tenencia', '=', 'tenencia.id_tenencia')
            ->join('centro_formacion', 'ambiente.id_centro_formacion', '=', 'centro_formacion.id_centro_formacion')
            ->join('municipios', 'centro_formacion.id_municipio', '=', 'municipios.id_municipio')
            ->join('regional', 'centro_formacion.id_regional', '=', 'regional.id_regional')
            ->orderBy('ambiente.id_ambiente', 'desc')
            ->get();
        }
        else{
            $reporte_ambiente = Ambiente::select($campos_informacion)
            /* información general */
            ->join('areas_cualificacion', 'ambiente.area_cualificacion_id', '=', 'areas_cualificacion.id_area_cualificacion')
            ->join('tipo_ambiente', 'ambiente.id_tipo_ambiente', '=', 'tipo_ambiente.id_tipo_ambiente')
            ->join('tenencia', 'ambiente.id_tenencia', '=', 'tenencia.id_tenencia')
            ->join('centro_formacion', 'ambiente.id_centro_formacion', '=', 'centro_formacion.id_centro_formacion')
            ->join('municipios', 'centro_formacion.id_municipio', '=', 'municipios.id_municipio')
            ->join('regional', 'centro_formacion.id_regional', '=', 'regional.id_regional')
            /* filtro centro de formacion */
            ->where('ambiente.id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->orderBy('ambiente.id_ambiente', 'desc')
            ->get();
        }

        return $reporte_ambiente;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Informacion General';
    }


    public function headings(): array
    {
        return [
            array_map('strtoupper', ['codigo_ambiente','nombre_ambiente', 'cantidad_aprendices',
            'area_cualificacion', 'tipo_ambiente','tenencia', 'regional',
            'municipio', 'centro_formacion', 'fecha_creacion_ambiente'])
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:J1',
                    [
                        'font' => [
                            'name' => 'Calibri',
                            'bold' => true,
                            'size' => 12,
                            'color' => ['argb' => 'FFFFFFFF'],
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'FF078F7E'],
                        ]
                    ]
                );
            },
        ];
    }

}

