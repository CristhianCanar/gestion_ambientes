<?php

namespace App\Exports\Sheets;

use App\EspacioAmbiente;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;

class AmbienteEspaciosComplementariosSheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithEvents

{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $campos_espacios = ['ambiente.codigo_ambiente','espacio','area'];

        if(Auth::user()->id_perfil_usuario == 1){
            $reporte_ambiente = EspacioAmbiente::select($campos_espacios)
            ->join('ambiente', 'espacio_ambiente.id_ambiente', '=', 'ambiente.id_ambiente')
            ->orderBy('espacio_ambiente.id_ambiente', 'desc')
            ->get();
        }
        else{
            $reporte_ambiente = EspacioAmbiente::select($campos_espacios)
            ->join('ambiente', 'espacio_ambiente.id_ambiente', '=', 'ambiente.id_ambiente')
            /* filtro centro de formacion */
            ->where('ambiente.id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->orderBy('espacio_ambiente.id_ambiente', 'desc')
            ->get();
        }
        return $reporte_ambiente;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Espacios Complementarios';
    }


    public function headings(): array
    {
        return [
            array_map('strtoupper', ['codigo_ambiente','espacio','area'])
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:C1',
                    [
                        'font' => [
                            'name' => 'Calibri',
                            'bold' => true,
                            'size' => 12,
                            'color' => ['argb' => 'FFFFFFFF'],
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'FF078F7E'],
                        ]
                    ]
                );
            },
        ];
    }

}

