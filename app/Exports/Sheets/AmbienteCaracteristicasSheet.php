<?php

namespace App\Exports\Sheets;

use App\Ambiente;
use App\ProductoAmbiente;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;

class AmbienteCaracteristicasSheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithEvents

{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $campos_caracteristicas = ['codigo_ambiente', 'produccion_centros', 'producto', 'tipo_riesgo',
        'otro_tipo_riesgo'];
        if(Auth::user()->id_perfil_usuario == 1){
            $reporte_ambiente = ProductoAmbiente::select($campos_caracteristicas)
            /* caracteristicas */
            ->join('ambiente', 'productos_ambiente.ambiente_id', '=', 'ambiente.id_ambiente')
            ->join('productos', 'productos_ambiente.producto_id', '=', 'productos.id_producto')
            ->join('tipos_riesgo_ambiente', 'ambiente.id_ambiente', '=', 'tipos_riesgo_ambiente.ambiente_id')
            ->join('tipos_riesgo', 'tipos_riesgo_ambiente.tipo_riesgo_id', '=', 'tipos_riesgo.id_tipo_riesgo')
            ->orderBy('ambiente.id_ambiente', 'desc')
            ->get();
        }
        else{
            $reporte_ambiente = ProductoAmbiente::select($campos_caracteristicas)
            /* caracteristicas */
            ->join('ambiente', 'productos_ambiente.ambiente_id', '=', 'ambiente.id_ambiente')
            ->join('productos', 'productos_ambiente.producto_id', '=', 'productos.id_producto')
            ->join('tipos_riesgo_ambiente', 'ambiente.id_ambiente', '=', 'tipos_riesgo_ambiente.ambiente_id')
            ->join('tipos_riesgo', 'tipos_riesgo_ambiente.tipo_riesgo_id', '=', 'tipos_riesgo.id_tipo_riesgo')
            /* filtro centro de formacion */
            ->where('ambiente.id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->orderBy('ambiente.id_ambiente', 'desc')
            ->get();
        }

        return $reporte_ambiente;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Caracteristicas';
    }


    public function headings(): array
    {
        return [
            array_map('strtoupper', ['codigo_ambiente', 'produccion_centros', 'producto', 'tipo_riesgo',
            'otro_tipo_riesgo'])
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:E1',
                    [
                        'font' => [
                            'name' => 'Calibri',
                            'bold' => true,
                            'size' => 12,
                            'color' => ['argb' => 'FFFFFFFF'],
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'FF078F7E'],
                        ]
                    ]
                );
            },
        ];
    }

}

