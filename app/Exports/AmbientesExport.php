<?php
namespace App\Exports;

use App\Exports\Sheets\AmbienteCaracteristicasSheet;
use App\Exports\Sheets\AmbienteCondicionesConfortSheet;
use App\Exports\Sheets\AmbienteDescripcionFisicaSheet;
use App\Exports\Sheets\AmbienteElementosSheet;
use App\Exports\Sheets\AmbienteEspaciosComplementariosSheet;
use App\Exports\Sheets\AmbienteInformacionGeneralSheet;
use App\Exports\Sheets\AmbienteProgramasFormacionSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class AmbientesExport implements WithMultipleSheets
{
    private $items_ambientes;

    public function setItemsAmbientes(array $items_ambientes)
    {
        $this->items_ambientes = $items_ambientes;
    }

    /**
    * @return array
    */
    public function sheets(): array
    {
        $sheets = [];
        $b_info = 0;
        $b_des = 0;
        $b_car = 0;
        $b_con = 0;
        $b_pro = 0;
        $b_esp = 0;
        $b_ele = 0;

        for ($hojas = 1; $hojas <= count($this->items_ambientes); $hojas++) {
            if(in_array("informacion_general", $this->items_ambientes) && $b_info == 0){
                $sheets[] = new AmbienteInformacionGeneralSheet();
                $b_info = 1;
            }
            if(in_array("descripcion_fisica", $this->items_ambientes) && $b_des == 0){
                $sheets[] = new AmbienteDescripcionFisicaSheet();
                $b_des = 1;
            }
            if(in_array("caracteristicas", $this->items_ambientes) && $b_car == 0){
                $sheets[] = new AmbienteCaracteristicasSheet();
                $b_car = 1;
            }
            if(in_array("condiciones_confort", $this->items_ambientes) && $b_con == 0){
                $sheets[] = new AmbienteCondicionesConfortSheet();
                $b_con = 1;
            }
            if(in_array("programa_formacion", $this->items_ambientes) && $b_pro == 0){
                $sheets[] = new AmbienteProgramasFormacionSheet();
                $b_pro = 1;
            }
            if(in_array("espacio_complementario", $this->items_ambientes) && $b_esp == 0){
                $sheets[] = new AmbienteEspaciosComplementariosSheet();
                $b_esp = 1;
            }
            if(in_array("elemento", $this->items_ambientes) && $b_ele == 0){
                $sheets[] = new AmbienteElementosSheet();
                $b_ele = 1;
            }

        }

        return $sheets;
    }

}
