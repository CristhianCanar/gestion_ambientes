<?php

namespace App\Exports;

use App\CentroFormacion;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use \Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class CentrosFormacionExport implements FromCollection, ShouldAutoSize, WithHeadings,
WithTitle, WithEvents
{

    private $centrosf;

    public function setCentrosFormacion(array $centrosf)
    {
        $this->centrosf = $centrosf;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if(Auth::user()->id_perfil_usuario == 1){
            $centro_formacion = CentroFormacion::select($this->centrosf)
            ->join('regional', 'centro_formacion.id_regional', '=', 'regional.id_regional')
            ->orderBy('centro_formacion', 'asc')
            ->get();
        }
        else{
            $centro_formacion = CentroFormacion::select($this->centrosf)
            ->join('regional', 'centro_formacion.id_regional', '=', 'regional.id_regional')
             /*Filtro de centro*/
            ->where('centro_formacion.id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->orderBy('centro_formacion', 'asc')
            ->get();
        }
        return $centro_formacion;
    }

    public function headings(): array
    {
        return [
            array_map('strtoupper', $this->centrosf)
        ];
    }

    public function title(): string
    {
        return 'Centros_Formación';
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:G1',
                    [
                        'font' => [
                            'name' => 'Calibri',
                            'bold' => true,
                            'size' => 12,
                            'color' => ['argb' => 'FFFFFFFF'],
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'FF078F7E'],
                        ]
                    ]
                );
            },
        ];
    }
}
