<?php

namespace App\Exports;

use App\RedConocimiento;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use \Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class RedesConocimientoExport implements FromCollection, ShouldAutoSize, WithHeadings,
WithTitle, WithEvents
{

    private $redesc;

    public function setRedesConocimiento(array $redesc)
    {
        $this->redesc = $redesc;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $red_conocimiento = RedConocimiento::select($this->redesc)
        ->orderBy('redconocimiento', 'asc')
        ->get();

        return $red_conocimiento;
    }

    public function headings(): array
    {
        return [
            array_map('strtoupper', $this->redesc)
        ];
    }

    public function title(): string
    {
        return 'Redes_Conocimiento';
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:G1',
                    [
                        'font' => [
                            'name' => 'Calibri',
                            'bold' => true,
                            'size' => 12,
                            'color' => ['argb' => 'FFFFFFFF'],
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'FF078F7E'],
                        ]
                    ]
                );
            },
        ];
    }
}
