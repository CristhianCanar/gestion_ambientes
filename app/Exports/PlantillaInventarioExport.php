<?php

namespace App\Exports;

use App\Inventario;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use \Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class PlantillaInventarioExport implements FromCollection, ShouldAutoSize, WithHeadings,
WithTitle, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $inventario = Inventario::where('id_inventario', '=', 0)
        ->get();
        
        return $inventario;
    }

    public function headings(): array
    {
        return [
            array_map('strtoupper', [
                'Codigo_Centro',
                'Codigo_Subcategoria',
                'Codigo_Centro_Costos(origen)',
                'Numero_Inventario',
                'Serial',
                'Nombre_Elemento',
                'Cantidad',
                'Estado',
                'Fecha_Adquisicion',
                'Vida_Util(años)',
                'Valor_Adquisicion',
                'Observaciones'
            ])
        ];
    }

    public function title(): string
    {
        return 'Plantilla Elementos';
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:L1',
                    [
                        'font' => [
                            'name' => 'Calibri',
                            'bold' => true,
                            'size' => 12,
                            'color' => ['argb' => 'FFFFFFFF'],
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'FF078F7E'],
                        ]
                    ]
                );
            },
        ];
    }
}
