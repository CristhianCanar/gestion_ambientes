<?php

namespace App\Exports;

use App\ProgramaFormacion;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use \Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ProgramasFormacionExport implements FromCollection, ShouldAutoSize, WithHeadings,
WithTitle, WithEvents
{

    private $programasf;

    public function setProgramasFormacion(array $programasf)
    {
        $this->programasf = $programasf;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $programa_formacion = ProgramaFormacion::select($this->programasf)
        ->join('redes_conocimiento', 'programas_formacion.red_conocimiento_id', '=', 'redes_conocimiento.idredconocimiento')
        ->join('linea_tematica', 'programas_formacion.id_linea_tematica', '=', 'linea_tematica.id_linea_tematica')
        ->join('nivel_programa', 'programas_formacion.id_nivel_programa', '=', 'nivel_programa.id_nivel_programa')
        ->orderBy('nombre_programa', 'asc')
        ->get();

        return $programa_formacion;
    }

    public function headings(): array
    {
        return [
            array_map('strtoupper', $this->programasf)
        ];
    }

    public function title(): string
    {
        return 'Programas_Formacion';
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:G1',
                    [
                        'font' => [
                            'name' => 'Calibri',
                            'bold' => true,
                            'size' => 12,
                            'color' => ['argb' => 'FFFFFFFF'],
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'FF078F7E'],
                        ]
                    ]
                );
            },
        ];
    }
}

