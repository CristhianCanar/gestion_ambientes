<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuardaEscoba extends Model
{
    protected $table = "guardaescoba";
    protected $guarded = [];
    protected $primaryKey = "id_guardaescoba";

}
