<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puerta extends Model
{
    protected $table = "puerta";
    protected $guarded = [];
    protected $primaryKey = "id_puerta";

}
