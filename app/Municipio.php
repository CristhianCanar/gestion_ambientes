<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $table = "municipios";
    protected $guarded = [];
    protected $primaryKey = "id_municipio";

    public function departamento(){
        return $this->belongsTo('App\Departamento','departamento_id','id_departamento');
    }

    public function programa_formacion(){
        return $this->belongsTo('App\ProgramaFormacion');
    }
}
