<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ventaneria extends Model
{
    protected $table = "ventaneria";
    protected $guarded = [];
    protected $primaryKey = "id_ventaneria";
}
