<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramacionInstructor extends Model
{
    protected $table = "programacion_instructores";
    protected $guarded = [];
    protected $primaryKey = "id_programacion_instructor";

    public function programacion_ambiente(){
        return $this->belongsTo('App\ProgramacionAmbiente', 'programacion_ambiente_id', 'id_programacion_ambiente');
    } 

    public function usuario(){
        return $this->belongsTo('App\User', 'instructor_id', 'id');
    } 
}

