<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Modulo extends Model
{
    protected $table = "modulo";
    protected $guarded = ["hijos"];    
    protected $primaryKey = "id_modulo";

    public static function get_menu($padre = null, $user_id){

        $resultado = [];

        $datos = DB::table('users')->distinct()
        ->select('modulo.*')
        ->join('perfiles','perfiles.id_perfil_usuario','=','users.id_perfil_usuario')
        ->join('moduloxperfil','moduloxperfil.id_perfil_usuario','=','perfiles.id_perfil_usuario')
        ->join('modulo','modulo.id_modulo','=', 'moduloxperfil.id_modulo')        
        ->where('modulo.id_modulo_padre',$padre)
        ->where('users.id',$user_id)
        ->orderBy('id_modulo','asc')
        ->get();

        $i=0;
        foreach($datos as $d){
            $resultado[$i] = $d;
            
            if($d->url_modulo =='#'){
                $resultado[$i]->hijos = Modulo::get_menu($d->id_modulo, $user_id);
            }
            $i++;
        }
              
        return $resultado;
    }


}
