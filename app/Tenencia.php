<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenencia extends Model
{
    protected $table = "tenencia";
    protected $guarded = [];
    protected $primaryKey = "id_tenencia";

}
