<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Piso extends Model
{
    protected $table = "piso";
    protected $guarded = [];
    protected $primaryKey = "id_piso";

}
