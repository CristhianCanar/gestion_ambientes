<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RedConocimientoLineaTematica extends Model
{
    protected $table = "redes_conocimiento_lineas_tematicas";
    protected $guarded = [];
    protected $primaryKey = "red_conocimiento_id";

    public function red_conocimiento() {
        return $this->belongsTo('App\RedConocimiento', 'red_conocimiento_id', 'idredconocimiento');
    }

    public function linea_tematica() {
        return $this->belongsTo('App\LineaTematica', 'linea_tematica_id', 'id_linea_tematica');
    }
}
