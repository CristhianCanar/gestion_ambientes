<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoRiesgo extends Model
{
    protected $table = "tipos_riesgo";
    protected $guarded = [];
    protected $primaryKey = "id_tipo_riesgo";

    public function tipo_riesgo_ambiente(){
        return $this->hasMany('App\TipoRiesgoAmbiente', 'id_tipo_riesgo', 'tipo_riesgo_id');
    }
}
