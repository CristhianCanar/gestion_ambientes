<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table = "departamentos";
    protected $guarded = [];
    protected $primaryKey = "id_departamento";

    public function regional(){
        return $this->belongsTo('App\Departamento');
    }
    
    public function municipio(){
        return $this->hasMany('App\Municipio');
    }
}
