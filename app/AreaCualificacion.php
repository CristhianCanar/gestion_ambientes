<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaCualificacion extends Model
{
    protected $table = "areas_cualificacion";
    protected $guarded = [];
    protected $primaryKey = "id_area_cualificacion";

    public function ambiente(){
        return $this->hasMany('App\Ambiente', 'id_area_cualificacion', 'area_cualificacion_id');
    }
}
