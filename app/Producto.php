<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = "productos";
    protected $guarded = [];
    protected $primaryKey = "id_producto";

    public function producto_ambiente(){
        return $this->hasMany('App\ProductoAmbiente', 'id_producto', 'producto_id');
    }
}
