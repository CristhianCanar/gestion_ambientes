<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaElemento extends Model
{
    protected $table = "categoria_elemento";
    protected $guarded = [];
    protected $primaryKey = "id_categoria_elemento";

    public function subcategoria_elemento(){
        return $this->hasMany('App\SubcategoriaElemento', 'id_subcategoria_elemento', 'id_subcategoria_elemento');
    }
}
