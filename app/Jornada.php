<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jornada extends Model
{
    protected $table = "jornadas";
    protected $guarded = [];
    protected $primaryKey = "id_jornada";

    public function ficha(){
        return $this->hasOne('App\Ficha', 'id_jornada', 'jornada_id');
    }
}
