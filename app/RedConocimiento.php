<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RedConocimiento extends Model
{
    protected $table = "redes_conocimiento";
    protected $guarded = [];
    protected $primaryKey = "idredconocimiento";

    public function programa_formacion(){
        return $this->hasMany('App\ProgramaFormacion', 'idredconocimiento', 'red_conocimiento_id');
    }

    public function actividad_aprendizaje(){
        return $this->hasMany('App\ActividadAprendizaje', 'idredconocimiento', 'red_conocimiento_id');
    }
}
