<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = "users";
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identificacion', 'nombres', 'apellidos', 'email', 'password', 'telefono', 'estado', 'perfil_id', 'centro_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function perfiles(){
        return $this->belongsTo('App\Perfil', 'id_perfil_usuario', 'id_perfil_usuario');
    }

    public function centro_formacion(){
        return $this->belongsTo('App\CentroFormacion', 'id_centro_formacion', 'id_centro_formacion');
    }

    public function programacion_ambiente(){
        return $this->hasMany('App\ProgramacionAmbiente', 'id', 'instructor_id');
    }

    public function programacion_instructor(){
        return $this->hasMany('App\ProgramacionInstructor', 'id', 'instructor_id');
    } 
}
