<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regional extends Model
{
    protected $table = "regional";
    protected $guarded = [];
    protected $primaryKey = "id_regional";

    public function departamento(){
        return $this->hasMany('App\Departamento');
    }
    
    public function centro_formacion(){
        return $this->belongsTo('App\CentroFormacion');
    }
    
}
