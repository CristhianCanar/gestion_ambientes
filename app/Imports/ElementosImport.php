<?php

namespace App\Imports;

use App\Inventario;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class ElementosImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return  new Inventario([
            'centro_formacion_id'       => $row['Codigo_Centro'],
            'subcategoria_elemento_id'  => $row['Codigo_Subcategoria'],
            'origen_elemento_id'        => $row['Codigo_Centro_Costos(origen)'],
            'numero_inventario'         => $row['Numero_Inventario'],
            'serial'                    => $row['Serial'],
            'elemento_ambiente'         => $row['Nombre_Elemento'],
            'cantidad'                  => $row['Cantidad'],
            'estado'                    => $row['Estado'],
            'fecha_adquisicion'         => $row['Fecha_Adquisicion'],
            'tiempo_vida_util'          => $row['Vida_Util(años)'],
            'precio'                    => $row['Valor_Adquisicion'],
            'observacion_general'       => $row['Observaciones'],
        ]);
    }

}
