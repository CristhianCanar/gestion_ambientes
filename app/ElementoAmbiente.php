<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElementoAmbiente extends Model
{
    protected $table = "elemento_ambiente";
    protected $guarded = [];
    protected $primaryKey = "id_elemento_ambiente";

    public function ambiente(){
        return $this->belongsTo('App\Ambiente', 'id_ambiente', 'id_ambiente');
    }

    public function subcategoria_elemento(){
        return $this->belongsTo('App\SubcategoriaElemento', 'subcategoria_elemento_id', 'id_subcategoria_elemento');
    }

    public function origen_elemento(){
        return $this->belongsTo('App\OrigenElemento', 'origen_elemento_id', 'id_origen_elemento');
    }

}
