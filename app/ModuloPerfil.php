<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuloPerfil extends Model
{
    protected $table = "moduloxperfil";
    protected $guarded = [];
    protected $primaryKey = "id_moduloxperfil";

    public function perfiles(){
        return $this->belongsTo('App\Perfil','id_perfil_usuario','id_perfil_usuario');
    }

    public function modulo(){
        return $this->belongsTo('App\Modulo','id_modulo','id_modulo');
    }
}
