<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrigenElemento extends Model
{
    protected $table = "origen_elemento";
    protected $guarded = [];
    protected $primaryKey = "id_origen_elemento";

    public function elemento_ambiente(){
        return $this->hasMany('App\ElementoAmbiente', 'id_elemento_ambiente', 'id_elemento_ambiente');
    }
}
