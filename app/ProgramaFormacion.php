<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramaFormacion extends Model
{
    protected $table = "programas_formacion";
    protected $guarded = [];
    protected $primaryKey = "id_programa";

    public function ambiente_programa(){
        return $this->hasMany('App\AmbientePrograma', 'id_programa', 'id_programa');
    }

    public function redconocimiento(){
        return $this->belongsTo('App\RedConocimiento', 'red_conocimiento_id', 'idredconocimiento');
    }

    public function linea_tematica(){
        return $this->belongsTo('App\LineaTematica', 'id_linea_tematica', 'id_linea_tematica');
    }

    public function nivel_programa(){
        return $this->belongsTo('App\NivelFormacion','id_nivel_programa', 'id_nivel_programa');
    }

    public function ficha(){
        return $this->hasMany('App\Ficha', 'id_programa', 'programa_id');
    }

    public function competencia_laboral_programa(){
        return $this->hasMany('App\CompetenciaLaboralPrograma', 'id_programa ', 'programa_id');
    }
}
