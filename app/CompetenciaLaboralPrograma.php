<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompetenciaLaboralPrograma extends Model
{
    protected $table = "competencia_laboral_programa";
    protected $guarded = [];
    protected $primaryKey = "competencia_laboral_id";

    public function competencia_laboral(){
        return $this->belongsTo('App\CompetenciaLaboral', 'competencia_laboral_id', 'id_competencia_laboral');
    }

    public function programa_formacion(){
        return $this->belongsTo('App\ProgramaFormacion', 'programa_id', 'id_programa');
    }

    public function resultado_aprendizaje(){
        return $this->hasMany('App\ResultadoAprendizaje', 'id_competencia_programa', 'competencia_programa_id');
    }

}
