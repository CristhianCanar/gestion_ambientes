<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ambiente extends Model
{
    protected $table = "ambiente";
    protected $guarded = [];
    protected $primaryKey = "id_ambiente";

    public function elemento_ambiente(){
        return $this->hasMany('App\ElementoAmbiente', 'id_ambiente', 'id_ambiente');
    }

    public function ambiente_programa(){
        return $this->hasMany('App\AmbientePrograma', 'id_ambiente', 'id_ambiente');
    }

    public function espacio_ambiente(){
        return $this->hasMany('App\EspacioAmbiente', 'id_ambiente', 'id_ambiente');
    }

    public function sedes(){
        return $this->belongsTo('App\Sede', 'id_sede', 'id_sede');
    }

    public function tenencia(){
        return $this->belongsTo('App\Tenencia', 'id_tenencia', 'id_tenencia');
    }

    public function tipo_ambiente(){
        return $this->belongsTo('App\TipoAmbiente', 'id_tipo_ambiente', 'id_tipo_ambiente');
    }

    public function guardaescoba(){
        return $this->belongsTo('App\GuardaEscoba', 'id_guardaescoba', 'id_guardaescoba');
    }

    public function piso(){
        return $this->belongsTo('App\Piso', 'id_piso', 'id_piso');
    }

    public function muro(){
        return $this->belongsTo('App\Muro', 'id_muro', 'id_muro');
    }

    public function cieloraso(){
        return $this->belongsTo('App\CieloRaso', 'id_cieloraso', 'id_cieloraso');
    }

    public function ventaneria(){
        return $this->belongsTo('App\Ventaneria', 'id_ventaneria', 'id_ventaneria');
    }

    public function puerta(){
        return $this->belongsTo('App\Puerta', 'id_puerta', 'id_puerta');
    }

    public function centro_formacion(){
        return $this->belongsTo('App\CentroFormacion', 'id_centro_formacion', 'id_centro_formacion');
    }

    public function tipo_riesgo_ambiente(){
        return $this->hasMany('App\TipoRiesgoAmbiente', 'id_ambiente', 'ambiente_id');
    }

    public function producto_ambiente(){
        return $this->hasMany('App\ProductoAmbiente', 'id_ambiente', 'ambiente_id');
    }

    public function programacion_ambiente(){
        return $this->hasMany('App\ProgramacionAmbiente', 'id_ambiente', 'ambiente_id');
    }

    public function area_cualificacion(){
        return $this->belongsTo('App\AreaCualificacion', 'area_cualificacion_id', 'id_area_cualificacion');
    }

}
