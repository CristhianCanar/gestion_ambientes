<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoRiesgoAmbiente extends Model
{
    protected $table = "tipos_riesgo_ambiente";
    protected $guarded = [];
    protected $primaryKey = "id_tipo_riesgo_ambiente";

    public function tipo_riesgo(){
        return $this->belongsTo('App\TipoRiesgo', 'tipo_riesgo_id', 'id_tipo_riesgo');
    }
    
    public function ambiente(){
        return $this->belongsTo('App\Ambiente', 'ambiente_id', 'id_ambiente');
    }
}
