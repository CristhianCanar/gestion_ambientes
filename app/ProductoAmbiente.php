<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoAmbiente extends Model
{
    protected $table = "productos_ambiente";
    protected $guarded = [];
    protected $primaryKey = "id_producto_ambiente";

    public function ambiente(){
        return $this->belongsTo('App\Ambiente', 'ambiente_id', 'id_ambiente');
    }

    public function producto(){
        return $this->belongsTo('App\Producto', 'producto_id', 'id_producto');
    }
}
