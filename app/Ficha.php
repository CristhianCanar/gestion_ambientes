<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ficha extends Model
{
    protected $table = "ficha";
    protected $guarded = [];
    protected $primaryKey = "id_ficha";

    public function programa_formacion(){
        return $this->belongsTo('App\ProgramaFormacion', 'programa_id', 'id_programa');
    }

    public function centro_formacion(){
        return $this->belongsTo('App\CentroFormacion', 'centro_formacion_id', 'id_centro_formacion');
    }

    public function programacion_ambiente(){
        return $this->hasMany('App\ProgramacionAmbiente', 'id_ficha', 'ficha_id');
    }

    public function jornada(){
        return $this->belongsTo('App\Jornada', 'jornada_id', 'id_jornada');
    }

}
