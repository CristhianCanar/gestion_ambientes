<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NivelFormacion extends Model
{
    protected $table = "nivel_programa";
    protected $guarded = [];
    protected $primaryKey = "id_nivel_programa";

    public function programa(){
        return $this->hasMany('App\ProgramaFormacion');
    }
}
