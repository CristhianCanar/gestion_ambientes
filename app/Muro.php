<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Muro extends Model
{
    protected $table = "muro";
    protected $guarded = [];
    protected $primaryKey = "id_muro";

}
