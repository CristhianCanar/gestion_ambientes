<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $table = "perfiles";
    protected $guarded = [];
    protected $primaryKey = "id_perfil_usuario";

    public function user(){
        return $this->hasMany('App\User', 'id_perfil_usuario', 'id_perfil_usuario');
    }
}
