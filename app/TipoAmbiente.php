<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAmbiente extends Model
{
    protected $table = "tipo_ambiente";
    protected $guarded = [];
    protected $primaryKey = "id_tipo_ambiente";
    
}
