<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramacionAmbiente extends Model
{
    protected $table = "programacion_ambientes";
    protected $guarded = [];
    protected $primaryKey = "id_programacion_ambiente";

    public function ambiente(){
        return $this->belongsTo('App\Ambiente', 'ambiente_id', 'id_ambiente');
    }

    public function ficha(){
        return $this->belongsTo('App\Ficha', 'ficha_id', 'id_ficha');
    }

    public function user(){
        return $this->belongsTo('App\User', 'instructor_id', 'id');
    }

    public function programacion_resultado_aprendizaje(){
        return $this->hasMany('App\ProgramacionResultadoAprendizaje', 'id_programacion_ambiente', 'programacion_ambiente_id');
    }

    public function programacion_instructor(){
        return $this->hasMany('App\ProgramacionInstructor', 'id_programacion_ambiente', 'programacion_ambiente_id');
    } 

}
