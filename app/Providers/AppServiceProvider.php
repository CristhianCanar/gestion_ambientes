<?php

namespace App\Providers;
use Illuminate\Routing\UrlGenerator;
use App\Http\Controllers\HomeAuthController;
use App\Modulo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        if(env('REDIRECT_HTTPS')) {
            $url->forceScheme('https');
        }

        view()->composer('*', function($view) {

            if(isset(Auth::user()->id)){
                $id = Auth::user()->id;
            }else{
                $id = 1;
            }
            $shtml = HomeAuthController::show_menu(Modulo::get_menu(null, $id));
            $view->with(['shtml' => $shtml]);
        });
    }
}
