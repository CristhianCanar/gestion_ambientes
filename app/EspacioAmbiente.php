<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EspacioAmbiente extends Model
{
    protected $table = "espacio_ambiente";
    protected $guarded = [];
    protected $primaryKey = "id_espacio";

    public function ambiente(){
        return $this->belongsTo('App\Ambiente', 'id_ambiente', 'id_ambiente');
    }

}
