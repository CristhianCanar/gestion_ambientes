<?php

namespace App\Http\Controllers;

use App\CentroFormacion;
use App\Http\Controllers\Controller;
use App\Municipio;
use App\Regional;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
/*Ruta para usar Alertas*/
use RealRashid\SweetAlert\Facades\Alert;

class CentrosFormacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->id_perfil_usuario == 1) {
            $centros = CentroFormacion::orderBy('centro_formacion', 'asc')
            ->orderBy('created_at', 'desc')
            ->paginate(50);    
        }
        else {
            $centros = CentroFormacion::where('id_centro_formacion','=', Auth::user()->id_centro_formacion)
            ->orderBy('centro_formacion', 'asc')
            ->orderBy('created_at', 'desc')
            ->paginate(10);  
        }
        
        return view('admin.centros.gestionar',compact('centros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regional = Regional::get();
        $municipio = Municipio::get();

        return view('admin.centros.registro',compact('regional','municipio'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $centros_registrados = CentroFormacion::where('centro_formacion', '=', ucwords(strtolower($request->input('centro_formacion'))))
        ->first();

        if ($centros_registrados == null) {
            $centro = new CentroFormacion;
            $centro->id_regional            = $request->input('id_regional');
            $centro->id_municipio           = $request->input('id_municipio');
            $centro->centro_formacion       = ucwords(strtolower($request->input('centro_formacion')));
            $centro->direccion_centro       = $request->input('direccion_centro');
            $centro->telefono_centro        = $request->input('telefono_centro');
            $centro->email_centro           = strtolower($request->input('email_centro'));
            $centro->subdirector            = ucwords(strtolower($request->input('subdirector')));
            $centro->contacto_subdirector   = strtolower($request->input('contacto_subdirector'));
            $centro->save();

            Alert::success('Registrado', 'Centro de Formación con éxito');
        }
        else {
            Alert::error('Error', 'El Centro de Formación '.ucwords(strtolower($request->input('centro_formacion'))).' ya esta Registrado');
        }
        
        return redirect(route('centros_formacion.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $centro = CentroFormacion::where('id_centro_formacion', $id)->first();

        return view('admin.centros.detalle', compact('centro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $centro = CentroFormacion::where('id_centro_formacion', $id)->first();

        $regional = Regional::where('id_regional', '!=', $centro->id_regional)
        ->orderBy('regional')
        ->get();

        $municipio = Municipio::where('id_municipio', '!=', $centro->id_municipio)
        ->orderBy('municipio')
        ->get();

        return view('admin.centros.actualizar', compact('centro','regional','municipio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $centros_registrados = CentroFormacion::where('id_centro_formacion', '!=', $id)
        ->where('centro_formacion', '=', ucwords(strtolower($request->input('centro_formacion'))))
        ->first();

        if ($centros_registrados == null) {
            CentroFormacion::where('id_centro_formacion',$id)->update([
                'id_regional'           => $request->id_regional,
                'id_municipio'          => $request->id_municipio,
                'centro_formacion'      => ucwords(strtolower($request->centro_formacion)),
                'direccion_centro'      => $request->direccion_centro,
                'telefono_centro'       => $request->telefono_centro,
                'email_centro'          => strtolower($request->email_centro),
                'subdirector'           => ucwords(strtolower($request->subdirector)),
                'contacto_subdirector'  => strtolower($request->contacto_subdirector),
            ]);
            
            Alert::success('Actualizado', 'Centro de Formación con éxito');
        }
        else {
            Alert::error('Error', 'El Centro de Formación 
            '.ucwords(strtolower($request->centro_formacion)).' ya Existe');
        }

        return redirect(route('centros_formacion.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Busqueda de Centros de Formacion
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function buscar_centros_formacion(Request $request)
    {
        $centro_formacion = ucwords(strtolower($request->input('buscar_centro_formacion')));

        $busqueda = CentroFormacion::where('centro_formacion', 'LIKE', "%$centro_formacion%")
            ->orderBy('centro_formacion', 'asc')
            ->first();

        if ($centro_formacion == "") {
            Alert::error('Error', 'Escriba para Buscar');
            return redirect(route('centros_formacion.index'));
        }
        if (isset($busqueda)) {
            $centros = CentroFormacion::where('centro_formacion', 'LIKE', "%$centro_formacion%")
                ->orderBy('centro_formacion', 'asc')
                ->paginate(20);
            return view('admin.centros.gestionar', compact('centros'));
        }
        else {
            Alert::info('Información', 'No se Encontraron Centros de Formacion');
            return redirect(route('centros_formacion.index'));
        }
    }
}
