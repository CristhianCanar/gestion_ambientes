<?php

namespace App\Http\Controllers;

use App\AmbientePrograma;
use App\RedConocimiento;
use Illuminate\Http\Request;

/*Ruta para usar Alertas*/
use RealRashid\SweetAlert\Facades\Alert;

class AmbientesProgramaFormacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_ambiente)
    {
        $programas_formacion = AmbientePrograma::where('id_ambiente', '=', $id_ambiente)
        ->orderBy('created_at', 'desc')
        ->paginate(10);
        
        return view('admin.ambientes.programas_formacion.gestionar', compact('programas_formacion', 'id_ambiente'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gestionar_programas_ambiente($id_ambiente)
    {
        $programas_formacion = AmbientePrograma::where('id_ambiente', '=', $id_ambiente)
        ->paginate(10);

        return view('admin.ambientes.programas_formacion.gestionar', compact('programas_formacion', 'id_ambiente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_programas_ambiente($id_ambiente)
    {
        $redes = RedConocimiento::orderBy('redconocimiento')->get();
        return view('admin.ambientes.programas_formacion.registro', compact('id_ambiente', 'redes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_ambiente = $request->input('id_ambiente');

        $programas_formacion_asociados = AmbientePrograma::where('id_ambiente', '=', $request->input('id_ambiente'))
        ->where('id_programa', '=', $request->input('id_programa'))
        ->first();

        if($programas_formacion_asociados == null){
            $programa_ambiente = new AmbientePrograma;
            $programa_ambiente->id_ambiente = $request->input('id_ambiente');
            $programa_ambiente->id_programa = $request->input('id_programa');
            $programa_ambiente->save();

            Alert::success('Asociado', 'Programa de Formación con éxito');
        }
        else {
            Alert::error('Error', 'El Programa '.$programas_formacion_asociados->programa_formacion->nombre_programa.' ya esta Asociado al Ambiente');
        }

        return redirect(route('programas_formacion_ambiente.create_programas_ambiente', $id_ambiente));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $programa_ambiente = AmbientePrograma::where('id_programa', '=', $id)->first();

        return view('admin.ambientes.programas_formacion.detalle',compact('programa_ambiente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete_programas_ambiente($id, $id_ambiente)
    {
        AmbientePrograma::where('id_ambiente', '=', $id_ambiente)
        ->where('id_programa', '=', $id)
        ->delete();

        Alert::success('Eliminado', 'Programa de Formación con éxito');
        return redirect(route('programas_formacion_ambiente.gestionar_programas_ambiente', $id_ambiente));
    }
}
