<?php

namespace App\Http\Controllers;

use App\ActividadAprendizaje;
use App\Ambiente;
use App\AmbientePrograma;
use App\CompetenciaLaboral;
use App\Festivo;
use App\Ficha;
use App\ProgramacionAmbiente;
use App\ProgramacionResultadoAprendizaje;
use App\RedConocimiento;
use App\ResultadoAprendizaje;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class AmbientesProgramacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_centro = Auth::user()->id_centro_formacion;
        $ambientes = Ambiente::orderBy('nombre_ambiente', 'asc')
        ->where('id_centro_formacion', '=', $id_centro)
        ->get();

        $usuarios = User::orderBy('apellidos', 'asc')
        ->where('id_perfil_usuario', '=', 4)
        ->where('id_centro_formacion', '=', $id_centro)
        ->get();

        $programacion_ambientes = ProgramacionAmbiente::paginate(10);

        return view('admin.programaciones_ambiente.gestionar', compact('programacion_ambientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id_centro = Auth::user()->id_centro_formacion;

        if (Auth::user()->id_perfil_usuario == 1) {
            $ambientes = Ambiente::orderBy('nombre_ambiente', 'asc')
            ->get();

            $usuarios = User::orderBy('apellidos', 'asc')
            ->where('id_perfil_usuario', '=', 4)
            ->get();
        }
        else {
            $ambientes = Ambiente::orderBy('nombre_ambiente', 'asc')
            ->where('id_centro_formacion', '=', $id_centro)
            ->get();

            $usuarios = User::orderBy('apellidos', 'asc')
            ->where('id_perfil_usuario', '=', 4)
            ->get();
        }

        return view('admin.programaciones_ambiente.registro', compact('ambientes', 'usuarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion_evento_programado = ProgramacionAmbiente::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
        ->where('ambiente_id', '=', $request->input('ambiente_id'))
        ->where('fecha_inicio', '<=', $request->input('fecha_fin'))
        ->where('fecha_fin', '>=', $request->input('fecha_inicio'))
        ->where('hora_inicio', '<', $request->input('hora_fin'))
        ->where('hora_fin', '>', $request->input('hora_inicio'))
        ->where(function ($query) use ($request) {
            if ($request->input('domingo') != null) {
                $query->orWhere('domingo', $request->input('domingo'));
            }
            if ($request->input('lunes') != null) {
                $query->orWhere('lunes', $request->input('lunes'));
            }
            if ($request->input('martes') != null) {
                $query->orWhere('martes', $request->input('martes'));
            }
            if ($request->input('miercoles') != null) {
                $query->orWhere('miercoles', $request->input('miercoles'));
            }
            if ($request->input('jueves') != null) {
                $query->orWhere('jueves', $request->input('jueves'));
            }
            if ($request->input('viernes') != null) {
                $query->orWhere('viernes', $request->input('viernes'));
            }
            if ($request->input('sabado') != null) {
                $query->orWhere('sabado', $request->input('sabado'));
            }
        })
        ->first();

        if ($validacion_evento_programado != null) {
            Alert::error('Error', 'El día '.$validacion_evento_programado->domingo.' '.
                            $validacion_evento_programado->lunes.' '.$validacion_evento_programado->martes.' '.
                            $validacion_evento_programado->miercoles.' '.$validacion_evento_programado->jueves.' '.
                            $validacion_evento_programado->viernes.' '. $validacion_evento_programado->sabado.
                            ' ya esta programado');
            return redirect(route('ambientes_programacion.create'));
        }

        $programacion_ambiente = new ProgramacionAmbiente;
        $programacion_ambiente->ambiente_id             = $request->input('ambiente_id');
        $programacion_ambiente->ficha_id                = $request->input('ficha_id');
        $programacion_ambiente->instructor_id           = $request->input('instructor_id');
        $programacion_ambiente->actividad_aprendizaje   = $request->input('actividad_aprendizaje');
        $programacion_ambiente->fecha_inicio            = $request->input('fecha_inicio');
        $programacion_ambiente->fecha_fin               = $request->input('fecha_fin');
        $programacion_ambiente->hora_inicio             = $request->input('hora_inicio');
        $programacion_ambiente->hora_fin                = $request->input('hora_fin');
        $programacion_ambiente->domingo                 = $request->input('domingo');
        $programacion_ambiente->lunes                   = $request->input('lunes');
        $programacion_ambiente->martes                  = $request->input('martes');
        $programacion_ambiente->miercoles               = $request->input('miercoles');
        $programacion_ambiente->jueves                  = $request->input('jueves');
        $programacion_ambiente->viernes                 = $request->input('viernes');
        $programacion_ambiente->sabado                  = $request->input('sabado');
        $programacion_ambiente->responsable_id          = Auth::user()->id;
        $programacion_ambiente->centro_formacion_id     = Auth::user()->id_centro_formacion;
        $programacion_ambiente->save();


        $programacion_ambiente = ProgramacionAmbiente::get()->last();
        $id_resultados_aprendizaje = $request->input('resultado_aprendizaje_id');

        for ($i=0; $i <count($id_resultados_aprendizaje); $i++) {
            $programacion_resultados = new ProgramacionResultadoAprendizaje();
            $programacion_resultados->programacion_ambiente_id   = $programacion_ambiente->id_programacion_ambiente;
            $programacion_resultados->resultado_aprendizaje_id   = $id_resultados_aprendizaje[$i];
            $programacion_resultados->estado                     = "Programado";
            $programacion_resultados->save();
        }

        Alert::success('Registrada', 'Programación con éxito');
        return redirect(route('ambientes_programacion.create'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $programacion_ambiente = ProgramacionAmbiente::where('id_programacion_ambiente',$id)->first();

        $programacion_resultados = ProgramacionResultadoAprendizaje::where('programacion_ambiente_id', $programacion_ambiente->id_programacion_ambiente)
        ->get();

        return view('admin.programaciones_ambiente.detalle', compact('programacion_ambiente', 'programacion_resultados'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $programacion_ambiente = ProgramacionAmbiente::where('id_programacion_ambiente', $id)->first();

        $programacion_resultados = ProgramacionResultadoAprendizaje::where('programacion_ambiente_id', $programacion_ambiente->id_programacion_ambiente)
        ->first();

        $resultados_aprendizaje = ResultadoAprendizaje::where('id_resultado_aprendizaje', $programacion_resultados->resultado_aprendizaje_id)
        ->get();

        $resultados_aprendizaje_programacion = DB::select('SELECT resultados_aprendizaje.*  FROM resultados_aprendizaje
        WHERE NOT EXISTS (SELECT  * FROM programacion_resultados_aprendizaje WHERE programacion_resultados_aprendizaje.resultado_aprendizaje_id = resultados_aprendizaje.id_resultado_aprendizaje
            AND programacion_resultados_aprendizaje.resultado_aprendizaje_id = '.$programacion_resultados->resultado_aprendizaje_id.'
        )');

        $ambientes = Ambiente::orderBy('nombre_ambiente', 'asc')
        ->where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
        ->where('id_ambiente', '!=', $programacion_ambiente->ambiente_id)
        ->get();

        $usuarios = User::orderBy('apellidos', 'asc')
        ->where('id_perfil_usuario', '=', 4)
        ->where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
        ->where('id', '!=', $programacion_ambiente->instructor_id)
        ->get();

        return view('admin.programaciones_ambiente.actualizar', compact('programacion_ambiente', 'programacion_resultados',
            'resultados_aprendizaje', 'resultados_aprendizaje_programacion', 'ambientes', 'usuarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validacion_evento_programado = ProgramacionAmbiente::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
        ->where('id_programacion_ambiente', '=', $id)
        ->where('ambiente_id', '=', $request->input('ambiente_id'))
        ->where('fecha_inicio', '<=', $request->input('fecha_fin'))
        ->where('fecha_fin', '>=', $request->input('fecha_inicio'))
        ->where('hora_inicio', '<=', $request->input('hora_fin'))
        ->where('hora_fin', '>=', $request->input('hora_inicio'))
        ->where(function ($query) use ($request) {
            if ($request->input('domingo') != null) {
                $query->orWhere('domingo', $request->input('domingo'));
            }
            if ($request->input('lunes') != null) {
                $query->orWhere('lunes', $request->input('lunes'));
            }
            if ($request->input('martes') != null) {
                $query->orWhere('martes', $request->input('martes'));
            }
            if ($request->input('miercoles') != null) {
                $query->orWhere('miercoles', $request->input('miercoles'));
            }
            if ($request->input('jueves') != null) {
                $query->orWhere('jueves', $request->input('jueves'));
            }
            if ($request->input('viernes') != null) {
                $query->orWhere('viernes', $request->input('viernes'));
            }
            if ($request->input('sabado') != null) {
                $query->orWhere('sabado', $request->input('sabado'));
            }
        })
        ->first();

        if ($validacion_evento_programado != null) {
            Alert::error('Error', 'El día '.$validacion_evento_programado->domingo.' '.
                            $validacion_evento_programado->lunes.' '.$validacion_evento_programado->martes.' '.
                            $validacion_evento_programado->miercoles.' '.$validacion_evento_programado->jueves.' '.
                            $validacion_evento_programado->viernes.' '. $validacion_evento_programado->sabado.
                            ' ya esta programado');
            return redirect(route('ambientes_programacion.create'));
        }

        ProgramacionAmbiente::where('id_programacion_ambiente', $id)->update([
            'ambiente_id'             => $request->ambiente_id,
            'ficha_id'                => $request->ficha_id,
            'instructor_id'           => $request->instructor_id,
            'actividad_aprendizaje'   => $request->actividad_aprendizaje,
            'fecha_inicio'            => $request->fecha_inicio,
            'fecha_fin'               => $request->fecha_fin,
            'hora_inicio'             => $request->hora_inicio,
            'hora_fin'                => $request->hora_fin,
            'domingo'                 => $request->domingo,
            'lunes'                   => $request->lunes,
            'martes'                  => $request->martes,
            'miercoles'               => $request->miercoles,
            'jueves'                  => $request->jueves,
            'viernes'                 => $request->viernes,
            'sabado'                  => $request->sabado,
            'responsable_id'          => Auth::user()->id,
            'centro_formacion_id'     => Auth::user()->id_centro_formacion,
        ]);
        


        $programacion_ambiente = ProgramacionAmbiente::where('id_programacion_ambiente', $id)
        ->get();

        $id_resultados_aprendizaje = $request->input('resultado_aprendizaje_id');

        for ($i=0; $i <count($id_resultados_aprendizaje); $i++) {
            ProgramacionResultadoAprendizaje::where('resultado_aprendizaje_id', $programacion_ambiente->id_programacion_ambiente)->update([
                'programacion_ambiente_id'   => $programacion_ambiente->id_programacion_ambiente,
                'resultado_aprendizaje_id'   => $id_resultados_aprendizaje[$i],
                'estado'                     => "Programado",
            ]);
        }

        Alert::success('Actualizada', 'Programación con éxito');
        return redirect(route('ambientes_programacion.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Filtro para Calendario de Programación de Ambientes Formación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function filtro_programacion()
    {   
        $redes_conocimiento = RedConocimiento::get();
        return view('admin.programaciones_ambiente.filtro', compact('redes_conocimiento'));
    }

    /**
     * Calendario de Programación de Ambientes Formación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function calendario_programacion(Request $request)
    {
        $ambiente = $request->input('ambiente_id');
        $ambientes = Ambiente::where('id_ambiente', $ambiente)
        ->get();
        return view('admin.programaciones_ambiente.calendario', compact('ambiente', 'ambientes'));
    }


    /**
     * Calendario de Programación de Ambientes Formación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function calendario_detalles($id_programacion_ambiente)
    {
        $programacion_ambiente = ProgramacionAmbiente::where('id_programacion_ambiente', $id_programacion_ambiente)->first();

        $programacion_resultados = ProgramacionResultadoAprendizaje::where('programacion_ambiente_id', $programacion_ambiente->id_programacion_ambiente)
        ->get();

        return view('admin.programaciones_ambiente.evento', compact('programacion_ambiente', 'programacion_resultados'));
    }

    /**
     * Calendario de Programación de Ambientes Formación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function calendario_eventos(Request $request)
    {
        setlocale(LC_ALL, 'es_CO.UTF-8');
        $programacion_ambientes = ProgramacionAmbiente::where('ambiente_id', $request->id_ambiente)
        ->whereDate('fecha_inicio', '>=', $request->start)
        ->whereDate('fecha_inicio', '<', $request->end)
        ->get();

        $dias_festivos = Festivo::where('festivo', '>=', $request->start)
        ->whereDate('festivo', '<', $request->end)        
        ->get();

        $festivos = array();

        foreach($dias_festivos as $dia_festivo){
            $festivos[] = $dia_festivo->festivo;
        }

        foreach ($programacion_ambientes as $programacion) {
            /* Fechas de Inicio y Fin de la Programacion  */

                $fecha_inicio = new DateTime($programacion->fecha_inicio);
                $fecha_fin = new DateTime($programacion->fecha_fin);

                /* Dias de la Programacion */
                $dias_programados = array($programacion->domingo, $programacion->lunes, $programacion->martes,
                                    $programacion->miercoles, $programacion->jueves,
                                    $programacion->viernes, $programacion->sabado);
                /* Dias Festivos */
                
                /* Iteracion desde la Fecha Inicio hasta la Fecha Fin de la Programacion */
                for($i = $fecha_inicio; $i <= $fecha_fin; $i->modify('+1 day')){
                    for ($d=0; $d<count($dias_programados); $d++) {
                        if(strftime("%A", $i->getTimestamp()) == $dias_programados[$d] and !in_array(strftime("%Y-%m-%d", $i->getTimestamp()), $festivos)) {
                            $eventos[] = [
                                "title" => 'Ficha: '. $programacion->ficha->numero_ficha,
                                "start" => strftime("%Y-%m-%d", $i->getTimestamp()) . ' '. $programacion->hora_inicio,
                                "end"   => strftime("%Y-%m-%d", $i->getTimestamp()) .' '. $programacion->hora_fin,
                                "url"   => route("calendario.detalles", $programacion->id_programacion_ambiente),
                                "extendedProps" => [
                                    'Competencia' => 'Desarrollo'
                                ],
                                "display" => 'block',                   
                                "color" => "#38c172"
                            ];
                        }
                    }   
                    
                }
        
        }
        echo json_encode($eventos);
    }

    /**
     * Calendario de Programación de Dias Festivos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function calendario_festivos()
    {
        $dias_festivos = Festivo::get();

        foreach ($dias_festivos as $dias) {
            $festivos[] = [
                "title" => 'Festivo',
                "start" => $dias->festivo,
                "end"   => $dias->festivo,
                "color" => "#FC7323",
                "allDay" => true
            ];
        }

        echo json_encode($festivos);
    }

    /**
     * Programas de Formación del Ambiente
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function programas_ambiente($id_ambiente)
    {
        $programas_ambiente = AmbientePrograma::where('id_ambiente', $id_ambiente)
        ->get();

        foreach ($programas_ambiente as $programa_ambiente) {
            echo "<option value='{$programa_ambiente->id_programa}'>{$programa_ambiente->programa_formacion->nombre_programa}</option>";
        }
    }

    /**
     * Fichas de Programas de Formación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fichas_programa($id_programa)
    {
        $id_centro = Auth::user()->id_centro_formacion;
        $fichas_programa = Ficha::where('centro_formacion_id', $id_centro)
        ->where('programa_id', $id_programa)
        ->get();

        foreach ($fichas_programa as $ficha_programa) {
            echo "<option value='{$ficha_programa->id_ficha}'>{$ficha_programa->numero_ficha}</option>";
        }
    }

    /**
     * Actividades de Programas de Formación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actividades_programa($id_programa)
    {
        $id_centro = Auth::user()->id_centro_formacion;
        $actividades_programa = ActividadAprendizaje::where('programa_formacion_id', $id_programa)
        ->get();

        foreach ($actividades_programa as $actividad_programa) {
            echo "<option value='{$actividad_programa->id_actividad_aprendizaje}'>{$actividad_programa->actividad_aprendizaje}</option>";
        }
    }

    /**
     * Competencias de Programas de Formación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function competencias_programa($id_programa)
    {
        $id_centro = Auth::user()->id_centro_formacion;
        $competencias_programa = CompetenciaLaboral::where('programa_formacion_id', $id_programa)
        ->get();

        foreach ($competencias_programa as $competencia_programa) {
            echo "<option value='{$competencia_programa->id_competencia_laboral}'>{$competencia_programa->competencia_laboral}</option>";
        }
    }


    /**
     * Resultados de Aprendizaje de Programas de Formación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function resultados_programa($id_competencia)
    {
        $id_centro = Auth::user()->id_centro_formacion;
        $resultados_programa = ResultadoAprendizaje::where('competencia_laboral_id', $id_competencia)
        ->get();

        foreach ($resultados_programa as $resultado_programa) {
            echo "<option value='{$resultado_programa->id_resultado_aprendizaje}'>{$resultado_programa->resultado_aprendizaje}</option>";
        }
    }

}
