<?php

namespace App\Http\Controllers;

use App\CategoriaElemento;
use App\CentroFormacion;
use App\Exports\PlantillaInventarioExport;
use App\Imports\ElementosImport;
use App\Inventario;
use App\OrigenElemento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class ElementosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.administracion.elementos.importar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->id_perfil_usuario == 1) {
            $centros_formacion = CentroFormacion::orderBy('centro_formacion', 'asc')->get();
        }
        else {
            $centros_formacion = CentroFormacion::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->orderBy('centro_formacion', 'asc')->get();
        }

        $categorias = CategoriaElemento::orderBy('categoria_elemento', 'asc')->get();
        $origenes   = OrigenElemento::orderBy('origen_elemento', 'asc')->get();

        return view('admin.administracion.elementos.registro', compact('centros_formacion', 'categorias', 'origenes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $elementos_inventario_registrados = Inventario::where('numero_inventario', '=', $request->input('numero_inventario'))
        ->orWhere('serial', '=', $request->input('serial'))
        ->first();
       
        if ($elementos_inventario_registrados == null) {
            $inventario = new Inventario;
            $inventario->centro_formacion_id         = $request->input('centro_formacion_id');
            $inventario->subcategoria_elemento_id    = $request->input('subcategoria_elemento_id');
            $inventario->origen_elemento_id          = $request->input('origen_elemento_id');
            $inventario->numero_inventario           = $request->input('numero_inventario');
            $inventario->serial                      = $request->input('serial');
            $inventario->cantidad                    = $request->input('cantidad');
            $inventario->elemento_ambiente           = ucwords(strtolower($request->input('elemento_ambiente')));
            $inventario->estado                      = $request->input('estado');
            $inventario->fecha_adquisicion           = $request->input('fecha_adquisicion');
            $inventario->tiempo_vida_util            = $request->input('tiempo_vida_util');
            $inventario->precio                      = $request->input('precio');
            $inventario->observacion_general         = $request->input('observacion_general');
            $inventario->save();

            Alert::success('Registrado', 'Elemento con éxito, Ya lo puede Asociar al Ambiente');
        }
        else {
            Alert::error('Error', 'El Número de Inventario o Serial ya estan Registrados');
        }
        
        return redirect(route('elementos.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Subcategorias Elementos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_subcategorias_elementos($id_categoria_elemento)
    {
        $subcategorias_elementos = DB::table('subcategoria_elemento')
        ->select('id_subcategoria_elemento', 'subcategoria_elemento')
        ->where('categoria_elemento_id',$id_categoria_elemento)
        ->get();

        foreach ($subcategorias_elementos as $subce) {
            echo "<option value='{$subce->id_subcategoria_elemento}'>{$subce->subcategoria_elemento}</option>";
        }
    }

    /**
     * Importación elementos de administración.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_importacion(Request $request)
    {
        Excel::import(new ElementosImport, request()->file('elementos'));

        Alert::success('Importados', 'Elementos con éxito');
        return redirect(route('elementos.index'));
    }

     /**
     * Exportación plantilla inventarios.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportar_plantilla()
    {
        $exportacion_plantilla = new PlantillaInventarioExport();
        return Excel::download($exportacion_plantilla, 'Plantilla_Inventarios.xlsx');

    }
}
