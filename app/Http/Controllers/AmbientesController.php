<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ambiente;
use App\AmbientePrograma;
use App\AreaCualificacion;
use App\CentroFormacion;
use App\CieloRaso;
use App\GuardaEscoba;
use App\Muro;
use App\Piso;
use App\Producto;
use App\ProductoAmbiente;
use App\Puerta;
use App\RedConocimiento;
use App\Regional;
use App\Sede;
use App\Tenencia;
use App\TipoAmbiente;
use App\TipoRiesgo;
use App\TipoRiesgoAmbiente;
use App\Ventaneria;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

/*Ruta para usar Alertas*/
use RealRashid\SweetAlert\Facades\Alert;

class AmbientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->id_perfil_usuario == 1) {
            $ambientes = Ambiente::orderBy('nombre_ambiente', 'asc')
            ->orderBy('created_at', 'desc')
            ->paginate(100);
        }
        else {
            $ambientes = Ambiente::where('id_centro_formacion','=', Auth::user()->id_centro_formacion)  
            ->orderBy('nombre_ambiente', 'asc')
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        }
        return view('admin.ambientes.gestionar',compact('ambientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos_tenencia     = Tenencia::orderBy('tenencia', 'asc')->get();
        $tipos_ambientes    = TipoAmbiente::orderBy('tipo_ambiente')->get();

        if (Auth::user()->id_perfil_usuario == 1) {
            $regionales = Regional::orderBy('regional', 'asc')->get();
        }
        else {
            $regional = CentroFormacion::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->first();

            $regionales = Regional::orderBy('regional', 'asc')
            ->where('id_regional', '=', $regional->id_regional)
            ->get();
        }

        $pisos                  = Piso::get();
        $muros                  = Muro::get();
        $guardaescobas          = GuardaEscoba::get();
        $cielorasos             = CieloRaso::get();
        $ventanerias            = Ventaneria::get();
        $puertas                = Puerta::get();
        $productos              = Producto::orderBy('producto')->get();
        $tipos_riesgo           = TipoRiesgo::get();
        $areas_cualificacion    = AreaCualificacion::orderBy('area_cualificacion')->get();

        return view('admin.ambientes.registro', compact('tipos_tenencia', 'tipos_ambientes', 'regionales', 'pisos', 'muros',
        'guardaescobas', 'cielorasos', 'ventanerias', 'puertas', 'productos', 'tipos_riesgo', 'areas_cualificacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ambientes_registrados = Ambiente::where('id_centro_formacion', '=', $request->input('id_centro_formacion'))
        ->where('nombre_ambiente', '=', ucwords(strtolower($request->input('nombre_ambiente'))))
        ->first();

        $regional = CentroFormacion::where('id_centro_formacion', '=', $request->input('id_centro_formacion'))
        ->first();

        if ($ambientes_registrados == null) {
            $nombre_esquema_ambiente = null;
            $nombre_foto_ambiente = null;

            if($request->hasFile('esquema_ambiente')){
                $esquema_ambiente = $request->file('esquema_ambiente');
                $nombre_esquema_ambiente = $esquema_ambiente->getClientOriginalName();
                $esquema_ambiente->move('imagenes_ambiente', $nombre_esquema_ambiente);
            }

            if($request->hasFile('foto_ambiente')){
                $foto_ambiente = $request->file('foto_ambiente');
                $nombre_foto_ambiente = $foto_ambiente->getClientOriginalName();
                $foto_ambiente->move('imagenes_ambiente', $nombre_foto_ambiente);

            }

            $ambiente = new Ambiente;
            $ambiente->codigo_ambiente              = $regional->id_regional.$request->input('id_centro_formacion').$request->input('area_cualificacion_id').$request->input('id_tipo_ambiente');                                                 
            $ambiente->nombre_ambiente              = ucwords(strtolower($request->input('nombre_ambiente')));
            $ambiente->area_cualificacion_id        = $request->input('area_cualificacion_id');
            $ambiente->id_tipo_ambiente             = $request->input('id_tipo_ambiente');
            $ambiente->id_tenencia                  = $request->input('id_tenencia');
            $ambiente->id_centro_formacion          = $request->input('id_centro_formacion');
            $ambiente->id_sede                      = $request->input('id_sede');

            $ambiente->cantidad_aprendices          = $request->input('cantidad_aprendices');
            $ambiente->dimension_largo              = $request->input('dimension_largo');
            $ambiente->dimension_ancho              = $request->input('dimension_ancho');
            $ambiente->dimension_alto               = $request->input('dimension_alto');
            $ambiente->area_ambiente                = $request->input('area_ambiente');
            $ambiente->puerta_alto                  = $request->input('puerta_alto');
            $ambiente->puerta_ancho                 = $request->input('puerta_ancho');
            $ambiente->id_piso                      = $request->input('id_piso');
            $ambiente->id_muro                      = $request->input('id_muro');
            $ambiente->id_guardaescoba              = $request->input('id_guardaescoba');
            $ambiente->id_cieloraso                 = $request->input('id_cieloraso');
            $ambiente->id_ventaneria                = $request->input('id_ventaneria');
            $ambiente->id_puerta                    = $request->input('id_puerta');
            $ambiente->otro_piso                    = $request->input('otro_piso');
            $ambiente->otro_muro                    = $request->input('otro_muro');
            $ambiente->otro_guardaescoba            = $request->input('otro_guardaescoba');
            $ambiente->otro_cieloraso               = $request->input('otro_cieloraso');
            $ambiente->otra_ventaneria              = $request->input('otra_ventaneria');
            $ambiente->otra_puerta                  = $request->input('otra_puerta');

            $ambiente->produccion_centros           = $request->input('produccion_centros');
            $ambiente->otro_tipo_riesgo             = $request->input('otro_tipo_riesgo');
            $ambiente->esquema_ambiente             = $nombre_esquema_ambiente;
            $ambiente->foto_ambiente                = $nombre_foto_ambiente;

            $ambiente->iluminacion_natural          = $request->input('iluminacion_natural');
            $ambiente->iluminacion_artificial       = $request->input('iluminacion_artificial');
            $ambiente->ventilacion_natural          = $request->input('ventilacion_natural');
            $ambiente->ventilacion_mecanica         = $request->input('ventilacion_mecanica');
            $ambiente->gas_natural                  = $request->input('gas_natural');
            $ambiente->gas_propano                  = $request->input('gas_propano');
            $ambiente->electrica_bifasica           = $request->input('electrica_bifasica');
            $ambiente->electrica_trifasica          = $request->input('electrica_trifasica');
            $ambiente->electrica_regulada           = $request->input('electrica_regulada');
            $ambiente->red_datos                    = $request->input('red_datos');
            $ambiente->hidro_aguafria               = $request->input('hidro_aguafria');
            $ambiente->hidro_aguacaliente           = $request->input('hidro_aguacaliente');
            $ambiente->hidro_lavaojos               = $request->input('hidro_lavaojos');
            $ambiente->hidro_banco_hielo            = $request->input('hidro_banco_hielo');
            $ambiente->hidro_trampa_grasas          = $request->input('hidro_trampa_grasas');
            $ambiente->hidro_vapor                  = $request->input('hidro_vapor');
            $ambiente->hidro_desarenador            = $request->input('hidro_desarenador');
            $ambiente->hidro_lavamanos              = $request->input('hidro_lavamanos');
            $ambiente->hidro_desagues               = $request->input('hidro_desagues');
            $ambiente->hidro_lavapies               = null;

            $ambiente->hidro_otro                   = $request->input('hidro_otro');
            $ambiente->observaciones_generales      = $request->input('observaciones_generales');
            $ambiente->fecha_creacion_ambiente      = date('Y-m-d');
            $ambiente->save();

            $producto = $request->input('id_producto');
            $tipo_riesgo = $request->input('id_tipo_riesgo');

            $id_ambiente = Ambiente::get()->last();

            if ($producto != null) {
                for ($i=0; $i <count($producto); $i++) {
                    $producto_ambiente = new ProductoAmbiente;
                    $producto_ambiente->producto_id = $producto[$i];
                    $producto_ambiente->ambiente_id = $id_ambiente->id_ambiente;
                    $producto_ambiente->save();
                }
            }

            if ($tipo_riesgo != null) {
                for ($i=0; $i <count($tipo_riesgo); $i++) {
                    $tipo_riesgo_ambiente = new TipoRiesgoAmbiente;
                    $tipo_riesgo_ambiente->tipo_riesgo_id   = $tipo_riesgo[$i];
                    $tipo_riesgo_ambiente->ambiente_id      = $id_ambiente->id_ambiente;
                    $tipo_riesgo_ambiente->save();
                }
            }

            Alert::success('Registrado', 'Ambiente con éxito');
        }
        else {
            Alert::error('Error', 'El Ambiente '.ucwords(strtolower($request->input('nombre_ambiente'))).' ya esta Registrado');
        }

        return redirect(route('ambientes.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ambiente = Ambiente::where('id_ambiente', $id)->first();
        $productos_ambiente = ProductoAmbiente::where('ambiente_id', '=', $id)->get();
        $tipos_riesgo_ambiente = TipoRiesgoAmbiente::where('ambiente_id', '=', $id)->get();

        return view('admin.ambientes.detalle',compact('ambiente', 'productos_ambiente', 'tipos_riesgo_ambiente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ambiente = Ambiente::where('id_ambiente', $id)->first();

        $tipos_ambientes = TipoAmbiente::where('id_tipo_ambiente', '!=', $ambiente->id_tipo_ambiente)->get();

        $tipos_tenencia = Tenencia::where('id_tenencia', '!=', $ambiente->id_tenencia)->get();

        if (Auth::user()->id_perfil_usuario == 1) {
            $regionales = Regional::orderBy('regional')->get();
        }
        else {
            $regionales = Regional::orderBy('regional')
            ->where('id_regional', '=', $ambiente->centro_formacion->id_regional)
            ->get();
        }

        $pisos = Piso::where('id_piso', '!=', $ambiente->id_piso)->get();

        $muros = Muro::where('id_muro', '!=', $ambiente->id_muro)->get();

        $guardaescobas = GuardaEscoba::where('id_guardaescoba', '!=', $ambiente->id_guardaescoba)->get();

        $cielorasos = CieloRaso::where('id_cieloraso', '!=', $ambiente->id_cieloraso)->get();

        $ventanerias = Ventaneria::where('id_ventaneria', '!=', $ambiente->id_ventaneria)->get();

        $puertas = Puerta::where('id_puerta', '!=', $ambiente->id_puerta)->get();

        $areas_cualificacion = AreaCualificacion::where('id_area_cualificacion', '!=', $ambiente->area_cualificacion_id)->get();

        $productos_ambiente = ProductoAmbiente::where('ambiente_id', '=', $id)->get();

        $productos = DB::select('SELECT productos.* FROM productos
        WHERE NOT EXISTS (SELECT * FROM productos_ambiente WHERE productos_ambiente.producto_id = productos.id_producto
            AND productos_ambiente.ambiente_id = '.$id.'
        )');

        $tipos_riesgo_ambiente = TipoRiesgoAmbiente::where('ambiente_id', '=', $id)->get();

        $tipos_riesgo = DB::select('SELECT tipos_riesgo.* FROM tipos_riesgo
        WHERE NOT EXISTS (SELECT * FROM tipos_riesgo_ambiente WHERE tipos_riesgo_ambiente.tipo_riesgo_id = tipos_riesgo.id_tipo_riesgo
            AND tipos_riesgo_ambiente.ambiente_id = '.$id.'
        )');

        $redes = RedConocimiento::orderBy('redconocimiento')->get();

        return view('admin.ambientes.actualizar', compact('ambiente', 'tipos_ambientes', 'tipos_tenencia',
        'regionales','redes','pisos','muros', 'guardaescobas','cielorasos','ventanerias',
        'puertas', 'areas_cualificacion', 'productos_ambiente', 'productos', 'tipos_riesgo_ambiente', 'tipos_riesgo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ambientes_registrados = Ambiente::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
        ->where('id_ambiente', '!=', $id)
        ->where('nombre_ambiente', '=', ucwords(strtolower($request->input('nombre_ambiente'))))
        ->first();

        $regional = CentroFormacion::where('id_centro_formacion', '=', $request->input('id_centro_formacion'))
        ->first();
        
        if($ambientes_registrados == null) {
            $ambiente = Ambiente::where('id_ambiente', $id)->first();
            $nombre_esquema_ambiente = $ambiente->esquema_ambiente;
            $nombre_foto_ambiente = $ambiente->foto_ambiente;
    
            if($request->hasFile('esquema_ambiente')){
                File::delete(File::glob('imagenes_ambiente/'.$ambiente->esquema_ambiente));
    
                $esquema_ambiente = $request->file('esquema_ambiente');
                $nombre_esquema_ambiente = $esquema_ambiente->getClientOriginalName();
                $esquema_ambiente->move('imagenes_ambiente', $nombre_esquema_ambiente);
            }
    
            if($request->hasFile('foto_ambiente')){
                File::delete(File::glob('imagenes_ambiente/'.$ambiente->foto_ambiente));
    
                $foto_ambiente = $request->file('foto_ambiente');
                $nombre_foto_ambiente = $foto_ambiente->getClientOriginalName();
                $foto_ambiente->move('imagenes_ambiente', $nombre_foto_ambiente);
            }
    
            Ambiente::where('id_ambiente', $id)->update([
                'codigo_ambiente'           => $regional->id_regional.$request->id_centro_formacion.$request->area_cualificacion_id.$request->id_tipo_ambiente,
                'nombre_ambiente'           => ucwords(strtolower($request->nombre_ambiente)),
                'area_cualificacion_id'     => $request->area_cualificacion_id,
                'id_tipo_ambiente'          => $request->id_tipo_ambiente,
                'id_tenencia'               => $request->id_tenencia,
                'id_centro_formacion'       => $request->id_centro_formacion,
                'id_sede'                   => $request->id_sede,
    
                'cantidad_aprendices'       => $request->cantidad_aprendices,
                'dimension_largo'           => $request->dimension_largo,
                'dimension_ancho'           => $request->dimension_ancho,
                'dimension_alto'            => $request->dimension_alto,
                'area_ambiente'             => $request->area_ambiente,
                'puerta_alto'               => $request->puerta_alto,
                'puerta_ancho'              => $request->puerta_ancho,
                'id_piso'                   => $request->id_piso,
                'id_muro'                   => $request->id_muro,
                'id_guardaescoba'           => $request->id_guardaescoba,
                'id_cieloraso'              => $request->id_cieloraso,
                'id_ventaneria'             => $request->id_ventaneria,
                'id_puerta'                 => $request->id_puerta,
                'otro_piso'                 => $request->otro_piso,
                'otro_muro'                 => $request->otro_muro,
                'otro_guardaescoba'         => $request->otro_guardaescoba,
                'otro_cieloraso'            => $request->otro_cieloraso,
                'otra_ventaneria'           => $request->otra_ventaneria,
                'otra_puerta'               => $request->otra_puerta,
    
                'otro_tipo_riesgo'          => $request->otro_tipo_riesgo,
    
                'esquema_ambiente'          => $nombre_esquema_ambiente,
                'foto_ambiente'             => $nombre_foto_ambiente,
    
                'produccion_centros'        => $request->produccion_centros,
    
                'iluminacion_natural'       => $request->iluminacion_natural,
                'iluminacion_artificial'    => $request->iluminacion_artificial,
                'ventilacion_natural'       => $request->ventilacion_natural,
                'ventilacion_mecanica'      => $request->ventilacion_mecanica,
                'gas_natural'               => $request->gas_natural,
                'gas_propano'               => $request->gas_propano,
                'electrica_bifasica'        => $request->electrica_bifasica,
                'electrica_trifasica'       => $request->electrica_trifasica,
                'electrica_regulada'        => $request->electrica_regulada,
                'red_datos'                 => $request->red_datos,
                'hidro_aguafria'            => $request->hidro_aguafria,
                'hidro_aguacaliente'        => $request->hidro_aguacaliente,
                'hidro_lavaojos'            => $request->hidro_lavaojos,
                'hidro_banco_hielo'         => $request->hidro_banco_hielo,
                'hidro_trampa_grasas'       => $request->hidro_trampa_grasas,
                'hidro_vapor'               => $request->hidro_vapor,
                'hidro_desarenador'         => $request->hidro_desarenador,
                'hidro_desagues'            => $request->hidro_desagues,
                'hidro_lavapies'            => $request->hidro_lavapies,
    
                'hidro_otro'                => $request->hidro_otro,
                'observaciones_generales'   => $request->observaciones_generales,
                'fecha_creacion_ambiente'   => date('Y-m-d')
            ]);
    
            $producto = $request->input('id_producto');
            $tipo_riesgo = $request->input('id_tipo_riesgo');
    
            $validacion_productos = ProductoAmbiente::where('ambiente_id', $id)->get();
            $validacion_tipo_riesgo = TipoRiesgoAmbiente::where('ambiente_id', $id)->get();
    
            if ($producto != null) {
                if($validacion_productos != "[]"){
                    $validacion_productos = ProductoAmbiente::where('ambiente_id', $id)->delete();
                    for ($i=0; $i < count($producto); $i++) {
                        $producto_ambiente = new ProductoAmbiente;
                        $producto_ambiente->producto_id = $producto[$i];
                        $producto_ambiente->ambiente_id = $id;
                        $producto_ambiente->save();
                    }
                }
                else {
                    for ($i=0; $i < count($producto); $i++) {
                        $producto_ambiente = new ProductoAmbiente;
                        $producto_ambiente->producto_id = $producto[$i];
                        $producto_ambiente->ambiente_id = $id;
                        $producto_ambiente->save();
                    }
                }
            }
            else {
                $validacion_productos = ProductoAmbiente::where('ambiente_id', $id)->delete();
            }
    
            if ($tipo_riesgo != null) {
                if($validacion_tipo_riesgo != "[]"){
                    $validacion_productos = TipoRiesgoAmbiente::where('ambiente_id', $id)->delete();
                    for ($i=0; $i <count($tipo_riesgo); $i++) {
                        $tipo_riesgo_ambiente =  new TipoRiesgoAmbiente;
                        $tipo_riesgo_ambiente->tipo_riesgo_id = $tipo_riesgo[$i];
                        $tipo_riesgo_ambiente->ambiente_id  = $id;
                        $tipo_riesgo_ambiente->save();
                    }
                }
                else {
                    for ($i=0; $i <count($tipo_riesgo); $i++) {
                        $tipo_riesgo_ambiente =  new TipoRiesgoAmbiente;
                        $tipo_riesgo_ambiente->tipo_riesgo_id = $tipo_riesgo[$i];
                        $tipo_riesgo_ambiente->ambiente_id  = $id;
                        $tipo_riesgo_ambiente->save();
                    }
                }
            }
            else {
                $validacion_productos = TipoRiesgoAmbiente::where('ambiente_id', $id)->delete();
            }
    
            Alert::success('Actualizado', 'Ambiente con éxito');
        }
        else {
            Alert::error('Error', 'El Ambiente '.ucwords(strtolower($request->input('nombre_ambiente'))).' ya Existe');
        }
       
        return redirect(route('ambientes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Busqueda de Ambientes de Formacion
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function buscar_ambientes(Request $request)
    {
        $ambiente = ucwords(strtolower($request->input('buscar_ambiente')));

        $busqueda = Ambiente::where('nombre_ambiente', 'LIKE', "%$ambiente%")
            ->orderBy('nombre_ambiente', 'asc')
            ->first();

        if ($ambiente == "") {
            Alert::error('Error', 'Escriba para Buscar');
            return redirect(route('ambientes.index'));
        }
        if (isset($busqueda)) {
            $ambientes = Ambiente::where('nombre_ambiente', 'LIKE', "%$ambiente%")
                ->orderBy('nombre_ambiente', 'asc')
                ->paginate(20);
            return view('admin.ambientes.gestionar', compact('ambientes'));
        }
        else {
            Alert::info('Información', 'No se Encontraron Ambientes de Formación');
            return redirect(route('ambientes.index'));
        }
    }

    /**
     * Autocompletar Productos
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function autocompletar_productos(Request $request)
    {
        $producto = ucwords(strtolower($request->get('autocompletar_producto')));

        if ($producto != "") {
            $productos = Producto::where('producto', 'LIKE', "$producto%")
                ->orderBy('producto', 'asc')
                ->limit(10)
                ->get();
        }
        else {
            $productos = Producto::orderBy('producto', 'asc')
                ->limit(10)
                ->get();
        }

        $resultados= array();
        foreach ($productos as $producto) {
            $resultados[]= array("label" => $producto->producto, "value" => $producto->id_producto);
        }

        echo json_encode($resultados);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_ambientes($id_programa)
    {
        $programas_ambiente = AmbientePrograma::where('id_programa', $id_programa)
        ->get();

        foreach ($programas_ambiente as $programa_ambiente) {
            echo "<option value='{$programa_ambiente->id_ambiente}'>{$programa_ambiente->ambiente->nombre_ambiente}</option>";
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_sede(Request $request)
    {
        $sedes_registradas = Sede::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
        ->where('sede', '=', ucwords(strtolower($request->input('sede'))))
        ->first();
  
        if ($sedes_registradas == null) {
            $sede = new Sede;
            $sede->id_centro_formacion  = Auth::user()->id_centro_formacion;
            $sede->sede                 = ucwords(strtolower($request->input('sede')));
            $sede->direccion            = $request->input('direccion');
            $sede->telefono             = $request->input('telefono');
            $sede->save();

            Alert::success('Registrada', 'Sede con éxito');
        }
        else {
            Alert::error('Error', 'La Sede '.ucwords(strtolower($request->input('sede'))).' ya esta Registrada');
        }

        return redirect(route('ambientes.create'));              
    }
}
