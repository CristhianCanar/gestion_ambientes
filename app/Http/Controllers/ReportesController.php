<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\AmbientesExport;
use App\Exports\CentrosFormacionExport;
use App\Exports\ProgramasFormacionExport;
use App\Exports\RedesConocimientoExport;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class ReportesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.reportes.reporte');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_ambientes(Request $request)
    {
        $items_ambientes = $request->input('ambiente');
        if($items_ambientes != ""){
            $exportacion_ambientes = new AmbientesExport();
            $exportacion_ambientes->setItemsAmbientes($items_ambientes);
            return Excel::download($exportacion_ambientes, 'Reporte_Ambientes.xlsx');
        }
        else{
            Alert::error('Seleccione', 'Items para Descargar');
            return redirect(route('reportes.index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_centros_formacion(Request $request)
    {
        $centros_formacion = $request->input('centro_formacion');
        if($centros_formacion != ""){
            $exportacion_centrosf = new CentrosFormacionExport();
            $exportacion_centrosf->setCentrosFormacion($centros_formacion);
            return Excel::download($exportacion_centrosf, 'Reporte_Centros_Formación.xlsx');
        }
        else{
            Alert::error('Seleccione', 'Items para Descargar');
            return redirect(route('reportes.index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_redes_conocimiento(Request $request)
    {
        $red_conocimiento = $request->input('red_conocimiento');
        if($red_conocimiento != ""){
            array_unshift($red_conocimiento,'redconocimiento');
            $exportacion_redesc = new RedesConocimientoExport();
            $exportacion_redesc->setRedesConocimiento($red_conocimiento);
            return Excel::download($exportacion_redesc, 'Reporte_Redes_Conocimiento.xlsx');
        }
        else{
            Alert::error('Seleccione', 'Items para Descargar');
            return redirect(route('reportes.index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_programas_formacion(Request $request)
    {
        $programas_formacion = $request->input('programa_formacion');
        if($programas_formacion != ""){
            array_unshift($programas_formacion,'nombre_programa');
            $exportacion_programasf = new ProgramasFormacionExport();
            $exportacion_programasf->setProgramasFormacion($programas_formacion);
            return Excel::download($exportacion_programasf, 'Reporte_Programas_Formación.xlsx');
        }
        else{
            Alert::error('Seleccione', 'Items para Descargar');
            return redirect(route('reportes.index'));
        }
    }

}
