<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modulo;
use App\Perfil;
use App\ModuloPerfil;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class PerfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perfiles = Perfil::orderBy('id_perfil_usuario', 'asc')
        ->orderBy('created_at', 'desc')
        ->paginate(10);
        
        return view('admin.perfiles.gestionar', compact('perfiles'));
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gestionar_permisos()
    {
        $perfiles = Perfil::orderBy('id_perfil_usuario', 'asc')->get();
        $modulos = Modulo:: orderBy('id_modulo', 'asc')
        ->orderBy('id_modulo_padre', 'asc')
        ->get();
        $moduloxperfil = ModuloPerfil::get();

        return view('admin.perfiles.gestionar_permisos', compact('perfiles', 'modulos', 'moduloxperfil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.perfiles.registro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $perfiles_registrados = Perfil::where('perfil_usuario', ucwords(strtolower($request->input('perfil_usuario'))))
        ->first();

        if($perfiles_registrados == null) {
            $perfil = New Perfil;
            $perfil->perfil_usuario = ucwords(strtolower($request->input('perfil_usuario')));
            $perfil->save();

            Alert::success('Registrado', 'Perfil con éxito');
        }
        else {
            Alert::error('Error', 'El Perfil '.ucwords(strtolower($request->input('perfil_usuario'))).' ya esta Registrado');
        }

        return redirect(route('perfiles.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perfil = Perfil::where('id_perfil_usuario', $id)->first();

        return view('admin.perfiles.actualizar', compact('perfil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $perfiles_registrados = Perfil::where('id_perfil_usuario', '!=', $id)
        ->where('perfil_usuario', ucwords(strtolower($request->input('perfil_usuario'))))
        ->first();

        if($perfiles_registrados == null) {
            Perfil::where('id_perfil_usuario', $id)->update([
                'perfil_usuario' => ucwords(strtolower($request->perfil_usuario))
            ]);

            Alert::success('Actualizado', 'Perfil con éxito');
        }
        else {
            Alert::error('Error', 'El Perfil '.ucwords(strtolower($request->input('perfil_usuario'))).' ya Existe');
        }

        return redirect(route('perfiles.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function gestion_permisos(Request $request)
    {
        $id_modulo              = $request->input('id_modulo');
        $id_perfil              = $request->input('id_perfil');
        $oper                   = $request->input('oper');

        $id = false;
        if($oper =='add'){
            $id = DB::table('moduloxperfil')->insert([
                'id_perfil_usuario' => $id_perfil,
                'id_modulo'         => $id_modulo
                ]);
        }else{
            $id = DB::table('moduloxperfil')->where('id_perfil_usuario', '=', $id_perfil)
            ->where('id_modulo', '=', $id_modulo)
            ->delete();
        }

        echo json_encode([
            '_token'    =>  csrf_token(),
            'isOK'      =>  !!$id
        ]);
    }
}
