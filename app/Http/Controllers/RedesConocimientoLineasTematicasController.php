<?php

namespace App\Http\Controllers;

use App\LineaTematica;
use App\RedConocimientoLineaTematica;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class RedesConocimientoLineasTematicasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_red_conocimiento)
    {
        /*$red = RedConocimiento::where('idredconocimiento', $id_red_conocimiento)->first();
        $lineas = LineaTematica::where('id_redconocimiento', $id_red_conocimiento)
        ->orderBy('linea_tematica', 'asc')
        ->orderBy('created_at', 'desc')
        ->get();
        */

        $redes_lineas = RedConocimientoLineaTematica::where('red_conocimiento_id', '=', $id_red_conocimiento)
        ->get();

        return view('admin.redes.lineas.gestionar', compact('redes_lineas', 'id_red_conocimiento'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_red_conocimiento)
    {
        $lineas_tematicas = LineaTematica::orderBy('linea_tematica', 'asc')
        ->get();

        return view('admin.redes.lineas.registro', compact('lineas_tematicas', 'id_red_conocimiento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id_red_conocimiento)
    {
        $lineas_registradas = RedConocimientoLineaTematica::where('red_conocimiento_id', '=', $id_red_conocimiento)
        ->where('linea_tematica_id', '=', $request->input('linea_tematica'))
        ->first();

        if ($lineas_registradas == null) {
            $red_linea = new RedConocimientoLineaTematica;
            $red_linea->red_conocimiento_id = $id_red_conocimiento;
            $red_linea->linea_tematica_id   = $request->input('linea_tematica');
            $red_linea->save();

            Alert::success('Asociada', 'Linea Temática con éxito');
        }
        else {
            Alert::error('Error', 'La Linea Temática '.$lineas_registradas->linea_tematica->linea_tematica.' ya esta Registrada');
        }
        
        return redirect(route('redes_conocimiento.lineas_tematicas.create', $id_red_conocimiento));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_red_conocimiento, $id_linea_tematica)
    {
        $red_linea = RedConocimientoLineaTematica::where('red_conocimiento_id', '=', $id_red_conocimiento)
        ->where('linea_tematica_id', '=', $id_linea_tematica)
        ->first();

        return view('admin.redes.lineas.detalle', compact('red_linea', 'id_red_conocimiento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_red_conocimiento, $id_linea_tematica)
    {
        /*
        $linea = LineaTematica::where('id_linea_tematica', $id_linea_tematica)->first();
        return view('admin.redes.lineas.actualizar', compact('linea', 'id_red_conocimiento'));
        */
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_red_conocimiento, $id_linea_tematica)
    {
        /*
        $lineas_registradas = LineaTematica::where('id_linea_tematica', '!=', $id_linea_tematica)
        ->where('linea_tematica', '=', ucwords(strtolower($request->input('linea_tematica'))))
        ->first();

        if ($lineas_registradas == null) {
            LineaTematica::where('id_linea_tematica', $id_linea_tematica)->update([
                'linea_tematica' => ucwords(strtolower($request->linea_tematica)),
            ]);
    
            Alert::success('Actualizada', 'Linea Temática con éxito');
        }
        else {
            Alert::error('Error', 'La Linea Temática '.ucwords(strtolower($request->input('linea_tematica'))).' ya Existe');
        }
        
        return redirect(route('redes_conocimiento.lineas_tematicas.index', $id_red_conocimiento));
        */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_red_conocimiento, $id_linea_tematica)
    {
        RedConocimientoLineaTematica::where('red_conocimiento_id', '=', $id_red_conocimiento)
        ->where('linea_tematica_id', '=', $id_linea_tematica)
        ->delete();

        Alert::success('Eliminada', 'Linea Tecnológica con éxito');
        return redirect(route('redes_conocimiento.lineas_tematicas.index', $id_red_conocimiento));
    }
}
