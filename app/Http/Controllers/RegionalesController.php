<?php

namespace App\Http\Controllers;

use App\Departamento;
use Illuminate\Http\Request;
use App\Municipio;
use Illuminate\Support\Facades\DB;

class RegionalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Obtiene Municipios
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_municipios($id_regional)
    {
        $municipios = DB::table('departamentos')
        ->join('municipios','departamentos.id_departamento', '=', 'municipios.departamento_id')
        ->select('municipios.id_municipio', 'municipio')
        ->where('departamentos.regional_id',$id_regional)
        ->get();

        foreach ($municipios as $m) {
            echo "<option value='{$m->id_municipio}'>{$m->municipio}</option>";
        }

    }

    /**
     * Obtiene Centros de formación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_centros($id_municipio)
    {
        $centros = DB::table('centro_formacion')        
        ->select('id_centro_formacion', 'centro_formacion')
        ->where('id_municipio',$id_municipio)
        ->get();

        foreach ($centros as $c) {
            echo "<option value='{$c->id_centro_formacion}'>{$c->centro_formacion}</option>";
        }
        
    }

    /**
     * Obtiene Sedes
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_sedes($id_centro_formacion)
    {
        $sedes = DB::table('sedes')        
        ->select('id_sede', 'sede')
        ->where('id_centro_formacion',$id_centro_formacion)
        ->get();

        foreach ($sedes as $s) {
            echo "<option value='{$s->id_sede}'>{$s->sede}</option>";
        }
        
    }
}
