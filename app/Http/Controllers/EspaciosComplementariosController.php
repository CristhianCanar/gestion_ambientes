<?php

namespace App\Http\Controllers;

use App\Ambiente;
use App\EspacioAmbiente;
use Illuminate\Http\Request;

/*Ruta para usar Alertas*/
use RealRashid\SweetAlert\Facades\Alert;

class EspaciosComplementariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

        /**
     * Gestionar Espacios Complementarios
     *
     * @return \Illuminate\Http\Response
     */
    public function gestionar_espacios_complementarios($id_ambiente)
    {
        $espacios_complementarios = EspacioAmbiente::where('id_ambiente', '=', $id_ambiente)
        ->orderBy('espacio', 'asc')
        ->orderBy('created_at', 'desc')
        ->paginate(10);

        return view('admin.ambientes.espacios_complementarios.gestionar', compact('espacios_complementarios', 'id_ambiente'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_espacios_complementarios($id_ambiente)
    {
        return view('admin.ambientes.espacios_complementarios.registro', compact('id_ambiente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_ambiente = $request->input('id_ambiente');
        $ambiente = Ambiente::where('id_ambiente', '=', $id_ambiente)->first();

        $espacios_registrados = EspacioAmbiente::where('id_ambiente', '=', $id_ambiente)
        ->where('espacio', '=', ucwords(strtolower($request->input('espacio'))))
        ->first();

        if ($espacios_registrados == null) {
            if($request->input('area') < $ambiente->area_ambiente) {
                $espacio_ambiente = new EspacioAmbiente;
                $espacio_ambiente->id_ambiente  = $id_ambiente;
                $espacio_ambiente->espacio      = ucwords(strtolower($request->input('espacio')));
                $espacio_ambiente->area         = $request->input('area');
                $espacio_ambiente->save();
    
                Alert::success('Asociado', 'Espacio Complementario con éxito');
            }
            else {
                Alert::error('Error', 'La Area del Espacio Complementario deber ser Menor al Area del Ambiente');
            }
        }
        else {
            Alert::error('Error', 'El Espacio Complementario '.ucwords(strtolower($request->input('espacio'))).' ya esta Asociado al Ambiente');
        }

        return redirect(route('espacios_complementarios.create_espacios_complementarios', $id_ambiente));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $espacio_complementario = EspacioAmbiente::where('id_espacio',$id)->first();

        return view('admin.ambientes.espacios_complementarios.actualizar',compact('espacio_complementario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id_ambiente = $request->input('id_ambiente');

        $ambiente = Ambiente::where('id_ambiente', $id_ambiente)->first();

        $espacios_registrados = EspacioAmbiente::where('id_espacio', '!=', $id)
        ->where('id_ambiente', '=', $id_ambiente)
        ->where('espacio', '=', ucwords(strtolower($request->input('espacio'))))
        ->first();

        if ($espacios_registrados == null) {
            if($request->input('area') < $ambiente->area_ambiente){
                EspacioAmbiente::where('id_espacio', $id)->update([
                    'espacio'   => ucwords(strtolower($request->espacio)),
                    'area'      => $request->area,
                ]);

                Alert::success('Actualizado', 'Espacio Complementario con éxito');
            }
            else {
                Alert::error('Error', 'La Area del Espacio Complementario deber ser Menor al Area del Ambiente');
            }
        }
        else {
            Alert::error('Error', 'El Espacio Complementario '.ucwords(strtolower($request->input('espacio'))).' ya Existe');
        }

        return redirect(route('espacios_complementarios.gestionar_espacios_complementarios',$id_ambiente));
    }


        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete_espacios_complementarios($id, $id_ambiente)
    {
        EspacioAmbiente::where('id_ambiente', $id_ambiente)
        ->where('id_espacio', $id)
        ->delete();

        Alert::success('Eliminado', 'Espacio Complementario con éxito');
        return redirect(route('espacios_complementarios.gestionar_espacios_complementarios',$id_ambiente));
    }
}
