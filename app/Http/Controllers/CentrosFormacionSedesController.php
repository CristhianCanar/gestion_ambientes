<?php

namespace App\Http\Controllers;

use App\Sede;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class CentrosFormacionSedesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_centro)
    {
        $sedes = Sede::where('id_centro_formacion', $id_centro)
        ->orderBy('sede', 'asc')
        ->orderBy('created_at', 'desc')
        ->paginate('10');

        return view('admin.sedes.gestionar', compact('sedes', 'id_centro'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_centro)
    {
        return view('admin.sedes.registro', compact('id_centro'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_centro = $request->input('id_centro_formacion');

        $sedes_registradas = Sede::where('id_centro_formacion', '=', $id_centro)
        ->where('sede', '=', ucwords(strtolower($request->input('sede'))))
        ->first();

        if ($sedes_registradas == null) {
            $sede = new Sede;
            $sede->id_centro_formacion  = $id_centro;
            $sede->sede                 = ucwords(strtolower($request->input('sede')));
            $sede->direccion            = $request->input('direccion');
            $sede->telefono             = $request->input('telefono');
            $sede->save();

            Alert::success('Registrada', 'Sede con éxito');
        }
        else {
            Alert::error('Error', 'La Sede '.ucwords(strtolower($request->input('sede'))).' ya esta Registrada');
        } 
        
        return redirect(route('centros_formacion.sedes.create', $id_centro));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_centro, $id_sede)
    {
        $sede = Sede::where('id_sede', $id_sede)->first();
        return view('admin.sedes.detalle', compact('sede', 'id_centro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_centro, $id_sede)
    {
        $sede = Sede::where('id_sede', '=', $id_sede)->first();
        return view('admin.sedes.actualizar', compact('sede', 'id_centro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_centro, $id_sede)
    {
        $sedes_registradas = Sede::where('id_centro_formacion', '=', $id_centro)
        ->where('id_sede', '!=', $id_sede)
        ->where('sede', '=', ucwords(strtolower($request->input('sede'))))
        ->first();

        if ($sedes_registradas == null) {
            Sede::where('id_sede', $id_sede)->update([
                'sede'      => ucwords(strtolower($request->sede)),
                'direccion' => $request->direccion,
                'telefono'  => $request->telefono,
            ]);

            Alert::success('Actualizada', 'Sede con éxito');
        }
        else {
            Alert::error('Error', 'La Sede '.ucwords(strtolower($request->input('sede'))).' ya Existe');
        }
        
        return redirect(route('centros_formacion.sedes.index',$id_centro));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_centro, $id_sede)
    {
        Sede::where('id_sede', $id_sede)
        ->where('id_centro_formacion', $id_centro)
        ->delete();

        Alert::success('Eliminada', 'Sede con éxito');
        return redirect(route('centros_formacion.sedes.index',$id_centro));
    }
}
