<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\LineaTematica;
use App\ProgramaFormacion;
use App\RedConocimiento;
use App\RedConocimientoLineaTematica;
use Illuminate\Http\Request;
/*Ruta para usar Alertas*/
use RealRashid\SweetAlert\Facades\Alert;

class RedesConocimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $redes = RedConocimiento::orderBy('redconocimiento','asc')
        ->orderBy('created_at', 'desc')
        ->paginate(10);

        return view('admin.redes.gestionar', compact('redes'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.redes.registro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $redes_registradas = RedConocimiento::where('redconocimiento', '=', ucwords(strtolower($request->input('redconocimiento'))))
        ->first();

        if ($redes_registradas == null) {
            $red = new RedConocimiento;
            $red->redconocimiento   =   ucwords(strtolower($request->input('redconocimiento')));
            $red->nombre_gestor     =   ucwords(strtolower($request->input('nombre_gestor')));
            $red->mail_gestor       =   strtolower($request->input('mail_gestor'));
            $red->tel_gestor        =   $request->input('tel_gestor');
            $red->nombre_asesor     =   ucwords(strtolower($request->input('nombre_asesor')));
            $red->mail_asesor       =   strtolower($request->input('mail_asesor'));
            $red->tel_asesor        =   $request->input('tel_asesor');
            $red->save();

            Alert::success('Registrada', 'Red de Conocimiento con éxito');
        }
        else {
            Alert::error('Error', 'La Red de Conocimiento '.ucwords(strtolower($request->input('redconocimiento'))).' ya esta Registrada');
        }
        
        return redirect(route('redes_conocimiento.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $red = RedConocimiento::where('idredconocimiento', $id)->first();
        return view('admin.redes.detalle', compact('red'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $red = RedConocimiento::where('idredconocimiento', $id)->first();
        return view('admin.redes.actualizar', compact('red'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $redes_registradas = RedConocimiento::where('idredconocimiento', '!=', $id)
        ->where('redconocimiento', '=', ucwords(strtolower($request->input('redconocimiento'))))
        ->first();
        
        if ($redes_registradas == null) {

            RedConocimiento::where('idredconocimiento', $id)->update([
                'redconocimiento'   => ucwords(strtolower($request->redconocimiento)),
                'nombre_gestor'     => ucwords(strtolower($request->nombre_gestor)),
                'mail_gestor'       => strtolower($request->mail_gestor),
                'tel_gestor'        => $request->tel_gestor,
                'nombre_asesor'     => ucwords(strtolower($request->nombre_asesor)),
                'mail_asesor'       => strtolower($request->mail_asesor),
                'tel_asesor'        => $request->tel_asesor,
                ]);

            Alert::success('Actualizada', 'Red de Conocimiento con éxito');
        }
        else {
            Alert::error('Error', 'La Red de Conocimiento '.ucwords(strtolower($request->input('redconocimiento'))).' ya Existe');
        }

        return redirect(route('redes_conocimiento.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Busqueda de Redes de Conocimiento
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function buscar_redes_conocimiento(Request $request)
    {
        $red_conocimiento = ucwords(strtolower($request->input('buscar_red_conocimiento')));

        $busqueda = RedConocimiento::where('redconocimiento', 'LIKE', "%$red_conocimiento%")
            ->orderBy('redconocimiento', 'asc')
            ->first();

        if ($red_conocimiento == "") {
            Alert::error('Error', 'Escriba para Buscar');
            return redirect(route('redes_conocimiento.index'));
        }
        if (isset($busqueda)) {
            $redes = RedConocimiento::where('redconocimiento', 'LIKE', "%$red_conocimiento%")
                ->orderBy('redconocimiento', 'asc')
                ->paginate(20);
            return view('admin.redes.gestionar', compact('redes'));
        }
        else {
            Alert::info('Información', 'No se Encontraron Redes de Conocimiento');
            return redirect(route('redes_conocimiento.index'));
        }
    }

    /**
     * Listar las Lineas tematicas de las redes de conocimiento
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lineas_red($idred)
    {
        $lineas = LineaTematica::where('id_redconocimiento', $idred)
        ->orderBy('linea_tematica', 'asc')
        ->get();

        $red = RedConocimiento::where('idredconocimiento', $idred)
        ->orderBy('redconocimiento', 'asc')
        ->first();

        return view('admin.redes.lineas.gestionar', compact('lineas', 'red'));
    }

    /**
     * Lineas tematicas de registro ambientes
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_lineas($idredconocimiento)
    {
        $lineas_tematicas = RedConocimientoLineaTematica::where('red_conocimiento_id', '=', $idredconocimiento)
        ->get();

        foreach ($lineas_tematicas as $lt) {
            echo "<option value='{$lt->linea_tematica->id_linea_tematica}'>{$lt->linea_tematica->linea_tematica}</option>";
        }
    }

    /**
     * Programas de formacion de registro ambientes
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_programas($id_linea_tematica)
    {
        $programa = ProgramaFormacion::where('id_linea_tematica', '=', $id_linea_tematica)
        ->orderBy('nombre_programa', 'asc')
        ->get();

        foreach ($programa as $p) {
            echo "<option value='{$p->id_programa}'>{$p->codigo_programa} {$p->nombre_programa}</option>";
        }
    }

}
