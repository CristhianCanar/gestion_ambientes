<?php

namespace App\Http\Controllers;

use App\Ambiente;
use App\CentroFormacion;
use App\Http\Controllers\Controller;
use App\Jornada;
use App\LineaTematica;
use App\Modulo;
use App\Perfil;
use App\ProgramacionAmbiente;
use App\ProgramaFormacion;
use App\RedConocimiento;
use App\Sede;
use App\Tenencia;
use App\TipoAmbiente;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Tablero Ambientes Registrados.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $shtml = HomeAuthController::show_menu(Modulo::get_menu(null, 1));
        $numero = 55;

        /* consulta cantidad total de ambientes registrados, ambientes programados y usuarios registrados */
        if (Auth::user()->id_perfil_usuario == 1) {
            $cantidad_ambientes_registrados = Ambiente::count();
            $cantidad_ambientes_programados = ProgramacionAmbiente::count();
            $cantidad_usuarios_registrados = User::where('id_perfil_usuario', '!=', 1)->count();
        }
        else {
            $cantidad_ambientes_registrados = Ambiente::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->count();
            $cantidad_ambientes_programados = ProgramacionAmbiente::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
            ->count();
            $cantidad_usuarios_registrados = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->where('id_perfil_usuario', '!=', 1)
            ->count();
        } 
        
        /* consulta ambientes por tipo */
        $tipos_ambiente = TipoAmbiente::orderBy('tipo_ambiente', 'asc')
        ->get();

        $cantidad_tipos_ambiente = [];
        foreach ($tipos_ambiente as $tipos) {
            if (Auth::user()->id_perfil_usuario == 1) {
                $numero_ambientes_tipo = Ambiente::where('id_tipo_ambiente', '=', $tipos->id_tipo_ambiente)->count();
                $cantidad_tipos_ambiente[] = $numero_ambientes_tipo;
            }
            else {
                $numero_ambientes_tipo = Ambiente::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
                ->where('id_tipo_ambiente', '=', $tipos->id_tipo_ambiente)->count();
                $cantidad_tipos_ambiente[] = $numero_ambientes_tipo;
            } 
        }        

        /* consulta ambientes por tenencia */
        $tenencia_ambiente = Tenencia::orderBy('tenencia', 'asc')
        ->get();

        $cantidad_tenencia_ambiente = [];
        foreach ($tenencia_ambiente as $tenencia) {
            if (Auth::user()->id_perfil_usuario == 1) {
                $numero_ambientes_tenencia = Ambiente::where('id_tenencia', '=', $tenencia->id_tenencia)->count();
                $cantidad_tenencia_ambiente[] = $numero_ambientes_tenencia;
            }
            else {
                $numero_ambientes_tenencia = Ambiente::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
                ->where('id_tenencia', '=', $tenencia->id_tenencia)->count();
                $cantidad_tenencia_ambiente[] = $numero_ambientes_tenencia;
            }
            
        }

        /* consulta ambientes por sede */
        $ambientes_sede = Sede::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
        ->orderBy('sede', 'asc')
        ->get();

        $cantidad_ambientes_sede = [];
        foreach ($ambientes_sede as $sede) {
            $numero_ambientes_sede = Ambiente::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->where('id_sede', '=', $sede->id_sede)->count();
            $cantidad_ambientes_sede[] = $numero_ambientes_sede;
        }

        /* Consulta ambientes por centro de formacion */
        $ambientes_centro_formacion = Ambiente::join('centro_formacion', 'ambiente.id_centro_formacion', 'centro_formacion.id_centro_formacion')
        ->select(DB::raw('centro_formacion.centro_formacion, count(ambiente.id_ambiente) as cantidad_ambientes'))
        ->groupBy('centro_formacion.centro_formacion')
        ->orderBy('cantidad_ambientes', 'desc')
        ->get()
        ->take(10);
        /*$cantidad_ambientes_centro_formacion = [];
        foreach ($ambientes_centro_formacion as $ambiente_centro) {
            $numero_ambientes_centro = Ambiente::where()
        }*/
        /* Consulta ambientes por red de conocimiento 
        $redes_conocimiento = RedConocimiento::get();

        foreach ($redes_conocimiento as $red) {
            $ambientes_red_conocimiento = Ambiente::select('ambiente.nombre_ambiente','redes_conocimiento.redconocimiento')
            ->join('ambiente_programa', 'ambiente.id_ambiente', 'ambiente_programa.id_ambiente')
            ->join('programas_formacion', 'ambiente_programa.id_programa', 'programas_formacion.id_programa')
            ->join('redes_conocimiento', 'programas_formacion.idredconocimiento', 'redes_conocimiento.idredconocimiento')
            ->where('ambiente.id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->where('redes_conocimiento.idredconocimiento', '=', $red->idredconocimiento)
            ->count();
            $cantidad_ambientes_red[] = $ambientes_red_conocimiento;
        }
        */

        return view('admin.tablero.ambientes_registrados', compact('numero','shtml', 'cantidad_ambientes_registrados', 
            'cantidad_usuarios_registrados', 'cantidad_ambientes_programados', 'tipos_ambiente', 'cantidad_tipos_ambiente', 
            'tenencia_ambiente', 'cantidad_tenencia_ambiente', 'ambientes_sede', 'cantidad_ambientes_sede', 
            'ambientes_centro_formacion'));
    }


    /**
     * Tablero Ambientes Programados.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ambientes_programados()
    {
        /* consulta cantidad total de ambientes registrados, ambientes programados y usuarios registrados */
        if (Auth::user()->id_perfil_usuario == 1) {
            $cantidad_ambientes_registrados = Ambiente::count();
            $cantidad_ambientes_programados = ProgramacionAmbiente::count();
            $cantidad_usuarios_registrados = User::where('id_perfil_usuario', '!=', 1)->count();
        }
        else {
            $cantidad_ambientes_registrados = Ambiente::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->count();
            $cantidad_ambientes_programados = ProgramacionAmbiente::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
            ->count();
            $cantidad_usuarios_registrados = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->where('id_perfil_usuario', '!=', 1)
            ->count();
        }         

        /* Consulta ambientes por mes, jornada y lineas tecnologicas*/
        $lineas_tecnologicas = LineaTematica::orderBy('linea_tematica', 'asc')->get();

        $cantidad_ambientes_linea = [];

        if (Auth::user()->id_perfil_usuario == 1) {
            for ($i = 1; $i <= 12; $i++) {
                $numero_ambientes_mes = ProgramacionAmbiente::whereMonth('created_at', '=', $i)
                ->count();
                $cantidad_ambientes_mes[$i] = $numero_ambientes_mes;
            }

            foreach ($lineas_tecnologicas as $linea) {
                $numero_ambientes_linea = ProgramacionAmbiente::join('ficha', 'programacion_ambientes.ficha_id', 'ficha.id_ficha')
                ->join('programas_formacion', 'ficha.programa_id', 'programas_formacion.id_programa')
                ->join('linea_tematica', 'programas_formacion.id_linea_tematica', 'linea_tematica.id_linea_tematica')
                ->where('linea_tematica.id_linea_tematica', '=', $linea->id_linea_tematica)
                ->count();
                $cantidad_ambientes_linea[] = $numero_ambientes_linea;
            }
        }
        else {
            for ($i = 1; $i <= 12; $i++) {
                $numero_ambientes_mes = ProgramacionAmbiente::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
                ->whereMonth('created_at', '=', $i)
                ->count();
                $cantidad_ambientes_mes[$i] = $numero_ambientes_mes;
            }

            foreach ($lineas_tecnologicas as $linea) {
                $numero_ambientes_linea = ProgramacionAmbiente::join('ficha', 'programacion_ambientes.ficha_id', 'ficha.id_ficha')
                ->join('programas_formacion', 'ficha.programa_id', 'programas_formacion.id_programa')
                ->join('linea_tematica', 'programas_formacion.id_linea_tematica', 'linea_tematica.id_linea_tematica')
                ->where('programacion_ambientes.centro_formacion_id', '=', Auth::user()->id_centro_formacion)
                ->where('linea_tematica.id_linea_tematica', '=', $linea->id_linea_tematica)
                ->count();
                $cantidad_ambientes_linea[] = $numero_ambientes_linea;
            }
        }

        return view('admin.tablero.ambientes_programados', compact('cantidad_ambientes_registrados', 'cantidad_ambientes_programados',
            'cantidad_usuarios_registrados', 'lineas_tecnologicas', 'cantidad_ambientes_linea', 'cantidad_ambientes_mes'));
    }

    public function usuarios_registrados()
    {
        /* consulta cantidad total de ambientes registrados, ambientes programados y usuarios registrados */
        if (Auth::user()->id_perfil_usuario == 1) {
            $cantidad_ambientes_registrados = Ambiente::count();
            $cantidad_ambientes_programados = ProgramacionAmbiente::count();
            $cantidad_usuarios_registrados = User::where('id_perfil_usuario', '!=', 1)->count();
        }
        else {
            $cantidad_ambientes_registrados = Ambiente::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->count();
            $cantidad_ambientes_programados = ProgramacionAmbiente::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
            ->count();
            $cantidad_usuarios_registrados = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->where('id_perfil_usuario', '!=', 1)
            ->count();
        }     
        

        /* consulta usuarios por perfil e instructores programados */
        $perfiles_usuario = Perfil::where('id_perfil_usuario', '!=', 1)
        ->orderBy('perfil_usuario', 'asc')->get();

        $cantidad_usuarios_perfil = [];
        if (Auth::user()->id_perfil_usuario == 1) {
            foreach ($perfiles_usuario as $perfil) {
                $numero_usuarios_perfil = User::where('id_perfil_usuario', '=', $perfil->id_perfil_usuario)
                ->count();
                $cantidad_usuarios_perfil[] = $numero_usuarios_perfil;
            }
        }
        else {
            foreach ($perfiles_usuario as $perfil) {
                $numero_usuarios_perfil = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
                ->where('id_perfil_usuario', '=', $perfil->id_perfil_usuario)
                ->count();
                $cantidad_usuarios_perfil[] = $numero_usuarios_perfil;
            }
        }

        return view('admin.tablero.usuarios_registrados', compact('cantidad_ambientes_registrados', 'cantidad_ambientes_programados',
            'cantidad_usuarios_registrados', 'perfiles_usuario', 'cantidad_usuarios_perfil'));
    }

    public static function show_menu($lista, $nivel1 = true){

            $html = '<ul class="'.  ($nivel1?'nav nav-pills nav-sidebar flex-column nav-child-indent ':'nav nav-treeview') .'"   '.($nivel1?'data-widget="treeview" role="menu" data-accordion="false"':'').'  >'.PHP_EOL;

        foreach($lista as $i){
            $html .= '<li class="nav-item has-treeview">'.PHP_EOL;

                $html .= "<a href='". ($i->url_modulo!='#'?route($i->url_modulo):'#')."' class='nav-link'>".PHP_EOL;
                $html .= "<i class='{$i->icono}'></i>".PHP_EOL;
                $html .= "<p>{$i->modulo}</p>".PHP_EOL;
                if( is_array( $i->hijos) and count($i->hijos) > 0 ){
                    $html .="<i class='right fas fa-angle-left'></i>".PHP_EOL;
                }
                $html .= '</a>'.PHP_EOL;

                if( is_array( $i->hijos) and count($i->hijos) > 0 ){
                    $html .= HomeAuthController::show_menu($i->hijos,false);
                }

            $html .= '</li>'.PHP_EOL;

        }
            $html .= '</ul>'.PHP_EOL;

            return $html;
    }
}
