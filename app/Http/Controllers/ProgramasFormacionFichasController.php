<?php

namespace App\Http\Controllers;

use App\Ficha;
use App\Jornada;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProgramasFormacionFichasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_programa)
    {
        $fichas = Ficha::where('programa_id', $id_programa)
        ->orderBy('numero_ficha', 'asc')
        ->orderBy('created_at', 'desc')
        ->paginate(10);

        return view('admin.fichas.gestionar', compact('fichas', 'id_programa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_programa)
    {
        $jornadas = Jornada::get();

        return view('admin.fichas.registro', compact('id_programa', 'jornadas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id_programa)
    {
        $id_centro = Auth::user()->id_centro_formacion;
        $fichas_registradas = Ficha::where('numero_ficha', '=', $request->input('numero_ficha'))
        ->first();

        if ($fichas_registradas == null) {
            if ($request->input('fecha_fin') > $request->input('fecha_inicio')) {
                $ficha = new Ficha;
                $ficha->programa_id         = $id_programa;
                $ficha->centro_formacion_id = $id_centro;
                $ficha->jornada_id          = $request->input('jornada_id');
                $ficha->numero_ficha        = $request->input('numero_ficha');
                $ficha->fecha_inicio        = $request->input('fecha_inicio');
                $ficha->fecha_fin           = $request->input('fecha_fin');
                $ficha->cupo_aprendices     = $request->input('cupo_aprendices');
                $ficha->save();

                Alert::success('Registrada', 'Ficha de Formación con éxito');
            }
            else {
                Alert::error('Error', 'La Fecha de Finalización debe ser Mayor a la Fecha de Apertura');
            }
        }
        else {
            Alert::error('Error', 'La Ficha de Formación '.$request->input('numero_ficha').' ya esta Registrada');
        }
        
        return redirect(route('programas_formacion.fichas.create', $id_programa));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_programa, $id_ficha)
    {
        $ficha = Ficha::where('id_ficha', $id_ficha)
        ->first();

        return view('admin.fichas.detalle', compact('id_programa', 'ficha'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_programa, $id_ficha)
    {
        $ficha = Ficha::where('id_ficha', $id_ficha)
        ->first();

        $jornadas = Jornada::where('id_jornada', '!=', $ficha->jornada_id)->get();

        return view('admin.fichas.actualizar',compact('id_programa', 'ficha', 'jornadas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_programa, $id_ficha)
    {
        $fichas_registradas = Ficha::where('id_ficha', '!=', $id_ficha)
        ->where('numero_ficha', '=', $request->input('numero_ficha'))
        ->first();

        if ($fichas_registradas == null) {
            if ($request->input('fecha_fin') > $request->input('fecha_inicio')) {
                Ficha::where('id_ficha', $id_ficha)->update([
                    'jornada_id'        => $request->jornada_id,
                    'numero_ficha'      => $request->numero_ficha,
                    'fecha_inicio'      => $request->fecha_inicio,
                    'fecha_fin'         => $request->fecha_fin,
                    'cupo_aprendices'   => $request->cupo_aprendices
                ]);

                Alert::success('Actualizada', 'Ficha de Formación con éxito');
            }
            else {
                Alert::error('Error', 'La Fecha de Finalización debe ser Mayor a la Fecha de Apertura');
            }
        }
        else {
            Alert::error('Error', 'La Ficha de Formación '.$request->input('numero_ficha').' ya Existe');
        }

        return redirect(route('programas_formacion.fichas.index', $id_programa));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
