<?php

namespace App\Http\Controllers;

use App\CentroFormacion;
use App\Perfil;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->id_perfil_usuario == 1) {
            $usuarios = User::orderBy('nombres', 'asc')
            ->orderBy('created_at', 'desc')
            ->paginate(100);
        }
        else {
            $usuarios = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->orderBy('nombres', 'asc')
            ->orderBy('created_at', 'desc')
            ->paginate(50);
        }

        return view('admin.usuarios.gestionar', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->id_perfil_usuario == 1) {
            $perfiles = Perfil::all();
            $centros_formacion = CentroFormacion::all();
        }
        else {
            $perfiles = Perfil::where('perfil_usuario', '!=', 'SuperAdministrador')
            ->get();
            $centros_formacion = CentroFormacion::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->get();
        }

        return view('admin.usuarios.registro', compact('perfiles', 'centros_formacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuarios_registrados = User::where('identificacion', '=', $request->input('identificacion'))
        ->first();

        if ($usuarios_registrados == null) {
            $usuario = new User;
            $usuario->id_perfil_usuario     = $request->input('id_perfil_usuario');
            $usuario->id_centro_formacion   = $request->input('id_centro_formacion');
            $usuario->identificacion        = $request->input('identificacion');
            $usuario->nombres               = ucwords(strtolower($request->input('nombres')));
            $usuario->apellidos             = ucwords(strtolower($request->input('apellidos')));
            $usuario->telefono              = $request->input('telefono');
            $usuario->email                 = strtolower($request->input('email'));
            $usuario->password              = Hash::make($request->input('password'));
            $usuario->estado                = true;
            $usuario->save();

            Alert::success('Registrado', 'Usuario con éxito');
        }
        else {
            Alert::error('Error', 'La Identificación '.$request->input('identificacion').' ya esta Registrada');
        }
        
        return redirect(route('usuarios.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::where('id', $id)->first();
        return view('admin.usuarios.detalle', compact('usuario'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::where('id', $id)->first();

        if (Auth::user()->id_perfil_usuario == 1) {
            $perfiles = Perfil::where('id_perfil_usuario', '!=', $usuario->id_perfil_usuario)
            ->get();

            $centros_formacion = CentroFormacion::where('id_centro_formacion', '!=', $usuario->id_centro_formacion)
            ->get();
        }
        else {
            $perfiles = Perfil::where('id_perfil_usuario', '!=', $usuario->id_perfil_usuario)
            ->where('id_perfil_usuario', '!=', 1)
            ->get();

            $centros_formacion = CentroFormacion::where('id_centro_formacion', '!=', $usuario->id_centro_formacion)
            ->where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->get();
        }

        return view('admin.usuarios.actualizar', compact('usuario', 'perfiles', 'centros_formacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuarios_registrados = User::where('id', '!=', $id)
        ->where('identificacion', '=', $request->input('identificacion'))
        ->first();

        if ($usuarios_registrados == null) {
            if ($request->input('password') != null) {
                User::where('id', $id)->update([
                    'id_perfil_usuario'     => $request->id_perfil_usuario,
                    'id_centro_formacion'   => $request->id_centro_formacion,
                    'identificacion'        => $request->identificacion,
                    'nombres'               => ucwords(strtolower($request->nombres)),
                    'apellidos'             => ucwords(strtolower($request->apellidos)),
                    'telefono'              => $request->telefono,
                    'email'                 => strtolower($request->email),
                    'password'              => Hash::make($request->input('password'))
                ]);
        
                Alert::success('Actualizado', 'Usuario con éxito');
            }
            else {
                User::where('id', $id)->update([
                    'id_perfil_usuario'     => $request->id_perfil_usuario,
                    'id_centro_formacion'   => $request->id_centro_formacion,
                    'identificacion'        => $request->identificacion,
                    'nombres'               => ucwords(strtolower($request->nombres)),
                    'apellidos'             => ucwords(strtolower($request->apellidos)),
                    'telefono'              => $request->telefono,
                    'email'                 => strtolower($request->email)
                ]);
        
                Alert::success('Actualizado', 'Usuario con éxito');
            }
        }
        else {
            Alert::error('Error', 'La Identificación '.$request->input('identificacion').' ya Existe');
        }

        return redirect(route('usuarios.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id', $id)->delete();

        Alert::success('Eliminado', 'Usuario con éxito');

        return redirect(route('usuarios.index'));
    }

    /**
     * Busqueda de Ambientes de Formacion
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function buscar_usuarios(Request $request)
    {
        $usuario = ucwords(strtolower($request->input('buscar_usuario')));

        $busqueda = User::where('nombres', 'LIKE', "%$usuario%")
            ->orWhere('apellidos', 'LIKE', "%$usuario%")
            ->orderBy('nombres', 'asc')
            ->first();

        if ($usuario == "") {
            Alert::error('Error', 'Escriba para Buscar');
            return redirect(route('usuarios.index'));
        }
        if (isset($busqueda)) {
            $usuarios = User::where('nombres', 'LIKE', "%$usuario%")
                ->orWhere('apellidos', 'LIKE', "%$usuario%")
                ->orderBy('nombres', 'asc')
                ->paginate(50);
            return view('admin.usuarios.gestionar', compact('usuarios'));
        }
        else {
            Alert::info('Información', 'No se Encontraron Usuarios');
            return redirect(route('usuarios.index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_perfil($id)
    {
        $usuario = User::where('id', $id)->first();

        $perfiles = Perfil::where('id_perfil_usuario', '!=', $usuario->id_perfil_usuario)
        ->get();

        $centros_formacion = CentroFormacion::where('id_centro_formacion', '!=', $usuario->id_centro_formacion)
        ->get();

        return view('admin.usuarios.perfil', compact('usuario', 'perfiles', 'centros_formacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_perfil(Request $request, $id)
    {
        $contrasena = $request->input('password');
        $contrasena_actual = Hash::check($contrasena, Auth::user()->password);

        $usuarios_registrados = User::where('id', '!=', $id)
        ->where('identificacion', '=', $request->input('identificacion'))
        ->first();

        if ($contrasena == $contrasena_actual) {
            if ($usuarios_registrados == null) {
                if ($request->input('new_password') and $request->input('confirm_new_password') != null
                    and $request->input('new_password') == $request->input('confirm_new_password')) {
                    User::where('id', $id)->update([
                        'identificacion'    => $request->identificacion,
                        'nombres'           => ucwords(strtolower($request->nombres)),
                        'apellidos'         => ucwords(strtolower($request->apellidos)),
                        'telefono'          => $request->telefono,
                        'email'             => strtolower($request->email),
                        'password'          => Hash::make($request->input('new_password'))
                    ]);

                    Alert::success('Actualizado', 'Perfil con éxito');
                }
                else {
                    User::where('id', $id)->update([
                        'identificacion'    => $request->identificacion,
                        'nombres'           => ucwords(strtolower($request->nombres)),
                        'apellidos'         => ucwords(strtolower($request->apellidos)),
                        'telefono'          => $request->telefono,
                        'email'             => strtolower($request->email)
                    ]);

                    Alert::success('Actualizado', 'Perfil con éxito');
                }
            }
            else {
                Alert::error('Error', 'La Identificación '.$request->input('identificacion').' ya Existe');   
            }
        }
        else {
            Alert::error('Error', 'Contraseña Actual Incorrecta');
        }

        return redirect(route('usuario.perfil', $id));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function usuarios_acceso($id, $estado)
    {
        if($estado == 0){
            User::where('id', $id)->update([
                'estado' => false
            ]);

            Alert::error('Actualizado', 'Acceso de Usuario Denegado');
            return redirect(route('usuarios.index'));
        }
        elseif($estado == 1){
            User::where('id', $id)->update([
                'estado' => true
            ]);

            Alert::success('Actualizado', 'Acceso de Usuario Permitido');
            return redirect(route('usuarios.index'));
        }
    }
}
