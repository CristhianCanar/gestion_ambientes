<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\NivelFormacion;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class NivelesFormacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $niveles = NivelFormacion::orderBy('nivel_programa', 'asc')
        ->orderBy('created_at', 'desc')
        ->paginate(10);

        return view('admin.administracion.programas_formacion.niveles_formacion.gestionar', compact('niveles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.administracion.programas_formacion.niveles_formacion.registro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $niveles_formacion_registrados = NivelFormacion::where('nivel_programa', '=', ucwords(strtolower($request->input('nivel_programa'))))
        ->first();

        if ($niveles_formacion_registrados == null) {
            $nivel = new NivelFormacion;
            $nivel->nivel_programa = ucwords(strtolower($request->input('nivel_programa')));
            $nivel->save();

            Alert::success('Registrado', 'Nivel de Formación con éxito');
        } 
        else {
            Alert::error('Error', 'El Nivel de Formación '.ucwords(strtolower($request->input('nivel_programa'))).' ya esta Registrado');
        }

        return redirect(route('niveles_formacion.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nivel = NivelFormacion::where('id_nivel_programa', $id)->first();

        return view('admin.administracion.programas_formacion.niveles_formacion.actualizar', compact('nivel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $niveles_formacion_registrados = NivelFormacion::where('id_nivel_programa', '!=', $id)
        ->where('nivel_programa', '=', ucwords(strtolower($request->input('nivel_programa'))))
        ->first();

        if ($niveles_formacion_registrados == null) {
            NivelFormacion::where('id_nivel_programa', $id)->update([
                'nivel_programa' => ucwords(strtolower($request->nivel_programa))
            ]);

            Alert::success('Actualizado', 'Nivel de Formación con éxito');
        }
        else {
            Alert::error('Error', 'El Nivel de Formación '.ucwords(strtolower($request->input('nivel_programa'))).' ya Existe');
        }

        return redirect(route('niveles_formacion.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
