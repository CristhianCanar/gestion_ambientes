<?php

namespace App\Http\Controllers;

use App\ActividadAprendizaje;
use App\Ambiente;
use App\AmbientePrograma;
use App\CompetenciaLaboral;
use App\CompetenciaLaboralPrograma;
use App\Festivo;
use App\Ficha;
use App\ProgramacionAmbiente;
use App\ProgramacionInstructor;
use App\ProgramacionResultadoAprendizaje;
use App\RedConocimiento;
use App\ResultadoAprendizaje;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class ProgramacionesAmbienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->perfiles->id_perfil_usuario == 1) {
            $programacion_ambientes = ProgramacionAmbiente::paginate(200);
        }
        else {
            $programacion_ambientes = ProgramacionAmbiente::where('centro_formacion_id', Auth::user()->id_centro_formacion)
            ->paginate(100);
        }

        return view('admin.programaciones_ambiente.gestionar', compact('programacion_ambientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ambientes = Ambiente::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
        ->orderBy('nombre_ambiente', 'asc')
        ->get();

        $usuarios = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
        ->where('id_perfil_usuario', '=', 4)
        ->orWhere('id_perfil_usuario', '=', 5)
        ->orderBy('nombres', 'asc')
        ->orderBy('apellidos', 'asc')
        ->get();

        return view('admin.programaciones_ambiente.registro', compact('ambientes', 'usuarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ambientes_programados = ProgramacionAmbiente::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
        ->where('ambiente_id', '=', $request->input('ambiente_id'))
        ->where('fecha_inicio', '<', $request->input('fecha_fin'))
        ->where('fecha_fin', '>', $request->input('fecha_inicio'))
        ->where(function ($query) use ($request) {
            if ($request->input('domingo') != null) {
                $query->orWhere('domingo', $request->input('domingo'));
            }
            if ($request->input('lunes') != null) {
                $query->orWhere('lunes', $request->input('lunes'));
            }
            if ($request->input('martes') != null) {
                $query->orWhere('martes', $request->input('martes'));
            }
            if ($request->input('miercoles') != null) {
                $query->orWhere('miercoles', $request->input('miercoles'));
            }
            if ($request->input('jueves') != null) {
                $query->orWhere('jueves', $request->input('jueves'));
            }
            if ($request->input('viernes') != null) {
                $query->orWhere('viernes', $request->input('viernes'));
            }
            if ($request->input('sabado') != null) {
                $query->orWhere('sabado', $request->input('sabado'));
            }
        })
        ->where('hora_inicio', '<', $request->input('hora_fin'))
        ->where('hora_fin', '>', $request->input('hora_inicio'))
        ->first();

        $fichas_programadas = ProgramacionAmbiente::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
        ->where('ficha_id', '=', $request->input('ficha_id'))
        ->where('fecha_inicio', '<', $request->input('fecha_fin'))
        ->where('fecha_fin', '>', $request->input('fecha_inicio'))
        ->where(function ($query) use ($request) {
            if ($request->input('domingo') != null) {
                $query->orWhere('domingo', $request->input('domingo'));
            }
            if ($request->input('lunes') != null) {
                $query->orWhere('lunes', $request->input('lunes'));
            }
            if ($request->input('martes') != null) {
                $query->orWhere('martes', $request->input('martes'));
            }
            if ($request->input('miercoles') != null) {
                $query->orWhere('miercoles', $request->input('miercoles'));
            }
            if ($request->input('jueves') != null) {
                $query->orWhere('jueves', $request->input('jueves'));
            }
            if ($request->input('viernes') != null) {
                $query->orWhere('viernes', $request->input('viernes'));
            }
            if ($request->input('sabado') != null) {
                $query->orWhere('sabado', $request->input('sabado'));
            }
        })
        ->where('hora_inicio', '<', $request->input('hora_fin'))
        ->where('hora_fin', '>', $request->input('hora_inicio'))
        ->first();

        $fechas_programadas = ProgramacionAmbiente::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
        ->where('fecha_inicio', '<', $request->input('fecha_fin'))
        ->where('fecha_fin', '>', $request->input('fecha_inicio'))
        ->where(function ($query) use ($request) {
            if ($request->input('domingo') != null) {
                $query->orWhere('domingo', $request->input('domingo'));
            }
            if ($request->input('lunes') != null) {
                $query->orWhere('lunes', $request->input('lunes'));
            }
            if ($request->input('martes') != null) {
                $query->orWhere('martes', $request->input('martes'));
            }
            if ($request->input('miercoles') != null) {
                $query->orWhere('miercoles', $request->input('miercoles'));
            }
            if ($request->input('jueves') != null) {
                $query->orWhere('jueves', $request->input('jueves'));
            }
            if ($request->input('viernes') != null) {
                $query->orWhere('viernes', $request->input('viernes'));
            }
            if ($request->input('sabado') != null) {
                $query->orWhere('sabado', $request->input('sabado'));
            }
        })
        ->where('hora_inicio', '<', $request->input('hora_fin'))
        ->where('hora_fin', '>', $request->input('hora_inicio'))
        ->get();

        foreach ($fechas_programadas as $fecha) {
            $programacion_instructores = ProgramacionInstructor::where('programacion_ambiente_id', '=', $fecha->id_programacion_ambiente)
            ->get();
        }

        $instructores_programados = array();

        if ($fechas_programadas != '[]') {
            foreach ($programacion_instructores as $programacion) {
                $instructores_programados[] = $programacion->instructor_id;
            } 
        }
        else {
            $instructores_programados[] = 0;
        }
       
        $instructores = $request->input('instructor_id');
        
        $instructor_programado = array_intersect($instructores, $instructores_programados);
        
        if (empty($instructor_programado)) {
            $instructor = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->whereIn('id', $instructores)
            ->first();
        }
        else {
            $instructor = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->whereIn('id', $instructor_programado)
            ->first();
        }
    
        $ambiente_programado = Ambiente::where('id_ambiente', '=', $request->input('ambiente_id'))
        ->first();

        if ($ambientes_programados == null) {
            if ($fichas_programadas == null) {
                if (empty($instructor_programado)) {
                
                    $resultados_aprendizaje = $request->input('resultado_aprendizaje_id');

                    foreach ($resultados_aprendizaje as $resultado) {
                        $resultados_aprendizaje_programados = ProgramacionResultadoAprendizaje::join('programacion_ambientes', 
                        'programacion_resultados_aprendizaje.programacion_ambiente_id', 'programacion_ambientes.id_programacion_ambiente')
                        ->where('programacion_ambientes.ficha_id', '=', $request->input('ficha_id'))
                        ->where('programacion_resultados_aprendizaje.resultado_aprendizaje_id', '=', $resultado)
                        ->get();
                    }

                    if ($resultados_aprendizaje_programados == '[]') {
                        $fecha_inicio   = new DateTime($request->input('fecha_inicio'));
                        $fecha_fin      = new DateTime($request->input('fecha_fin'));
                        
                        $rango_fechas = $fecha_inicio->diff($fecha_fin);

                        $dias_programados = $rango_fechas->format('%d');

                        $hora_inicio    = new DateTime($request->input('hora_inicio'));
                        $hora_fin       = new DateTime($request->input('hora_fin'));
                        
                        $rango_horas = $hora_inicio->diff($hora_fin);

                        $horas_programados = $rango_horas->format('%h');
                        $minutos_programados = $rango_horas->format('%i');

                        $total_minutos_programados = ($minutos_programados*$dias_programados)/60;
                        $total_horas_programadas = ($dias_programados*$horas_programados)+$total_minutos_programados;
                        
                        $programacion_ambiente = new ProgramacionAmbiente;
                        $programacion_ambiente->ambiente_id             = $request->input('ambiente_id');
                        $programacion_ambiente->ficha_id                = $request->input('ficha_id');
                        $programacion_ambiente->actividad_aprendizaje   = $request->input('actividad_aprendizaje');
                        $programacion_ambiente->fecha_inicio            = $request->input('fecha_inicio');
                        $programacion_ambiente->fecha_fin               = $request->input('fecha_fin');
                        $programacion_ambiente->hora_inicio             = $request->input('hora_inicio');
                        $programacion_ambiente->hora_fin                = $request->input('hora_fin');
                        $programacion_ambiente->domingo                 = $request->input('domingo');
                        $programacion_ambiente->lunes                   = $request->input('lunes');
                        $programacion_ambiente->martes                  = $request->input('martes');
                        $programacion_ambiente->miercoles               = $request->input('miercoles');
                        $programacion_ambiente->jueves                  = $request->input('jueves');
                        $programacion_ambiente->viernes                 = $request->input('viernes');
                        $programacion_ambiente->sabado                  = $request->input('sabado');
                        $programacion_ambiente->horas_programadas       = $total_horas_programadas;
                        $programacion_ambiente->responsable_id          = Auth::user()->id;
                        $programacion_ambiente->centro_formacion_id     = Auth::user()->id_centro_formacion;
                        $programacion_ambiente->save();

                        $programacion_ambiente = ProgramacionAmbiente::get()->last();

                        foreach ($resultados_aprendizaje as $resultado_aprendizaje) {
                            $programacion_resultados = new ProgramacionResultadoAprendizaje;
                            $programacion_resultados->programacion_ambiente_id   = $programacion_ambiente->id_programacion_ambiente;
                            $programacion_resultados->resultado_aprendizaje_id   = $resultado_aprendizaje;
                            $programacion_resultados->estado                     = "Programado";
                            $programacion_resultados->save();
                        }

                        foreach ($instructores as $instruct) {
                            $programacion_instructores = new ProgramacionInstructor;
                            $programacion_instructores->programacion_ambiente_id    = $programacion_ambiente->id_programacion_ambiente;
                            $programacion_instructores->instructor_id               = $instruct;
                            $programacion_instructores->save();
                        }

                        Alert::success('Registrada', 'Programación de Ambiente con éxito');
                    } 
                    else {
                        Alert::error('Error', 'El Resultado de Aprendizaje '.$resultados_aprendizaje_programados[0]->resultado_aprendizaje->resultado_aprendizaje.' ya esta Programado');
                    }

                }
                else {
                    Alert::error('Error', 'El Instructor '.$instructor->nombres.' '.$instructor->apellidos.' ya esta Programado');
                }
            }
            else {
                Alert::error('Error', 'La Ficha '.$fichas_programadas->ficha->numero_ficha.' ya esta Programada');
            }     
        }
        else {
            Alert::error('Error', 'El Ambiente '.$ambiente_programado->nombre_ambiente.' ya esta Programado');
        }
        
        return redirect(route('programaciones_ambiente.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $programacion_ambiente = ProgramacionAmbiente::where('id_programacion_ambiente', $id)->first();

        $programacion_resultados = ProgramacionResultadoAprendizaje::where('programacion_ambiente_id', $programacion_ambiente->id_programacion_ambiente)
        ->get();

        $programacion_instructores = ProgramacionInstructor::where('programacion_ambiente_id', '=', $programacion_ambiente->id_programacion_ambiente)
        ->get();

        return view('admin.programaciones_ambiente.detalle', compact('programacion_ambiente', 'programacion_resultados', 'programacion_instructores'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $programacion_ambiente = ProgramacionAmbiente::where('id_programacion_ambiente', $id)->first();

        $programacion_resultados = ProgramacionResultadoAprendizaje::where('programacion_ambiente_id', $programacion_ambiente->id_programacion_ambiente)
        ->get();

        /*
        $resultados_aprendizaje = DB::select('SELECT resultados_aprendizaje.* FROM resultados_aprendizaje
        WHERE resultados_aprendizaje.competencia_programa_id = '.$programacion_resultados[0]->resultado_aprendizaje->competencia_laboral_programa->competencia_laboral->id_competencia_laboral.' AND NOT EXISTS 
            (SELECT * FROM programacion_resultados_aprendizaje 
                WHERE programacion_resultados_aprendizaje.resultado_aprendizaje_id = resultados_aprendizaje.id_resultado_aprendizaje
                AND programacion_resultados_aprendizaje.resultado_aprendizaje_id = '.$programacion_resultados[0]->resultado_aprendizaje_id.'
            )'
        );
        */

        $programacion_instructores = ProgramacionInstructor::where('programacion_ambiente_id', '=', $programacion_ambiente->id_programacion_ambiente)
        ->get();

        $ambientes = Ambiente::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
        ->where('id_ambiente', '!=', $programacion_ambiente->ambiente_id)
        ->orderBy('nombre_ambiente', 'asc')
        ->get();
        
        $instructores_programados = array();

        foreach ($programacion_instructores as $programacion_instructor) {
            $instructores_programados[] = $programacion_instructor->instructor_id;
        }

        $instructores = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
        ->whereNotIn('id', $instructores_programados)
        ->where('id_perfil_usuario', '=', 4)
        ->orWhere('id_perfil_usuario', '=', 5)
        ->orderBy('nombres', 'asc')
        ->orderBy('apellidos', 'asc')
        ->get();

        return view('admin.programaciones_ambiente.actualizar', compact('programacion_ambiente', 'programacion_resultados',
            'programacion_instructores', 'ambientes', 'instructores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ambientes_programados = ProgramacionAmbiente::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
        ->where('id_programacion_ambiente', '!=', $id)
        ->where('ambiente_id', '=', $request->input('ambiente_id'))
        ->where('fecha_inicio', '<', $request->input('fecha_fin'))
        ->where('fecha_fin', '>', $request->input('fecha_inicio'))
        ->where(function ($query) use ($request) {
            if ($request->input('domingo') != null) {
                $query->orWhere('domingo', $request->input('domingo'));
            }
            if ($request->input('lunes') != null) {
                $query->orWhere('lunes', $request->input('lunes'));
            }
            if ($request->input('martes') != null) {
                $query->orWhere('martes', $request->input('martes'));
            }
            if ($request->input('miercoles') != null) {
                $query->orWhere('miercoles', $request->input('miercoles'));
            }
            if ($request->input('jueves') != null) {
                $query->orWhere('jueves', $request->input('jueves'));
            }
            if ($request->input('viernes') != null) {
                $query->orWhere('viernes', $request->input('viernes'));
            }
            if ($request->input('sabado') != null) {
                $query->orWhere('sabado', $request->input('sabado'));
            }
        })
        ->where('hora_inicio', '<', $request->input('hora_fin'))
        ->where('hora_fin', '>', $request->input('hora_inicio'))
        ->first();

        $fichas_programadas = ProgramacionAmbiente::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
        ->where('id_programacion_ambiente', '!=', $id)
        ->where('ficha_id', '=', $request->input('ficha_id'))
        ->where('fecha_inicio', '<', $request->input('fecha_fin'))
        ->where('fecha_fin', '>', $request->input('fecha_inicio'))
        ->where(function ($query) use ($request) {
            if ($request->input('domingo') != null) {
                $query->orWhere('domingo', $request->input('domingo'));
            }
            if ($request->input('lunes') != null) {
                $query->orWhere('lunes', $request->input('lunes'));
            }
            if ($request->input('martes') != null) {
                $query->orWhere('martes', $request->input('martes'));
            }
            if ($request->input('miercoles') != null) {
                $query->orWhere('miercoles', $request->input('miercoles'));
            }
            if ($request->input('jueves') != null) {
                $query->orWhere('jueves', $request->input('jueves'));
            }
            if ($request->input('viernes') != null) {
                $query->orWhere('viernes', $request->input('viernes'));
            }
            if ($request->input('sabado') != null) {
                $query->orWhere('sabado', $request->input('sabado'));
            }
        })
        ->where('hora_inicio', '<', $request->input('hora_fin'))
        ->where('hora_fin', '>', $request->input('hora_inicio'))
        ->first();

        $fechas_programadas = ProgramacionAmbiente::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
        ->where('id_programacion_ambiente', '!=', $id)
        ->where('fecha_inicio', '<', $request->input('fecha_fin'))
        ->where('fecha_fin', '>', $request->input('fecha_inicio'))
        ->where(function ($query) use ($request) {
            if ($request->input('domingo') != null) {
                $query->orWhere('domingo', $request->input('domingo'));
            }
            if ($request->input('lunes') != null) {
                $query->orWhere('lunes', $request->input('lunes'));
            }
            if ($request->input('martes') != null) {
                $query->orWhere('martes', $request->input('martes'));
            }
            if ($request->input('miercoles') != null) {
                $query->orWhere('miercoles', $request->input('miercoles'));
            }
            if ($request->input('jueves') != null) {
                $query->orWhere('jueves', $request->input('jueves'));
            }
            if ($request->input('viernes') != null) {
                $query->orWhere('viernes', $request->input('viernes'));
            }
            if ($request->input('sabado') != null) {
                $query->orWhere('sabado', $request->input('sabado'));
            }
        })
        ->where('hora_inicio', '<', $request->input('hora_fin'))
        ->where('hora_fin', '>', $request->input('hora_inicio'))
        ->get();

        foreach ($fechas_programadas as $fecha) {
            $programacion_instructores = ProgramacionInstructor::where('programacion_ambiente_id', '=', $fecha->id_programacion_ambiente)
            ->get();
        }

        $instructores_programados = array();

        if ($fechas_programadas != '[]') {
            foreach ($programacion_instructores as $programacion) {
                $instructores_programados[] = $programacion->instructor_id;
            } 
        }
        else {
            $instructores_programados[] = 0;
        }
       
        $instructores = $request->input('instructor_id');
        
        $instructor_programado = array_intersect($instructores, $instructores_programados);
        
        if (empty($instructor_programado)) {
            $instructor = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->whereIn('id', $instructores)
            ->first();
        }
        else {
            $instructor = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->whereIn('id', $instructor_programado)
            ->first();
        }
    
        $ambiente_programado = Ambiente::where('id_ambiente', '=', $request->input('ambiente_id'))
        ->first();

        if ($ambientes_programados == null) {
            if ($fichas_programadas == null) {
                if (empty($instructor_programado)) {

                    $resultados_aprendizaje = $request->input('resultado_aprendizaje_id');

                    foreach ($resultados_aprendizaje as $resultado) {
                        $resultados_aprendizaje_programados = ProgramacionResultadoAprendizaje::join('programacion_ambientes', 
                        'programacion_resultados_aprendizaje.programacion_ambiente_id', 'programacion_ambientes.id_programacion_ambiente')
                        ->where('programacion_resultados_aprendizaje.programacion_ambiente_id', '!=', $id)
                        ->where('programacion_ambientes.ficha_id', '=', $request->input('ficha_id'))
                        ->where('programacion_resultados_aprendizaje.resultado_aprendizaje_id', '=', $resultado)
                        ->first();
                    }

                    if ($resultados_aprendizaje_programados == null) {

                        $fecha_inicio   = new DateTime($request->input('fecha_inicio'));
                        $fecha_fin      = new DateTime($request->input('fecha_fin'));
                        
                        $rango_fechas = $fecha_inicio->diff($fecha_fin);

                        $dias_programados = $rango_fechas->format('%d');

                        $hora_inicio    = new DateTime($request->input('hora_inicio'));
                        $hora_fin       = new DateTime($request->input('hora_fin'));
                        
                        $rango_horas = $hora_inicio->diff($hora_fin);

                        $horas_programados = $rango_horas->format('%h');
                        $minutos_programados = $rango_horas->format('%i');

                        $total_minutos_programados = ($minutos_programados*$dias_programados)/60;
                        $total_horas_programadas = ($dias_programados*$horas_programados)+$total_minutos_programados;

                        ProgramacionAmbiente::where('id_programacion_ambiente', $id)->update([
                            'ambiente_id'             => $request->ambiente_id,
                            'ficha_id'                => $request->ficha_id,
                            'actividad_aprendizaje'   => $request->actividad_aprendizaje,
                            'fecha_inicio'            => $request->fecha_inicio,
                            'fecha_fin'               => $request->fecha_fin,
                            'hora_inicio'             => $request->hora_inicio,
                            'hora_fin'                => $request->hora_fin,
                            'domingo'                 => $request->domingo,
                            'lunes'                   => $request->lunes,
                            'martes'                  => $request->martes,
                            'miercoles'               => $request->miercoles,
                            'jueves'                  => $request->jueves,
                            'viernes'                 => $request->viernes,
                            'sabado'                  => $request->sabado,
                            'horas_programadas'       => $total_horas_programadas,
                            'responsable_id'          => Auth::user()->id,
                            'centro_formacion_id'     => Auth::user()->id_centro_formacion
                        ]);
                        
                        $programacion_ambiente = ProgramacionAmbiente::where('id_programacion_ambiente', $id)
                        ->first();

                        $validacion_resultados = ProgramacionResultadoAprendizaje::where('programacion_ambiente_id', '=', $programacion_ambiente->id_programacion_ambiente)    
                        ->get();

                        if ($validacion_resultados != "[]") {
                            ProgramacionResultadoAprendizaje::where('programacion_ambiente_id', '=', $programacion_ambiente->id_programacion_ambiente)    
                            ->delete();
                            
                            foreach ($resultados_aprendizaje as $resultado) {
                                $programacion_resultado_aprendizaje = new ProgramacionResultadoAprendizaje;
                                $programacion_resultado_aprendizaje->programacion_ambiente_id   = $programacion_ambiente->id_programacion_ambiente;
                                $programacion_resultado_aprendizaje->resultado_aprendizaje_id   = $resultado;
                                $programacion_resultado_aprendizaje->estado                     = "Programado";
                                $programacion_resultado_aprendizaje->save();
                            }
                        }
                        else {
                            foreach ($resultados_aprendizaje as $resultado) {
                                $programacion_resultado_aprendizaje = new ProgramacionResultadoAprendizaje;
                                $programacion_resultado_aprendizaje->programacion_ambiente_id   = $programacion_ambiente->id_programacion_ambiente;
                                $programacion_resultado_aprendizaje->resultado_aprendizaje_id   = $resultado;
                                $programacion_resultado_aprendizaje->estado                     = "Programado";
                                $programacion_resultado_aprendizaje->save();
                            }
                        }

                        $validacion_instructores = ProgramacionInstructor::where('programacion_ambiente_id', '=', $programacion_ambiente->id_programacion_ambiente)
                        ->get();

                        if ($validacion_instructores != "[]") {
                            ProgramacionInstructor::where('programacion_ambiente_id', '=', $programacion_ambiente->id_programacion_ambiente)
                            ->delete();

                            foreach ($instructores as $instructor_programado) {
                                $programacion_instructores = new ProgramacionInstructor;
                                $programacion_instructores->programacion_ambiente_id    = $programacion_ambiente->id_programacion_ambiente;
                                $programacion_instructores->instructor_id               = $instructor_programado;
                                $programacion_instructores->save();
                            }
                        }
                        else {
                            foreach ($instructores as $instructor_programado) {
                                $programacion_instructores = new ProgramacionInstructor;
                                $programacion_instructores->programacion_ambiente_id    = $programacion_ambiente->id_programacion_ambiente;
                                $programacion_instructores->instructor_id               = $instructor_programado;
                                $programacion_instructores->save();
                            }
                        }
                   
                        Alert::success('Actualizada', 'Programación de Ambiente con éxito');
                    }
                    else {
                        Alert::error('Error', 'El Resultado de Aprendizaje '.$resultados_aprendizaje_programados->resultado_aprendizaje->resultado_aprendizaje.' ya esta Programado');
                    }
                }
                else {
                    Alert::error('Error', 'El Instructor '.$instructor->nombres.' '.$instructor->apellidos.' ya esta Programado');
                }
            }
            else {
                Alert::error('Error', 'La Ficha '.$fichas_programadas->ficha->numero_ficha.' ya esta Programada');
            }     
        }
        else {
            Alert::error('Error', 'El Ambiente '.$ambiente_programado->nombre_ambiente.' ya esta Programado');
        }
            
        return redirect(route('programaciones_ambiente.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Filtro Calendario de Programación de Ambientes de Aprendizaje
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function filtro_programacion()
    {   
        $redes_conocimiento = RedConocimiento::get();

        /*
        $instructores_programados = ProgramacionInstructor::join('programacion_ambientes',
        'programacion_instructores.programacion_ambiente_id', 'programacion_ambientes.id_programacion_ambiente')
        ->where('programacion_ambientes.centro_formacion_id', '=', Auth::user()->id_centro_formacion)
        ->distinct('programacion_instructores.instructor_id')
        ->get();  
        */

        $instructores_programados = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
        ->where('id_perfil_usuario', '=', 4)
        ->orWhere('id_perfil_usuario', '=', 5)
        ->orderBy('nombres', 'asc')
        ->orderBy('apellidos', 'asc')
        ->get();

        return view('admin.programaciones_ambiente.filtro', compact('redes_conocimiento', 'instructores_programados'));
    }

    /**
     * Calendario de Programación de Ambientes de Aprendizaje
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function calendario_programacion(Request $request)
    {
        if ($request->input('ambiente_id') != null) {
            $filtro_programacion = Ambiente::select('id_ambiente as id', 'nombre_ambiente as nombre')
            ->where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->where('id_ambiente', '=', $request->input('ambiente_id'))
            ->first();

            $id_filtro = 1;
        }
        elseif ($request->input('ficha_id') != null) {
            $filtro_programacion = Ficha::select('id_ficha as id', 'numero_ficha as nombre')
            ->where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
            ->where('id_ficha', '=', $request->input('ficha_id'))
            ->first();

            $id_filtro = 2;
        }
        else {
            $filtro_programacion = User::select('id', 'nombres as nombre')
            ->where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->where('id', '=', $request->input('instructor_id'))
            ->first();

            $id_filtro = 3;
        }
        return view('admin.programaciones_ambiente.calendario', compact('filtro_programacion', 'id_filtro'));
    }

    /**
     * Calendario de Programación de Dias Festivos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function calendario_festivos()
    {
        $dias_festivos = Festivo::get();

        foreach ($dias_festivos as $dias) {
            $festivos[] = [
                "title" => 'Festivo',
                "start" => $dias->festivo,
                "end"   => $dias->festivo,
                "color" => "#FC7323",
                "allDay" => true
            ];
        }

        echo json_encode($festivos);
    }

    /**
     * Calendario de Programación de Eventos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function calendario_eventos(Request $request)
    {
        setlocale(LC_ALL, "spanish.UTF-8");
        if ($request->filtro == 1) {
            $programacion_ambientes = ProgramacionAmbiente::where('ambiente_id', '=', $request->id)
            ->get();
        }
        elseif ($request->filtro == 2) {
            $programacion_ambientes = ProgramacionAmbiente::where('ficha_id', '=', $request->id)
            ->get();
        }
        else {
            $instructor_programado = ProgramacionInstructor::where('instructor_id', '=', $request->id)
            ->get();

            $programaciones_instructor = array();

            foreach ($instructor_programado as $instructor) {
                $programaciones_instructor[] = $instructor->programacion_ambiente_id;
            }

            $programacion_ambientes = ProgramacionAmbiente::whereIn('id_programacion_ambiente', $programaciones_instructor)
            ->get();
        }

        $dias_festivos = Festivo::get();
        
        $festivos = array();

        foreach($dias_festivos as $dia_festivo) {
            $festivos[] = $dia_festivo->festivo;
        }

        foreach ($programacion_ambientes as $programacion) {
            /* Fechas de Inicio y Fin de la Programacion  */
            $fecha_inicio = new DateTime($programacion->fecha_inicio);
            $fecha_fin = new DateTime($programacion->fecha_fin);

            /* Dias de la Programacion */
            $dias_programados = array($programacion->domingo, $programacion->lunes, $programacion->martes,
                                $programacion->miercoles, $programacion->jueves,
                                $programacion->viernes, $programacion->sabado);
        
            $programacion_resultados = ProgramacionResultadoAprendizaje::where('programacion_ambiente_id', '=', $programacion->id_programacion_ambiente)
            ->get();

            $resultados_aprendizaje_programados = [];
            foreach ($programacion_resultados as $resultado) {
                $resultados_aprendizaje_programados[] = $resultado->resultado_aprendizaje;               
            }

            $programacion_instructores = ProgramacionInstructor::where('programacion_ambiente_id', '=', $programacion->id_programacion_ambiente)
            ->get();

            $instructores_programados = array();
            foreach ($programacion_instructores as $instructor) {
                $instructores_programados[] = $instructor->usuario;
            }

            /* Iteracion desde la Fecha Inicio hasta la Fecha Fin de la Programacion */
            for ($i = $fecha_inicio; $i <= $fecha_fin; $i->modify('+1 day')) {
                for ($d=0; $d<count($dias_programados); $d++) {
                    if(strftime("%A", $i->getTimestamp()) == $dias_programados[$d] and !in_array(strftime("%Y-%m-%d", $i->getTimestamp()), $festivos)) {
                        $eventos[] = [
                            "title" => 'Ficha: '. $programacion->ficha->numero_ficha,
                            "start" => strftime("%Y-%m-%d", $i->getTimestamp()) . ' '. $programacion->hora_inicio,
                            "end"   => strftime("%Y-%m-%d", $i->getTimestamp()) .' '. $programacion->hora_fin,
                            "fecha_inicio"          => date('d-m-Y', strtotime($programacion->fecha_inicio)),
                            "fecha_fin"             => date('d-m-Y', strtotime($programacion->fecha_fin)),
                            "dias"                  => [
                                $programacion->domingo. ' '.$programacion->lunes. ' '.$programacion->martes. ' '. 
                                $programacion->miercoles. ' '.$programacion->jueves. ' '.$programacion->viernes. ' '.
                                $programacion->sabado. ' '.$programacion->viernes
                            ],
                            "hora_inicio"           => date('h:i a', strtotime($programacion->hora_inicio)),
                            "hora_fin"              => date('h:i a', strtotime($programacion->hora_fin)),
                            "ambiente"              => $programacion->ambiente->nombre_ambiente,
                            "programa_formacion"    => $programacion_resultados[0]->resultado_aprendizaje->competencia_laboral_programa->programa_formacion->nombre_programa,
                            "actividad_aprendizaje" => $programacion->actividad_aprendizaje,
                            "competencia_laboral"   => $programacion_resultados[0]->resultado_aprendizaje->competencia_laboral_programa->competencia_laboral->competencia_laboral,
                            "resultados_aprendizaje" => $resultados_aprendizaje_programados,
                            "instructor"            => $instructores_programados,
                            "horas_programadas"     => $programacion->horas_programadas,
                            "display" => 'block',                   
                            "color" => "#38c172",
                            "className" => "cursor-evento"
                        ]; 
                    }
                }     
            }
        }
        echo json_encode($eventos);
    }

    /**
     * Exportacion de Programaciones de Ambiente
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function exportar_programacion($id, $filtro)
    {                
        $documento_pdf = app('dompdf.wrapper');        
        
        if ($filtro == 1) {
            $ambiente = Ambiente::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->where('id_ambiente', '=', $id)
            ->first();
            
            $programaciones_ambiente = ProgramacionInstructor::select('programacion_ambientes.*', 'users.nombres',
            'users.apellidos', 'ambiente.nombre_ambiente', 'ficha.numero_ficha')
            ->join('users', 'programacion_instructores.instructor_id', 'users.id')
            ->join('programacion_ambientes', 'programacion_instructores.programacion_ambiente_id', 
            'programacion_ambientes.id_programacion_ambiente')
            ->join('ficha', 'programacion_ambientes.ficha_id', 'ficha.id_ficha')
            ->join('ambiente', 'programacion_ambientes.ambiente_id', 'ambiente.id_ambiente')
            ->where('programacion_ambientes.ambiente_id', '=', $id)
            ->get();
            
            if ($programaciones_ambiente != '[]') {
                $documento_pdf->loadView('admin.programaciones_ambiente.exportar', [
                    'programaciones_ambiente'   => $programaciones_ambiente,
                    'tipo_programacion'         => 1
                   
                ]);
    
                return $documento_pdf->setPaper('a4', 'landscape')->download('Programacion Ambiente '.$ambiente->nombre_ambiente.'.pdf');
            }
            else {
                Alert::error('Error', 'El Ambiente '.$ambiente->nombre_ambiente.' no tiene Programaciones Registradas para Descargar');
                return redirect(route('programaciones_ambiente.filtro'));
            }
            
        }
        elseif ($filtro == 2) {
            $ficha = Ficha::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
            ->where('id_ficha', '=', $id)
            ->first();

            $programaciones_ambiente = ProgramacionInstructor::select('programacion_ambientes.*', 'users.nombres',
            'users.apellidos', 'ambiente.nombre_ambiente', 'ficha.numero_ficha')
            ->join('users', 'programacion_instructores.instructor_id', 'users.id')
            ->join('programacion_ambientes', 'programacion_instructores.programacion_ambiente_id', 
            'programacion_ambientes.id_programacion_ambiente')
            ->join('ficha', 'programacion_ambientes.ficha_id', 'ficha.id_ficha')
            ->join('ambiente', 'programacion_ambientes.ambiente_id', 'ambiente.id_ambiente')
            ->where('programacion_ambientes.ficha_id', '=', $id)
            ->get();

            if ($programaciones_ambiente != '[]') {
                $documento_pdf->loadView('admin.programaciones_ambiente.exportar', [
                    'programaciones_ambiente'   => $programaciones_ambiente,
                    'tipo_programacion'         => 2
                ]);

                return $documento_pdf->setPaper('a4', 'landscape')->download('Programacion Ficha '.$ficha->numero_ficha.'.pdf');
            }
            else {
                Alert::error('Error', 'La Ficha '.$ficha->numero_ficha.' no tiene Programaciones Registradas para Descargar');
                return redirect(route('programaciones_ambiente.filtro'));
            }
        }
        else {
            $instructor = User::where('id_centro_formacion', '=', Auth::user()->id_centro_formacion)
            ->where('id', '=', $id)
            ->first();

            $instructor_programado = ProgramacionInstructor::where('instructor_id', '=', $id)
            ->get();

            $programaciones_instructor = array();

            foreach ($instructor_programado as $instructor_p) {
                $programaciones_instructor[] = $instructor_p->programacion_ambiente_id;
            }

            $programaciones_ambiente = ProgramacionInstructor::select('programacion_ambientes.*', 'users.nombres',
            'users.apellidos', 'ambiente.nombre_ambiente', 'ficha.numero_ficha')
            ->join('users', 'programacion_instructores.instructor_id', 'users.id')
            ->join('programacion_ambientes', 'programacion_instructores.programacion_ambiente_id', 
            'programacion_ambientes.id_programacion_ambiente')
            ->join('ficha', 'programacion_ambientes.ficha_id', 'ficha.id_ficha')
            ->join('ambiente', 'programacion_ambientes.ambiente_id', 'ambiente.id_ambiente')
            ->whereIn('programacion_ambientes.id_programacion_ambiente', $programaciones_instructor)
            ->where('programacion_instructores.instructor_id', '=', $id)
            ->get();

            if ($programaciones_ambiente != '[]') {
                $documento_pdf->loadView('admin.programaciones_ambiente.exportar', [
                    'programaciones_ambiente'   => $programaciones_ambiente,
                    'tipo_programacion'         => 3
                ]);

                return $documento_pdf->setPaper('a4', 'landscape')
                ->download('Programacion Instructor '.$instructor->nombres.' '.$instructor->apellidos.'.pdf');
            }
            else {
                Alert::error('Error', 'El Instructor '.$instructor->nombres.' '.$instructor->apellidos.' no tiene Programaciones Registradas para Descargar');
                return redirect(route('programaciones_ambiente.filtro'));
            }
        }
    }

    /**
     * Programas de Formación del Ambiente de Aprendizaje
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function programas_ambiente($id_ambiente)
    {
        $programas_ambiente = AmbientePrograma::where('id_ambiente', '=', $id_ambiente)
        ->get();

        foreach ($programas_ambiente as $programa_ambiente) {
            echo "<option value='{$programa_ambiente->id_programa}'>{$programa_ambiente->programa_formacion->codigo_programa} {$programa_ambiente->programa_formacion->nombre_programa}</option>";
        }
    }

    /**
     * Fichas de Programas de Formación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fichas_programa($id_programa)
    {
        $fichas_programa = Ficha::where('centro_formacion_id', '=', Auth::user()->id_centro_formacion)
        ->where('programa_id', '=', $id_programa)
        ->where('fecha_fin', '>=', date('Y-m-d'))
        ->get();

        foreach ($fichas_programa as $ficha_programa) {
            echo "<option value='{$ficha_programa->id_ficha}'>{$ficha_programa->numero_ficha}</option>";
        }
    }

    /**
     * Competencias de Programas de Formación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function competencias_programa($id_programa)
    {
        $competencias_programa = CompetenciaLaboralPrograma::where('programa_id', '=', $id_programa)
        ->get();

        foreach ($competencias_programa as $competencia_programa) {
            echo "<option value='{$competencia_programa->competencia_laboral->id_competencia_laboral}'>{$competencia_programa->competencia_laboral->competencia_laboral}</option>";
        }
    }


    /**
     * Resultados de Aprendizaje de Programas de Formación
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function resultados_programa($id_competencia, $id_ficha)
    {
        $competencia_programa = CompetenciaLaboralPrograma::where('competencia_laboral_id', '=', $id_competencia)
        ->first();

        $resultados_programa = ResultadoAprendizaje::where('competencia_programa_id', '=', $competencia_programa->id_competencia_programa)
        ->get();

        /*
        $ficha_programada = ProgramacionResultadoAprendizaje::join('programacion_ambientes', 
        'programacion_resultados_aprendizaje.programacion_ambiente_id', 'programacion_ambientes.id_programacion_ambiente')
        ->join('ficha', 'programacion_ambientes.ficha_id', 'ficha.id_ficha')
        ->join('resultados_aprendizaje', 'programacion_resultados_aprendizaje.resultado_aprendizaje_id', 'resultados_aprendizaje.id_resultado_aprendizaje')
        ->join('competencia_laboral_programa', 'resultados_aprendizaje.competencia_programa_id', 'competencia_laboral_programa.id_competencia_programa')
        ->where('competencia_laboral_programa.competencia_laboral_id', '=', $id_competencia)
        ->first();
        */

        $resultados_programados = ProgramacionResultadoAprendizaje::select('programacion_resultados_aprendizaje.resultado_aprendizaje_id')
        ->join('programacion_ambientes', 'programacion_resultados_aprendizaje.programacion_ambiente_id', 'programacion_ambientes.id_programacion_ambiente')
        ->join('ficha', 'programacion_ambientes.ficha_id', 'ficha.id_ficha')
        ->join('resultados_aprendizaje', 'programacion_resultados_aprendizaje.resultado_aprendizaje_id', 'resultados_aprendizaje.id_resultado_aprendizaje')
        ->join('competencia_laboral_programa', 'resultados_aprendizaje.competencia_programa_id', 'competencia_laboral_programa.id_competencia_programa')
        ->where('competencia_laboral_programa.competencia_laboral_id', '=', $id_competencia)
        ->where('ficha.id_ficha', '=', $id_ficha)
        ->get();

        if ($resultados_programados != '[]') {
            $rap_programados = array();

            foreach ($resultados_programados as $resultado_programado) {
                $rap_programados[] = $resultado_programado->resultado_aprendizaje_id;
            }

            $resultados_sin_programar = $resultados_programa->whereNotIn('id_resultado_aprendizaje', $rap_programados);
        }
        else {
            $resultados_sin_programar = $resultados_programa;
        }
        
        foreach ($resultados_sin_programar as $resultado) {
            echo "<option value='{$resultado->id_resultado_aprendizaje}'>{$resultado->resultado_aprendizaje}</option>";
        }
    }

    /**
     * Busqueda de Programaciones de Ambiente
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function buscar_programaciones_ambiente(Request $request)
    {
        $programacion_ambiente = ucwords(strtolower($request->input('buscar_programacion_ambiente')));

        $busqueda = ProgramacionAmbiente::join('ambiente', 'programacion_ambientes.ambiente_id', 'ambiente.id_ambiente')
            ->where('ambiente.nombre_ambiente', 'LIKE', "%$programacion_ambiente%")
            ->orderBy('ambiente.nombre_ambiente', 'asc')
            ->first();

        if ($programacion_ambiente == "") {
            Alert::error('Error', 'Escriba para Buscar');
            return redirect(route('programaciones_ambiente.index'));
        }
        if (isset($busqueda)) {
            $programacion_ambientes = ProgramacionAmbiente::join('ambiente', 'programacion_ambientes.ambiente_id', 'ambiente.id_ambiente')
                ->where('ambiente.nombre_ambiente', 'LIKE', "%$programacion_ambiente%")
                ->orderBy('ambiente.nombre_ambiente', 'asc')
                ->paginate(50);
            return view('admin.programaciones_ambiente.gestionar', compact('programacion_ambientes'));
        }
        else {
            Alert::info('Información', 'No se Encontraron Programaciones de Ambiente');
            return redirect(route('programaciones_ambiente.index'));
        }
    }
}
