<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ElementoAmbiente;
use App\OrigenElemento;
use App\CategoriaElemento;
use App\Inventario;
use App\SubcategoriaElemento;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class AmbienteElementoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_ambiente)
    {
        $elementos = ElementoAmbiente::where('id_ambiente', '=', $id_ambiente)
        ->orderBy('elemento_ambiente', 'asc')
        ->orderBy('created_at', 'desc')
        ->paginate(40);

        return view('admin.ambientes.elementos.gestionar', compact('elementos', 'id_ambiente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_ambiente)
    {
        $validacion = 1;
        return view('admin.ambientes.elementos.registro', compact('id_ambiente', 'validacion'));
    }


    public function validacion_elemento(Request $request, $id_ambiente)
    {
        $elemento = Inventario::where('numero_inventario', $request->input('numero_inventario'))
        ->first();

        if ($elemento != null) {
            $validacion = 2;
            $subcategoria = SubcategoriaElemento::where('id_subcategoria_elemento',$elemento->subcategoria_elemento_id)->first();
            $categoria = CategoriaElemento::where('id_categoria_elemento', $subcategoria->categoria_elemento_id)->first();
            $origen = OrigenElemento::where('id_origen_elemento',$elemento->origen_elemento_id)->first();
            if ('Bueno' == $elemento->estado) {
                $estados = array('Regular','Malo');
            }
            elseif ('Regular' == $elemento->estado) {
                $estados = array('Bueno','Malo');
            }
            else {
                $estados = array('Bueno','Regular');
            }

            return view('admin.ambientes.elementos.registro', compact('elemento','id_ambiente', 'subcategoria', 'categoria', 'origen', 'validacion', 'estados'));
        }
        else {
            if (Auth::user()->id_perfil_usuario == 1 || Auth::user()->id_perfil_usuario == 2) {
                $validacion = 1;
                Alert::error('Error', 'Elemento no Encontrado debe Registrarlo en el Inventario');

                return redirect(route('elementos.create'));
            }
            else {
                $validacion = 1;
                Alert::error('Error', 'Elemento no Encontrado Contáctese con el Administrador de su Centro de Formación');

                return redirect(route('ambientes.elementos.index', $id_ambiente));  
            } 
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id_ambiente)
    {
        $numero_inventario_registrado = ElementoAmbiente::where('id_ambiente', '=', $id_ambiente)
        ->where('numero_inventario', '=', $request->input('numero_inventario'))
        ->first();

        if ($numero_inventario_registrado == null) {
            $elemento_ambiente = new ElementoAmbiente;
            $elemento_ambiente->id_ambiente                 = $id_ambiente;
            $elemento_ambiente->subcategoria_elemento_id    = $request->input('subcategoria_elemento_id');
            $elemento_ambiente->origen_elemento_id          = $request->input('origen_elemento_id');
            $elemento_ambiente->numero_inventario           = $request->input('numero_inventario');
            $elemento_ambiente->serial                      = $request->input('serial');
            $elemento_ambiente->cantidad                    = $request->input('cantidad');
            $elemento_ambiente->elemento_ambiente           = $request->input('elemento_ambiente');
            $elemento_ambiente->estado                      = $request->input('estado');
            $elemento_ambiente->fecha_adquisicion           = $request->input('fecha_adquisicion');
            $elemento_ambiente->tiempo_vida_util            = $request->input('tiempo_vida_util');
            $elemento_ambiente->precio                      = $request->input('precio');
            $elemento_ambiente->observacion_general         = $request->input('observacion_general');
            $elemento_ambiente->save();

            Alert::success('Asociado', 'Elemento con éxito');
        }
        else {
            Alert::error('Error', 'El Elemento '.$numero_inventario_registrado->elemento_ambiente.' ya esta Asociado al Ambiente');
        }

        return redirect(route('ambientes.elementos.create', $id_ambiente));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $elemento = ElementoAmbiente::where('id_elemento_ambiente',$id)->first();
        return view('admin.ambientes.elementos.detalle',compact('elemento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $elemento = ElementoAmbiente::where('id_elemento_ambiente',$id)->first();
        $categorias = CategoriaElemento::where('id_categoria_elemento','!=', $elemento->subcategoria_elemento->categoria_elemento_id)
        ->orderBy('categoria_elemento')
        ->get();
        $origenes = OrigenElemento::where('id_origen_elemento','!=', $elemento->origen_elemento_id)
        ->orderBy('origen_elemento')
        ->get();

        if('Bueno' == $elemento->estado){
            $estados = array('Regular','Malo');
        }elseif('Regular' == $elemento->estado){
            $estados = array('Bueno','Malo');
        }else{
            $estados = array('Bueno','Regular');
        }

        return view('admin.ambientes.elementos.actualizar',compact('elemento','categorias','origenes', 'estados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id_ambiente = $request->input('id_ambiente');

        ElementoAmbiente::where('id_elemento_ambiente', $id)->update([
            'subcategoria_elemento_id' => $request->subcategoria_elemento_id,
            'origen_elemento_id' => $request->origen_elemento_id,
            'numero_inventario' => $request->numero_inventario,
            'serial' => $request->serial,
            'cantidad' => $request->cantidad,
            'elemento_ambiente' => $request->elemento_ambiente,
            'estado' => $request->estado,
            'fecha_adquisicion' => $request->fecha_adquisicion,
            'tiempo_vida_util' => $request->tiempo_vida_util,
            'precio' => $request->precio,
            'observacion_general' => $request->observacion_general,
        ]);
        
        Alert::success('Actualizado', 'Elemento con éxito');
        return redirect(route('ambientes.elementos.index',$id_ambiente));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_elemento , $id_ambiente)
    {
        ElementoAmbiente::where('id_ambiente', $id_ambiente)
        ->where('id_elemento_ambiente', $id_elemento)
        ->delete();

        Alert::success('Eliminado', 'Elemento con éxito');
        return redirect(route('ambientes.elementos.index',$id_ambiente));
    }

    /**
     * Subcategorias Elementos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_subcategorias_elementos($id_categoria_elemento)
    {
        $subcategorias_elementos = DB::table('subcategoria_elemento')
        ->select('id_subcategoria_elemento', 'subcategoria_elemento')
        ->where('categoria_elemento_id',$id_categoria_elemento)
        ->get();

        foreach ($subcategorias_elementos as $subce) {
            echo "<option value='{$subce->id_subcategoria_elemento}'>{$subce->subcategoria_elemento}</option>";
        }
    }

    /**
     * Importación de Elementos
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function importar_elementos($id_ambiente)
    {
        return view('admin.ambientes.elementos.importar', compact('id_ambiente'));
    }

}
