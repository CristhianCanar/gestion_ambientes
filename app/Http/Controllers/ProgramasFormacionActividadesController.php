<?php

namespace App\Http\Controllers;

use App\ActividadAprendizaje;
use App\CompetenciaLaboral;
use App\ResultadoAprendizaje;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ProgramasFormacionActividadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_programa)
    {
        $actividades_aprendizaje = ActividadAprendizaje::where('programa_formacion_id', $id_programa)
        ->paginate(10);
        return view('admin.actividades.gestionar', compact('actividades_aprendizaje', 'id_programa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_programa)
    {
        return view('admin.actividades.registro', compact('id_programa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id_programa)
    {
        $actividad = new ActividadAprendizaje;
        $actividad->programa_formacion_id       = $id_programa;
        $actividad->actividad_aprendizaje       = $request->input('actividad_aprendizaje');
        $actividad->save();

        $actividad_aprendizaje = ActividadAprendizaje::get()
        ->last();

        $competencia_laboral = new CompetenciaLaboral;
        $competencia_laboral->actividad_aprendizaje_id      = $actividad_aprendizaje->id_actividad_aprendizaje;
        $competencia_laboral->codigo_competencia_laboral    = $request->input('codigo_competencia_laboral');
        $competencia_laboral->version_competencia_laboral   = $request->input('version_competencia_laboral');
        $competencia_laboral->competencia_laboral           = $request->input('competencia_laboral');
        $competencia_laboral->save();

        $competencia_laboral = CompetenciaLaboral::get()
        ->last();

        $resultado_aprendizaje = new ResultadoAprendizaje;
        $resultado_aprendizaje->competencia_laboral_id     = $competencia_laboral->id_competencia_laboral;
        $resultado_aprendizaje->resultado_aprendizaje      = $request->input('resultado_aprendizaje');
        $resultado_aprendizaje->fecha_terminacion          = $request->input('fecha_terminacion');
        $resultado_aprendizaje->save();

        Alert::success('Registrada', 'Actividad de aprendizaje con éxito');
        return redirect(route('programas_formacion.actividades.create', $id_programa));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_programa, $id_actividad)
    {
        $actividad_aprendizaje = ActividadAprendizaje::where('id_actividad_aprendizaje', $id_actividad)
        ->first();

        $competencias_laborales = CompetenciaLaboral::where('actividad_aprendizaje_id', $id_actividad)
        ->get();

        for($j = 0; $j < count($competencias_laborales); $j++){
            $resultados_aprendizaje[$j] = ResultadoAprendizaje::where('competencia_laboral_id', $competencias_laborales[$j]->id_competencia_laboral)
            ->get();
        }

        //return $resultados_aprendizaje[0][0]->competencia_laboral->competencia_laboral;

        return view('admin.actividades.detalle',compact('id_programa', 'actividad_aprendizaje', 'competencias_laborales',
        'resultados_aprendizaje'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
