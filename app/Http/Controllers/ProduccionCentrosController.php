<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Producto;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ProduccionCentrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::orderBy('producto', 'asc')
        ->orderBy('created_at', 'desc')
        ->paginate(100);

        return view('admin.administracion.ambientes.produccion_centros.gestionar', compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.administracion.ambientes.produccion_centros.registro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $productos_registrados = Producto::where('producto', '=', ucwords(strtolower($request->input('producto'))))
        ->first();

        if ($productos_registrados == null) {
            $producto = new Producto;
            $producto->producto = ucwords(strtolower($request->input('producto')));
            $producto->save();

            Alert::success('Registrado', 'Producto con éxito');
        }
        else {
            Alert::error('Error', 'El Producto '.ucwords(strtolower($request->input('producto'))).' ya esta Registrado');
        }

        return redirect(route('produccion_centros.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::where('id_producto', $id)->first();

        return view('admin.administracion.ambientes.produccion_centros.actualizar', compact('producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productos_registrados = Producto::where('id_producto', '!=', $id)
        ->where('producto', '=', ucwords(strtolower($request->input('producto'))))
        ->first();

        if ($productos_registrados == null) {
            Producto::where('id_producto', $id)->update([
                'producto' => ucwords(strtolower($request->producto))
            ]);
    
            Alert::success('Actualizado', 'Producto con éxito');
        }
        else {
            Alert::error('Error', 'El Producto '.ucwords(strtolower($request->input('producto'))).' ya Existe');
        }
        
        return redirect(route('produccion_centros.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
