<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\RedConocimiento;
use App\LineaTematica;
use App\NivelFormacion;
use App\ProgramaFormacion;
use App\ResultadoAprendizaje;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ProgramasFormacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programas = ProgramaFormacion::orderBy('nombre_programa', 'asc')
        ->orderBy('created_at', 'desc')
        ->paginate(100);

        return view('admin.programas.gestionar', compact('programas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $redes_conocimiento = RedConocimiento::orderBy('redconocimiento', 'asc')
        ->get();

        $niveles_programa = NivelFormacion::orderBy('nivel_programa', 'asc')
        ->get();

        return view('admin.programas.registro', compact('redes_conocimiento', 'niveles_programa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $programas_registrados = ProgramaFormacion::where('nombre_programa', '=', ucwords(strtolower($request->input('nombre_programa'))))
        ->first();

        if ($programas_registrados == null) {
            $programa = new ProgramaFormacion;
            $programa->red_conocimiento_id  = $request->input('red_conocimiento_id');
            $programa->id_linea_tematica    = $request->input('id_linea_tematica');
            $programa->id_nivel_programa    = $request->input('id_nivel_programa');
            $programa->codigo_programa      = $request->input('codigo_programa');
            $programa->version_programa     = $request->input('version_programa');
            $programa->nombre_programa      = ucwords(strtolower($request->input('nombre_programa')));
            $programa->duracion_total       = $request->input('duracion_total');
            $programa->save();

            Alert::success('Registrado', 'Programa de Formación con éxito');
        }
        else {
            Alert::error('Error', 'El Programa de Formación '.ucwords(strtolower($request->input('nombre_programa'))).' ya esta Registrado');
        }

        return redirect(route('programas_formacion.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $programa = ProgramaFormacion::where('id_programa', $id)->first();

        return view('admin.programas.detalle', compact('programa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $programa = ProgramaFormacion::where('id_programa', $id)->first();

        $redes_conocimiento = RedConocimiento::where('idredconocimiento', '!=', $programa->red_conocimiento_id)
        ->orderBy('redconocimiento', 'asc')
        ->get();

        $niveles_programa = NivelFormacion::where('id_nivel_programa', '!=', $programa->id_nivel_programa)
        ->orderBy('nivel_programa', 'asc')
        ->get();

        return view('admin.programas.actualizar', compact('programa', 'redes_conocimiento', 'niveles_programa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $programas_registrados = ProgramaFormacion::where('id_programa', '!=', $id)
        ->where('nombre_programa', '=', ucwords(strtolower($request->input('nombre_programa'))))
        ->first();
        
        if ($programas_registrados == null) {
            ProgramaFormacion::where('id_programa', $id)->update([
                'red_conocimiento_id'   => $request->red_conocimiento_id,
                'id_linea_tematica'     => $request->id_linea_tematica, 
                'id_nivel_programa'     => $request->id_nivel_programa,
                'codigo_programa'       => $request->codigo_programa,
                'version_programa'      => $request->version_programa,
                'nombre_programa'       => ucwords(strtolower($request->nombre_programa)),
                'duracion_total'        => $request->duracion_total
            ]);
    
            Alert::success('Actualizado', 'Programa de Formación con éxito');
        }
        else {
            Alert::error('Error', 'El Programa de Formación '.ucwords(strtolower($request->input('nombre_programa'))).' ya Existe');
        }
        
        return redirect(route('programas_formacion.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Busqueda de Programas de Formacion
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function buscar_programas_formacion(Request $request)
    {
        $programa_formacion = ucwords(strtolower($request->input('buscar_programa_formacion')));

        $busqueda = ProgramaFormacion::where('nombre_programa', 'LIKE', "%$programa_formacion%")
            ->orderBy('nombre_programa', 'asc')
            ->first();

        if ($programa_formacion == "") {
            Alert::error('Error', 'Escriba para Buscar');
            return redirect(route('programas_formacion.index'));
        }
        if (isset($busqueda)) {
            $programas = ProgramaFormacion::where('nombre_programa', 'LIKE', "%$programa_formacion%")
                ->orderBy('nombre_programa', 'asc')
                ->paginate(20);
            return view('admin.programas.gestionar', compact('programas'));
        }
        else {
            Alert::info('Información', 'No se Encontraron Programas de Formacion');
            return redirect(route('programas_formacion.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actividades_aprendizaje($id_programa)
    {
        $resultados_aprendizaje = ResultadoAprendizaje::where('programa_formacion_id', '=', $id_programa)
        ->get();
        return view('admin.programas.actividades_aprendizaje.detalle', compact('resultados_aprendizaje'));
    }
}
