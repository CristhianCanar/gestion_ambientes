<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\TipoAmbiente;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class TiposAmbienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos_ambiente = TipoAmbiente::orderBy('tipo_ambiente', 'asc')
        ->orderBy('created_at', 'desc')        
        ->paginate(10);

        return view('admin.administracion.ambientes.tipos.gestionar',compact('tipos_ambiente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.administracion.ambientes.tipos.registro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipos_ambiente_registrados = TipoAmbiente::where('tipo_ambiente', '=', ucwords(strtolower($request->input('tipo_ambiente'))))
        ->first();

        if ($tipos_ambiente_registrados == null) {
            $tipo_ambiente = new TipoAmbiente;
            $tipo_ambiente->tipo_ambiente           = ucwords(strtolower($request->input('tipo_ambiente')));
            $tipo_ambiente->detalle_tipo_ambiente   = $request->input('detalle_tipo_ambiente');
            $tipo_ambiente->save();

            Alert::success('Registrado', 'Tipo de Ambiente con éxito');
        }
        else{
            Alert::error('Error', 'El Tipo de Ambiente '.ucwords(strtolower($request->input('tipo_ambiente'))).' ya esta Registrado');
        }

        return redirect(route('tipos_ambiente.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo_ambiente = TipoAmbiente::where('id_tipo_ambiente', $id)->first();

        return view('admin.administracion.ambientes.tipos.actualizar', compact('tipo_ambiente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipos_ambiente_registrados = TipoAmbiente::where('id_tipo_ambiente', '!=', $id)
        ->where('tipo_ambiente', ucwords(strtolower($request->input('tipo_ambiente'))))
        ->first();
        
        if ($tipos_ambiente_registrados == null) {
            TipoAmbiente::where('id_tipo_ambiente', $id)->update([
                'tipo_ambiente'         => ucwords(strtolower($request->tipo_ambiente)),
                'detalle_tipo_ambiente' => $request->detalle_tipo_ambiente
            ]);
    
            Alert::success('Actualizado', 'Tipo de Ambiente con éxito');
        }
        else {
            Alert::error('Error', 'El Tipo de Ambiente '.ucwords(strtolower($request->input('tipo_ambiente'))).' ya Existe');
        }
        
        return redirect(route('tipos_ambiente.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
