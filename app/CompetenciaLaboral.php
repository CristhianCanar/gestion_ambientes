<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompetenciaLaboral extends Model
{
    protected $table = "competencias_laborales";
    protected $guarded = [];
    protected $primaryKey = "id_competencia_laboral";

    public function competencia_laboral_programa(){
        return $this->hasMany('App\CompetenciaLaboralPrograma', 'id_competencia_laboral ', 'competencia_laboral_id');
    }
    
}
