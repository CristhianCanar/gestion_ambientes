<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActividadAprendizaje extends Model
{
    protected $table = "actividades_aprendizaje";
    protected $guarded = [];
    protected $primaryKey = "id_actividad_aprendizaje";

    public function competencia_laboral(){
        return $this->hasMany('App\CompetenciaLaboral', 'id_actividad_aprendizaje', 'actividad_aprendizaje_id');
    }
}
