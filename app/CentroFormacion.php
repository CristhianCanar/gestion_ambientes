<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentroFormacion extends Model
{
    protected $table = "centro_formacion";
    protected $guarded = [];
    protected $primaryKey = "id_centro_formacion";

    public function regional(){
        return $this->belongsTo('App\Regional', 'id_regional', 'id_regional');
    }

    public function municipios(){
        return $this->belongsTo('App\Municipio', 'id_municipio', 'id_municipio');
    }

    public function sede(){
        return $this->hasMany('App\Sede', 'id_centro_formacion', 'id_centro_formacion');
    }

    public function user(){
        return $this->hasMany('App\User', 'id_centro_formacion', 'id_centro_formacion');
    }

    public function ficha(){
        return $this->hasMany('App\Ficha', 'id_centro_formacion', 'centro_formacion_id');
    }

}
