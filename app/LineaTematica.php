<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineaTematica extends Model
{
    protected $table = "linea_tematica";
    protected $guarded = [];
    protected $primaryKey = "id_linea_tematica";

    public function programa(){
        return $this->hasMany('App\ProgramaFormacion');
    }
}
