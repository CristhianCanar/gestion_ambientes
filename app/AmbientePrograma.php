<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmbientePrograma extends Model
{
    protected $table = "ambiente_programa";
    protected $guarded = [];
    protected $primaryKey = "id_ambiente";

    public function ambiente(){
        return $this->belongsTo('App\Ambiente', 'id_ambiente', 'id_ambiente');
    }

    public function programa_formacion(){
        return $this->belongsTo('App\ProgramaFormacion', 'id_programa', 'id_programa');
    }
}
