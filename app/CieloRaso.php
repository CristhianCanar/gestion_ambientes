<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CieloRaso extends Model
{
    protected $table = "cieloraso";
    protected $guarded = [];
    protected $primaryKey = "id_cieloraso";
    
}
