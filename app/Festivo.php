<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Festivo extends Model
{
    protected $table = "festivos";
    protected $guarded = [];
    protected $primaryKey = "id_festivo";
}
