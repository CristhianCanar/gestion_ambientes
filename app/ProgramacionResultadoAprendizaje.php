<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramacionResultadoAprendizaje extends Model
{
    protected $table = "programacion_resultados_aprendizaje";
    protected $guarded = [];
    protected $primaryKey = "id_programacion_resultado";

    public function programacion_ambiente(){
        return $this->belongsTo('App\ProgramacionAmbiente', 'programacion_ambiente_id', 'id_programacion_ambiente');
    }

    public function resultado_aprendizaje(){
        return $this->belongsTo('App\ResultadoAprendizaje', 'resultado_aprendizaje_id', 'id_resultado_aprendizaje');
    }
}
