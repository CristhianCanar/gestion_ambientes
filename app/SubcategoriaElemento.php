<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubcategoriaElemento extends Model
{
    protected $table = "subcategoria_elemento";
    protected $guarded = [];
    protected $primaryKey = "id_subcategoria_elemento";

    public function categoria_elemento(){
        return $this->belongsTo('App\CategoriaElemento', 'categoria_elemento_id', 'id_categoria_elemento');
    }

    public function elemento_ambiente(){
        return $this->hasMany('App\ElementoAmbiente', 'id_elemento_ambiente', 'id_elemento_ambiente');
    }


}
