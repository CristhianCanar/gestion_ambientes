<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sede extends Model
{
    protected $table = "sedes";
    protected $guarded = [];
    protected $primaryKey = "id_sede";

    public function centro_formacion(){
        return $this->belongsTo('App\CentroFormacion', 'id_centro_formacion', 'id_centro_formacion');
    }

    public function ambiente(){
        return $this->hasMany('App\Ambiente', 'id_sede', 'id_sede');
    }
    
}
